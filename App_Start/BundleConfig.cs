﻿using System.Web;
using System.Web.Optimization;

namespace SoftservePortalNew
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //THEME CSS
            #region THEME CSS
            bundles.Add(new StyleBundle("~/Content/themecss").Include(
                // ** -- BEGIN VENDOR CSS -- ** //
                "~/Content/app-assets/css/vendors.css",
                "~/Content/app-assets/vendors/css/calendars/fullcalendar.min.css",
                "~/Content/app-assets/vendors/css/forms/selects/select2.min.css",
                "~/Content/app-assets/vendors/css/forms/selects/selectize.css",
                "~/Content/app-assets/vendors/css/forms/selects/selectize.default.css",
                "~/Content/app-assets/vendors/css/pickers/daterange/daterangepicker.css",
                "~/Content/app-assets/vendors/css/pickers/pickadate/pickadate.css",
                "~/Content/app-assets/vendors/css/forms/icheck/icheck.css",
                "~/Content/app-assets/vendors/css/extensions/datedropper.min.css",
                "~/Content/app-assets/vendors/css/extensions/timedropper.min.css",
                "~/Content/app-assets/vendors/css/extensions/sweetalert.css",
                "~/Content/app-assets/vendors/css/forms/icheck/custom.css",
                // ** -- END VENDOR CSS -- ** //

                // ** -- BEGIN ROBUST CSS -- ** //
                "~/Content/app-assets/css/app.css",
                // ** -- END ROBUST CSS -- ** //

                // ** -- BEGIN Page Level CSS -- ** //
                "~/Content/app-assets/css/core/menu/menu-types/vertical-compact-menu.css",
                "~/Content/app-assets/css/core/colors/palette-gradient.css",
                "~/Content/app-assets/css/core/colors/palette-climacon.css",
                "~/Content/app-assets/css/plugins/loaders/loaders.min.css",
                "~/Content/app-assets/css/core/colors/palette-loader.css",
                "~/Content/app-assets/css/pages/users.css",
                "~/Content/app-assets/css/pages/timeline.css",
                "~/Content/app-assets/css/plugins/calendars/fullcalendar.css",
                "~/Content/app-assets/css/plugins/forms/selectize/selectize.css",
                "~/Content/app-assets/css/plugins/calendars/clndr.css",
                "~/Content/app-assets/css/plugins/forms/checkboxes-radios.css",
                "~/Content/app-assets/css/plugins/pickers/daterange/daterange.css",
                "~/Content/app-assets/css/plugins/animate/animate.css",
                // ** -- BEGIN Page Level CSS -- ** //

                // ** -- DATATABLES CSS -- ** //
                "~/Content/app-assets/vendors/css/tables/datatable/datatables.min.css",
                "~/Content/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css",
                // ** -- END DATATABLES CSS -- ** //

                // ** -- FONT ICONS -- ** //
                "~/Content/app-assets/fonts/icomoon.css",
                "~/Content/app-assets/fonts/flag-icon-css/css/flag-icon.min.css",
                "~/Content/app-assets/vendors/css/sliders/slick/slick.css",
                "~/Content/app-assets/vendors/css/extensions/pace.css",
                "~/Content/app-assets/vendors/css/charts/morris.css",
                "~/Content/app-assets/vendors/css/extensions/unslider.css",
                "~/Content/app-assets/vendors/css/weather-icons/climacons.min.css",
                // ** -- END FONT ICONS -- ** //

                // ** -- BEGIN Custom CSS -- ** //
                "~/Content/assets/css/style.css"
                // ** -- END Custom CSS -- ** //
                ));

            #endregion

            //THEME JS
            #region THEME JS
            bundles.Add(new ScriptBundle("~/bundles/themescripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/vendors.min.js",
                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //

                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //
                "~/Content/app-assets/js/core/app-menu.js",
                "~/Content/app-assets/js/core/app.js",
                "~/Content/app-assets/js/scripts/customizer.js"
                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //

                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // -- Slim - Crop -- //
            bundles.Add(new StyleBundle("~/Content/slimCropStylesheets").Include(
                       "~/Content/app-assets/vendors/slim_crop/slim.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/slimCropScripts").Include(
                      "~/Content/app-assets/vendors/slim_crop/slim.kickstart.min.js"));

            //THEME ACCOUNT CSS
            #region THEME ACCOUNT CSS
            bundles.Add(new StyleBundle("~/Content/themeaccountscss").Include(
                // ** -- BEGIN VENDOR CSS -- ** //
                "~/Content/app-assets/css/vendors.css",
                "~/Content/app-assets/vendors/css/forms/icheck/icheck.css",
                "~/Content/vendors/css/forms/icheck/custom.css",
                // ** -- END VENDOR CSS -- ** //

                // ** -- BEGIN ROBUST CSS -- ** //
                "~/Content/app-assets/css/app.css",
                // ** -- END ROBUST CSS -- ** //

                // ** -- BEGIN Page Level CSS -- ** //
                "~/Content/app-assets/css/core/menu/menu-types/vertical-compact-menu.css",
                "~/Content/app-assets/css/core/colors/palette-gradient.css",
                "~/Content/app-assets/css/pages/login-register.css",
                // ** -- BEGIN Page Level CSS -- ** //

                // ** -- FONT ICONS -- ** //
                "~/Content/app-assets/fonts/icomoon.css",
                "~/Content/app-assets/fonts/flag-icon-css/css/flag-icon.min.css",
                "~/Content/app-assets/vendors/css/sliders/slick/slick.css",
                "~/Content/app-assets/vendors/css/extensions/pace.css",
                "~/Content/app-assets/vendors/css/charts/morris.css",
                "~/Content/app-assets/vendors/css/extensions/unslider.css",
                "~/Content/app-assets/vendors/css/weather-icons/climacons.min.css",
                // ** -- END FONT ICONS -- ** //

                // ** -- BEGIN Custom CSS -- ** //
                "~/Content/assets/css/login-style-animation.css",
                "~/Content/assets/css/style.css"
                // ** -- END Custom CSS -- ** //
                ));
            #endregion

            //THEME ACCOUNT JS
            #region THEME ACCOUNT JS
            bundles.Add(new ScriptBundle("~/bundles/themeaccountsscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/vendors.min.js",
                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js",
                "~/Content/app-assets/vendors/js/forms/icheck/icheck.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //
                "~/Content/app-assets/js/core/app-menu.js",
                "~/Content/app-assets/js/core/app.js",
                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/forms/form-login-register.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));
            #endregion
            

            bundles.Add(new ScriptBundle("~/bundles/googlechartsscripts").Include(

               // ** -- GOOGLE BAR CHARTS JS -- ** //
               "~/Content/app-assets/js/charts/chart.min.js",
               "~/Content/app-assets/js/scripts/analytics.js",
               "~/Content/app-assets/js/scripts/charts/google/bar/bar.js",
               "~/Content/app-assets/js/scripts/charts/google/bar/bar-stacked.js",
               "~/Content/app-assets/js/scripts/charts/google/bar/column.js"));
            // ** -- END GOOGLE BAR CHARTS JS -- ** //

            bundles.Add(new ScriptBundle("~/bundles/datatablesscripts").Include(

                 // ** -- BEGIN VENDOR JS -- ** //
                 // ** -- END VENDOR JS -- ** //

                 // ** -- BEGIN PAGE VENDOR JS -- ** //
               "~/Content/app-assets/vendors/js/tables/datatable/datatables.min.js",
               "~/Content/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js",
               "~/Content/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js",
               "~/Content/app-assets/js/scripts/tables/datatables/datatable-basic.js",
               "~/Content/app-assets/vendors/js/tables/buttons.flash.min.js",
               "~/Content/app-assets/vendors/js/tables/jszip.min.js",
               "~/Content/app-assets/vendors/js/tables/pdfmake.min.js",
               "~/Content/app-assets/vendors/js/tables/vfs_fonts.js",
               "~/Content/app-assets/vendors/js/tables/buttons.html5.min.js",
               "~/Content/app-assets/vendors/js/tables/buttons.print.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/tables/datatables/datatable-advanced.js",
                 "~/Content/app-assets/js/scripts/tables/datatables/datatable-api.js"
               // ** -- END PAGE LEVEL JS -- ** //
               ));

            //CHANGE LOG CSS
            #region CHANGE LOG CSS
            bundles.Add(new StyleBundle("~/Content/changelogcss").Include(
                // ** -- BEGIN VENDOR CSS -- ** //
                //"~/Content/app-assets/css/vendors.css",
                // ** -- END VENDOR CSS -- ** //

                // ** -- BEGIN ROBUST CSS -- ** //
                //"~/Content/app-assets/css/app.css",
                // ** -- END ROBUST CSS -- ** //

                // ** -- BEGIN Page Level CSS -- ** //
                "~/Content/app-assets/css/changelog/bootstrap.css",
                "~/Content/app-assets/css/changelog/bootstrap-extended.css",
                "~/Content/app-assets/css/changelog/vertical-compact-menu.css",
                "~/Content/app-assets/css/changelog/vertical-overlay-menu.css",
                "~/Content/app-assets/css/changelog/timeline.css"
                // ** -- BEGIN Page Level CSS -- ** //

                // ** -- DATATABLES CSS -- ** //
                //"~/Content/app-assets/vendors/css/tables/datatable/datatables.min.css",
                // ** -- END DATATABLES CSS -- ** //

                // ** -- FONT ICONS -- ** //
                //"~/Content/app-assets/fonts/icomoon.css",
                //"~/Content/app-assets/fonts/flag-icon-css/css/flag-icon.min.css",
                //"~/Content/app-assets/vendors/css/sliders/slick/slick.css",
                //"~/Content/app-assets/vendors/css/extensions/pace.css",
                //"~/Content/app-assets/vendors/css/forms/icheck/icheck.css",
                //"~/Content/app-assets/vendors/css/charts/morris.css",
                //"~/Content/app-assets/vendors/css/extensions/unslider.css",
                //"~/Content/app-assets/vendors/css/weather-icons/climacons.min.css",
                // ** -- END FONT ICONS -- ** //

                // ** -- BEGIN Custom CSS -- ** //
                //"~/Content/assets/css/style.css"
                // ** -- END Custom CSS -- ** //
                ));

            #endregion

            // CALENDAR JS
            #region CALENDAR JS
            bundles.Add(new ScriptBundle("~/bundles/calendarscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/extensions/moment.min.js",
                "~/Content/app-assets/vendors/js/extensions/fullcalendar.min.js",
                "~/Content/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/extensions/fullcalendar.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // CLNDR JS
            #region CLNDR JS
            bundles.Add(new ScriptBundle("~/bundles/clndrscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/extensions/moment.min.js",
                "~/Content/app-assets/vendors/js/extensions/underscore-min.js",
                "~/Content/app-assets/vendors/js/extensions/clndr.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/extensions/clndr.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // DATE TIME PICKER JS
            #region DATE TIME PICKER JS
            bundles.Add(new ScriptBundle("~/bundles/datetimescripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/pickers/pickadate/picker.js",
                "~/Content/app-assets/vendors/js/pickers/pickadate/picker.date.js",
                "~/Content/app-assets/vendors/js/pickers/pickadate/picker.time.js",
                "~/Content/app-assets/vendors/js/pickers/pickadate/legacy.js",
                "~/Content/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js",
                "~/Content/app-assets/vendors/js/pickers/daterange/daterangepicker.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // CHECK JS
            #region CHECK JS
            bundles.Add(new ScriptBundle("~/bundles/checkscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/forms/icheck/icheck.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/forms/checkbox-radio.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // SELECTIZE JS
            #region SELECTIZE JS
            bundles.Add(new ScriptBundle("~/bundles/selectizescripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/forms/select/selectize.min.js",
                "~/Content/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/forms/select/form-selectize.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // SELECT JS
            #region SELECT JS
            bundles.Add(new ScriptBundle("~/bundles/selectscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/forms/select/select2.full.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/forms/select/form-select2.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // DATE TIME DROPPER JS
            #region DATE TIME DROPPER JS
            bundles.Add(new ScriptBundle("~/bundles/datetimedropperscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/extensions/datedropper.min.js",
                "~/Content/app-assets/vendors/js/extensions/timedropper.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/extensions/date-time-dropper.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion

            // SWEET ALERTS JS
            #region SWEET ALERTS JS
            bundles.Add(new ScriptBundle("~/bundles/sweetalertsscripts").Include(

                // ** -- BEGIN VENDOR JS -- ** //

                // ** -- END VENDOR JS -- ** //

                // ** -- BEGIN PAGE VENDOR JS -- ** //
                "~/Content/app-assets/vendors/js/extensions/sweetalert.min.js",
                // ** -- END PAGE VENDOR JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN ROBUST JS -- ** //

                // ** -- BEGIN PAGE LEVEL JS -- ** //
                "~/Content/app-assets/js/scripts/extensions/sweet-alerts.js"
                // ** -- END PAGE LEVEL JS -- ** //
                ));

            #endregion
        }
    }
}
