﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.ClientLinks
{
    public class clsClientLinkAdd
    {
        public clsClientLinkAdd()
        {
            clsClientLink = new clsClientLinks();
        }
        public clsClientLinks clsClientLink { get; set; }
        public List<clsClients> lstClients { get; set; }
    }
}
