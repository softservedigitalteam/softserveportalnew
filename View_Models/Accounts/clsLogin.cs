﻿using System.ComponentModel.DataAnnotations;

namespace SoftservePortalNew.View_Models.Accounts
{
    public class clsLogin
    {
        [Required(ErrorMessage = "Field is required")]
        public string strEmail { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [DataType(DataType.Password)]
        public string strPassword { get; set; }

        public bool bRememberMe { get; set; }
    }
}
