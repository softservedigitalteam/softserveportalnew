﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;


namespace SoftservePortalNew.View_Models.Accounts
{
    public class clsCompleteProfile
    {
        [Required(ErrorMessage = "Company Name is required")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Company name must be at least 2 characters long")]
        public string strCompanyName { get; set; }

        [Required(ErrorMessage = "Contact Name is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Contact name must be at least 3 characters long")]
        public string strContactName { get; set; }

        [Required(ErrorMessage = "Contact Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid number")]
        public string strContactNumber { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        [Remote("checkIfClientContactEmailExists", "Clients", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string strEmail { get; set; }
    }
}
