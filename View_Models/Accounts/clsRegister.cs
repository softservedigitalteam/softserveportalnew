﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Accounts
{
    public class clsRegister
    {
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        [Remote("checkIfClientExists", "Clients", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string strEmail { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [RegularExpression(@"^[^\s\,]+$", ErrorMessage = "Username cannot have spaces")]
        [Remote("checkIfUsernameExists", "Clients", HttpMethod = "POST", ErrorMessage = "Username already exists")]
        public string strUsername { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be at least 6 characters long")]
        public string strNewPassword { get; set; }

        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be at least 6 characters long")]
        [System.ComponentModel.DataAnnotations.Compare("strNewPassword", ErrorMessage = "Password does not match")]
        public string strConfirmPassword { get; set; }

    }
}
