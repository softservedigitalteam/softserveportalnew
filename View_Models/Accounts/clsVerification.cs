﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.View_Models.Accounts
{
    public class clsVerification
    {
        [Required(ErrorMessage = "Verification Key is required")]
        public string strVerificationKey { get; set; }
    }
}
