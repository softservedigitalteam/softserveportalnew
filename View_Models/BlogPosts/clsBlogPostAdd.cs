﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.BlogPosts
{
    public class clsBlogPostAdd
    {
        public clsBlogPostAdd()
        {
            clsBlogPost = new clsBlogPosts();
        }
        public clsBlogPosts clsBlogPost { get; set; }
        public List<clsBlogTypes> lstBlogTypes { get; set; }
        public List<clsBlogTags> lstBlogTags { get; set; }
        public string strReleaseDate { get; set; }
    }
}
