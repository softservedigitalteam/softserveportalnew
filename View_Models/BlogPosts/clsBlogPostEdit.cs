﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.BlogPosts
{
    public class clsBlogPostEdit
    {
        public clsBlogPostEdit()
        {
            clsBlogPost = new clsBlogPosts();
        }
        public clsBlogPosts clsBlogPost { get; set; }
        public List<clsBlogTypes> lstBlogTypes { get; set; }
        public List<clsBlogTags> lstBlogTags { get; set; }
        public string strSelectedBlogType { get; set; }
        public string strReleaseDate { get; set; }
        public string strFullImagePath { get; set; }
        public string strImageName { get; set; }
        public string strFullAudioPath { get; set; }
        public string strAudioName { get; set; }
        public string strFullVideoPath { get; set; }
        public string strVideoName { get; set; }
    }
}
