﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Notifications
{
    public class clsNotificationEdit
    {
        public clsNotificationEdit()
        {
            clsNotification = new clsNotifications();
        }
        public clsNotifications clsNotification { get; set; }
        public List<clsClients> lstClients { get; set; }
        public List<clsNotificationTypes> lstNotificationTypes { get; set; }
    }
}
