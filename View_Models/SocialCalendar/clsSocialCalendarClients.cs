﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.SocialCalendar
{
    public class clsSocialCalendarClients
    {
        public clsSocialCalendarClients()
        {
            lstClients = new List<clsClients>();
        }
        public List<clsClients> lstClients { get; set; }
        public int iClientID { get; set; }
    }
}
