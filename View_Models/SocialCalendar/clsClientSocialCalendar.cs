﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.SocialCalendar
{
    public class clsClientSocialCalendar
    {
        public int iClientID { get; set; }
        public string strClientCompanyName { get; set; }
        public List<clsClients> lstClients { get; set; }
        public List<clsSocialCalendarEventsTypes> lstSocialCalendarEventsTypes { get; set; }
    }
}
