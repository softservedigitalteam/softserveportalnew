﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Assistant_Classes;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.SocialCalendar
{
    public class clsSocialCalendarView
    {
        public clsSocialCalendarView()
        {
            lstSocialCalendarEventsViewItemsFacebook = new List<clsSocialCalendarEventsViewItems>();
            lstSocialCalendarEventsViewItemsInstagram = new List<clsSocialCalendarEventsViewItems>();
            lstSocialCalendarEventsViewItemsTwitter = new List<clsSocialCalendarEventsViewItems>();
            lstSocialCalendarEventsViewItemsGooglePus = new List<clsSocialCalendarEventsViewItems>();
        }
        public List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItemsFacebook { get; set; }
        public List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItemsInstagram { get; set; }
        public List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItemsTwitter { get; set; }
        public List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItemsGooglePus { get; set; }
    }
}
