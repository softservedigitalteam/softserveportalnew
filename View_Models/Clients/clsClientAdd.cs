﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;
using SoftservePortalNew.Classes;

namespace SoftservePortalNew.View_Models.Clients
{
    public class clsClientAdd
    {
        public clsClientAdd()
        {
            clsClient = new clsClients();
            clsClientContactDetails = new clsClientContactDetails();
            clsClientSecondaryContactDetails = new clsClientSecondaryContactDetails();
        }

        public clsClients clsClient { get; set; }
        public clsClientContactDetails clsClientContactDetails { get; set; }
        public clsClientSecondaryContactDetails clsClientSecondaryContactDetails { get; set; }
        public string strCropImageName { get; set; }
        public string strCropImageData { get; set; }
    }
}
