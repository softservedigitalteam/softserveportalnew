﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;
using SoftservePortalNew.Classes;

namespace SoftservePortalNew.View_Models.Clients
{
    public class clsClientSocialMediaReport
    {
        public clsClientSocialMediaReport()
        {
            lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
        }

        public List<clsSocialCalendarEvents> lstSocialCalendarEvents { get; set; }
        public DateTime dtCurrentDate { get; set; }
    }
}
