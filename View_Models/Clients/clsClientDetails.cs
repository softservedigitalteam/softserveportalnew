﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Clients
{
    public class clsClientDetails
    {
        public clsClients clsClient { get; set; }
        public clsClientContactDetails clsClientContactDetail { get; set; }
    }
}
