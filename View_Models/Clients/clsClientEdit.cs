﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;
using SoftservePortalNew.Classes;

namespace SoftservePortalNew.View_Models.Clients
{
    public class clsClientEdit
    {
        public clsClientEdit()
        {
            clsClient = new clsClients();
            clsClientContactDetails = new clsClientContactDetails();
            clsClientSecondaryContactDetails = new clsClientSecondaryContactDetails();
        }

        public clsClients clsClient { get; set; }
        public clsClientContactDetails clsClientContactDetails { get; set; }
        public clsClientSecondaryContactDetails clsClientSecondaryContactDetails { get; set; }

        public string strUsername { get; set; }

        public string strEmail { get; set; }

        public bool bResetPassword { get; set; }

        [DataType(DataType.Password)]
        public string strNewPassword { get; set; }

        [DataType(DataType.Password)]

        public string strCropImageName { get; set; }
        public string strCropImageData { get; set; }
        public string strFullImagePath { get; set; }
    }
}
