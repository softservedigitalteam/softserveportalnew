﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Management
{
    public class clsChangeLog
    {
        public clsChangeLog()
        {
            lstNewTasks = new List<string>();
            lstEnhancedTasks = new List<string>();
            lstFixedTasks = new List<string>();
            lstRemovedTasks = new List<string>();
        }
        public List<string> lstNewTasks { get; set; }
        public List<string> lstEnhancedTasks { get; set; }
        public List<string> lstFixedTasks { get; set; }
        public List<string> lstRemovedTasks { get; set; }
    }
}
