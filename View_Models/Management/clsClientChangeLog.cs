﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Management
{
    public class clsClientChangeLog
    {
        public clsClientChangeLog()
        {
            lstClients = new List<clsClients>();
            lstProjects = new List<clsProjects>();
            lstTasks = new List<clsTasks>();
            lstTaskTags = new List<clsTaskTags>();
            lstTimeTrackUsers = new List<clsTimeTrackUsers>();
        }
        public List<clsClients> lstClients { get; set; }
        public List<clsProjects> lstProjects { get; set; }
        public List<clsTasks> lstTasks { get; set; }
        public List<clsTaskTags> lstTaskTags { get; set; }
        public List<clsTimeTrackUsers> lstTimeTrackUsers { get; set; }

        public clsClients clsClient { get; set; }
        public int iClientID { get; set; }
        public string strTasksMonthTitle { get; set; }

        public string strSelectedDate { get; set; }
    }
}
