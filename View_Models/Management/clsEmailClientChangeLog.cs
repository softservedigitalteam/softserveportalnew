﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Management
{
    public class clsEmailClientChangeLog
    {
        public clsProjects ClientProject { get; set; }

        public string strClientEmail { get; set; }
        public string strEmailSubject { get; set; }
        public string strNewTasks { get; set; }
        public string strEnhancedTasks { get; set; }
        public string strFixedTasks { get; set; }
        public string strRemovedTasks { get; set; }
    }
}
