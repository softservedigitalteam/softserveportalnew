﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Analytics
{
    public class clsGogleAnalytics
    {
        public List<clsDomains> lstDomains { get; set; }
        public int iDomainID { get; set; }
    }
}
