﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Analytics
{
    public class analytics
    {
        public Page_Impressions Page_Impressions { get; set; }
        public RootObject RootObject { get; set; }
    }
}