﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.View_Models.LayoutPartialViews
{
    public class clsLogoutPartialView
    {
        public string strName { get; set; }
        public string strImagePath { get; set; }
        public bool bIsUser { get; set; }
        public bool bIsClient { get; set; }
    }
}
