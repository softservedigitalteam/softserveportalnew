﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.View_Models.BlogTypes
{
    public class clsBlogTypeEdit
    {
        public int iBlogTypeID { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
    }
}
