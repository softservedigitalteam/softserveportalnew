﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.View_Models.BlogTags
{
    public class clsBlogTagEdit
    {
        public int iBlogTagID { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
    }
}
