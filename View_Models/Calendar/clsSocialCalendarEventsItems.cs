﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.View_Models.Calendar
{
    public class clsSocialCalendarEventsItems
    {
        public string id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }
    }

    public class clsSocialCalendarEventsViewItems
    {
        public string title { get; set; }
        public string date { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string location { get; set; }
    }
}
