﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Calendar
{
    public class clsSocialCalendarView
    {
        public List<clsSocialCalendarEventsTypes> lstSocialCalendarEventsTypes { get; set; }
    }
}
