﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.CIDocuments
{
    public class clsCIDocumentEdit
    {
        public clsCIDocumentEdit()
        {
            clsCIDocument = new clsCIDocuments();
            lstDocumentsList = new List<string>();
        }
        public clsCIDocuments clsCIDocument { get; set; }
        public List<clsClients> lstClients { get; set; }
        public string strFullImagePath { get; set; }
        public List<string> lstDocumentsList { get; set; }
    }
}
