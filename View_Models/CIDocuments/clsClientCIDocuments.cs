﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.CIDocuments
{
    public class clsClientCIDocuments
    {
        public clsClientCIDocuments()
        {
            clsClient = new clsClients();
        }
        public clsClients clsClient { get; set; }
        public string strCIGuideDocumentPath { get; set; }
        public string strCIGuideDocumentName { get; set; }
    }
}
