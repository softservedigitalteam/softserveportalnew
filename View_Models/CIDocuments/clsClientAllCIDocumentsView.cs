﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.CIDocuments
{
    public class clsClientAllCIDocumentsView
    {
        public clsClientAllCIDocumentsView()
        {
            clsClient = new clsClients();
            lstDocumentsList = new List<string>();
        }
        public clsClients clsClient { get; set; }
        public List<string> lstDocumentsList { get; set; }
        public string strFullCoperateIdentityPackPath { get; set; }
        public string strFullCoperateIdentityPackName { get; set; }
    }
}
