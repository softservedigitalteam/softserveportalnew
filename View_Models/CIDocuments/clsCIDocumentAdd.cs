﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.CIDocuments
{
    public class clsCIDocumentAdd
    {
        public clsCIDocumentAdd()
        {
            clsCIDocument = new clsCIDocuments();
        }
        public clsCIDocuments clsCIDocument { get; set; }
        public List<clsClients> lstClients { get; set; }
    }
}
