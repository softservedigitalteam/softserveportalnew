﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Domains
{
    public class clsDomainPurchases
    {
        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Please enter a feasible Domain name")]
        public string strDomainName { get; set; }

        public bool bIsDomainAvailable { get; set; }
    }
}
