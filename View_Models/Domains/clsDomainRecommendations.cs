﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.View_Models.Domains
{
    public class clsDomainRecommendations
    {
        public string strDomainName { get; set; }
        public bool bIsDomainAvailable { get; set; }
        public double dblPrice { get; set; }
    }

    public class clsDomainRecommendationsList
    {
        public clsDomainRecommendationsList()
        {
            lstclsDomainRecommendationsCOZA = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsWEBZA = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsNETZA = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsORGZA = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsJOBURG = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsDURBAN = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsCAPETOWN = new List<clsDomainRecommendations>();
            lstclsDomainRecommendationsAFRICA = new List<clsDomainRecommendations>();
        }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsCOZA { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsWEBZA { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsNETZA { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsORGZA { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsJOBURG { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsDURBAN { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsCAPETOWN { get; set; }
        public List<clsDomainRecommendations> lstclsDomainRecommendationsAFRICA { get; set; }
    }
}
