﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.View_Models.Domains
{
    public class clsDomainEdit
    {
        public clsDomainEdit()
        {
            clsDomain = new clsDomains();
        }
        public clsDomains clsDomain { get; set; }
        public List<clsClients> lstClients { get; set; }
        public List<clsDomainEndings> lstDomainEndings { get; set; }
    }
}
