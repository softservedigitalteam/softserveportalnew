﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;
using System.ComponentModel.DataAnnotations;

namespace SoftservePortalNew.View_Models.Users
{
    public class clsUserEdit
    {
        public clsUserEdit()
        {
            clsUser = new clsUsers();
        }
        public clsUsers clsUser { get; set; }

        [DataType(DataType.Password)]
        public string strOldPassword { get; set; }

        [DataType(DataType.Password)]
        public string strNewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("strNewPassword", ErrorMessage = "Passwords do not match")]
        public string strConfirmPassword { get; set; }

        public string strCropImageName { get; set; }
        public string strCropImageData { get; set; }

        public string strFullImagePath { get; set; }

        public bool bIsProfileImageSelected { get; set; }
        public bool bIsAvatarImageSelected { get; set; }
        public string strAvatarImageName { get; set; }
    }
}
