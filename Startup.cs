﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SoftservePortalNew.Startup))]
namespace SoftservePortalNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
