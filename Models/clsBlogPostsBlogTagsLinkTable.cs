﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsBlogPostsBlogTagsLinkTable
    {
        public int iBlogPostsBlogTagsLinkTableID { get; set; }
        public int? iBlogPostID { get; set; }
        public int? iBlogTagID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsBlogPosts clsBlogPost { get; set; }
        public clsBlogTags clsBlogTag { get; set; }
    }
}
