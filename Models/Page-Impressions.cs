﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftservePortalNew.Models
{
    public class Page_Impressions
    {
        public List<impressions> impressions { get; set; }
    }

    public class impressions
    {
        public string name { get; set; }
        public string period { get; set; }
        public List<values> values { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string id { get; set; }
    }

    public class values
    {
        public int value { get; set; }
        public DateTime end_time { get; set; }
    }
}