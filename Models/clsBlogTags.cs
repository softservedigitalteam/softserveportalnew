﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsBlogTags
    {
        public int iBlogTagID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [Remote("CheckIfBlogTagExists", "BlogTags", HttpMethod = "POST", ErrorMessage = "Blog Tag already exists")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }

        //public List<clsBlogPostsBlogTagsLinkTable> lstBlogPostsBlogTagsLinkTable { get; set; }
    }
}
