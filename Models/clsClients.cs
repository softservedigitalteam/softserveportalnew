﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsClients
    {
        public int iClientID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Company Name is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Company name must be at least 3 characters long")]
        public string strCompanyName { get; set; }

        [Required(ErrorMessage = "Company Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid number")]
        public string strCompanyLandline { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Username must be at least 3 characters long")]
        [Remote("checkIfClientUsernameExists", "Clients", HttpMethod = "POST", ErrorMessage = "User Name already exists")]
        public string strUsername { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        [Remote("checkIfClientExists", "Clients", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string strEmail { get; set; }

        public string strRegisteredName { get; set; }
        public string strRegistrationNumber { get; set; }
        public string strVATNumber { get; set; }
        public string strPhysicalAddress { get; set; }
        public string strPostalAddress { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public double dblBillingRate { get; set; }

        public string strURL { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public string strPassword { get; set; }
        public string strVerificationKey { get; set; }
        public bool bIsVerified { get; set; }
        public string strIsApproved { get; set; }
        public bool bIsProfileCompleted { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsCIDocuments> lstCIDocuments { get; set; }
        public List<clsClientClientContactDetailsLinkTable> lstClientClientContactDetailsLinkTable { get; set; }
        public List<clsClientClientServicesLinkTable> lstClientClientServicesLinkTable { get; set; }
        public List<clsSocialCalendarEvents> lstSocialCalendarEvents { get; set; }
        public List<clsDomains> lstDomains { get; set; }
        public List<clsNotifications> lstNotifications { get; set; }
        public List<clsClientLinks> lstClientLinks { get; set; }
        public List<clsProjects> lstProjects { get; set; }
        //public List<clsProjectDesignQuestionnaires> lstProjectDesignQuestionnaires { get; set; }
        //public List<clsProjectGeneralQuestionnaires> lstProjectGeneralQuestionnaires { get; set; }
        //public List<clsProjectLogoQuestionnaires> lstProjectLogoQuestionnaires { get; set; }
        //public List<clsProjectSEOQuestionnaires> lstProjectSEOQuestionnaires { get; set; }
    }
}
