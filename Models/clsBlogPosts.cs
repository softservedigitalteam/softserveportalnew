﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsBlogPosts
    {
        public int iBlogPostID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string strTitle { get; set; }

        public string strTagLine { get; set; }
        public string strDescription { get; set; }
        public int? iBlogTypeID { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public string strPathToAudio { get; set; }
        public string strMasterAudio { get; set; }
        public string strAudioURLLink { get; set; }
        public string strPathToVideos { get; set; }
        public string strMasterVideo { get; set; }
        public string strVideoURLLink { get; set; }
        public DateTime? dtReleaseDate { get; set; }
        public string strTags { get; set; }
        public bool bDisplay { get; set; }
        public bool bIsDeleted { get; set; }

        public clsBlogTypes clsBlogType { get; set; }
        //public List<clsBlogPostsBlogTagsLinkTable> lstBlogPostsBlogTagsLinkTable { get; set; }
    }
}
