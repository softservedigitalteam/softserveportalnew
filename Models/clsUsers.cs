﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsUsers
    {
        public int iUserID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int? iEditedBy { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string strFirstName { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        public string strSurname { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        public string strEmailAddress { get; set; }

        [DataType(DataType.Password)]
        public string strPassword { get; set; }

        public string strClientID { get; set; }
        public string strClientSecret { get; set; }
        public string strRedirectURL { get; set; }
        public string strRefreshToken { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }

        public bool bIsDeleted { get; set; }
    }
}
