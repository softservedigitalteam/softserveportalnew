﻿using System;

namespace SoftservePortalNew.Models
{
    public class clsSocialCalendarEvents
    {
        public int iSocialCalendarEventID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strKeywords { get; set; }
        public string strLinks { get; set; }
        public DateTime dtStartDate { get; set; }
        public DateTime? dtEndDate { get; set; }
        public int? iClientID { get; set; }
        public bool? bIsConfirmed { get; set; }
        public int? iSocialCalendarEventsTypeID { get; set; }
        public bool bIsRepeatEvent { get; set; }
        public string strRepeatEventType { get; set; }
        public int? iRepeatEventStartDateID { get; set; }
        public DateTime? dtRepeatEventStartDate { get; set; }
        public bool bIsDeleted { get; set; }

        public clsClients clsClient { get; set; }
        public clsSocialCalendarEventsTypes clsSocialCalendarEventsType { get; set; }
    }
}
