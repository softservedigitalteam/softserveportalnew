﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsPortalLogins
    {
        public int iPortalLoginID { get; set; }
        public DateTime dtAdded { get; set; }
        public string strUsername { get; set; }
        public string strPassword { get; set; }
        public string strIPAddress { get; set; }
        public bool bIsSuccessfulLoginAttempt { get; set; }
        public bool bIsDeleted { get; set; }
    }
}
