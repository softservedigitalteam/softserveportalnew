﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsClientClientContactDetailsLinkTable
    {
        public int iClientClientContactDetailsLinkTableID { get; set; }
        public int iClientID { get; set; }
        public int iClientContactDetailID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsClientContactDetails clsClientContactDetail { get; set; }
        public clsClients clsClient { get; set; }
    }
}
