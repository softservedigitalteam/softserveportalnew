﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsDocuments
    {
        public int iDocumentID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public string strPathToDocuments { get; set; }
        public string strMasterDocument { get; set; }
        public bool bIsMultiple { get; set; }
        public bool bIsDeleted { get; set; }
    }
}
