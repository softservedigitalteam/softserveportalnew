﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftservePortalNew.Models
{
    public class RootObject
    {
        public List<Data> Data { get; set; }
    }

    public class Data
    {
        public DateTime created_time { get; set; }
        public string message { get; set; }
        public string id { get; set; }
        public string full_picture { get; set; }
    }
}