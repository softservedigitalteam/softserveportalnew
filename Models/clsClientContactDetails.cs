﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Models
{
    public class clsClientContactDetails
    {
        public int iClientContactDetailID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Contact Name is required")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Contact name should be at least 3 characters long")]
        public string strContactName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        [StringLength(100, ErrorMessage = "Email cannot be more than 100 charaters")]
        //[Remote("checkIfClientContactEmailExists", "Clients", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string strContactEmail { get; set; }

        [Required(ErrorMessage = "Contact Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid number")]
        [StringLength(15, ErrorMessage = "Number cannot be more than 15 charaters")]
        public string strContactNumber { get; set; }

        public bool bIsPrimaryContact { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsClientClientContactDetailsLinkTable> lstClientClientContactDetailsLinkTable { get; set; }
    }
}
