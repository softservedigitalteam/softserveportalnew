﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SoftservePortalNew.Models
{
    public class clsDomainEndings
    {
        public int iDomainEndingID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strTitle { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsDomains> lstDomains { get; set; }
    }
}
