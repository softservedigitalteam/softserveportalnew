﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SoftservePortalNew.Models
{
    public class clsNotifications
    {
        public int iNotificationID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strTitle { get; set; }

        public string strDescription { get; set; }
        public string strComment { get; set; }

        [Required(ErrorMessage = "Client is required")]
        public int? iClientID { get; set; }

        public int? iUserID { get; set; }

        [Required(ErrorMessage = "Notification Type is required")]
        public int iNotificationTypeID { get; set; }

        public bool bIsRead { get; set; }
        public bool bIsDeleted { get; set; }

        public clsClients clsClient { get; set; }
        public clsNotificationTypes clsNotificationType { get; set; }
    }
}
