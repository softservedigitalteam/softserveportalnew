﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsClientClientServicesLinkTable
    {
        public int iClientClientServicesLinkTableID { get; set; }
        public int iClientID { get; set; }
        public int iClientServiceID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsClients clsClient { get; set; }
        public clsClientServices clsClientService { get; set; }
    }
}
