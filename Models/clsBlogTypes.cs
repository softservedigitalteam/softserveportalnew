﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsBlogTypes
    {
        public int iBlogTypeID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [Remote("CheckIfBlogTypeExists", "BlogTypes", HttpMethod = "POST", ErrorMessage = "Blog Type already exists")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsBlogPosts> lstBlogPosts { get; set; }
    }
}
