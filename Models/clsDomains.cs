﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SoftservePortalNew.Models
{
    public class clsDomains
    {
        public int iDomainID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Client is required")]
        public int iClientID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strTitle { get; set; }

        [Required(ErrorMessage = "Domain Ending is required")]
        public int iDomainEndingID { get; set; }
        public DateTime? dtRegistrationDate { get; set; }
        public DateTime? dtNextDueDate { get; set; }
        public string strDomainStatus { get; set; }
        public string strGoogleAnalyticsCode { get; set; }
        public bool bAutoRenew { get; set; }
        public bool bIsDeleted { get; set; }

        public clsClients clsClient { get; set; }
        public clsDomainEndings clsDomainEnding { get; set; }
    }
}
