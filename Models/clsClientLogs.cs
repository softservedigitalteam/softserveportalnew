﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsClientLogs
    {
        public int iClientLogID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public DateTime dtChanged { get; set; }
        public string strCompanyName { get; set; }
        public string strCompanyLandline { get; set; }
        public string strUsername { get; set; }
        public string strEmail { get; set; }
        public string strRegisteredName { get; set; }
        public string strRegistrationNumber { get; set; }
        public string strVATNumber { get; set; }
        public string strPhysicalAddress { get; set; }
        public string strPostalAddress { get; set; }
        public decimal dblBillingRate { get; set; }
        public string strURL { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public string strPassword { get; set; }
        public string strVerificationKey { get; set; }
        public bool bIsVerified { get; set; }
        public bool bIsProfileCompleted { get; set; }
        public bool bIsDeleted { get; set; }
    }
}
