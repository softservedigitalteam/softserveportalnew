﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsTasks
    {
        public int iTaskID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public Nullable<int> iEditedBy { get; set; }
        public string strTaskName { get; set; }
        public string strDuration { get; set; }
        public DateTime? dtTimeSessionElapsed { get; set; }
        public bool bIsTaskcompleted { get; set; }
        public int iAsanaTaskID { get; set; }
        public Nullable<int> iProjectID { get; set; }
        public int iTaskTagID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsProjects clsProject { get; set; }
        public clsTaskTags clsTaskTag { get; set; }
    }
}
