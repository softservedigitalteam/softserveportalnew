﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Models
{
    public class clsTimeTrackUsers
    {
        public int iTimeTrackUserID { get; set; }
        public System.DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int? iEditedBy { get; set; }
        public int? iConfigurationID { get; set; }
        public string strFirstName { get; set; }
        public string strSurname { get; set; }
        public string strEmailAddress { get; set; }
        public string strPassword { get; set; }
        public string strClientID { get; set; }
        public string strClientSecret { get; set; }
        public string strRedirectURL { get; set; }
        public string strRefreshToken { get; set; }
        public string strAssigneeID { get; set; }
        public bool bIsDeleted { get; set; }
    }
}
