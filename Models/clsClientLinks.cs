﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SoftservePortalNew.Models
{
    public class clsClientLinks
    {
        public int iClientLinkID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strLink { get; set; }

        [Required(ErrorMessage = "Client is required")]
        public int? iClientID { get; set; }

        public bool bIsDeleted { get; set; }

        public clsClients clsClient { get; set; }
    }
}
