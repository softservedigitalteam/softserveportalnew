﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftservePortalNew.Assistant_Classes
{
    public class clsSocialCalendarEventsViewItems
    {
        public string title { get; set; }
        public string date { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string location { get; set; }
        public string eventType { get; set; }
    }
}
