﻿using SoftservePortalNew.SignalR.Utilities;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SoftservePortalNew
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region SignalR Required
        string con = ConfigurationManager.ConnectionStrings["sqlConString"].ConnectionString;
        #endregion

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            #region SignalR Required
            SqlDependency.Start(con);
            #endregion
        }

        #region SignalR Required
        protected void Session_Start(object sender, EventArgs e)
        {
            SqlDependency.Start(con);
            var notificationUtility = new NotificationUtility();
            var currentTime = DateTime.Now;
            HttpContext.Current.Session["LastUpdated"] = currentTime;
            notificationUtility.RegisterNotification(currentTime);
        }
        #endregion

        #region SignalR Required
        protected void Application_End()
        {
            SqlDependency.Stop(con);
        }
        #endregion
    }
}
