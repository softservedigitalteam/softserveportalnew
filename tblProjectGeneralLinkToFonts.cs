//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftservePortalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblProjectGeneralLinkToFonts
    {
        public int iProjectGeneralLinkToFontID { get; set; }
        public int iFontID { get; set; }
        public int iProjectGeneralQuestionnaireID { get; set; }
    
        public virtual tblFonts tblFonts { get; set; }
        public virtual tblProjectGeneralQuestionnaires tblProjectGeneralQuestionnaires { get; set; }
    }
}
