﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsCIDocumentsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsCIDocuments> getAllCIDocumentsList()
        {
            List<clsCIDocuments> lstCIDocuments = new List<clsCIDocuments>();
            var lstGetCIDocumentsList = db.tblCIDocuments.Where(CIDocument => CIDocument.bIsDeleted == false).ToList();

            if (lstGetCIDocumentsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();

                foreach (var item in lstGetCIDocumentsList)
                {
                    clsCIDocuments clsCIDocument = new clsCIDocuments();

                    clsCIDocument.iCIDocumentID = item.iCIDocumentID;
                    clsCIDocument.dtAdded = item.dtAdded;
                    clsCIDocument.iAddedBy = item.iAddedBy;
                    clsCIDocument.dtEdited = item.dtEdited;
                    clsCIDocument.iEditedBy = item.iEditedBy;

                    clsCIDocument.strTitle = item.strTitle;
                    clsCIDocument.strDescription = item.strDescription;
                    clsCIDocument.strPathToImages = item.strPathToImages;
                    clsCIDocument.strMasterImage = item.strMasterImage;
                    clsCIDocument.strPathToDocuments = item.strPathToDocuments;

                    clsCIDocument.strMasterDocument = item.strMasterDocument;
                    clsCIDocument.bIsMultiple = item.bIsMultiple;
                    clsCIDocument.iClientID = item.iClientID;
                    clsCIDocument.bIsCIGuide = item.bIsCIGuide;
                    clsCIDocument.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        clsCIDocument.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);

                    lstCIDocuments.Add(clsCIDocument);
                }
            }

            return lstCIDocuments;
        }

        public List<clsCIDocuments> getAllCIDocumentsOnlyList()
        {
            List<clsCIDocuments> lstCIDocuments = new List<clsCIDocuments>();
            var lstGetCIDocumentsList = db.tblCIDocuments.Where(CIDocument => CIDocument.bIsDeleted == false).ToList();

            if (lstGetCIDocumentsList.Count > 0)
            {

                foreach (var item in lstGetCIDocumentsList)
                {
                    clsCIDocuments clsCIDocument = new clsCIDocuments();

                    clsCIDocument.iCIDocumentID = item.iCIDocumentID;
                    clsCIDocument.dtAdded = item.dtAdded;
                    clsCIDocument.iAddedBy = item.iAddedBy;
                    clsCIDocument.dtEdited = item.dtEdited;
                    clsCIDocument.iEditedBy = item.iEditedBy;

                    clsCIDocument.strTitle = item.strTitle;
                    clsCIDocument.strDescription = item.strDescription;
                    clsCIDocument.strPathToImages = item.strPathToImages;
                    clsCIDocument.strMasterImage = item.strMasterImage;
                    clsCIDocument.strPathToDocuments = item.strPathToDocuments;

                    clsCIDocument.strMasterDocument = item.strMasterDocument;
                    clsCIDocument.bIsMultiple = item.bIsMultiple;
                    clsCIDocument.iClientID = item.iClientID;
                    clsCIDocument.bIsCIGuide = item.bIsCIGuide;
                    clsCIDocument.bIsDeleted = item.bIsDeleted;

                    lstCIDocuments.Add(clsCIDocument);
                }
            }

            return lstCIDocuments;
        }

        //Get
        public clsCIDocuments getCIDocumentByID(int iCIDocumentID)
        {
            clsCIDocuments clsCIDocument = null;
            tblCIDocuments tblCIDocuments = db.tblCIDocuments.FirstOrDefault(CIDocument => CIDocument.iCIDocumentID == iCIDocumentID && CIDocument.bIsDeleted == false);

            if (tblCIDocuments != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();

                clsCIDocument = new clsCIDocuments();

                clsCIDocument.iCIDocumentID = tblCIDocuments.iCIDocumentID;
                clsCIDocument.dtAdded = tblCIDocuments.dtAdded;
                clsCIDocument.iAddedBy = tblCIDocuments.iAddedBy;
                clsCIDocument.dtEdited = tblCIDocuments.dtEdited;
                clsCIDocument.iEditedBy = tblCIDocuments.iEditedBy;

                clsCIDocument.strTitle = tblCIDocuments.strTitle;
                clsCIDocument.strDescription = tblCIDocuments.strDescription;
                clsCIDocument.strPathToImages = tblCIDocuments.strPathToImages;
                clsCIDocument.strMasterImage = tblCIDocuments.strMasterImage;
                clsCIDocument.strPathToDocuments = tblCIDocuments.strPathToDocuments;

                clsCIDocument.strMasterDocument = tblCIDocuments.strMasterDocument;
                clsCIDocument.bIsMultiple = tblCIDocuments.bIsMultiple;
                clsCIDocument.iClientID = tblCIDocuments.iClientID;
                clsCIDocument.bIsCIGuide = tblCIDocuments.bIsCIGuide;
                clsCIDocument.bIsDeleted = tblCIDocuments.bIsDeleted;

                if (tblCIDocuments.tblClients != null)
                    clsCIDocument.clsClient = clsClientsManager.convertClientsTableToClass(tblCIDocuments.tblClients);
            }

            return clsCIDocument;
        }

        //Save
        public int saveCIDocument(clsCIDocuments clsCIDocument)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblCIDocuments tblCIDocuments = new tblCIDocuments();

            tblCIDocuments.iCIDocumentID = clsCIDocument.iCIDocumentID;

            tblCIDocuments.strTitle = clsCIDocument.strTitle;
            tblCIDocuments.strDescription = clsCIDocument.strDescription;
            tblCIDocuments.strPathToImages = clsCIDocument.strPathToImages;
            tblCIDocuments.strMasterImage = clsCIDocument.strMasterImage;
            tblCIDocuments.strPathToDocuments = clsCIDocument.strPathToDocuments;

            tblCIDocuments.strMasterDocument = clsCIDocument.strMasterDocument;
            tblCIDocuments.bIsMultiple = clsCIDocument.bIsMultiple;
            tblCIDocuments.iClientID = clsCIDocument.iClientID;
            tblCIDocuments.bIsCIGuide = clsCIDocument.bIsCIGuide;
            tblCIDocuments.bIsDeleted = clsCIDocument.bIsDeleted;

            //Add
            if (tblCIDocuments.iCIDocumentID == 0)
            {
                tblCIDocuments.dtAdded = DateTime.Now;
                //tblCIDocuments.iAddedBy = clsCMSUser.iCMSUserID;
                tblCIDocuments.iAddedBy = 1;
                tblCIDocuments.dtEdited = DateTime.Now;
                //tblCIDocuments.iEditedBy = clsCMSUser.iCMSUserID;
                tblCIDocuments.iEditedBy = 1;

                db.tblCIDocuments.Add(tblCIDocuments);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblCIDocuments.dtAdded = clsCIDocument.dtAdded;
                tblCIDocuments.iAddedBy = clsCIDocument.iAddedBy;
                tblCIDocuments.dtEdited = DateTime.Now;
                //tblCIDocuments.iEditedBy = clsCMSUser.iCMSUserID;
                tblCIDocuments.iEditedBy = 1;

                db.Set<tblCIDocuments>().AddOrUpdate(tblCIDocuments);
                db.SaveChanges();
            }
            //}
            return tblCIDocuments.iCIDocumentID;
        }

        //Remove
        public void removeCIDocumentByID(int iCIDocumentID)
        {
            tblCIDocuments tblCIDocument = db.tblCIDocuments.Find(iCIDocumentID);
            if (tblCIDocument != null)
            {
                tblCIDocument.bIsDeleted = true;
                db.Entry(tblCIDocument).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfCIDocumentExists(string strTitle)
        {
            bool bCIDocumentExists = db.tblCIDocuments.Any(CIDocument => CIDocument.strTitle.ToLower() == strTitle.ToLower() && CIDocument.bIsDeleted == false);
            return bCIDocumentExists;
        }

        //Convert database table to class
        public clsCIDocuments convertCIDocumentsTableToClass(tblCIDocuments tblCIDocument)
        {
            clsCIDocuments clsCIDocument = new clsCIDocuments();

            clsCIDocument.iCIDocumentID = tblCIDocument.iCIDocumentID;
            clsCIDocument.dtAdded = tblCIDocument.dtAdded;
            clsCIDocument.iAddedBy = tblCIDocument.iAddedBy;
            clsCIDocument.dtEdited = tblCIDocument.dtEdited;
            clsCIDocument.iEditedBy = tblCIDocument.iEditedBy;

            clsCIDocument.strTitle = tblCIDocument.strTitle;
            clsCIDocument.strDescription = tblCIDocument.strDescription;
            clsCIDocument.strPathToImages = tblCIDocument.strPathToImages;
            clsCIDocument.strMasterImage = tblCIDocument.strMasterImage;
            clsCIDocument.strPathToDocuments = tblCIDocument.strPathToDocuments;

            clsCIDocument.strMasterDocument = tblCIDocument.strMasterDocument;
            clsCIDocument.bIsMultiple = tblCIDocument.bIsMultiple;
            clsCIDocument.iClientID = tblCIDocument.iClientID;
            clsCIDocument.bIsCIGuide = tblCIDocument.bIsCIGuide;
            clsCIDocument.bIsDeleted = tblCIDocument.bIsDeleted;

            return clsCIDocument;
        }
    }
}
