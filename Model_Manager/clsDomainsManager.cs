﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsDomainsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsDomains> getAllDomainsList()
        {
            List<clsDomains> lstDomains = new List<clsDomains>();
            var lstGetDomainsList = db.tblDomains.Where(Domain => Domain.bIsDeleted == false).ToList();

            if (lstGetDomainsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();

                foreach (var item in lstGetDomainsList)
                {
                    clsDomains clsDomain = new clsDomains();

                    clsDomain.iDomainID = item.iDomainID;
                    clsDomain.dtAdded = item.dtAdded;
                    clsDomain.iAddedBy = item.iAddedBy;
                    clsDomain.dtEdited = item.dtEdited;
                    clsDomain.iEditedBy = item.iEditedBy;

                    clsDomain.iClientID = item.iClientID;
                    clsDomain.strTitle = item.strTitle;
                    clsDomain.iDomainEndingID = item.iDomainEndingID;
                    clsDomain.dtRegistrationDate = item.dtRegistrationDate;
                    clsDomain.dtNextDueDate = item.dtNextDueDate;

                    clsDomain.strDomainStatus = item.strDomainStatus;
                    clsDomain.strGoogleAnalyticsCode = item.strGoogleAnalyticsCode;
                    clsDomain.bAutoRenew = item.bAutoRenew;
                    clsDomain.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        if (item.tblClients.bIsDeleted != true)
                            clsDomain.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblDomainEndings != null)
                        if (item.tblDomainEndings.bIsDeleted != true)
                            clsDomain.clsDomainEnding = clsDomainEndingsManager.convertDomainEndingsTableToClass(item.tblDomainEndings);

                    lstDomains.Add(clsDomain);
                }
            }

            return lstDomains;
        }

        public List<clsDomains> getAllDomainsWithGoogleAnalyticsList()
        {
            List<clsDomains> lstDomains = new List<clsDomains>();
            var lstGetDomainsList = db.tblDomains.Where(Domain => Domain.strGoogleAnalyticsCode != null && Domain.strGoogleAnalyticsCode != "" && Domain.bIsDeleted == false).ToList();

            if (lstGetDomainsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();

                foreach (var item in lstGetDomainsList)
                {
                    clsDomains clsDomain = new clsDomains();

                    clsDomain.iDomainID = item.iDomainID;
                    clsDomain.dtAdded = item.dtAdded;
                    clsDomain.iAddedBy = item.iAddedBy;
                    clsDomain.dtEdited = item.dtEdited;
                    clsDomain.iEditedBy = item.iEditedBy;

                    clsDomain.iClientID = item.iClientID;
                    clsDomain.strTitle = item.strTitle;
                    clsDomain.iDomainEndingID = item.iDomainEndingID;
                    clsDomain.dtRegistrationDate = item.dtRegistrationDate;
                    clsDomain.dtNextDueDate = item.dtNextDueDate;

                    clsDomain.strDomainStatus = item.strDomainStatus;
                    clsDomain.strGoogleAnalyticsCode = item.strGoogleAnalyticsCode;
                    clsDomain.bAutoRenew = item.bAutoRenew;
                    clsDomain.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        if (item.tblClients.bIsDeleted != true)
                            clsDomain.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblDomainEndings != null)
                        if (item.tblDomainEndings.bIsDeleted != true)
                            clsDomain.clsDomainEnding = clsDomainEndingsManager.convertDomainEndingsTableToClass(item.tblDomainEndings);

                    lstDomains.Add(clsDomain);
                }
            }

            return lstDomains;
        }
        public List<clsDomains> getAllDomainsOnlyList()
        {
            List<clsDomains> lstDomains = new List<clsDomains>();
            var lstGetDomainsList = db.tblDomains.Where(Domain => Domain.bIsDeleted == false).ToList();

            if (lstGetDomainsList.Count > 0)
            {

                foreach (var item in lstGetDomainsList)
                {
                    clsDomains clsDomain = new clsDomains();

                    clsDomain.iDomainID = item.iDomainID;
                    clsDomain.dtAdded = item.dtAdded;
                    clsDomain.iAddedBy = item.iAddedBy;
                    clsDomain.dtEdited = item.dtEdited;
                    clsDomain.iEditedBy = item.iEditedBy;

                    clsDomain.iClientID = item.iClientID;
                    clsDomain.strTitle = item.strTitle;
                    clsDomain.iDomainEndingID = item.iDomainEndingID;
                    clsDomain.dtRegistrationDate = item.dtRegistrationDate;
                    clsDomain.dtNextDueDate = item.dtNextDueDate;

                    clsDomain.strDomainStatus = item.strDomainStatus;
                    clsDomain.strGoogleAnalyticsCode = item.strGoogleAnalyticsCode;
                    clsDomain.bAutoRenew = item.bAutoRenew;
                    clsDomain.bIsDeleted = item.bIsDeleted;

                    lstDomains.Add(clsDomain);
                }
            }

            return lstDomains;
        }

        //Get
        public clsDomains getDomainByID(int iDomainID)
        {
            clsDomains clsDomain = null;
            tblDomains tblDomains = db.tblDomains.FirstOrDefault(Domain => Domain.iDomainID == iDomainID && Domain.bIsDeleted == false);

            if (tblDomains != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();

                clsDomain = new clsDomains();

                clsDomain.iDomainID = tblDomains.iDomainID;
                clsDomain.dtAdded = tblDomains.dtAdded;
                clsDomain.iAddedBy = tblDomains.iAddedBy;
                clsDomain.dtEdited = tblDomains.dtEdited;
                clsDomain.iEditedBy = tblDomains.iEditedBy;

                clsDomain.iClientID = tblDomains.iClientID;
                clsDomain.strTitle = tblDomains.strTitle;
                clsDomain.iDomainEndingID = tblDomains.iDomainEndingID;
                clsDomain.dtRegistrationDate = tblDomains.dtRegistrationDate;
                clsDomain.dtNextDueDate = tblDomains.dtNextDueDate;

                clsDomain.strDomainStatus = tblDomains.strDomainStatus;
                clsDomain.strGoogleAnalyticsCode = tblDomains.strGoogleAnalyticsCode;
                clsDomain.bAutoRenew = tblDomains.bAutoRenew;
                clsDomain.bIsDeleted = tblDomains.bIsDeleted;

                if (tblDomains.tblClients != null)
                    if (tblDomains.tblClients.bIsDeleted != true)
                        clsDomain.clsClient = clsClientsManager.convertClientsTableToClass(tblDomains.tblClients);
                if (tblDomains.tblDomainEndings != null)
                    if (tblDomains.tblDomainEndings.bIsDeleted != true)
                        clsDomain.clsDomainEnding = clsDomainEndingsManager.convertDomainEndingsTableToClass(tblDomains.tblDomainEndings);
            }

            return clsDomain;
        }

        //Save
        public int saveDomain(clsDomains clsDomain)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblDomains tblDomains = new tblDomains();

            tblDomains.iDomainID = clsDomain.iDomainID;

            tblDomains.iClientID = clsDomain.iClientID;
            tblDomains.strTitle = clsDomain.strTitle;
            tblDomains.iDomainEndingID = clsDomain.iDomainEndingID;
            tblDomains.dtRegistrationDate = clsDomain.dtRegistrationDate;
            tblDomains.dtNextDueDate = clsDomain.dtNextDueDate;

            tblDomains.strDomainStatus = clsDomain.strDomainStatus;
            tblDomains.strGoogleAnalyticsCode = clsDomain.strGoogleAnalyticsCode;
            tblDomains.bAutoRenew = clsDomain.bAutoRenew;
            tblDomains.bIsDeleted = clsDomain.bIsDeleted;

            //Add
            if (tblDomains.iDomainID == 0)
            {
                tblDomains.dtAdded = DateTime.Now;
                //tblDomains.iAddedBy = clsCMSUser.iCMSUserID;
                tblDomains.iAddedBy = 1;
                tblDomains.dtEdited = DateTime.Now;
                //tblDomains.iEditedBy = clsCMSUser.iCMSUserID;
                tblDomains.iEditedBy = 1;

                db.tblDomains.Add(tblDomains);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblDomains.dtAdded = clsDomain.dtAdded;
                tblDomains.iAddedBy = clsDomain.iAddedBy;
                tblDomains.dtEdited = DateTime.Now;
                //tblDomains.iEditedBy = clsCMSUser.iCMSUserID;
                tblDomains.iEditedBy = 1;

                db.Set<tblDomains>().AddOrUpdate(tblDomains);
                db.SaveChanges();
            }
            //}
            return tblDomains.iDomainID;
        }

        //Remove
        public void removeDomainByID(int iDomainID)
        {
            tblDomains tblDomain = db.tblDomains.Find(iDomainID);
            if (tblDomain != null)
            {
                tblDomain.bIsDeleted = true;
                db.Entry(tblDomain).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfDomainExists(int iDomainID)
        {
            bool bDomainExists = db.tblDomains.Any(Domain => Domain.iDomainID == iDomainID && Domain.bIsDeleted == false);
            return bDomainExists;
        }

        //Convert database table to class
        public clsDomains convertDomainsTableToClass(tblDomains tblDomain)
        {
            clsDomains clsDomain = new clsDomains();

            clsDomain.iDomainID = tblDomain.iDomainID;
            clsDomain.dtAdded = tblDomain.dtAdded;
            clsDomain.iAddedBy = tblDomain.iAddedBy;
            clsDomain.dtEdited = tblDomain.dtEdited;
            clsDomain.iEditedBy = tblDomain.iEditedBy;

            clsDomain.iClientID = tblDomain.iClientID;
            clsDomain.strTitle = tblDomain.strTitle;
            clsDomain.iDomainEndingID = tblDomain.iDomainEndingID;
            clsDomain.dtRegistrationDate = tblDomain.dtRegistrationDate;
            clsDomain.dtNextDueDate = tblDomain.dtNextDueDate;

            clsDomain.strDomainStatus = tblDomain.strDomainStatus;
            clsDomain.strGoogleAnalyticsCode = tblDomain.strGoogleAnalyticsCode;
            clsDomain.bAutoRenew = tblDomain.bAutoRenew;
            clsDomain.bIsDeleted = tblDomain.bIsDeleted;

            return clsDomain;
        }
    }
}
