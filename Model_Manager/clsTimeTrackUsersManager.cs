﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsTimeTrackUsersManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsTimeTrackUsers> getAllTimeTrackUsersList()
        {
            List<clsTimeTrackUsers> lstTimeTrackUsers = new List<clsTimeTrackUsers>();
            var lstGetTimeTrackUsersList = db.tblTimeTrackUsers.Where(TimeTrackUser => TimeTrackUser.bIsDeleted == false).ToList();

            if (lstGetTimeTrackUsersList.Count > 0)
            {
                //Managers

                foreach (var item in lstGetTimeTrackUsersList)
                {
                    clsTimeTrackUsers clsTimeTrackUser = new clsTimeTrackUsers();

                    clsTimeTrackUser.iTimeTrackUserID = item.iTimeTrackUserID;
                    clsTimeTrackUser.dtAdded = item.dtAdded;
                    clsTimeTrackUser.iAddedBy = item.iAddedBy;
                    clsTimeTrackUser.dtEdited = item.dtEdited;
                    clsTimeTrackUser.iEditedBy = item.iEditedBy;

                    clsTimeTrackUser.iConfigurationID = item.iConfigurationID;
                    clsTimeTrackUser.strFirstName = item.strFirstName;
                    clsTimeTrackUser.strSurname = item.strSurname;
                    clsTimeTrackUser.strEmailAddress = item.strEmailAddress;
                    clsTimeTrackUser.strPassword = item.strPassword;

                    clsTimeTrackUser.strClientID = item.strClientID;
                    clsTimeTrackUser.strClientSecret = item.strClientSecret;
                    clsTimeTrackUser.strRedirectURL = item.strRedirectURL;
                    clsTimeTrackUser.strRefreshToken = item.strRefreshToken;
                    clsTimeTrackUser.strAssigneeID = item.strAssigneeID;

                    clsTimeTrackUser.bIsDeleted = item.bIsDeleted;

                    lstTimeTrackUsers.Add(clsTimeTrackUser);
                }
            }

            return lstTimeTrackUsers;
        }

        public List<clsTimeTrackUsers> getAllTimeTrackUsersOnlyList()
        {
            List<clsTimeTrackUsers> lstTimeTrackUsers = new List<clsTimeTrackUsers>();
            var lstGetTimeTrackUsersList = db.tblTimeTrackUsers.Where(TimeTrackUser => TimeTrackUser.bIsDeleted == false).ToList();

            if (lstGetTimeTrackUsersList.Count > 0)
            {

                foreach (var item in lstGetTimeTrackUsersList)
                {
                    clsTimeTrackUsers clsTimeTrackUser = new clsTimeTrackUsers();

                    clsTimeTrackUser.iTimeTrackUserID = item.iTimeTrackUserID;
                    clsTimeTrackUser.dtAdded = item.dtAdded;
                    clsTimeTrackUser.iAddedBy = item.iAddedBy;
                    clsTimeTrackUser.dtEdited = item.dtEdited;
                    clsTimeTrackUser.iEditedBy = item.iEditedBy;

                    clsTimeTrackUser.iConfigurationID = item.iConfigurationID;
                    clsTimeTrackUser.strFirstName = item.strFirstName;
                    clsTimeTrackUser.strSurname = item.strSurname;
                    clsTimeTrackUser.strEmailAddress = item.strEmailAddress;
                    clsTimeTrackUser.strPassword = item.strPassword;

                    clsTimeTrackUser.strClientID = item.strClientID;
                    clsTimeTrackUser.strClientSecret = item.strClientSecret;
                    clsTimeTrackUser.strRedirectURL = item.strRedirectURL;
                    clsTimeTrackUser.strRefreshToken = item.strRefreshToken;
                    clsTimeTrackUser.strAssigneeID = item.strAssigneeID;

                    clsTimeTrackUser.bIsDeleted = item.bIsDeleted;

                    lstTimeTrackUsers.Add(clsTimeTrackUser);
                }
            }

            return lstTimeTrackUsers;
        }

        //Get
        public clsTimeTrackUsers getTimeTrackUserByID(int iTimeTrackUserID)
        {
            clsTimeTrackUsers clsTimeTrackUser = null;
            tblTimeTrackUsers tblTimeTrackUsers = db.tblTimeTrackUsers.FirstOrDefault(TimeTrackUser => TimeTrackUser.iTimeTrackUserID == iTimeTrackUserID && TimeTrackUser.bIsDeleted == false);

            if (tblTimeTrackUsers != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                clsTimeTrackUser = new clsTimeTrackUsers();

                clsTimeTrackUser.iTimeTrackUserID = tblTimeTrackUsers.iTimeTrackUserID;
                clsTimeTrackUser.dtAdded = tblTimeTrackUsers.dtAdded;
                clsTimeTrackUser.iAddedBy = tblTimeTrackUsers.iAddedBy;
                clsTimeTrackUser.dtEdited = tblTimeTrackUsers.dtEdited;
                clsTimeTrackUser.iEditedBy = tblTimeTrackUsers.iEditedBy;

                clsTimeTrackUser.iConfigurationID = tblTimeTrackUsers.iConfigurationID;
                clsTimeTrackUser.strFirstName = tblTimeTrackUsers.strFirstName;
                clsTimeTrackUser.strSurname = tblTimeTrackUsers.strSurname;
                clsTimeTrackUser.strEmailAddress = tblTimeTrackUsers.strEmailAddress;
                clsTimeTrackUser.strPassword = tblTimeTrackUsers.strPassword;

                clsTimeTrackUser.strClientID = tblTimeTrackUsers.strClientID;
                clsTimeTrackUser.strClientSecret = tblTimeTrackUsers.strClientSecret;
                clsTimeTrackUser.strRedirectURL = tblTimeTrackUsers.strRedirectURL;
                clsTimeTrackUser.strRefreshToken = tblTimeTrackUsers.strRefreshToken;
                clsTimeTrackUser.strAssigneeID = tblTimeTrackUsers.strAssigneeID;

                clsTimeTrackUser.bIsDeleted = tblTimeTrackUsers.bIsDeleted;
            }

            return clsTimeTrackUser;
        }

        //Save
        public int saveTimeTrackUser(clsTimeTrackUsers clsTimeTrackUser)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblTimeTrackUsers tblTimeTrackUsers = new tblTimeTrackUsers();

            tblTimeTrackUsers.iTimeTrackUserID = clsTimeTrackUser.iTimeTrackUserID;

            clsTimeTrackUser.iConfigurationID = tblTimeTrackUsers.iConfigurationID;
            clsTimeTrackUser.strFirstName = tblTimeTrackUsers.strFirstName;
            clsTimeTrackUser.strSurname = tblTimeTrackUsers.strSurname;
            clsTimeTrackUser.strEmailAddress = tblTimeTrackUsers.strEmailAddress;
            clsTimeTrackUser.strPassword = tblTimeTrackUsers.strPassword;

            clsTimeTrackUser.strClientID = tblTimeTrackUsers.strClientID;
            clsTimeTrackUser.strClientSecret = tblTimeTrackUsers.strClientSecret;
            clsTimeTrackUser.strRedirectURL = tblTimeTrackUsers.strRedirectURL;
            clsTimeTrackUser.strRefreshToken = tblTimeTrackUsers.strRefreshToken;
            clsTimeTrackUser.strAssigneeID = tblTimeTrackUsers.strAssigneeID;

            clsTimeTrackUser.bIsDeleted = tblTimeTrackUsers.bIsDeleted;

            //Add
            if (tblTimeTrackUsers.iTimeTrackUserID == 0)
            {
                tblTimeTrackUsers.dtAdded = DateTime.Now;
                //tblTimeTrackUsers.iAddedBy = clsCMSUser.iCMSUserID;
                tblTimeTrackUsers.iAddedBy = 1;
                tblTimeTrackUsers.dtEdited = DateTime.Now;
                //tblTimeTrackUsers.iEditedBy = clsCMSUser.iCMSUserID;
                tblTimeTrackUsers.iEditedBy = 1;

                db.tblTimeTrackUsers.Add(tblTimeTrackUsers);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblTimeTrackUsers.dtAdded = clsTimeTrackUser.dtAdded;
                tblTimeTrackUsers.iAddedBy = clsTimeTrackUser.iAddedBy;
                tblTimeTrackUsers.dtEdited = DateTime.Now;
                //tblTimeTrackUsers.iEditedBy = clsCMSUser.iCMSUserID;
                tblTimeTrackUsers.iEditedBy = 1;

                db.Set<tblTimeTrackUsers>().AddOrUpdate(tblTimeTrackUsers);
                db.SaveChanges();
            }
            //}
            return tblTimeTrackUsers.iTimeTrackUserID;
        }

        //Remove
        public void removeTimeTrackUserByID(int iTimeTrackUserID)
        {
            tblTimeTrackUsers tblTimeTrackUser = db.tblTimeTrackUsers.Find(iTimeTrackUserID);
            if (tblTimeTrackUser != null)
            {
                tblTimeTrackUser.bIsDeleted = true;
                db.Entry(tblTimeTrackUser).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfTimeTrackUserExists(int iTimeTrackUserID)
        {
            bool bTimeTrackUserExists = db.tblTimeTrackUsers.Any(TimeTrackUser => TimeTrackUser.iTimeTrackUserID == iTimeTrackUserID && TimeTrackUser.bIsDeleted == false);
            return bTimeTrackUserExists;
        }

        //Convert database table to class
        public clsTimeTrackUsers convertTimeTrackUsersTableToClass(tblTimeTrackUsers tblTimeTrackUser)
        {
            clsTimeTrackUsers clsTimeTrackUser = new clsTimeTrackUsers();

            clsTimeTrackUser.iTimeTrackUserID = tblTimeTrackUser.iTimeTrackUserID;
            clsTimeTrackUser.dtAdded = tblTimeTrackUser.dtAdded;
            clsTimeTrackUser.iAddedBy = tblTimeTrackUser.iAddedBy;
            clsTimeTrackUser.dtEdited = tblTimeTrackUser.dtEdited;
            clsTimeTrackUser.iEditedBy = tblTimeTrackUser.iEditedBy;

            clsTimeTrackUser.iConfigurationID = tblTimeTrackUser.iConfigurationID;
            clsTimeTrackUser.strFirstName = tblTimeTrackUser.strFirstName;
            clsTimeTrackUser.strSurname = tblTimeTrackUser.strSurname;
            clsTimeTrackUser.strEmailAddress = tblTimeTrackUser.strEmailAddress;
            clsTimeTrackUser.strPassword = tblTimeTrackUser.strPassword;

            clsTimeTrackUser.strClientID = tblTimeTrackUser.strClientID;
            clsTimeTrackUser.strClientSecret = tblTimeTrackUser.strClientSecret;
            clsTimeTrackUser.strRedirectURL = tblTimeTrackUser.strRedirectURL;
            clsTimeTrackUser.strRefreshToken = tblTimeTrackUser.strRefreshToken;
            clsTimeTrackUser.strAssigneeID = tblTimeTrackUser.strAssigneeID;

            clsTimeTrackUser.bIsDeleted = tblTimeTrackUser.bIsDeleted;

            return clsTimeTrackUser;
        }
    }
}
