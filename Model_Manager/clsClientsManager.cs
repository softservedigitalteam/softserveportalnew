﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClients> getAllClientsList()
        {
            List<clsClients> lstClients = new List<clsClients>();
            var lstGetClientsList = db.tblClients.Where(Client => Client.bIsDeleted == false).ToList();

            if (lstGetClientsList.Count > 0)
            {
                //Managers
                clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();
                clsClientClientServicesLinkTableManager clsClientClientServicesLinkTableManager = new clsClientClientServicesLinkTableManager();
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsDomainsManager clsDomainsManager = new clsDomainsManager();
                clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
                clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                foreach (var item in lstGetClientsList)
                {
                    clsClients clsClient = new clsClients();

                    clsClient.iClientID = item.iClientID;
                    clsClient.dtAdded = item.dtAdded;
                    clsClient.iAddedBy = item.iAddedBy;
                    clsClient.dtEdited = item.dtEdited;
                    clsClient.iEditedBy = item.iEditedBy;

                    clsClient.strCompanyName = item.strCompanyName;
                    clsClient.strCompanyLandline = item.strCompanyLandline;
                    clsClient.strUsername = item.strUsername;
                    clsClient.strEmail = item.strEmail;

                    clsClient.strRegisteredName = item.strRegisteredName;
                    clsClient.strRegistrationNumber = item.strRegistrationNumber;
                    clsClient.strVATNumber = item.strVATNumber;
                    clsClient.strPhysicalAddress = item.strPhysicalAddress;
                    clsClient.strPostalAddress = item.strPostalAddress;

                    clsClient.dblBillingRate = item.dblBillingRate;
                    clsClient.strURL = item.strURL;
                    clsClient.strPathToImages = item.strPathToImages;
                    clsClient.strMasterImage = item.strMasterImage;
                    clsClient.strPassword = item.strPassword;

                    clsClient.strVerificationKey = item.strVerificationKey;
                    clsClient.bIsVerified = item.bIsVerified;
                    clsClient.strIsApproved = item.strIsApproved;
                    clsClient.bIsProfileCompleted = item.bIsProfileCompleted;
                    clsClient.bIsDeleted = item.bIsDeleted;

                    clsClient.lstCIDocuments = new List<clsCIDocuments>();
                    clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                    clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                    clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                    clsClient.lstDomains = new List<clsDomains>();
                    clsClient.lstNotifications = new List<clsNotifications>();
                    clsClient.lstClientLinks = new List<clsClientLinks>();
                    clsClient.lstProjects = new List<clsProjects>();

                    if (item.tblCIDocuments.Count > 0)
                    {
                        foreach (var CIDocument in item.tblCIDocuments)
                        {
                            if (CIDocument.bIsDeleted != true)
                            {
                                clsCIDocuments clsCIDocument = clsCIDocumentsManager.convertCIDocumentsTableToClass(CIDocument);
                                clsClient.lstCIDocuments.Add(clsCIDocument);
                            }
                        }
                    }
                    if (item.tblClientClientContactDetailsLinkTable.Count > 0)
                    {
                        foreach (var ClientClientContactDetailsLinkTableItem in item.tblClientClientContactDetailsLinkTable)
                        {
                            if (ClientClientContactDetailsLinkTableItem.bIsDeleted != true)
                            {
                                clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                                clsClient.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                            }
                        }
                    }
                    if (item.tblClientClientServicesLinkTable.Count > 0)
                    {
                        foreach (var ClientClientServicesLinkTable in item.tblClientClientServicesLinkTable)
                        {
                            if (ClientClientServicesLinkTable.bIsDeleted != true)
                            {
                                clsClientClientServicesLinkTable clsClientClientServicesLinkTable = clsClientClientServicesLinkTableManager.convertClientClientServicesLinkTableTableToClass(ClientClientServicesLinkTable);
                                clsClient.lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                            }
                            
                        }
                    }
                    if (item.tblSocialCalendarEvents.Count > 0)
                    {
                        foreach (var SocialCalendarEventItem in item.tblSocialCalendarEvents)
                        {
                            if (SocialCalendarEventItem.bIsDeleted != true)
                            {
                                clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.convertSocialCalendarEventsTableToClass(SocialCalendarEventItem);
                                clsClient.lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                            }
                        }
                    }
                    if (item.tblDomains.Count > 0)
                    {
                        foreach (var Domain in item.tblDomains)
                        {
                            if (Domain.bIsDeleted != true)
                            {
                                clsDomains clsDomain = clsDomainsManager.convertDomainsTableToClass(Domain);
                                clsClient.lstDomains.Add(clsDomain);
                            }
                        }
                    }
                    if (item.tblNotifications.Count > 0)
                    {
                        foreach (var Notification in item.tblNotifications)
                        {
                            if (Notification.bIsDeleted != true)
                            {
                                clsNotifications clsNotification = clsNotificationsManager.convertNotificationsTableToClass(Notification);
                                clsClient.lstNotifications.Add(clsNotification);
                            }
                        }
                    }
                    if (item.tblClientLinks.Count > 0)
                    {
                        foreach (var ClientLink in item.tblClientLinks)
                        {
                            if (ClientLink.bIsDeleted != true)
                            {
                                clsClientLinks clsClientLink = clsClientLinksManager.convertClientLinksTableToClass(ClientLink);
                                clsClient.lstClientLinks.Add(clsClientLink);
                            }
                        }
                    }
                    if (item.tblProjects.Count > 0)
                    {
                        foreach (var Project in item.tblProjects)
                        {
                            if (Project.bIsDeleted != true)
                            {
                                clsProjects clsProject = clsProjectsManager.convertProjectsTableToClass(Project);
                                clsClient.lstProjects.Add(clsProject);
                            }
                        }
                    }

                    lstClients.Add(clsClient);
                }
            }

            return lstClients;
        }

        public List<clsClients> getAllClientsOnlyList()
        {
            List<clsClients> lstClients = new List<clsClients>();
            var lstGetClientsList = db.tblClients.Where(Client => Client.bIsDeleted == false).ToList();

            if (lstGetClientsList.Count > 0)
            {

                foreach (var item in lstGetClientsList)
                {
                    clsClients clsClient = new clsClients();

                    clsClient.iClientID = item.iClientID;
                    clsClient.dtAdded = item.dtAdded;
                    clsClient.iAddedBy = item.iAddedBy;
                    clsClient.dtEdited = item.dtEdited;
                    clsClient.iEditedBy = item.iEditedBy;

                    clsClient.strCompanyName = item.strCompanyName;
                    clsClient.strCompanyLandline = item.strCompanyLandline;
                    clsClient.strUsername = item.strUsername;
                    clsClient.strEmail = item.strEmail;

                    clsClient.strRegisteredName = item.strRegisteredName;
                    clsClient.strRegistrationNumber = item.strRegistrationNumber;
                    clsClient.strVATNumber = item.strVATNumber;
                    clsClient.strPhysicalAddress = item.strPhysicalAddress;
                    clsClient.strPostalAddress = item.strPostalAddress;

                    clsClient.dblBillingRate = item.dblBillingRate;
                    clsClient.strURL = item.strURL;
                    clsClient.strPathToImages = item.strPathToImages;
                    clsClient.strMasterImage = item.strMasterImage;
                    clsClient.strPassword = item.strPassword;

                    clsClient.strVerificationKey = item.strVerificationKey;
                    clsClient.bIsVerified = item.bIsVerified;
                    clsClient.strIsApproved = item.strIsApproved;
                    clsClient.bIsProfileCompleted = item.bIsProfileCompleted;
                    clsClient.bIsDeleted = item.bIsDeleted;

                    clsClient.lstCIDocuments = new List<clsCIDocuments>();
                    clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                    clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                    clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                    clsClient.lstDomains = new List<clsDomains>();
                    clsClient.lstNotifications = new List<clsNotifications>();
                    clsClient.lstClientLinks = new List<clsClientLinks>();
                    clsClient.lstProjects = new List<clsProjects>();

                    lstClients.Add(clsClient);
                }
            }

            return lstClients;
        }

        //Get
        public clsClients getClientByID(int iClientID)
        {
            clsClients clsClient = null;
            tblClients tblClients = db.tblClients.FirstOrDefault(Client => Client.iClientID == iClientID && Client.bIsDeleted == false);

            if (tblClients != null)
            {
                //Managers
                clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();
                clsClientClientServicesLinkTableManager clsClientClientServicesLinkTableManager = new clsClientClientServicesLinkTableManager();
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsDomainsManager clsDomainsManager = new clsDomainsManager();
                clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
                clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                clsClient = new clsClients();

                clsClient.iClientID = tblClients.iClientID;
                clsClient.dtAdded = tblClients.dtAdded;
                clsClient.iAddedBy = tblClients.iAddedBy;
                clsClient.dtEdited = tblClients.dtEdited;
                clsClient.iEditedBy = tblClients.iEditedBy;

                clsClient.strCompanyName = tblClients.strCompanyName;
                clsClient.strCompanyLandline = tblClients.strCompanyLandline;
                clsClient.strUsername = tblClients.strUsername;
                clsClient.strEmail = tblClients.strEmail;

                clsClient.strRegisteredName = tblClients.strRegisteredName;
                clsClient.strRegistrationNumber = tblClients.strRegistrationNumber;
                clsClient.strVATNumber = tblClients.strVATNumber;
                clsClient.strPhysicalAddress = tblClients.strPhysicalAddress;
                clsClient.strPostalAddress = tblClients.strPostalAddress;

                clsClient.dblBillingRate = tblClients.dblBillingRate;
                clsClient.strURL = tblClients.strURL;
                clsClient.strPathToImages = tblClients.strPathToImages;
                clsClient.strMasterImage = tblClients.strMasterImage;
                clsClient.strPassword = tblClients.strPassword;

                clsClient.strVerificationKey = tblClients.strVerificationKey;
                clsClient.bIsVerified = tblClients.bIsVerified;
                clsClient.strIsApproved = tblClients.strIsApproved;
                clsClient.bIsProfileCompleted = tblClients.bIsProfileCompleted;
                clsClient.bIsDeleted = tblClients.bIsDeleted;

                clsClient.lstCIDocuments = new List<clsCIDocuments>();
                clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                clsClient.lstDomains = new List<clsDomains>();
                clsClient.lstNotifications = new List<clsNotifications>();
                clsClient.lstClientLinks = new List<clsClientLinks>();
                clsClient.lstProjects = new List<clsProjects>();

                if (tblClients.tblCIDocuments.Count > 0)
                {
                    foreach (var CIDocument in tblClients.tblCIDocuments)
                    {
                        if (CIDocument.bIsDeleted != true)
                        {
                            clsCIDocuments clsCIDocument = clsCIDocumentsManager.convertCIDocumentsTableToClass(CIDocument);
                            clsClient.lstCIDocuments.Add(clsCIDocument);
                        }
                    }
                }
                if (tblClients.tblClientClientContactDetailsLinkTable.Count > 0)
                {
                    foreach (var ClientClientContactDetailsLinkTableItem in tblClients.tblClientClientContactDetailsLinkTable)
                    {
                        if (ClientClientContactDetailsLinkTableItem.bIsDeleted != true)
                        {
                            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                            clsClient.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                        }
                    }
                }
                if (tblClients.tblClientClientServicesLinkTable.Count > 0)
                {
                    foreach (var ClientClientServicesLinkTable in tblClients.tblClientClientServicesLinkTable)
                    {
                        if (ClientClientServicesLinkTable.bIsDeleted != true)
                        {
                            clsClientClientServicesLinkTable clsClientClientServicesLinkTable = clsClientClientServicesLinkTableManager.convertClientClientServicesLinkTableTableToClass(ClientClientServicesLinkTable);
                            clsClient.lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                        }

                    }
                }
                if (tblClients.tblSocialCalendarEvents.Count > 0)
                {
                    foreach (var SocialCalendarEventItem in tblClients.tblSocialCalendarEvents)
                    {
                        if (SocialCalendarEventItem.bIsDeleted != true)
                        {
                            clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.convertSocialCalendarEventsTableToClass(SocialCalendarEventItem);
                            clsClient.lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                        }
                    }
                }
                if (tblClients.tblDomains.Count > 0)
                {
                    foreach (var Domain in tblClients.tblDomains)
                    {
                        if (Domain.bIsDeleted != true)
                        {
                            clsDomains clsDomain = clsDomainsManager.convertDomainsTableToClass(Domain);
                            clsClient.lstDomains.Add(clsDomain);
                        }
                    }
                }
                if (tblClients.tblNotifications.Count > 0)
                {
                    foreach (var Notification in tblClients.tblNotifications)
                    {
                        if (Notification.bIsDeleted != true)
                        {
                            clsNotifications clsNotification = clsNotificationsManager.convertNotificationsTableToClass(Notification);
                            clsClient.lstNotifications.Add(clsNotification);
                        }
                    }
                }
                if (tblClients.tblClientLinks.Count > 0)
                {
                    foreach (var ClientLink in tblClients.tblClientLinks)
                    {
                        if (ClientLink.bIsDeleted != true)
                        {
                            clsClientLinks clsClientLink = clsClientLinksManager.convertClientLinksTableToClass(ClientLink);
                            clsClient.lstClientLinks.Add(clsClientLink);
                        }
                    }
                }
                if (tblClients.tblProjects.Count > 0)
                {
                    foreach (var Project in tblClients.tblProjects)
                    {
                        if (Project.bIsDeleted != true)
                        {
                            clsProjects clsProject = clsProjectsManager.convertProjectsTableToClass(Project);
                            clsClient.lstProjects.Add(clsProject);
                        }
                    }
                }
            }

            return clsClient;
        }

        public clsClients getClientByIDOnly(int iClientID)
        {
            clsClients clsClient = null;
            tblClients tblClients = db.tblClients.FirstOrDefault(Client => Client.iClientID == iClientID && Client.bIsDeleted == false);

            if (tblClients != null)
            {
                //Managers

                clsClient = new clsClients();

                clsClient.iClientID = tblClients.iClientID;
                clsClient.dtAdded = tblClients.dtAdded;
                clsClient.iAddedBy = tblClients.iAddedBy;
                clsClient.dtEdited = tblClients.dtEdited;
                clsClient.iEditedBy = tblClients.iEditedBy;

                clsClient.strCompanyName = tblClients.strCompanyName;
                clsClient.strCompanyLandline = tblClients.strCompanyLandline;
                clsClient.strUsername = tblClients.strUsername;
                clsClient.strEmail = tblClients.strEmail;

                clsClient.strRegisteredName = tblClients.strRegisteredName;
                clsClient.strRegistrationNumber = tblClients.strRegistrationNumber;
                clsClient.strVATNumber = tblClients.strVATNumber;
                clsClient.strPhysicalAddress = tblClients.strPhysicalAddress;
                clsClient.strPostalAddress = tblClients.strPostalAddress;

                clsClient.dblBillingRate = tblClients.dblBillingRate;
                clsClient.strURL = tblClients.strURL;
                clsClient.strPathToImages = tblClients.strPathToImages;
                clsClient.strMasterImage = tblClients.strMasterImage;
                clsClient.strPassword = tblClients.strPassword;

                clsClient.strVerificationKey = tblClients.strVerificationKey;
                clsClient.bIsVerified = tblClients.bIsVerified;
                clsClient.strIsApproved = tblClients.strIsApproved;
                clsClient.bIsProfileCompleted = tblClients.bIsProfileCompleted;
                clsClient.bIsDeleted = tblClients.bIsDeleted;

                clsClient.lstCIDocuments = new List<clsCIDocuments>();
                clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                clsClient.lstDomains = new List<clsDomains>();
                clsClient.lstNotifications = new List<clsNotifications>();
                clsClient.lstClientLinks = new List<clsClientLinks>();
                clsClient.lstProjects = new List<clsProjects>();
            }

            return clsClient;
        }

        public clsClients getClientByEmail(string strClientEmail)
        {
            clsClients clsClient = null;
            tblClients tblClients = db.tblClients.FirstOrDefault(Client => Client.strEmail == strClientEmail.ToLower() && Client.bIsDeleted == false);

            if (tblClients != null)
            {
                //Managers
                clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();
                clsClientClientServicesLinkTableManager clsClientClientServicesLinkTableManager = new clsClientClientServicesLinkTableManager();
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsDomainsManager clsDomainsManager = new clsDomainsManager();
                clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
                clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                clsClient = new clsClients();

                clsClient.iClientID = tblClients.iClientID;
                clsClient.dtAdded = tblClients.dtAdded;
                clsClient.iAddedBy = tblClients.iAddedBy;
                clsClient.dtEdited = tblClients.dtEdited;
                clsClient.iEditedBy = tblClients.iEditedBy;

                clsClient.strCompanyName = tblClients.strCompanyName;
                clsClient.strCompanyLandline = tblClients.strCompanyLandline;
                clsClient.strUsername = tblClients.strUsername;
                clsClient.strEmail = tblClients.strEmail;

                clsClient.strRegisteredName = tblClients.strRegisteredName;
                clsClient.strRegistrationNumber = tblClients.strRegistrationNumber;
                clsClient.strVATNumber = tblClients.strVATNumber;
                clsClient.strPhysicalAddress = tblClients.strPhysicalAddress;
                clsClient.strPostalAddress = tblClients.strPostalAddress;

                clsClient.dblBillingRate = tblClients.dblBillingRate;
                clsClient.strURL = tblClients.strURL;
                clsClient.strPathToImages = tblClients.strPathToImages;
                clsClient.strMasterImage = tblClients.strMasterImage;
                clsClient.strPassword = tblClients.strPassword;

                clsClient.strVerificationKey = tblClients.strVerificationKey;
                clsClient.bIsVerified = tblClients.bIsVerified;
                clsClient.strIsApproved = tblClients.strIsApproved;
                clsClient.bIsProfileCompleted = tblClients.bIsProfileCompleted;
                clsClient.bIsDeleted = tblClients.bIsDeleted;

                clsClient.lstCIDocuments = new List<clsCIDocuments>();
                clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                clsClient.lstNotifications = new List<clsNotifications>();
                clsClient.lstClientLinks = new List<clsClientLinks>();
                clsClient.lstProjects = new List<clsProjects>();

                if (tblClients.tblCIDocuments.Count > 0)
                {
                    foreach (var CIDocument in tblClients.tblCIDocuments)
                    {
                        if (CIDocument.bIsDeleted != true)
                        {
                            clsCIDocuments clsCIDocument = clsCIDocumentsManager.convertCIDocumentsTableToClass(CIDocument);
                            clsClient.lstCIDocuments.Add(clsCIDocument);
                        }
                    }
                }
                if (tblClients.tblClientClientContactDetailsLinkTable.Count > 0)
                {
                    foreach (var ClientClientContactDetailsLinkTableItem in tblClients.tblClientClientContactDetailsLinkTable)
                    {
                        if (ClientClientContactDetailsLinkTableItem.bIsDeleted != true)
                        {
                            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                            clsClient.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                        }
                    }
                }
                if (tblClients.tblClientClientServicesLinkTable.Count > 0)
                {
                    foreach (var ClientClientServicesLinkTable in tblClients.tblClientClientServicesLinkTable)
                    {
                        if (ClientClientServicesLinkTable.bIsDeleted != true)
                        {
                            clsClientClientServicesLinkTable clsClientClientServicesLinkTable = clsClientClientServicesLinkTableManager.convertClientClientServicesLinkTableTableToClass(ClientClientServicesLinkTable);
                            clsClient.lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                        }

                    }
                }
                if (tblClients.tblSocialCalendarEvents.Count > 0)
                {
                    foreach (var SocialCalendarEventItem in tblClients.tblSocialCalendarEvents)
                    {
                        if (SocialCalendarEventItem.bIsDeleted != true)
                        {
                            clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.convertSocialCalendarEventsTableToClass(SocialCalendarEventItem);
                            clsClient.lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                        }
                    }
                }
                if (tblClients.tblDomains.Count > 0)
                {
                    foreach (var Domain in tblClients.tblDomains)
                    {
                        if (Domain.bIsDeleted != true)
                        {
                            clsDomains clsDomain = clsDomainsManager.convertDomainsTableToClass(Domain);
                            clsClient.lstDomains.Add(clsDomain);
                        }
                    }
                }
                if (tblClients.tblNotifications.Count > 0)
                {
                    foreach (var Notification in tblClients.tblNotifications)
                    {
                        if (Notification.bIsDeleted != true)
                        {
                            clsNotifications clsNotification = clsNotificationsManager.convertNotificationsTableToClass(Notification);
                            clsClient.lstNotifications.Add(clsNotification);
                        }
                    }
                }
                if (tblClients.tblClientLinks.Count > 0)
                {
                    foreach (var ClientLink in tblClients.tblClientLinks)
                    {
                        if (ClientLink.bIsDeleted != true)
                        {
                            clsClientLinks clsClientLink = clsClientLinksManager.convertClientLinksTableToClass(ClientLink);
                            clsClient.lstClientLinks.Add(clsClientLink);
                        }
                    }
                }
                if (tblClients.tblProjects.Count > 0)
                {
                    foreach (var Project in tblClients.tblProjects)
                    {
                        if (Project.bIsDeleted != true)
                        {
                            clsProjects clsProject = clsProjectsManager.convertProjectsTableToClass(Project);
                            clsClient.lstProjects.Add(clsProject);
                        }
                    }
                }
            }

            return clsClient;
        }

        public clsClients getClientByEmailOnly(string strClientEmail)
        {
            clsClients clsClient = null;
            tblClients tblClients = db.tblClients.FirstOrDefault(Client => Client.strEmail == strClientEmail.ToLower() && Client.bIsDeleted == false);

            if (tblClients != null)
            {
                //Managers

                clsClient = new clsClients();

                clsClient.iClientID = tblClients.iClientID;
                clsClient.dtAdded = tblClients.dtAdded;
                clsClient.iAddedBy = tblClients.iAddedBy;
                clsClient.dtEdited = tblClients.dtEdited;
                clsClient.iEditedBy = tblClients.iEditedBy;

                clsClient.strCompanyName = tblClients.strCompanyName;
                clsClient.strCompanyLandline = tblClients.strCompanyLandline;
                clsClient.strUsername = tblClients.strUsername;
                clsClient.strEmail = tblClients.strEmail;

                clsClient.strRegisteredName = tblClients.strRegisteredName;
                clsClient.strRegistrationNumber = tblClients.strRegistrationNumber;
                clsClient.strVATNumber = tblClients.strVATNumber;
                clsClient.strPhysicalAddress = tblClients.strPhysicalAddress;
                clsClient.strPostalAddress = tblClients.strPostalAddress;

                clsClient.dblBillingRate = tblClients.dblBillingRate;
                clsClient.strURL = tblClients.strURL;
                clsClient.strPathToImages = tblClients.strPathToImages;
                clsClient.strMasterImage = tblClients.strMasterImage;
                clsClient.strPassword = tblClients.strPassword;

                clsClient.strVerificationKey = tblClients.strVerificationKey;
                clsClient.bIsVerified = tblClients.bIsVerified;
                clsClient.strIsApproved = tblClients.strIsApproved;
                clsClient.bIsProfileCompleted = tblClients.bIsProfileCompleted;
                clsClient.bIsDeleted = tblClients.bIsDeleted;

                clsClient.lstCIDocuments = new List<clsCIDocuments>();
                clsClient.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
                clsClient.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
                clsClient.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
                clsClient.lstDomains = new List<clsDomains>();
                clsClient.lstNotifications = new List<clsNotifications>();
                clsClient.lstClientLinks = new List<clsClientLinks>();
                clsClient.lstProjects = new List<clsProjects>();
            }

            return clsClient;
        }
        //Save
        public int saveClient(clsClients clsClient)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
                //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            //try
            //{
                tblClients tblClients = new tblClients();

                tblClients.iClientID = clsClient.iClientID;

                tblClients.strCompanyName = clsClient.strCompanyName;
                tblClients.strCompanyLandline = clsClient.strCompanyLandline;
                tblClients.strUsername = clsClient.strUsername;
                tblClients.strEmail = clsClient.strEmail.ToLower();

                tblClients.strRegisteredName = clsClient.strRegisteredName;
                tblClients.strRegistrationNumber = clsClient.strRegistrationNumber;
                tblClients.strVATNumber = clsClient.strVATNumber;
                tblClients.strPhysicalAddress = clsClient.strPhysicalAddress;
                tblClients.strPostalAddress = clsClient.strPostalAddress;

                tblClients.dblBillingRate = clsClient.dblBillingRate;
                tblClients.strURL = clsClient.strURL;
                tblClients.strPathToImages = clsClient.strPathToImages;
                tblClients.strMasterImage = clsClient.strMasterImage;
                tblClients.strPassword = clsClient.strPassword;

                tblClients.strVerificationKey = clsClient.strVerificationKey;
                tblClients.bIsVerified = clsClient.bIsVerified;
                tblClients.strIsApproved = clsClient.strIsApproved;
                tblClients.bIsProfileCompleted = clsClient.bIsProfileCompleted;
                tblClients.bIsDeleted = clsClient.bIsDeleted;

                //Add
                if (tblClients.iClientID == 0)
                {
                    tblClients.dtAdded = DateTime.Now;
                    //tblClients.iAddedBy = clsCMSUser.iCMSUserID;
                    tblClients.iAddedBy = 1;
                    tblClients.dtEdited = DateTime.Now;
                    //tblClients.iEditedBy = clsCMSUser.iCMSUserID;
                    tblClients.iEditedBy = 1;

                    db.tblClients.Add(tblClients);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblClients.dtAdded = clsClient.dtAdded;
                    tblClients.iAddedBy = clsClient.iAddedBy;
                    tblClients.dtEdited = DateTime.Now;
                    //tblClients.iEditedBy = clsCMSUser.iCMSUserID;
                    tblClients.iEditedBy = 1;

                    db.Set<tblClients>().AddOrUpdate(tblClients);
                    db.SaveChanges();
                }
                //}
                return tblClients.iClientID;
            //}
            //catch (DbEntityValidationException dbEx)
            //{
            //    string err = "";
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
            //            err += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage + "\r\n";
            //        }
            //    }
            //    return 0;
            //}
        }

        //Remove
        public void removeClientByID(int iClientID)
        {
            tblClients tblClient = db.tblClients.Find(iClientID);
            if (tblClient != null)
            {
                tblClient.bIsDeleted = true;
                db.Entry(tblClient).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientExists(int iClientID)
        {
            bool bClientExists = db.tblClients.Any(Client => Client.iClientID == iClientID && Client.bIsDeleted == false);
            return bClientExists;
        }

        //Convert database table to class
        public clsClients convertClientsTableToClass(tblClients tblClient)
        {
            clsClients clsClient = new clsClients();

            clsClient.iClientID = tblClient.iClientID;
            clsClient.dtAdded = tblClient.dtAdded;
            clsClient.iAddedBy = tblClient.iAddedBy;
            clsClient.dtEdited = tblClient.dtEdited;
            clsClient.iEditedBy = tblClient.iEditedBy;

            clsClient.strCompanyName = tblClient.strCompanyName;
            clsClient.strCompanyLandline = tblClient.strCompanyLandline;
            clsClient.strUsername = tblClient.strUsername;
            clsClient.strEmail = tblClient.strEmail;

            clsClient.strRegisteredName = tblClient.strRegisteredName;
            clsClient.strRegistrationNumber = tblClient.strRegistrationNumber;
            clsClient.strVATNumber = tblClient.strVATNumber;
            clsClient.strPhysicalAddress = tblClient.strPhysicalAddress;
            clsClient.strPostalAddress = tblClient.strPostalAddress;

            clsClient.dblBillingRate = tblClient.dblBillingRate;
            clsClient.strURL = tblClient.strURL;
            clsClient.strPathToImages = tblClient.strPathToImages;
            clsClient.strMasterImage = tblClient.strMasterImage;
            clsClient.strPassword = tblClient.strPassword;

            clsClient.strVerificationKey = tblClient.strVerificationKey;
            clsClient.bIsVerified = tblClient.bIsVerified;
            clsClient.strIsApproved = tblClient.strIsApproved;
            clsClient.bIsProfileCompleted = tblClient.bIsProfileCompleted;
            clsClient.bIsDeleted = tblClient.bIsDeleted;

            return clsClient;
        }
    }
}
