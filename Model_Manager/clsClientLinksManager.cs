﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientLinksManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientLinks> getAllClientLinksList()
        {
            List<clsClientLinks> lstClientLinks = new List<clsClientLinks>();
            var lstGetClientLinksList = db.tblClientLinks.Where(ClientLink => ClientLink.bIsDeleted == false).ToList();

            if (lstGetClientLinksList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();

                foreach (var item in lstGetClientLinksList)
                {
                    clsClientLinks clsClientLink = new clsClientLinks();

                    clsClientLink.iClientLinkID = item.iClientLinkID;
                    clsClientLink.dtAdded = item.dtAdded;
                    clsClientLink.iAddedBy = item.iAddedBy;
                    clsClientLink.dtEdited = item.dtEdited;
                    clsClientLink.iEditedBy = item.iEditedBy;

                    clsClientLink.strTitle = item.strTitle;
                    clsClientLink.strDescription = item.strDescription;
                    clsClientLink.strLink = item.strLink;
                    clsClientLink.iClientID = item.iClientID;
                    clsClientLink.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        clsClientLink.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);

                    lstClientLinks.Add(clsClientLink);
                }
            }

            return lstClientLinks;
        }

        public List<clsClientLinks> getAllClientLinksOnlyList()
        {
            List<clsClientLinks> lstClientLinks = new List<clsClientLinks>();
            var lstGetClientLinksList = db.tblClientLinks.Where(ClientLink => ClientLink.bIsDeleted == false).ToList();

            if (lstGetClientLinksList.Count > 0)
            {

                foreach (var item in lstGetClientLinksList)
                {
                    clsClientLinks clsClientLink = new clsClientLinks();

                    clsClientLink.iClientLinkID = item.iClientLinkID;
                    clsClientLink.dtAdded = item.dtAdded;
                    clsClientLink.iAddedBy = item.iAddedBy;
                    clsClientLink.dtEdited = item.dtEdited;
                    clsClientLink.iEditedBy = item.iEditedBy;

                    clsClientLink.strTitle = item.strTitle;
                    clsClientLink.strDescription = item.strDescription;
                    clsClientLink.strLink = item.strLink;
                    clsClientLink.iClientID = item.iClientID;
                    clsClientLink.bIsDeleted = item.bIsDeleted;

                    lstClientLinks.Add(clsClientLink);
                }
            }

            return lstClientLinks;
        }

        //Get
        public clsClientLinks getClientLinkByID(int iClientLinkID)
        {
            clsClientLinks clsClientLink = null;
            tblClientLinks tblClientLinks = db.tblClientLinks.FirstOrDefault(ClientLink => ClientLink.iClientLinkID == iClientLinkID && ClientLink.bIsDeleted == false);

            if (tblClientLinks != null)
            {
                ///Managers
                clsClientsManager clsClientsManager = new clsClientsManager();

                clsClientLink = new clsClientLinks();

                clsClientLink.iClientLinkID = tblClientLinks.iClientLinkID;
                clsClientLink.dtAdded = tblClientLinks.dtAdded;
                clsClientLink.iAddedBy = tblClientLinks.iAddedBy;
                clsClientLink.dtEdited = tblClientLinks.dtEdited;
                clsClientLink.iEditedBy = tblClientLinks.iEditedBy;

                clsClientLink.strTitle = tblClientLinks.strTitle;
                clsClientLink.strDescription = tblClientLinks.strDescription;
                clsClientLink.strLink = tblClientLinks.strLink;
                clsClientLink.iClientID = tblClientLinks.iClientID;
                clsClientLink.bIsDeleted = tblClientLinks.bIsDeleted;

                if (tblClientLinks.tblClients != null)
                    clsClientLink.clsClient = clsClientsManager.convertClientsTableToClass(tblClientLinks.tblClients);
            }

            return clsClientLink;
        }

        //Save
        public int saveClientLink(clsClientLinks clsClientLink)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblClientLinks tblClientLinks = new tblClientLinks();

            tblClientLinks.iClientLinkID = clsClientLink.iClientLinkID;

            tblClientLinks.strTitle = clsClientLink.strTitle;
            tblClientLinks.strDescription = clsClientLink.strDescription;
            tblClientLinks.strLink = clsClientLink.strLink;
            tblClientLinks.iClientID = clsClientLink.iClientID;
            tblClientLinks.bIsDeleted = clsClientLink.bIsDeleted;

            //Add
            if (tblClientLinks.iClientLinkID == 0)
            {
                tblClientLinks.dtAdded = DateTime.Now;
                //tblClientLinks.iAddedBy = clsCMSUser.iCMSUserID;
                tblClientLinks.iAddedBy = 1;
                tblClientLinks.dtEdited = DateTime.Now;
                //tblClientLinks.iEditedBy = clsCMSUser.iCMSUserID;
                tblClientLinks.iEditedBy = 1;

                db.tblClientLinks.Add(tblClientLinks);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblClientLinks.dtAdded = clsClientLink.dtAdded;
                tblClientLinks.iAddedBy = clsClientLink.iAddedBy;
                tblClientLinks.dtEdited = DateTime.Now;
                //tblClientLinks.iEditedBy = clsCMSUser.iCMSUserID;
                tblClientLinks.iEditedBy = 1;

                db.Set<tblClientLinks>().AddOrUpdate(tblClientLinks);
                db.SaveChanges();
            }
            //}
            return tblClientLinks.iClientLinkID;
        }

        //Remove
        public void removeClientLinkByID(int iClientLinkID)
        {
            tblClientLinks tblClientLink = db.tblClientLinks.Find(iClientLinkID);
            if (tblClientLink != null)
            {
                tblClientLink.bIsDeleted = true;
                db.Entry(tblClientLink).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientLinkExists(string strTitle)
        {
            bool bClientLinkExists = db.tblClientLinks.Any(ClientLink => ClientLink.strTitle.ToLower() == strTitle.ToLower() && ClientLink.bIsDeleted == false);
            return bClientLinkExists;
        }

        //Convert database table to class
        public clsClientLinks convertClientLinksTableToClass(tblClientLinks tblClientLink)
        {
            clsClientLinks clsClientLink = new clsClientLinks();

            clsClientLink.iClientLinkID = tblClientLink.iClientLinkID;
            clsClientLink.dtAdded = tblClientLink.dtAdded;
            clsClientLink.iAddedBy = tblClientLink.iAddedBy;
            clsClientLink.dtEdited = tblClientLink.dtEdited;
            clsClientLink.iEditedBy = tblClientLink.iEditedBy;

            clsClientLink.strTitle = tblClientLink.strTitle;
            clsClientLink.strDescription = tblClientLink.strDescription;
            clsClientLink.strLink = tblClientLink.strLink;
            clsClientLink.iClientID = tblClientLink.iClientID;
            clsClientLink.bIsDeleted = tblClientLink.bIsDeleted;

            return clsClientLink;
        }
    }
}
