﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientClientServicesLinkTableManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientClientServicesLinkTable> getAllClientClientServicesLinkTableList()
        {
            List<clsClientClientServicesLinkTable> lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
            var lstGetClientClientServicesLinkTableList = db.tblClientClientServicesLinkTable.Where(ClientClientServicesLinkTable => ClientClientServicesLinkTable.bIsDeleted == false).ToList();

            if (lstGetClientClientServicesLinkTableList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsClientServicesManager clsClientServicesManager = new clsClientServicesManager();

                foreach (var item in lstGetClientClientServicesLinkTableList)
                {
                    clsClientClientServicesLinkTable clsClientClientServicesLinkTable = new clsClientClientServicesLinkTable();

                    clsClientClientServicesLinkTable.iClientClientServicesLinkTableID = item.iClientClientServicesLinkTableID;
                    clsClientClientServicesLinkTable.iClientID = item.iClientID;
                    clsClientClientServicesLinkTable.iClientServiceID = item.iClientServiceID;
                    clsClientClientServicesLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        clsClientClientServicesLinkTable.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblClientServices != null)
                        clsClientClientServicesLinkTable.clsClientService = clsClientServicesManager.convertClientServicesTableToClass(item.tblClientServices);

                    lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                }
            }

            return lstClientClientServicesLinkTable;
        }

        public List<clsClientClientServicesLinkTable> getAllClientClientServicesLinkTableOnlyList()
        {
            List<clsClientClientServicesLinkTable> lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();
            var lstGetClientClientServicesLinkTableList = db.tblClientClientServicesLinkTable.Where(ClientClientServicesLinkTable => ClientClientServicesLinkTable.bIsDeleted == false).ToList();

            if (lstGetClientClientServicesLinkTableList.Count > 0)
            {
                foreach (var item in lstGetClientClientServicesLinkTableList)
                {
                    clsClientClientServicesLinkTable clsClientClientServicesLinkTable = new clsClientClientServicesLinkTable();

                    clsClientClientServicesLinkTable.iClientClientServicesLinkTableID = item.iClientClientServicesLinkTableID;
                    clsClientClientServicesLinkTable.iClientID = item.iClientID;
                    clsClientClientServicesLinkTable.iClientServiceID = item.iClientServiceID;
                    clsClientClientServicesLinkTable.bIsDeleted = item.bIsDeleted;

                    lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                }
            }

            return lstClientClientServicesLinkTable;
        }

        //Get
        public clsClientClientServicesLinkTable getClientClientServicesLinkTableByID(int iClientClientServicesLinkTableID)
        {
            clsClientClientServicesLinkTable clsClientClientServicesLinkTable = null;
            tblClientClientServicesLinkTable tblClientClientServicesLinkTable = db.tblClientClientServicesLinkTable.FirstOrDefault(ClientClientServicesLinkTable => ClientClientServicesLinkTable.iClientClientServicesLinkTableID == iClientClientServicesLinkTableID && ClientClientServicesLinkTable.bIsDeleted == false);

            if (tblClientClientServicesLinkTable != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsClientServicesManager clsClientServicesManager = new clsClientServicesManager();

                clsClientClientServicesLinkTable = new clsClientClientServicesLinkTable();

                clsClientClientServicesLinkTable.iClientClientServicesLinkTableID = tblClientClientServicesLinkTable.iClientClientServicesLinkTableID;
                clsClientClientServicesLinkTable.iClientID = tblClientClientServicesLinkTable.iClientID;
                clsClientClientServicesLinkTable.iClientServiceID = tblClientClientServicesLinkTable.iClientServiceID;
                clsClientClientServicesLinkTable.bIsDeleted = tblClientClientServicesLinkTable.bIsDeleted;

                if (tblClientClientServicesLinkTable.tblClients != null)
                    clsClientClientServicesLinkTable.clsClient = clsClientsManager.convertClientsTableToClass(tblClientClientServicesLinkTable.tblClients);
                if (tblClientClientServicesLinkTable.tblClientServices != null)
                    clsClientClientServicesLinkTable.clsClientService = clsClientServicesManager.convertClientServicesTableToClass(tblClientClientServicesLinkTable.tblClientServices);
            }

            return clsClientClientServicesLinkTable;
        }

        //Save
        public int saveClientClientServicesLinkTable(clsClientClientServicesLinkTable clsClientClientServicesLinkTable)
        {
            tblClientClientServicesLinkTable tblClientClientServicesLinkTable = new tblClientClientServicesLinkTable();

            tblClientClientServicesLinkTable.iClientClientServicesLinkTableID = clsClientClientServicesLinkTable.iClientClientServicesLinkTableID;
            tblClientClientServicesLinkTable.iClientID = clsClientClientServicesLinkTable.iClientID;
            tblClientClientServicesLinkTable.iClientServiceID = clsClientClientServicesLinkTable.iClientServiceID;
            tblClientClientServicesLinkTable.bIsDeleted = clsClientClientServicesLinkTable.bIsDeleted;

            //Add
            if (tblClientClientServicesLinkTable.iClientClientServicesLinkTableID == 0)
            {
                db.tblClientClientServicesLinkTable.Add(tblClientClientServicesLinkTable);
                db.SaveChanges();
            }
            //Update
            else
            {
                db.Set<tblClientClientServicesLinkTable>().AddOrUpdate(tblClientClientServicesLinkTable);
                db.SaveChanges();
            }

            return tblClientClientServicesLinkTable.iClientClientServicesLinkTableID;
        }

        //Remove
        public void removeClientClientServicesLinkTableByID(int iClientClientServicesLinkTableID)
        {
            tblClientClientServicesLinkTable tblClientClientServicesLinkTable = db.tblClientClientServicesLinkTable.Find(iClientClientServicesLinkTableID);
            if (tblClientClientServicesLinkTable != null)
            {
                tblClientClientServicesLinkTable.bIsDeleted = true;
                db.Entry(tblClientClientServicesLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientClientServicesLinkTableExists(int iClientClientServicesLinkTableID)
        {
            bool bClientClientServicesLinkTableExists = db.tblClientClientServicesLinkTable.Any(ClientClientServicesLinkTable => ClientClientServicesLinkTable.iClientClientServicesLinkTableID == iClientClientServicesLinkTableID && ClientClientServicesLinkTable.bIsDeleted == false);
            return bClientClientServicesLinkTableExists;
        }

        //Convert database table to class
        public clsClientClientServicesLinkTable convertClientClientServicesLinkTableTableToClass(tblClientClientServicesLinkTable tblClientClientServicesLinkTable)
        {
            clsClientClientServicesLinkTable clsClientClientServicesLinkTable = new clsClientClientServicesLinkTable();

            clsClientClientServicesLinkTable.iClientClientServicesLinkTableID = tblClientClientServicesLinkTable.iClientClientServicesLinkTableID;
            clsClientClientServicesLinkTable.iClientID = tblClientClientServicesLinkTable.iClientID;
            clsClientClientServicesLinkTable.iClientServiceID = tblClientClientServicesLinkTable.iClientServiceID;
            clsClientClientServicesLinkTable.bIsDeleted = tblClientClientServicesLinkTable.bIsDeleted;

            return clsClientClientServicesLinkTable;
        }
    }
}
