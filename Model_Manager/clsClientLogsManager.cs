﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientLogsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientLogs> getAllClientLogsList()
        {
            List<clsClientLogs> lstClientLogs = new List<clsClientLogs>();
            var lstGetClientLogsList = db.tblClientLogs.Where(ClientLog => ClientLog.bIsDeleted == false).ToList();

            if (lstGetClientLogsList.Count > 0)
            {
                foreach (var item in lstGetClientLogsList)
                {
                    clsClientLogs clsClientLog = new clsClientLogs();

                    clsClientLog.iClientLogID = item.iClientLogID;
                    clsClientLog.dtAdded = item.dtAdded;
                    clsClientLog.iAddedBy = item.iAddedBy;
                    clsClientLog.dtEdited = item.dtEdited;
                    clsClientLog.iEditedBy = item.iEditedBy;
                    clsClientLog.dtChanged = item.dtChanged;

                    clsClientLog.strCompanyName = item.strCompanyName;
                    clsClientLog.strCompanyLandline = item.strCompanyLandline;
                    clsClientLog.strUsername = item.strUsername;
                    clsClientLog.strEmail = item.strEmail;

                    clsClientLog.strRegisteredName = item.strRegisteredName;
                    clsClientLog.strRegistrationNumber = item.strRegistrationNumber;
                    clsClientLog.strVATNumber = item.strVATNumber;
                    clsClientLog.strPhysicalAddress = item.strPhysicalAddress;
                    clsClientLog.strPostalAddress = item.strPostalAddress;

                    clsClientLog.dblBillingRate = item.dblBillingRate;
                    clsClientLog.strURL = item.strURL;
                    clsClientLog.strPathToImages = item.strPathToImages;
                    clsClientLog.strMasterImage = item.strMasterImage;
                    clsClientLog.strPassword = item.strPassword;

                    clsClientLog.strVerificationKey = item.strVerificationKey;
                    clsClientLog.bIsVerified = item.bIsVerified;
                    clsClientLog.bIsProfileCompleted = item.bIsProfileCompleted;
                    clsClientLog.bIsDeleted = item.bIsDeleted;

                    lstClientLogs.Add(clsClientLog);
                }
            }

            return lstClientLogs;
        }

        public List<clsClientLogs> getAllClientLogsOnlyList()
        {
            List<clsClientLogs> lstClientLogs = new List<clsClientLogs>();
            var lstGetClientLogsList = db.tblClientLogs.Where(ClientLog => ClientLog.bIsDeleted == false).ToList();

            if (lstGetClientLogsList.Count > 0)
            {

                foreach (var item in lstGetClientLogsList)
                {
                    clsClientLogs clsClientLog = new clsClientLogs();

                    clsClientLog.iClientLogID = item.iClientLogID;
                    clsClientLog.dtAdded = item.dtAdded;
                    clsClientLog.iAddedBy = item.iAddedBy;
                    clsClientLog.dtEdited = item.dtEdited;
                    clsClientLog.iEditedBy = item.iEditedBy;
                    clsClientLog.dtChanged = item.dtChanged;

                    clsClientLog.strCompanyName = item.strCompanyName;
                    clsClientLog.strCompanyLandline = item.strCompanyLandline;
                    clsClientLog.strUsername = item.strUsername;
                    clsClientLog.strEmail = item.strEmail;

                    clsClientLog.strRegisteredName = item.strRegisteredName;
                    clsClientLog.strRegistrationNumber = item.strRegistrationNumber;
                    clsClientLog.strVATNumber = item.strVATNumber;
                    clsClientLog.strPhysicalAddress = item.strPhysicalAddress;
                    clsClientLog.strPostalAddress = item.strPostalAddress;

                    clsClientLog.dblBillingRate = item.dblBillingRate;
                    clsClientLog.strURL = item.strURL;
                    clsClientLog.strPathToImages = item.strPathToImages;
                    clsClientLog.strMasterImage = item.strMasterImage;
                    clsClientLog.strPassword = item.strPassword;

                    clsClientLog.strVerificationKey = item.strVerificationKey;
                    clsClientLog.bIsVerified = item.bIsVerified;
                    clsClientLog.bIsProfileCompleted = item.bIsProfileCompleted;
                    clsClientLog.bIsDeleted = item.bIsDeleted;

                    lstClientLogs.Add(clsClientLog);
                }
            }

            return lstClientLogs;
        }

        //Get
        public clsClientLogs getClientLogByID(int iClientLogID)
        {
            clsClientLogs clsClientLog = null;
            tblClientLogs tblClientLogs = db.tblClientLogs.FirstOrDefault(ClientLog => ClientLog.iClientLogID == iClientLogID && ClientLog.bIsDeleted == false);

            if (tblClientLogs != null)
            {
                clsClientLog = new clsClientLogs();

                clsClientLog.iClientLogID = tblClientLogs.iClientLogID;
                clsClientLog.dtAdded = tblClientLogs.dtAdded;
                clsClientLog.iAddedBy = tblClientLogs.iAddedBy;
                clsClientLog.dtEdited = tblClientLogs.dtEdited;
                clsClientLog.iEditedBy = tblClientLogs.iEditedBy;
                clsClientLog.dtChanged = tblClientLogs.dtChanged;

                clsClientLog.strCompanyName = tblClientLogs.strCompanyName;
                clsClientLog.strCompanyLandline = tblClientLogs.strCompanyLandline;
                clsClientLog.strUsername = tblClientLogs.strUsername;
                clsClientLog.strEmail = tblClientLogs.strEmail;

                clsClientLog.strRegisteredName = tblClientLogs.strRegisteredName;
                clsClientLog.strRegistrationNumber = tblClientLogs.strRegistrationNumber;
                clsClientLog.strVATNumber = tblClientLogs.strVATNumber;
                clsClientLog.strPhysicalAddress = tblClientLogs.strPhysicalAddress;
                clsClientLog.strPostalAddress = tblClientLogs.strPostalAddress;

                clsClientLog.dblBillingRate = tblClientLogs.dblBillingRate;
                clsClientLog.strURL = tblClientLogs.strURL;
                clsClientLog.strPathToImages = tblClientLogs.strPathToImages;
                clsClientLog.strMasterImage = tblClientLogs.strMasterImage;
                clsClientLog.strPassword = tblClientLogs.strPassword;

                clsClientLog.strVerificationKey = tblClientLogs.strVerificationKey;
                clsClientLog.bIsVerified = tblClientLogs.bIsVerified;
                clsClientLog.bIsProfileCompleted = tblClientLogs.bIsProfileCompleted;
                clsClientLog.bIsDeleted = tblClientLogs.bIsDeleted;
            }

            return clsClientLog;
        }

        public clsClientLogs getClientLogByEmail(string strClientLogEmail)
        {
            clsClientLogs clsClientLog = null;
            tblClientLogs tblClientLogs = db.tblClientLogs.FirstOrDefault(ClientLog => ClientLog.strEmail == strClientLogEmail.ToLower() && ClientLog.bIsDeleted == false);

            if (tblClientLogs != null)
            {
                clsClientLog = new clsClientLogs();

                clsClientLog.iClientLogID = tblClientLogs.iClientLogID;
                clsClientLog.dtAdded = tblClientLogs.dtAdded;
                clsClientLog.iAddedBy = tblClientLogs.iAddedBy;
                clsClientLog.dtEdited = tblClientLogs.dtEdited;
                clsClientLog.iEditedBy = tblClientLogs.iEditedBy;
                clsClientLog.dtChanged = tblClientLogs.dtChanged;

                clsClientLog.strCompanyName = tblClientLogs.strCompanyName;
                clsClientLog.strCompanyLandline = tblClientLogs.strCompanyLandline;
                clsClientLog.strUsername = tblClientLogs.strUsername;
                clsClientLog.strEmail = tblClientLogs.strEmail;

                clsClientLog.strRegisteredName = tblClientLogs.strRegisteredName;
                clsClientLog.strRegistrationNumber = tblClientLogs.strRegistrationNumber;
                clsClientLog.strVATNumber = tblClientLogs.strVATNumber;
                clsClientLog.strPhysicalAddress = tblClientLogs.strPhysicalAddress;
                clsClientLog.strPostalAddress = tblClientLogs.strPostalAddress;

                clsClientLog.dblBillingRate = tblClientLogs.dblBillingRate;
                clsClientLog.strURL = tblClientLogs.strURL;
                clsClientLog.strPathToImages = tblClientLogs.strPathToImages;
                clsClientLog.strMasterImage = tblClientLogs.strMasterImage;
                clsClientLog.strPassword = tblClientLogs.strPassword;

                clsClientLog.strVerificationKey = tblClientLogs.strVerificationKey;
                clsClientLog.bIsVerified = tblClientLogs.bIsVerified;
                clsClientLog.bIsProfileCompleted = tblClientLogs.bIsProfileCompleted;
                clsClientLog.bIsDeleted = tblClientLogs.bIsDeleted;
            }

            return clsClientLog;
        }

        //Save
        public int saveClientLog(clsClientLogs clsClientLog)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
                //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblClientLogs tblClientLogs = new tblClientLogs();

                tblClientLogs.iClientLogID = clsClientLog.iClientLogID;

                tblClientLogs.strCompanyName = clsClientLog.strCompanyName;
                tblClientLogs.strCompanyLandline = clsClientLog.strCompanyLandline;
                tblClientLogs.strUsername = clsClientLog.strUsername;
                tblClientLogs.strEmail = clsClientLog.strEmail.ToLower();

                tblClientLogs.strRegisteredName = clsClientLog.strRegisteredName;
                tblClientLogs.strRegistrationNumber = clsClientLog.strRegistrationNumber;
                tblClientLogs.strVATNumber = clsClientLog.strVATNumber;
                tblClientLogs.strPhysicalAddress = clsClientLog.strPhysicalAddress;
                tblClientLogs.strPostalAddress = clsClientLog.strPostalAddress;

                tblClientLogs.dblBillingRate = clsClientLog.dblBillingRate;
                tblClientLogs.strURL = clsClientLog.strURL;
                tblClientLogs.strPathToImages = clsClientLog.strPathToImages;
                tblClientLogs.strMasterImage = clsClientLog.strMasterImage;
                tblClientLogs.strPassword = clsClientLog.strPassword;

                tblClientLogs.strVerificationKey = clsClientLog.strVerificationKey;
                tblClientLogs.bIsVerified = clsClientLog.bIsVerified;
                tblClientLogs.bIsProfileCompleted = clsClientLog.bIsProfileCompleted;
                tblClientLogs.bIsDeleted = clsClientLog.bIsDeleted;

            //Add
            if (tblClientLogs.iClientLogID == 0)
                {
                    tblClientLogs.dtAdded = DateTime.Now;
                    //tblClientLogs.iAddedBy = clsCMSUser.iCMSUserID;
                    tblClientLogs.iAddedBy = 1;
                    tblClientLogs.dtEdited = DateTime.Now;
                    //tblClientLogs.iEditedBy = clsCMSUser.iCMSUserID;
                    tblClientLogs.iEditedBy = 1;
                    tblClientLogs.dtChanged = DateTime.Now;

                    db.tblClientLogs.Add(tblClientLogs);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblClientLogs.dtAdded = clsClientLog.dtAdded;
                    tblClientLogs.iAddedBy = clsClientLog.iAddedBy;
                    tblClientLogs.dtEdited = DateTime.Now;
                    //tblClientLogs.iEditedBy = clsCMSUser.iCMSUserID;
                    tblClientLogs.iEditedBy = 1;
                    tblClientLogs.dtChanged = DateTime.Now;

                    db.Set<tblClientLogs>().AddOrUpdate(tblClientLogs);
                    db.SaveChanges();
                }
            //}
            return tblClientLogs.iClientLogID;
        }

        //Remove
        public void removeClientLogByID(int iClientLogID)
        {
            tblClientLogs tblClientLog = db.tblClientLogs.Find(iClientLogID);
            if (tblClientLog != null)
            {
                tblClientLog.bIsDeleted = true;
                db.Entry(tblClientLog).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientLogExists(int iClientLogID)
        {
            bool bClientLogExists = db.tblClientLogs.Any(ClientLog => ClientLog.iClientLogID == iClientLogID && ClientLog.bIsDeleted == false);
            return bClientLogExists;
        }

        //Convert database table to class
        public clsClientLogs convertClientLogsTableToClass(tblClientLogs tblClientLog)
        {
            clsClientLogs clsClientLog = new clsClientLogs();

            clsClientLog.iClientLogID = tblClientLog.iClientLogID;
            clsClientLog.dtAdded = tblClientLog.dtAdded;
            clsClientLog.iAddedBy = tblClientLog.iAddedBy;
            clsClientLog.dtEdited = tblClientLog.dtEdited;
            clsClientLog.iEditedBy = tblClientLog.iEditedBy;
            clsClientLog.dtChanged = tblClientLog.dtChanged;

            clsClientLog.strCompanyName = tblClientLog.strCompanyName;
            clsClientLog.strCompanyLandline = tblClientLog.strCompanyLandline;
            clsClientLog.strUsername = tblClientLog.strUsername;
            clsClientLog.strEmail = tblClientLog.strEmail;

            clsClientLog.strRegisteredName = tblClientLog.strRegisteredName;
            clsClientLog.strRegistrationNumber = tblClientLog.strRegistrationNumber;
            clsClientLog.strVATNumber = tblClientLog.strVATNumber;
            clsClientLog.strPhysicalAddress = tblClientLog.strPhysicalAddress;
            clsClientLog.strPostalAddress = tblClientLog.strPostalAddress;

            clsClientLog.dblBillingRate = tblClientLog.dblBillingRate;
            clsClientLog.strURL = tblClientLog.strURL;
            clsClientLog.strPathToImages = tblClientLog.strPathToImages;
            clsClientLog.strMasterImage = tblClientLog.strMasterImage;
            clsClientLog.strPassword = tblClientLog.strPassword;

            clsClientLog.strVerificationKey = tblClientLog.strVerificationKey;
            clsClientLog.bIsVerified = tblClientLog.bIsVerified;
            clsClientLog.bIsProfileCompleted = tblClientLog.bIsProfileCompleted;
            clsClientLog.bIsDeleted = tblClientLog.bIsDeleted;

            return clsClientLog;
        }
    }
}
