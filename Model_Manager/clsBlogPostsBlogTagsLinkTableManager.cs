﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsBlogPostsBlogTagsLinkTableManager
    {
        //SoftservePortalNewContext db = new SoftservePortalNewContext();

        ////Get All
        //public List<clsBlogPostsBlogTagsLinkTable> getAllBlogPostsBlogTagsLinkTableList()
        //{
        //    List<clsBlogPostsBlogTagsLinkTable> lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();
        //    var lstGetBlogPostsBlogTagsLinkTableList = db.tblBlogPostsBlogTagsLinkTable.Where(BlogPostsBlogTagsLinkTable => BlogPostsBlogTagsLinkTable.bIsDeleted == false).ToList();

        //    if (lstGetBlogPostsBlogTagsLinkTableList.Count > 0)
        //    {
        //        //Managers
        //        clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
        //        clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();

        //        foreach (var item in lstGetBlogPostsBlogTagsLinkTableList)
        //        {
        //            clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = new clsBlogPostsBlogTagsLinkTable();

        //            clsBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID = item.iBlogPostsBlogTagsLinkTableID;
        //            clsBlogPostsBlogTagsLinkTable.iBlogPostID = item.iBlogPostID;
        //            clsBlogPostsBlogTagsLinkTable.iBlogTagID = item.iBlogTagID;
        //            clsBlogPostsBlogTagsLinkTable.bIsDeleted = item.bIsDeleted;

        //            if (item.tblBlogPosts != null)
        //                clsBlogPostsBlogTagsLinkTable.clsBlogPost = clsBlogPostsManager.convertBlogPostsTableToClass(item.tblBlogPosts);
        //            if (item.tblBlogTags != null)
        //                clsBlogPostsBlogTagsLinkTable.clsBlogTag = clsBlogTagsManager.convertBlogTagsTableToClass(item.tblBlogTags);

        //            lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
        //        }
        //    }

        //    return lstBlogPostsBlogTagsLinkTable;
        //}

        //public List<clsBlogPostsBlogTagsLinkTable> getAllBlogPostsBlogTagsLinkTableOnlyList()
        //{
        //    List<clsBlogPostsBlogTagsLinkTable> lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();
        //    var lstGetBlogPostsBlogTagsLinkTableList = db.tblBlogPostsBlogTagsLinkTable.Where(BlogPostsBlogTagsLinkTable => BlogPostsBlogTagsLinkTable.bIsDeleted == false).ToList();

        //    if (lstGetBlogPostsBlogTagsLinkTableList.Count > 0)
        //    {

        //        foreach (var item in lstGetBlogPostsBlogTagsLinkTableList)
        //        {
        //            clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = new clsBlogPostsBlogTagsLinkTable();

        //            clsBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID = item.iBlogPostsBlogTagsLinkTableID;
        //            clsBlogPostsBlogTagsLinkTable.iBlogPostID = item.iBlogPostID;
        //            clsBlogPostsBlogTagsLinkTable.iBlogTagID = item.iBlogTagID;
        //            clsBlogPostsBlogTagsLinkTable.bIsDeleted = item.bIsDeleted;

        //            lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
        //        }
        //    }

        //    return lstBlogPostsBlogTagsLinkTable;
        //}

        ////Get
        //public clsBlogPostsBlogTagsLinkTable getBlogPostsBlogTagsLinkTableByID(int iBlogPostsBlogTagsLinkTableID)
        //{
        //    clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = null;
        //    tblBlogPostsBlogTagsLinkTable tblBlogPostsBlogTagsLinkTable = db.tblBlogPostsBlogTagsLinkTable.FirstOrDefault(BlogPostsBlogTagsLinkTable => BlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID == iBlogPostsBlogTagsLinkTableID && BlogPostsBlogTagsLinkTable.bIsDeleted == false);

        //    if (tblBlogPostsBlogTagsLinkTable != null)
        //    {
        //        //Managers
        //        clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
        //        clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();

        //        clsBlogPostsBlogTagsLinkTable = new clsBlogPostsBlogTagsLinkTable();

        //        clsBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID = tblBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID;
        //        clsBlogPostsBlogTagsLinkTable.iBlogPostID = tblBlogPostsBlogTagsLinkTable.iBlogPostID;
        //        clsBlogPostsBlogTagsLinkTable.iBlogTagID = tblBlogPostsBlogTagsLinkTable.iBlogTagID;
        //        clsBlogPostsBlogTagsLinkTable.bIsDeleted = tblBlogPostsBlogTagsLinkTable.bIsDeleted;

        //        if (tblBlogPostsBlogTagsLinkTable.tblBlogPosts != null)
        //            clsBlogPostsBlogTagsLinkTable.clsBlogPost = clsBlogPostsManager.convertBlogPostsTableToClass(tblBlogPostsBlogTagsLinkTable.tblBlogPosts);
        //        if (tblBlogPostsBlogTagsLinkTable.tblBlogTags != null)
        //            clsBlogPostsBlogTagsLinkTable.clsBlogTag = clsBlogTagsManager.convertBlogTagsTableToClass(tblBlogPostsBlogTagsLinkTable.tblBlogTags);
        //    }

        //    return clsBlogPostsBlogTagsLinkTable;
        //}

        ////Save
        //public int saveBlogPostsBlogTagsLinkTable(clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable)
        //{
        //    //if (HttpContext.Current.Session["clsCMSUser"] != null)
        //    //{
        //    //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
        //    tblBlogPostsBlogTagsLinkTable tblBlogPostsBlogTagsLinkTable = new tblBlogPostsBlogTagsLinkTable();

        //    tblBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID = clsBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID;

        //    tblBlogPostsBlogTagsLinkTable.iBlogPostID = clsBlogPostsBlogTagsLinkTable.iBlogPostID;
        //    tblBlogPostsBlogTagsLinkTable.iBlogTagID = clsBlogPostsBlogTagsLinkTable.iBlogTagID;
        //    tblBlogPostsBlogTagsLinkTable.bIsDeleted = clsBlogPostsBlogTagsLinkTable.bIsDeleted;

        //    //Add
        //    if (tblBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID == 0)
        //    {
        //        db.tblBlogPostsBlogTagsLinkTable.Add(tblBlogPostsBlogTagsLinkTable);
        //        db.SaveChanges();
        //    }
        //    //Update
        //    else
        //    {
        //        db.Set<tblBlogPostsBlogTagsLinkTable>().AddOrUpdate(tblBlogPostsBlogTagsLinkTable);
        //        db.SaveChanges();
        //    }
        //    //}
        //    return tblBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID;
        //}

        ////Remove
        //public void removeBlogPostsBlogTagsLinkTableByID(int iBlogPostsBlogTagsLinkTableID)
        //{
        //    tblBlogPostsBlogTagsLinkTable tblBlogPostsBlogTagsLinkTable = db.tblBlogPostsBlogTagsLinkTable.Find(iBlogPostsBlogTagsLinkTableID);
        //    if (tblBlogPostsBlogTagsLinkTable != null)
        //    {
        //        tblBlogPostsBlogTagsLinkTable.bIsDeleted = true;
        //        db.Entry(tblBlogPostsBlogTagsLinkTable).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }
        //}

        ////Check
        //public bool checkIfBlogPostsBlogTagsLinkTableExists(int iBlogPostsBlogTagsLinkTableID)
        //{
        //    bool bBlogPostsBlogTagsLinkTableExists = db.tblBlogPostsBlogTagsLinkTable.Any(BlogPostsBlogTagsLinkTable => BlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID == iBlogPostsBlogTagsLinkTableID && BlogPostsBlogTagsLinkTable.bIsDeleted == false);
        //    return bBlogPostsBlogTagsLinkTableExists;
        //}

        ////Convert database table to class
        //public clsBlogPostsBlogTagsLinkTable convertBlogPostsBlogTagsLinkTableTableToClass(tblBlogPostsBlogTagsLinkTable tblBlogPostsBlogTagsLinkTable)
        //{
        //    clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = new clsBlogPostsBlogTagsLinkTable();

        //    clsBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID = tblBlogPostsBlogTagsLinkTable.iBlogPostsBlogTagsLinkTableID;
        //    clsBlogPostsBlogTagsLinkTable.iBlogPostID = tblBlogPostsBlogTagsLinkTable.iBlogPostID;
        //    clsBlogPostsBlogTagsLinkTable.iBlogTagID = tblBlogPostsBlogTagsLinkTable.iBlogTagID;
        //    clsBlogPostsBlogTagsLinkTable.bIsDeleted = tblBlogPostsBlogTagsLinkTable.bIsDeleted;

        //    return clsBlogPostsBlogTagsLinkTable;
        //}
    }
}
