﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsDocumentsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsDocuments> getAllDocumentsList()
        {
            List<clsDocuments> lstDocuments = new List<clsDocuments>();
            var lstGetDocumentsList = db.tblDocuments.Where(Document => Document.bIsDeleted == false).ToList();

            if (lstGetDocumentsList.Count > 0)
            {
                //Managers

                foreach (var item in lstGetDocumentsList)
                {
                    clsDocuments clsDocument = new clsDocuments();

                    clsDocument.iDocumentID = item.iDocumentID;
                    clsDocument.dtAdded = item.dtAdded;
                    clsDocument.iAddedBy = item.iAddedBy;
                    clsDocument.dtEdited = item.dtEdited;
                    clsDocument.iEditedBy = item.iEditedBy;

                    clsDocument.strTitle = item.strTitle;
                    clsDocument.strDescription = item.strDescription;
                    clsDocument.strPathToImages = item.strPathToImages;
                    clsDocument.strMasterImage = item.strMasterImage;
                    clsDocument.strPathToDocuments = item.strPathToDocuments;

                    clsDocument.strMasterDocument = item.strMasterDocument;
                    clsDocument.bIsMultiple = item.bIsMultiple;
                    clsDocument.bIsDeleted = item.bIsDeleted;

                    lstDocuments.Add(clsDocument);
                }
            }

            return lstDocuments;
        }

        public List<clsDocuments> getAllDocumentsOnlyList()
        {
            List<clsDocuments> lstDocuments = new List<clsDocuments>();
            var lstGetDocumentsList = db.tblDocuments.Where(Document => Document.bIsDeleted == false).ToList();

            if (lstGetDocumentsList.Count > 0)
            {

                foreach (var item in lstGetDocumentsList)
                {
                    clsDocuments clsDocument = new clsDocuments();

                    clsDocument.iDocumentID = item.iDocumentID;
                    clsDocument.dtAdded = item.dtAdded;
                    clsDocument.iAddedBy = item.iAddedBy;
                    clsDocument.dtEdited = item.dtEdited;
                    clsDocument.iEditedBy = item.iEditedBy;

                    clsDocument.strTitle = item.strTitle;
                    clsDocument.strDescription = item.strDescription;
                    clsDocument.strPathToImages = item.strPathToImages;
                    clsDocument.strMasterImage = item.strMasterImage;
                    clsDocument.strPathToDocuments = item.strPathToDocuments;

                    clsDocument.strMasterDocument = item.strMasterDocument;
                    clsDocument.bIsMultiple = item.bIsMultiple;
                    clsDocument.bIsDeleted = item.bIsDeleted;

                    lstDocuments.Add(clsDocument);
                }
            }

            return lstDocuments;
        }

        //Get
        public clsDocuments getDocumentByID(int iDocumentID)
        {
            clsDocuments clsDocument = null;
            tblDocuments tblDocuments = db.tblDocuments.FirstOrDefault(Document => Document.iDocumentID == iDocumentID && Document.bIsDeleted == false);

            if (tblDocuments != null)
            {
                //Managers

                clsDocument = new clsDocuments();

                clsDocument.iDocumentID = tblDocuments.iDocumentID;
                clsDocument.dtAdded = tblDocuments.dtAdded;
                clsDocument.iAddedBy = tblDocuments.iAddedBy;
                clsDocument.dtEdited = tblDocuments.dtEdited;
                clsDocument.iEditedBy = tblDocuments.iEditedBy;

                clsDocument.strTitle = tblDocuments.strTitle;
                clsDocument.strDescription = tblDocuments.strDescription;
                clsDocument.strPathToImages = tblDocuments.strPathToImages;
                clsDocument.strMasterImage = tblDocuments.strMasterImage;
                clsDocument.strPathToDocuments = tblDocuments.strPathToDocuments;

                clsDocument.strMasterDocument = tblDocuments.strMasterDocument;
                clsDocument.bIsMultiple = tblDocuments.bIsMultiple;
                clsDocument.bIsDeleted = tblDocuments.bIsDeleted;
            }

            return clsDocument;
        }

        //Save
        public int saveDocument(clsDocuments clsDocument)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblDocuments tblDocuments = new tblDocuments();

            tblDocuments.iDocumentID = clsDocument.iDocumentID;

            tblDocuments.strTitle = clsDocument.strTitle;
            tblDocuments.strDescription = clsDocument.strDescription;
            tblDocuments.strPathToImages = clsDocument.strPathToImages;
            tblDocuments.strMasterImage = clsDocument.strMasterImage;
            tblDocuments.strPathToDocuments = clsDocument.strPathToDocuments;

            tblDocuments.strMasterDocument = clsDocument.strMasterDocument;
            tblDocuments.bIsMultiple = clsDocument.bIsMultiple;
            tblDocuments.bIsDeleted = clsDocument.bIsDeleted;

            //Add
            if (tblDocuments.iDocumentID == 0)
            {
                tblDocuments.dtAdded = DateTime.Now;
                //tblDocuments.iAddedBy = clsCMSUser.iCMSUserID;
                tblDocuments.iAddedBy = 1;
                tblDocuments.dtEdited = DateTime.Now;
                //tblDocuments.iEditedBy = clsCMSUser.iCMSUserID;
                tblDocuments.iEditedBy = 1;

                db.tblDocuments.Add(tblDocuments);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblDocuments.dtAdded = clsDocument.dtAdded;
                tblDocuments.iAddedBy = clsDocument.iAddedBy;
                tblDocuments.dtEdited = DateTime.Now;
                //tblDocuments.iEditedBy = clsCMSUser.iCMSUserID;
                tblDocuments.iEditedBy = 1;

                db.Set<tblDocuments>().AddOrUpdate(tblDocuments);
                db.SaveChanges();
            }
            //}
            return tblDocuments.iDocumentID;
        }

        //Remove
        public void removeDocumentByID(int iDocumentID)
        {
            tblDocuments tblDocument = db.tblDocuments.Find(iDocumentID);
            if (tblDocument != null)
            {
                tblDocument.bIsDeleted = true;
                db.Entry(tblDocument).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfDocumentExists(string strTitle)
        {
            bool bDocumentExists = db.tblDocuments.Any(Document => Document.strTitle.ToLower() == strTitle.ToLower() && Document.bIsDeleted == false);
            return bDocumentExists;
        }

        //Convert database table to class
        public clsDocuments convertDocumentsTableToClass(tblDocuments tblDocument)
        {
            clsDocuments clsDocument = new clsDocuments();

            clsDocument.iDocumentID = tblDocument.iDocumentID;
            clsDocument.dtAdded = tblDocument.dtAdded;
            clsDocument.iAddedBy = tblDocument.iAddedBy;
            clsDocument.dtEdited = tblDocument.dtEdited;
            clsDocument.iEditedBy = tblDocument.iEditedBy;

            clsDocument.strTitle = tblDocument.strTitle;
            clsDocument.strDescription = tblDocument.strDescription;
            clsDocument.strPathToImages = tblDocument.strPathToImages;
            clsDocument.strMasterImage = tblDocument.strMasterImage;
            clsDocument.strPathToDocuments = tblDocument.strPathToDocuments;

            clsDocument.strMasterDocument = tblDocument.strMasterDocument;
            clsDocument.bIsMultiple = tblDocument.bIsMultiple;
            clsDocument.bIsDeleted = tblDocument.bIsDeleted;

            return clsDocument;
        }
    }
}
