﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientClientContactDetailsLinkTableManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientClientContactDetailsLinkTable> getAllClientClientContactDetailsLinkTableList()
        {
            List<clsClientClientContactDetailsLinkTable> lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
            var lstGetClientClientContactDetailsLinkTableList = db.tblClientClientContactDetailsLinkTable.Where(ClientClientContactDetailsLinkTable => ClientClientContactDetailsLinkTable.bIsDeleted == false).ToList();

            if (lstGetClientClientContactDetailsLinkTableList.Count > 0)
            {
                //Managers
                clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
                clsClientsManager clsClientsManager = new clsClientsManager();

                foreach (var item in lstGetClientClientContactDetailsLinkTableList)
                {
                    clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();

                    clsClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID = item.iClientClientContactDetailsLinkTableID;
                    clsClientClientContactDetailsLinkTable.iClientID = item.iClientID;
                    clsClientClientContactDetailsLinkTable.iClientContactDetailID = item.iClientContactDetailID;
                    clsClientClientContactDetailsLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblClientContactDetails != null)
                        clsClientClientContactDetailsLinkTable.clsClientContactDetail = clsClientContactDetailsManager.convertClientContactDetailsTableToClass(item.tblClientContactDetails);
                    if (item.tblClients != null)
                        clsClientClientContactDetailsLinkTable.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);

                    lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                }
            }

            return lstClientClientContactDetailsLinkTable;
        }

        public List<clsClientClientContactDetailsLinkTable> getAllClientClientContactDetailsLinkTableOnlyList()
        {
            List<clsClientClientContactDetailsLinkTable> lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();
            var lstGetClientClientContactDetailsLinkTableList = db.tblClientClientContactDetailsLinkTable.Where(ClientClientContactDetailsLinkTable => ClientClientContactDetailsLinkTable.bIsDeleted == false).ToList();

            if (lstGetClientClientContactDetailsLinkTableList.Count > 0)
            {
                foreach (var item in lstGetClientClientContactDetailsLinkTableList)
                {
                    clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();

                    clsClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID = item.iClientClientContactDetailsLinkTableID;
                    clsClientClientContactDetailsLinkTable.iClientID = item.iClientID;
                    clsClientClientContactDetailsLinkTable.iClientContactDetailID = item.iClientContactDetailID;
                    clsClientClientContactDetailsLinkTable.bIsDeleted = item.bIsDeleted;

                    lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                }
            }

            return lstClientClientContactDetailsLinkTable;
        }

        //Get
        public clsClientClientContactDetailsLinkTable getClientClientContactDetailsLinkTableByID(int iClientClientContactDetailsLinkTableID)
        {
            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = null;
            tblClientClientContactDetailsLinkTable tblClientClientContactDetailsLinkTable = db.tblClientClientContactDetailsLinkTable.FirstOrDefault(ClientClientContactDetailsLinkTable => ClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID == iClientClientContactDetailsLinkTableID && ClientClientContactDetailsLinkTable.bIsDeleted == false);

            if (tblClientClientContactDetailsLinkTable != null)
            {
                //Managers
                clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
                clsClientsManager clsClientsManager = new clsClientsManager();

                clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();

                clsClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID = tblClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID;
                clsClientClientContactDetailsLinkTable.iClientID = tblClientClientContactDetailsLinkTable.iClientID;
                clsClientClientContactDetailsLinkTable.iClientContactDetailID = tblClientClientContactDetailsLinkTable.iClientContactDetailID;
                clsClientClientContactDetailsLinkTable.bIsDeleted = tblClientClientContactDetailsLinkTable.bIsDeleted;

                if (tblClientClientContactDetailsLinkTable.tblClientContactDetails != null)
                    clsClientClientContactDetailsLinkTable.clsClientContactDetail = clsClientContactDetailsManager.convertClientContactDetailsTableToClass(tblClientClientContactDetailsLinkTable.tblClientContactDetails);
                if (tblClientClientContactDetailsLinkTable.tblClients != null)
                    clsClientClientContactDetailsLinkTable.clsClient = clsClientsManager.convertClientsTableToClass(tblClientClientContactDetailsLinkTable.tblClients);
            }

            return clsClientClientContactDetailsLinkTable;
        }

        //Save
        public int saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable)
        {
            tblClientClientContactDetailsLinkTable tblClientClientContactDetailsLinkTable = new tblClientClientContactDetailsLinkTable();

            tblClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID = clsClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID;
            tblClientClientContactDetailsLinkTable.iClientID = clsClientClientContactDetailsLinkTable.iClientID;
            tblClientClientContactDetailsLinkTable.iClientContactDetailID = clsClientClientContactDetailsLinkTable.iClientContactDetailID;
            tblClientClientContactDetailsLinkTable.bIsDeleted = clsClientClientContactDetailsLinkTable.bIsDeleted;

            if (tblClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID == 0)
            {
                db.tblClientClientContactDetailsLinkTable.Add(tblClientClientContactDetailsLinkTable);
                db.SaveChanges();
            }
            //Update
            else
            {
                db.Set<tblClientClientContactDetailsLinkTable>().AddOrUpdate(tblClientClientContactDetailsLinkTable);
                db.SaveChanges();
            }

            return tblClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID;
        }

        //Remove
        public void removeClientClientContactDetailsLinkTableByID(int iClientClientContactDetailsLinkTableID)
        {
            tblClientClientContactDetailsLinkTable tblClientClientContactDetailsLinkTable = db.tblClientClientContactDetailsLinkTable.Find(iClientClientContactDetailsLinkTableID);
            if (tblClientClientContactDetailsLinkTable != null)
            {
                tblClientClientContactDetailsLinkTable.bIsDeleted = true;
                db.Entry(tblClientClientContactDetailsLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientClientContactDetailsLinkTableExists(int iClientClientContactDetailsLinkTableID)
        {
            bool bClientClientContactDetailsLinkTableExists = db.tblClientClientContactDetailsLinkTable.Any(ClientClientContactDetailsLinkTable => ClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID == iClientClientContactDetailsLinkTableID && ClientClientContactDetailsLinkTable.bIsDeleted == false);
            return bClientClientContactDetailsLinkTableExists;
        }

        //Convert database table to class
        public clsClientClientContactDetailsLinkTable convertClientClientContactDetailsLinkTableTableToClass(tblClientClientContactDetailsLinkTable tblClientClientContactDetailsLinkTable)
        {
            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();

            clsClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID = tblClientClientContactDetailsLinkTable.iClientClientContactDetailsLinkTableID;
            clsClientClientContactDetailsLinkTable.iClientID = tblClientClientContactDetailsLinkTable.iClientID;
            clsClientClientContactDetailsLinkTable.iClientContactDetailID = tblClientClientContactDetailsLinkTable.iClientContactDetailID;
            clsClientClientContactDetailsLinkTable.bIsDeleted = tblClientClientContactDetailsLinkTable.bIsDeleted;

            return clsClientClientContactDetailsLinkTable;
        }
    }
}
