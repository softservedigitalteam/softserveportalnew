﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsPortalLoginsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsPortalLogins> getAllPortalLoginsList()
        {
            List<clsPortalLogins> lstPortalLogins = new List<clsPortalLogins>();
            var lstGetPortalLoginsList = db.tblPortalLogins.Where(PortalLogin => PortalLogin.bIsDeleted == false).ToList();

            if (lstGetPortalLoginsList.Count > 0)
            {
                foreach (var item in lstGetPortalLoginsList)
                {
                    clsPortalLogins clsPortalLogin = new clsPortalLogins();

                    clsPortalLogin.iPortalLoginID = item.iPortalLoginID;
                    clsPortalLogin.dtAdded = item.dtAdded;
                    clsPortalLogin.strUsername = item.strUsername;
                    clsPortalLogin.strPassword = item.strPassword;
                    clsPortalLogin.strIPAddress = item.strIPAddress;

                    clsPortalLogin.bIsSuccessfulLoginAttempt = item.bIsSuccessfulLoginAttempt;
                    clsPortalLogin.bIsDeleted = item.bIsDeleted;

                    lstPortalLogins.Add(clsPortalLogin);
                }
            }

            return lstPortalLogins;
        }

        public List<clsPortalLogins> getAllPortalLoginsOnlyList()
        {
            List<clsPortalLogins> lstPortalLogins = new List<clsPortalLogins>();
            var lstGetPortalLoginsList = db.tblPortalLogins.Where(PortalLogin => PortalLogin.bIsDeleted == false).ToList();

            if (lstGetPortalLoginsList.Count > 0)
            {

                foreach (var item in lstGetPortalLoginsList)
                {
                    clsPortalLogins clsPortalLogin = new clsPortalLogins();

                    clsPortalLogin.iPortalLoginID = item.iPortalLoginID;
                    clsPortalLogin.dtAdded = item.dtAdded;
                    clsPortalLogin.strUsername = item.strUsername;
                    clsPortalLogin.strPassword = item.strPassword;
                    clsPortalLogin.strIPAddress = item.strIPAddress;

                    clsPortalLogin.bIsSuccessfulLoginAttempt = item.bIsSuccessfulLoginAttempt;
                    clsPortalLogin.bIsDeleted = item.bIsDeleted;

                    lstPortalLogins.Add(clsPortalLogin);
                }
            }

            return lstPortalLogins;
        }

        //Get
        public clsPortalLogins getPortalLoginByID(int iPortalLoginID)
        {
            clsPortalLogins clsPortalLogin = null;
            tblPortalLogins tblPortalLogins = db.tblPortalLogins.FirstOrDefault(PortalLogin => PortalLogin.iPortalLoginID == iPortalLoginID && PortalLogin.bIsDeleted == false);

            if (tblPortalLogins != null)
            {
                clsPortalLogin = new clsPortalLogins();

                clsPortalLogin.iPortalLoginID = tblPortalLogins.iPortalLoginID;
                clsPortalLogin.dtAdded = tblPortalLogins.dtAdded;
                clsPortalLogin.strUsername = tblPortalLogins.strUsername;
                clsPortalLogin.strPassword = tblPortalLogins.strPassword;
                clsPortalLogin.strIPAddress = tblPortalLogins.strIPAddress;

                clsPortalLogin.bIsSuccessfulLoginAttempt = tblPortalLogins.bIsSuccessfulLoginAttempt;
                clsPortalLogin.bIsDeleted = tblPortalLogins.bIsDeleted;
            }

            return clsPortalLogin;
        }

        //Save
        public int savePortalLogin(clsPortalLogins clsPortalLogin)
        {
            tblPortalLogins tblPortalLogins = new tblPortalLogins();

            tblPortalLogins.iPortalLoginID = clsPortalLogin.iPortalLoginID;
            tblPortalLogins.dtAdded = DateTime.Now;

            tblPortalLogins.strUsername = clsPortalLogin.strUsername;
            tblPortalLogins.strPassword = clsPortalLogin.strPassword;
            tblPortalLogins.strIPAddress = clsPortalLogin.strIPAddress;
            tblPortalLogins.bIsSuccessfulLoginAttempt = clsPortalLogin.bIsSuccessfulLoginAttempt;

            tblPortalLogins.bIsDeleted = clsPortalLogin.bIsDeleted;

            if (tblPortalLogins.iPortalLoginID == 0)
            {
                db.tblPortalLogins.Add(tblPortalLogins);
                db.SaveChanges();
            }
            else
            {
                db.Set<tblPortalLogins>().AddOrUpdate(tblPortalLogins);
                db.SaveChanges();
            }
            return tblPortalLogins.iPortalLoginID;
        }

        //Remove
        public void removePortalLoginByID(int iPortalLoginID)
        {
            tblPortalLogins tblPortalLogin = db.tblPortalLogins.Find(iPortalLoginID);
            if (tblPortalLogin != null)
            {
                tblPortalLogin.bIsDeleted = true;
                db.Entry(tblPortalLogin).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfPortalLoginExists(int iPortalLoginID)
        {
            bool bPortalLoginExists = db.tblPortalLogins.Any(PortalLogin => PortalLogin.iPortalLoginID == iPortalLoginID && PortalLogin.bIsDeleted == false);
            return bPortalLoginExists;
        }

        //Convert database table to class
        public clsPortalLogins convertPortalLoginsTableToClass(tblPortalLogins tblPortalLogin)
        {
            clsPortalLogins clsPortalLogin = new clsPortalLogins();

            clsPortalLogin.iPortalLoginID = tblPortalLogin.iPortalLoginID;
            clsPortalLogin.dtAdded = tblPortalLogin.dtAdded;

            clsPortalLogin.strUsername = tblPortalLogin.strUsername;
            clsPortalLogin.strPassword = tblPortalLogin.strPassword;
            clsPortalLogin.strIPAddress = tblPortalLogin.strIPAddress;
            clsPortalLogin.bIsSuccessfulLoginAttempt = tblPortalLogin.bIsSuccessfulLoginAttempt;

            clsPortalLogin.bIsDeleted = tblPortalLogin.bIsDeleted;

            return clsPortalLogin;
        }
    }
}
