﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsNotificationTypesManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsNotificationTypes> getAllNotificationTypesList()
        {
            List<clsNotificationTypes> lstNotificationTypes = new List<clsNotificationTypes>();
            var lstGetNotificationTypesList = db.tblNotificationTypes.Where(NotificationType => NotificationType.bIsDeleted == false).ToList();

            if (lstGetNotificationTypesList.Count > 0)
            {
                //Managers
                clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();

                foreach (var item in lstGetNotificationTypesList)
                {
                    clsNotificationTypes clsNotificationType = new clsNotificationTypes();

                    clsNotificationType.iNotificationTypeID = item.iNotificationTypeID;
                    clsNotificationType.dtAdded = item.dtAdded;
                    clsNotificationType.iAddedBy = item.iAddedBy;
                    clsNotificationType.dtEdited = item.dtEdited;
                    clsNotificationType.iEditedBy = item.iEditedBy;

                    clsNotificationType.strTitle = item.strTitle;
                    clsNotificationType.strDescription = item.strDescription;
                    clsNotificationType.bIsDeleted = item.bIsDeleted;

                    clsNotificationType.lstNotifications = new List<clsNotifications>();

                    if (item.tblNotifications.Count > 0)
                    {
                        foreach (var Notification in item.tblNotifications)
                        {
                            if (Notification.bIsDeleted != true)
                            {
                                clsNotifications clsNotification = clsNotificationsManager.convertNotificationsTableToClass(Notification);
                                clsNotificationType.lstNotifications.Add(clsNotification);
                            }
                        }
                    }

                    lstNotificationTypes.Add(clsNotificationType);
                }
            }

            return lstNotificationTypes;
        }

        public List<clsNotificationTypes> getAllNotificationTypesOnlyList()
        {
            List<clsNotificationTypes> lstNotificationTypes = new List<clsNotificationTypes>();
            var lstGetNotificationTypesList = db.tblNotificationTypes.Where(NotificationType => NotificationType.bIsDeleted == false).ToList();

            if (lstGetNotificationTypesList.Count > 0)
            {

                foreach (var item in lstGetNotificationTypesList)
                {
                    clsNotificationTypes clsNotificationType = new clsNotificationTypes();

                    clsNotificationType.iNotificationTypeID = item.iNotificationTypeID;
                    clsNotificationType.dtAdded = item.dtAdded;
                    clsNotificationType.iAddedBy = item.iAddedBy;
                    clsNotificationType.dtEdited = item.dtEdited;
                    clsNotificationType.iEditedBy = item.iEditedBy;

                    clsNotificationType.strTitle = item.strTitle;
                    clsNotificationType.strDescription = item.strDescription;
                    clsNotificationType.bIsDeleted = item.bIsDeleted;

                    clsNotificationType.lstNotifications = new List<clsNotifications>();

                    lstNotificationTypes.Add(clsNotificationType);
                }
            }

            return lstNotificationTypes;
        }

        //Get
        public clsNotificationTypes getNotificationTypeByID(int iNotificationTypeID)
        {
            clsNotificationTypes clsNotificationType = null;
            tblNotificationTypes tblNotificationTypes = db.tblNotificationTypes.FirstOrDefault(NotificationType => NotificationType.iNotificationTypeID == iNotificationTypeID && NotificationType.bIsDeleted == false);

            if (tblNotificationTypes != null)
            {
                //Managers
                clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();

                clsNotificationType = new clsNotificationTypes();

                clsNotificationType.iNotificationTypeID = tblNotificationTypes.iNotificationTypeID;
                clsNotificationType.dtAdded = tblNotificationTypes.dtAdded;
                clsNotificationType.iAddedBy = tblNotificationTypes.iAddedBy;
                clsNotificationType.dtEdited = tblNotificationTypes.dtEdited;
                clsNotificationType.iEditedBy = tblNotificationTypes.iEditedBy;

                clsNotificationType.strTitle = tblNotificationTypes.strTitle;
                clsNotificationType.strDescription = tblNotificationTypes.strDescription;
                clsNotificationType.bIsDeleted = tblNotificationTypes.bIsDeleted;

                clsNotificationType.lstNotifications = new List<clsNotifications>();

                if (tblNotificationTypes.tblNotifications.Count > 0)
                {
                    foreach (var Notification in tblNotificationTypes.tblNotifications)
                    {
                        if (Notification.bIsDeleted != true)
                        {
                            clsNotifications clsNotification = clsNotificationsManager.convertNotificationsTableToClass(Notification);
                            clsNotificationType.lstNotifications.Add(clsNotification);
                        }
                    }
                }
            }

            return clsNotificationType;
        }

        //Save
        public int saveNotificationType(clsNotificationTypes clsNotificationType)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblNotificationTypes tblNotificationTypes = new tblNotificationTypes();

            tblNotificationTypes.iNotificationTypeID = clsNotificationType.iNotificationTypeID;

            tblNotificationTypes.strTitle = clsNotificationType.strTitle;
            tblNotificationTypes.strDescription = clsNotificationType.strDescription;
            tblNotificationTypes.bIsDeleted = clsNotificationType.bIsDeleted;

            //Add
            if (tblNotificationTypes.iNotificationTypeID == 0)
            {
                tblNotificationTypes.dtAdded = DateTime.Now;
                //tblNotificationTypes.iAddedBy = clsCMSUser.iCMSUserID;
                tblNotificationTypes.iAddedBy = 1;
                tblNotificationTypes.dtEdited = DateTime.Now;
                //tblNotificationTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblNotificationTypes.iEditedBy = 1;

                db.tblNotificationTypes.Add(tblNotificationTypes);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblNotificationTypes.dtAdded = clsNotificationType.dtAdded;
                tblNotificationTypes.iAddedBy = clsNotificationType.iAddedBy;
                tblNotificationTypes.dtEdited = DateTime.Now;
                //tblNotificationTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblNotificationTypes.iEditedBy = 1;

                db.Set<tblNotificationTypes>().AddOrUpdate(tblNotificationTypes);
                db.SaveChanges();
            }
            //}
            return tblNotificationTypes.iNotificationTypeID;
        }

        //Remove
        public void removeNotificationTypeByID(int iNotificationTypeID)
        {
            tblNotificationTypes tblNotificationType = db.tblNotificationTypes.Find(iNotificationTypeID);
            if (tblNotificationType != null)
            {
                tblNotificationType.bIsDeleted = true;
                db.Entry(tblNotificationType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfNotificationTypeExists(string strTitle)
        {
            bool bNotificationTypeExists = db.tblNotificationTypes.Any(NotificationType => NotificationType.strTitle.ToLower() == strTitle.ToLower() && NotificationType.bIsDeleted == false);
            return bNotificationTypeExists;
        }

        //Convert database table to class
        public clsNotificationTypes convertNotificationTypesTableToClass(tblNotificationTypes tblNotificationType)
        {
            clsNotificationTypes clsNotificationType = new clsNotificationTypes();

            clsNotificationType.iNotificationTypeID = tblNotificationType.iNotificationTypeID;
            clsNotificationType.dtAdded = tblNotificationType.dtAdded;
            clsNotificationType.iAddedBy = tblNotificationType.iAddedBy;
            clsNotificationType.dtEdited = tblNotificationType.dtEdited;
            clsNotificationType.iEditedBy = tblNotificationType.iEditedBy;

            clsNotificationType.strTitle = tblNotificationType.strTitle;
            clsNotificationType.strDescription = tblNotificationType.strDescription;
            clsNotificationType.bIsDeleted = tblNotificationType.bIsDeleted;

            return clsNotificationType;
        }
    }
}
