﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;
using System.Web;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientContactDetailsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientContactDetails> getAllClientContactDetailsList()
        {
            List<clsClientContactDetails> lstClientContactDetails = new List<clsClientContactDetails>();
            var lstGetClientContactDetailsList = db.tblClientContactDetails.Where(ClientContactDetail => ClientContactDetail.bIsDeleted == false).ToList();

            if (lstGetClientContactDetailsList.Count > 0)
            {
                //Managers
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

                foreach (var item in lstGetClientContactDetailsList)
                {
                    clsClientContactDetails clsClientContactDetail = new clsClientContactDetails();

                    clsClientContactDetail.iClientContactDetailID = item.iClientContactDetailID;
                    clsClientContactDetail.dtAdded = item.dtAdded;
                    clsClientContactDetail.iAddedBy = item.iAddedBy;
                    clsClientContactDetail.dtEdited = item.dtEdited;
                    clsClientContactDetail.iEditedBy = item.iEditedBy;

                    clsClientContactDetail.strContactName = item.strContactName;
                    clsClientContactDetail.strContactEmail = item.strContactEmail;
                    clsClientContactDetail.strContactNumber = item.strContactNumber;
                    clsClientContactDetail.bIsPrimaryContact = item.bIsPrimaryContact;
                    clsClientContactDetail.bIsDeleted = item.bIsDeleted;

                    clsClientContactDetail.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();

                    if (item.tblClientClientContactDetailsLinkTable.Count > 0)
                    {
                        foreach (var ClientClientContactDetailsLinkTableItem in item.tblClientClientContactDetailsLinkTable)
                        {
                            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                            clsClientContactDetail.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                        }
                    }

                    lstClientContactDetails.Add(clsClientContactDetail);
                }
            }

            return lstClientContactDetails;
        }

        public List<clsClientContactDetails> getAllClientContactDetailsOnlyList()
        {
            List<clsClientContactDetails> lstClientContactDetails = new List<clsClientContactDetails>();
            var lstGetClientContactDetailsList = db.tblClientContactDetails.Where(ClientContactDetail => ClientContactDetail.bIsDeleted == false).ToList();

            if (lstGetClientContactDetailsList.Count > 0)
            {
                foreach (var item in lstGetClientContactDetailsList)
                {
                    clsClientContactDetails clsClientContactDetail = new clsClientContactDetails();

                    clsClientContactDetail.iClientContactDetailID = item.iClientContactDetailID;
                    clsClientContactDetail.dtAdded = item.dtAdded;
                    clsClientContactDetail.iAddedBy = item.iAddedBy;
                    clsClientContactDetail.dtEdited = item.dtEdited;
                    clsClientContactDetail.iEditedBy = item.iEditedBy;

                    clsClientContactDetail.strContactName = item.strContactName;
                    clsClientContactDetail.strContactEmail = item.strContactEmail;
                    clsClientContactDetail.strContactNumber = item.strContactNumber;
                    clsClientContactDetail.bIsPrimaryContact = item.bIsPrimaryContact;
                    clsClientContactDetail.bIsDeleted = item.bIsDeleted;

                    clsClientContactDetail.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();

                    lstClientContactDetails.Add(clsClientContactDetail);
                }
            }

            return lstClientContactDetails;
        }

        //Get
        public clsClientContactDetails getClientContactDetailByID(int iClientContactDetailID)
        {
            clsClientContactDetails clsClientContactDetail = null;
            tblClientContactDetails tblClientContactDetails = db.tblClientContactDetails.FirstOrDefault(ClientContactDetail => ClientContactDetail.iClientContactDetailID == iClientContactDetailID && ClientContactDetail.bIsDeleted == false);

            if (tblClientContactDetails != null)
            {
                //Managers
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

                clsClientContactDetail = new clsClientContactDetails();

                clsClientContactDetail.iClientContactDetailID = tblClientContactDetails.iClientContactDetailID;
                clsClientContactDetail.dtAdded = tblClientContactDetails.dtAdded;
                clsClientContactDetail.iAddedBy = tblClientContactDetails.iAddedBy;
                clsClientContactDetail.dtEdited = tblClientContactDetails.dtEdited;
                clsClientContactDetail.iEditedBy = tblClientContactDetails.iEditedBy;

                clsClientContactDetail.strContactName = tblClientContactDetails.strContactName;
                clsClientContactDetail.strContactEmail = tblClientContactDetails.strContactEmail;
                clsClientContactDetail.strContactNumber = tblClientContactDetails.strContactNumber;
                clsClientContactDetail.bIsPrimaryContact = tblClientContactDetails.bIsPrimaryContact;
                clsClientContactDetail.bIsDeleted = tblClientContactDetails.bIsDeleted;

                clsClientContactDetail.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();

                if (tblClientContactDetails.tblClientClientContactDetailsLinkTable.Count > 0)
                {
                    foreach (var ClientClientContactDetailsLinkTableItem in tblClientContactDetails.tblClientClientContactDetailsLinkTable)
                    {
                        clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                        clsClientContactDetail.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                    }
                }
            }

            return clsClientContactDetail;
        }

        public clsClientContactDetails getClientContactDetailByEmail(string strClientContactDetailEmail)
        {
            clsClientContactDetails clsClientContactDetail = null;
            tblClientContactDetails tblClientContactDetails = db.tblClientContactDetails.FirstOrDefault(ClientContactDetail => ClientContactDetail.strContactEmail == strClientContactDetailEmail.ToLower() && ClientContactDetail.bIsDeleted == false);

            if (tblClientContactDetails != null)
            {
                //Managers
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

                clsClientContactDetail = new clsClientContactDetails();

                clsClientContactDetail.iClientContactDetailID = tblClientContactDetails.iClientContactDetailID;
                clsClientContactDetail.dtAdded = tblClientContactDetails.dtAdded;
                clsClientContactDetail.iAddedBy = tblClientContactDetails.iAddedBy;
                clsClientContactDetail.dtEdited = tblClientContactDetails.dtEdited;
                clsClientContactDetail.iEditedBy = tblClientContactDetails.iEditedBy;

                clsClientContactDetail.strContactName = tblClientContactDetails.strContactName;
                clsClientContactDetail.strContactEmail = tblClientContactDetails.strContactEmail;
                clsClientContactDetail.strContactNumber = tblClientContactDetails.strContactNumber;
                clsClientContactDetail.bIsPrimaryContact = tblClientContactDetails.bIsPrimaryContact;
                clsClientContactDetail.bIsDeleted = tblClientContactDetails.bIsDeleted;

                clsClientContactDetail.lstClientClientContactDetailsLinkTable = new List<clsClientClientContactDetailsLinkTable>();

                if (tblClientContactDetails.tblClientClientContactDetailsLinkTable.Count > 0)
                {
                    foreach (var ClientClientContactDetailsLinkTableItem in tblClientContactDetails.tblClientClientContactDetailsLinkTable)
                    {
                        clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = clsClientClientContactDetailsLinkTableManager.convertClientClientContactDetailsLinkTableTableToClass(ClientClientContactDetailsLinkTableItem);
                        clsClientContactDetail.lstClientClientContactDetailsLinkTable.Add(clsClientClientContactDetailsLinkTable);
                    }
                }
            }

            return clsClientContactDetail;
        }

        //Save
        public int saveClientContactDetail(clsClientContactDetails clsClientContactDetail)
        {
            int tblClientContactDetailID = 0;

            tblClientContactDetails tblClientContactDetails = new tblClientContactDetails();

            tblClientContactDetails.iClientContactDetailID = clsClientContactDetail.iClientContactDetailID;

            tblClientContactDetails.strContactName = clsClientContactDetail.strContactName;
            tblClientContactDetails.strContactEmail = clsClientContactDetail.strContactEmail.ToLower();
            tblClientContactDetails.strContactNumber = clsClientContactDetail.strContactNumber;
            tblClientContactDetails.bIsPrimaryContact = clsClientContactDetail.bIsPrimaryContact;
            tblClientContactDetails.bIsDeleted = clsClientContactDetail.bIsDeleted;

            //Add
            if (tblClientContactDetails.iClientContactDetailID == 0)
            {
                tblClientContactDetails.dtAdded = DateTime.Now;
                tblClientContactDetails.iAddedBy = 1;
                tblClientContactDetails.dtEdited = DateTime.Now;
                tblClientContactDetails.iEditedBy = 1;

                db.tblClientContactDetails.Add(tblClientContactDetails);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblClientContactDetails.dtAdded = clsClientContactDetail.dtAdded;
                tblClientContactDetails.iAddedBy = clsClientContactDetail.iAddedBy;
                tblClientContactDetails.dtEdited = DateTime.Now;
                tblClientContactDetails.iEditedBy = 1;

                db.Set<tblClientContactDetails>().AddOrUpdate(tblClientContactDetails);
                db.SaveChanges();
            }
            tblClientContactDetailID = tblClientContactDetails.iClientContactDetailID;

            return tblClientContactDetailID;
        }

        //Remove
        public void removeClientContactDetailByID(int iClientContactDetailID)
        {
            tblClientContactDetails tblClientContactDetail = db.tblClientContactDetails.Find(iClientContactDetailID);
            if (tblClientContactDetail != null)
            {
                tblClientContactDetail.bIsDeleted = true;
                db.Entry(tblClientContactDetail).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientContactDetailExists(int iClientContactDetailID)
        {
            bool bClientContactDetailExists = db.tblClientContactDetails.Any(ClientContactDetail => ClientContactDetail.iClientContactDetailID == iClientContactDetailID && ClientContactDetail.bIsDeleted == false);
            return bClientContactDetailExists;
        }

        //Convert database table to class
        public clsClientContactDetails convertClientContactDetailsTableToClass(tblClientContactDetails tblClientContactDetail)
        {
            clsClientContactDetails clsClientContactDetail = new clsClientContactDetails();

            clsClientContactDetail.iClientContactDetailID = tblClientContactDetail.iClientContactDetailID;
            clsClientContactDetail.dtAdded = tblClientContactDetail.dtAdded;
            clsClientContactDetail.iAddedBy = tblClientContactDetail.iAddedBy;
            clsClientContactDetail.dtEdited = tblClientContactDetail.dtEdited;
            clsClientContactDetail.iEditedBy = tblClientContactDetail.iEditedBy;

            clsClientContactDetail.strContactName = tblClientContactDetail.strContactName;
            clsClientContactDetail.strContactEmail = tblClientContactDetail.strContactEmail;
            clsClientContactDetail.strContactNumber = tblClientContactDetail.strContactNumber;
            clsClientContactDetail.bIsPrimaryContact = tblClientContactDetail.bIsPrimaryContact;
            clsClientContactDetail.bIsDeleted = tblClientContactDetail.bIsDeleted;

            return clsClientContactDetail;
        }
    }
}
