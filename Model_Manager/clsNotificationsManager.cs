﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsNotificationsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsNotifications> getAllNotificationsList()
        {
            List<clsNotifications> lstNotifications = new List<clsNotifications>();
            var lstGetNotificationsList = db.tblNotifications.Where(Notification => Notification.bIsDeleted == false).ToList();

            if (lstGetNotificationsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();

                foreach (var item in lstGetNotificationsList)
                {
                    clsNotifications clsNotification = new clsNotifications();

                    clsNotification.iNotificationID = item.iNotificationID;
                    clsNotification.dtAdded = item.dtAdded;
                    clsNotification.iAddedBy = item.iAddedBy;
                    clsNotification.dtEdited = item.dtEdited;
                    clsNotification.iEditedBy = item.iEditedBy;

                    clsNotification.strTitle = item.strTitle;
                    clsNotification.strDescription = item.strDescription;
                    clsNotification.iClientID = item.iClientID;
                    clsNotification.iNotificationTypeID = item.iNotificationTypeID;
                    clsNotification.bIsRead = item.bIsRead;

                    clsNotification.bIsDeleted = item.bIsDeleted;

                    if (item.tblClients != null)
                        if (item.tblClients.bIsDeleted != true)
                            clsNotification.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblNotificationTypes != null)
                        if (item.tblNotificationTypes.bIsDeleted != true)
                            clsNotification.clsNotificationType = clsNotificationTypesManager.convertNotificationTypesTableToClass(item.tblNotificationTypes);

                    lstNotifications.Add(clsNotification);
                }
            }

            return lstNotifications;
        }

        public List<clsNotifications> getAllNotificationsOnlyList()
        {
            List<clsNotifications> lstNotifications = new List<clsNotifications>();
            var lstGetNotificationsList = db.tblNotifications.Where(Notification => Notification.bIsDeleted == false).ToList();

            if (lstGetNotificationsList.Count > 0)
            {

                foreach (var item in lstGetNotificationsList)
                {
                    clsNotifications clsNotification = new clsNotifications();

                    clsNotification.iNotificationID = item.iNotificationID;
                    clsNotification.dtAdded = item.dtAdded;
                    clsNotification.iAddedBy = item.iAddedBy;
                    clsNotification.dtEdited = item.dtEdited;
                    clsNotification.iEditedBy = item.iEditedBy;

                    clsNotification.strTitle = item.strTitle;
                    clsNotification.strDescription = item.strDescription;
                    clsNotification.iClientID = item.iClientID;
                    clsNotification.iNotificationTypeID = item.iNotificationTypeID;
                    clsNotification.bIsRead = item.bIsRead;

                    clsNotification.bIsDeleted = item.bIsDeleted;

                    lstNotifications.Add(clsNotification);
                }
            }

            return lstNotifications;
        }

        //Get
        public clsNotifications getNotificationByID(int iNotificationID)
        {
            clsNotifications clsNotification = null;
            tblNotifications tblNotifications = db.tblNotifications.FirstOrDefault(Notification => Notification.iNotificationID == iNotificationID && Notification.bIsDeleted == false);

            if (tblNotifications != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();

                clsNotification = new clsNotifications();

                clsNotification.iNotificationID = tblNotifications.iNotificationID;
                clsNotification.dtAdded = tblNotifications.dtAdded;
                clsNotification.iAddedBy = tblNotifications.iAddedBy;
                clsNotification.dtEdited = tblNotifications.dtEdited;
                clsNotification.iEditedBy = tblNotifications.iEditedBy;

                clsNotification.strTitle = tblNotifications.strTitle;
                clsNotification.strDescription = tblNotifications.strDescription;
                clsNotification.iClientID = tblNotifications.iClientID;
                clsNotification.iNotificationTypeID = tblNotifications.iNotificationTypeID;
                clsNotification.bIsRead = tblNotifications.bIsRead;

                clsNotification.bIsDeleted = tblNotifications.bIsDeleted;

                if (tblNotifications.tblClients != null)
                    if (tblNotifications.tblClients.bIsDeleted != true)
                        clsNotification.clsClient = clsClientsManager.convertClientsTableToClass(tblNotifications.tblClients);
                if (tblNotifications.tblNotificationTypes != null)
                    if (tblNotifications.tblNotificationTypes.bIsDeleted != true)
                        clsNotification.clsNotificationType = clsNotificationTypesManager.convertNotificationTypesTableToClass(tblNotifications.tblNotificationTypes);
            }

            return clsNotification;
        }

        //Save
        public int saveNotification(clsNotifications clsNotification)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            var tblNotifications = new tblNotifications
            {
                iNotificationID = clsNotification.iNotificationID,
                strTitle = clsNotification.strTitle,
                strDescription = clsNotification.strDescription,
                strComment = clsNotification.strComment,
                iClientID = clsNotification.iClientID,
                iUserID = clsNotification.iUserID,
                iNotificationTypeID = clsNotification.iNotificationTypeID,
                bIsRead = clsNotification.bIsRead,
                bIsDeleted = clsNotification.bIsDeleted
            };




            //Add
            if (tblNotifications.iNotificationID == 0)
            {
                tblNotifications.dtAdded = DateTime.Now;
                tblNotifications.iAddedBy = clsNotification.iAddedBy;
                tblNotifications.dtEdited = DateTime.Now;
                tblNotifications.iEditedBy = clsNotification.iEditedBy;

                db.tblNotifications.Add(tblNotifications);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblNotifications.dtAdded = clsNotification.dtAdded;
                tblNotifications.iAddedBy = clsNotification.iAddedBy;
                tblNotifications.dtEdited = DateTime.Now;
                tblNotifications.iEditedBy = clsNotification.iEditedBy;

                db.Set<tblNotifications>().AddOrUpdate(tblNotifications);
                db.SaveChanges();
            }
            //}
            return tblNotifications.iNotificationID;
        }

        //Remove
        public void removeNotificationByID(int iNotificationID)
        {
            tblNotifications tblNotification = db.tblNotifications.Find(iNotificationID);
            if (tblNotification != null)
            {
                tblNotification.bIsDeleted = true;
                db.Entry(tblNotification).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfNotificationExists(int iNotificationID)
        {
            bool bNotificationExists = db.tblNotifications.Any(Notification => Notification.iNotificationID == iNotificationID && Notification.bIsDeleted == false);
            return bNotificationExists;
        }

        //Convert database table to class
        public clsNotifications convertNotificationsTableToClass(tblNotifications tblNotification)
        {
            clsNotifications clsNotification = new clsNotifications();

            clsNotification.iNotificationID = tblNotification.iNotificationID;
            clsNotification.dtAdded = tblNotification.dtAdded;
            clsNotification.iAddedBy = tblNotification.iAddedBy;
            clsNotification.dtEdited = tblNotification.dtEdited;
            clsNotification.iEditedBy = tblNotification.iEditedBy;

            clsNotification.strTitle = tblNotification.strTitle;
            clsNotification.strDescription = tblNotification.strDescription;
            clsNotification.iClientID = tblNotification.iClientID;
            clsNotification.iNotificationTypeID = tblNotification.iNotificationTypeID;
            clsNotification.bIsRead = tblNotification.bIsRead;

            clsNotification.bIsDeleted = tblNotification.bIsDeleted;

            return clsNotification;
        }
    }
}
