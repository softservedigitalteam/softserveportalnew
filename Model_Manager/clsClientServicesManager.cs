﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsClientServicesManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsClientServices> getAllClientServicesList()
        {
            List<clsClientServices> lstClientServices = new List<clsClientServices>();
            var lstGetClientServicesList = db.tblClientServices.Where(ClientService => ClientService.bIsDeleted == false).ToList();

            if (lstGetClientServicesList.Count > 0)
            {
                //Managers
                clsClientClientServicesLinkTableManager clsClientClientServicesLinkTableManager = new clsClientClientServicesLinkTableManager();

                foreach (var item in lstGetClientServicesList)
                {
                    clsClientServices clsClientService = new clsClientServices();

                    clsClientService.iClientServiceID = item.iClientServiceID;
                    clsClientService.dtAdded = item.dtAdded;
                    clsClientService.iAddedBy = item.iAddedBy;
                    clsClientService.dtEdited = item.dtEdited;
                    clsClientService.iEditedBy = item.iEditedBy;

                    clsClientService.strTitle = item.strTitle;
                    clsClientService.strDescription = item.strDescription;

                    clsClientService.bIsDeleted = item.bIsDeleted;

                    clsClientService.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();

                    if (item.tblClientClientServicesLinkTable.Count > 0)
                    {
                        foreach (var ClientClientServicesLinkTable in item.tblClientClientServicesLinkTable)
                        {
                            clsClientClientServicesLinkTable clsClientClientServicesLinkTable = clsClientClientServicesLinkTableManager.convertClientClientServicesLinkTableTableToClass(ClientClientServicesLinkTable);
                            clsClientService.lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                        }
                    }

                    lstClientServices.Add(clsClientService);
                }
            }

            return lstClientServices;
        }

        public List<clsClientServices> getAllClientServicesOnlyList()
        {
            List<clsClientServices> lstClientServices = new List<clsClientServices>();
            var lstGetClientServicesList = db.tblClientServices.Where(ClientService => ClientService.bIsDeleted == false).ToList();

            if (lstGetClientServicesList.Count > 0)
            {

                foreach (var item in lstGetClientServicesList)
                {
                    clsClientServices clsClientService = new clsClientServices();

                    clsClientService.iClientServiceID = item.iClientServiceID;
                    clsClientService.dtAdded = item.dtAdded;
                    clsClientService.iAddedBy = item.iAddedBy;
                    clsClientService.dtEdited = item.dtEdited;
                    clsClientService.iEditedBy = item.iEditedBy;

                    clsClientService.strTitle = item.strTitle;
                    clsClientService.strDescription = item.strDescription;

                    clsClientService.bIsDeleted = item.bIsDeleted;

                    clsClientService.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();

                    lstClientServices.Add(clsClientService);
                }
            }

            return lstClientServices;
        }

        //Get
        public clsClientServices getClientServiceByID(int iClientServiceID)
        {
            clsClientServices clsClientService = null;
            tblClientServices tblClientServices = db.tblClientServices.FirstOrDefault(ClientService => ClientService.iClientServiceID == iClientServiceID && ClientService.bIsDeleted == false);

            if (tblClientServices != null)
            {
                //Managers
                clsClientClientServicesLinkTableManager clsClientClientServicesLinkTableManager = new clsClientClientServicesLinkTableManager();

                clsClientService = new clsClientServices();

                clsClientService.iClientServiceID = tblClientServices.iClientServiceID;
                clsClientService.dtAdded = tblClientServices.dtAdded;
                clsClientService.iAddedBy = tblClientServices.iAddedBy;
                clsClientService.dtEdited = tblClientServices.dtEdited;
                clsClientService.iEditedBy = tblClientServices.iEditedBy;

                clsClientService.strTitle = tblClientServices.strTitle;
                clsClientService.strDescription = tblClientServices.strDescription;

                clsClientService.bIsDeleted = tblClientServices.bIsDeleted;

                clsClientService.lstClientClientServicesLinkTable = new List<clsClientClientServicesLinkTable>();

                if (tblClientServices.tblClientClientServicesLinkTable.Count > 0)
                {
                    foreach (var ClientClientServicesLinkTable in tblClientServices.tblClientClientServicesLinkTable)
                    {
                        clsClientClientServicesLinkTable clsClientClientServicesLinkTable = clsClientClientServicesLinkTableManager.convertClientClientServicesLinkTableTableToClass(ClientClientServicesLinkTable);
                        clsClientService.lstClientClientServicesLinkTable.Add(clsClientClientServicesLinkTable);
                    }
                }
            }

            return clsClientService;
        }

        //Save
        public int saveClientService(clsClientServices clsClientService)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblClientServices tblClientServices = new tblClientServices();

            tblClientServices.iClientServiceID = clsClientService.iClientServiceID;

            tblClientServices.strTitle = clsClientService.strTitle;
            tblClientServices.strDescription = clsClientService.strDescription;

            tblClientServices.bIsDeleted = clsClientService.bIsDeleted;

            //Add
            if (tblClientServices.iClientServiceID == 0)
            {
                tblClientServices.dtAdded = DateTime.Now;
                //tblClientServices.iAddedBy = clsCMSUser.iCMSUserID;
                tblClientServices.iAddedBy = 1;
                tblClientServices.dtEdited = DateTime.Now;
                //tblClientServices.iEditedBy = clsCMSUser.iCMSUserID;
                tblClientServices.iEditedBy = 1;

                db.tblClientServices.Add(tblClientServices);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblClientServices.dtAdded = clsClientService.dtAdded;
                tblClientServices.iAddedBy = clsClientService.iAddedBy;
                tblClientServices.dtEdited = DateTime.Now;
                //tblClientServices.iEditedBy = clsCMSUser.iCMSUserID;
                tblClientServices.iEditedBy = 1;

                db.Set<tblClientServices>().AddOrUpdate(tblClientServices);
                db.SaveChanges();
            }
            //}
            return tblClientServices.iClientServiceID;
        }

        //Remove
        public void removeClientServiceByID(int iClientServiceID)
        {
            tblClientServices tblClientService = db.tblClientServices.Find(iClientServiceID);
            if (tblClientService != null)
            {
                tblClientService.bIsDeleted = true;
                db.Entry(tblClientService).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfClientServiceExists(int iClientServiceID)
        {
            bool bClientServiceExists = db.tblClientServices.Any(ClientService => ClientService.iClientServiceID == iClientServiceID && ClientService.bIsDeleted == false);
            return bClientServiceExists;
        }

        //Convert database table to class
        public clsClientServices convertClientServicesTableToClass(tblClientServices tblClientService)
        {
            clsClientServices clsClientService = new clsClientServices();

            clsClientService.iClientServiceID = tblClientService.iClientServiceID;
            clsClientService.dtAdded = tblClientService.dtAdded;
            clsClientService.iAddedBy = tblClientService.iAddedBy;
            clsClientService.dtEdited = tblClientService.dtEdited;
            clsClientService.iEditedBy = tblClientService.iEditedBy;

            clsClientService.strTitle = tblClientService.strTitle;
            clsClientService.strDescription = tblClientService.strDescription;

            clsClientService.bIsDeleted = tblClientService.bIsDeleted;

            return clsClientService;
        }
    }
}
