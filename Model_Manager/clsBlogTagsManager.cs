﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsBlogTagsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsBlogTags> getAllBlogTagsList()
        {
            List<clsBlogTags> lstBlogTags = new List<clsBlogTags>();
            var lstGetBlogTagsList = db.tblBlogTags.Where(BlogTag => BlogTag.bIsDeleted == false).ToList();

            if (lstGetBlogTagsList.Count > 0)
            {
                //Managers
                //clsBlogPostsBlogTagsLinkTableManager clsBlogPostsBlogTagsLinkTableManager = new clsBlogPostsBlogTagsLinkTableManager();

                foreach (var item in lstGetBlogTagsList)
                {
                    clsBlogTags clsBlogTag = new clsBlogTags();

                    clsBlogTag.iBlogTagID = item.iBlogTagID;
                    clsBlogTag.dtAdded = item.dtAdded;
                    clsBlogTag.iAddedBy = item.iAddedBy;
                    clsBlogTag.dtEdited = item.dtEdited;
                    clsBlogTag.iEditedBy = item.iEditedBy;

                    clsBlogTag.strTitle = item.strTitle;
                    clsBlogTag.strDescription = item.strDescription;
                    clsBlogTag.bIsDeleted = item.bIsDeleted;

                    //clsBlogTag.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                    //if (item.tblBlogPostsBlogTagsLinkTable.Count > 0)
                    //{
                    //    foreach (var BlogPostsBlogTagsLinkTable in item.tblBlogPostsBlogTagsLinkTable)
                    //    {
                    //        clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = clsBlogPostsBlogTagsLinkTableManager.convertBlogPostsBlogTagsLinkTableTableToClass(BlogPostsBlogTagsLinkTable);
                    //        clsBlogTag.lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
                    //    }
                    //}

                    lstBlogTags.Add(clsBlogTag);
                }
            }

            return lstBlogTags;
        }

        public List<clsBlogTags> getAllBlogTagsOnlyList()
        {
            List<clsBlogTags> lstBlogTags = new List<clsBlogTags>();
            var lstGetBlogTagsList = db.tblBlogTags.Where(BlogTag => BlogTag.bIsDeleted == false).ToList();

            if (lstGetBlogTagsList.Count > 0)
            {

                foreach (var item in lstGetBlogTagsList)
                {
                    clsBlogTags clsBlogTag = new clsBlogTags();

                    clsBlogTag.iBlogTagID = item.iBlogTagID;
                    clsBlogTag.dtAdded = item.dtAdded;
                    clsBlogTag.iAddedBy = item.iAddedBy;
                    clsBlogTag.dtEdited = item.dtEdited;
                    clsBlogTag.iEditedBy = item.iEditedBy;

                    clsBlogTag.strTitle = item.strTitle;
                    clsBlogTag.strDescription = item.strDescription;
                    clsBlogTag.bIsDeleted = item.bIsDeleted;

                    //clsBlogTag.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                    lstBlogTags.Add(clsBlogTag);
                }
            }

            return lstBlogTags;
        }

        //Get
        public clsBlogTags getBlogTagByID(int iBlogTagID)
        {
            clsBlogTags clsBlogTag = null;
            tblBlogTags tblBlogTags = db.tblBlogTags.FirstOrDefault(BlogTag => BlogTag.iBlogTagID == iBlogTagID && BlogTag.bIsDeleted == false);

            if (tblBlogTags != null)
            {
                ///Managers
                clsBlogPostsBlogTagsLinkTableManager clsBlogPostsBlogTagsLinkTableManager = new clsBlogPostsBlogTagsLinkTableManager();

                clsBlogTag = new clsBlogTags();

                clsBlogTag.iBlogTagID = tblBlogTags.iBlogTagID;
                clsBlogTag.dtAdded = tblBlogTags.dtAdded;
                clsBlogTag.iAddedBy = tblBlogTags.iAddedBy;
                clsBlogTag.dtEdited = tblBlogTags.dtEdited;
                clsBlogTag.iEditedBy = tblBlogTags.iEditedBy;

                clsBlogTag.strTitle = tblBlogTags.strTitle;
                clsBlogTag.strDescription = tblBlogTags.strDescription;
                clsBlogTag.bIsDeleted = tblBlogTags.bIsDeleted;

                //clsBlogTag.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                //if (tblBlogTags.tblBlogPostsBlogTagsLinkTable.Count > 0)
                //{
                //    foreach (var BlogPostsBlogTagsLinkTable in tblBlogTags.tblBlogPostsBlogTagsLinkTable)
                //    {
                //        clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = clsBlogPostsBlogTagsLinkTableManager.convertBlogPostsBlogTagsLinkTableTableToClass(BlogPostsBlogTagsLinkTable);
                //        clsBlogTag.lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
                //    }
                //}
            }

            return clsBlogTag;
        }

        //Save
        public int saveBlogTag(clsBlogTags clsBlogTag)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblBlogTags tblBlogTags = new tblBlogTags();

            tblBlogTags.iBlogTagID = clsBlogTag.iBlogTagID;

            tblBlogTags.strTitle = clsBlogTag.strTitle;
            tblBlogTags.strDescription = clsBlogTag.strDescription;
            tblBlogTags.bIsDeleted = clsBlogTag.bIsDeleted;

            //Add
            if (tblBlogTags.iBlogTagID == 0)
            {
                tblBlogTags.dtAdded = DateTime.Now;
                //tblBlogTags.iAddedBy = clsCMSUser.iCMSUserID;
                tblBlogTags.iAddedBy = 1;
                tblBlogTags.dtEdited = DateTime.Now;
                //tblBlogTags.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogTags.iEditedBy = 1;

                db.tblBlogTags.Add(tblBlogTags);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblBlogTags.dtAdded = clsBlogTag.dtAdded;
                tblBlogTags.iAddedBy = clsBlogTag.iAddedBy;
                tblBlogTags.dtEdited = DateTime.Now;
                //tblBlogTags.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogTags.iEditedBy = 1;

                db.Set<tblBlogTags>().AddOrUpdate(tblBlogTags);
                db.SaveChanges();
            }
            //}
            return tblBlogTags.iBlogTagID;
        }

        //Remove
        public void removeBlogTagByID(int iBlogTagID)
        {
            tblBlogTags tblBlogTag = db.tblBlogTags.Find(iBlogTagID);
            if (tblBlogTag != null)
            {
                tblBlogTag.bIsDeleted = true;
                db.Entry(tblBlogTag).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfBlogTagExists(string strTitle)
        {
            bool bBlogTagExists = db.tblBlogTags.Any(BlogTag => BlogTag.strTitle.ToLower() == strTitle.ToLower() && BlogTag.bIsDeleted == false);
            return bBlogTagExists;
        }

        //Convert database table to class
        public clsBlogTags convertBlogTagsTableToClass(tblBlogTags tblBlogTag)
        {
            clsBlogTags clsBlogTag = new clsBlogTags();

            clsBlogTag.iBlogTagID = tblBlogTag.iBlogTagID;
            clsBlogTag.dtAdded = tblBlogTag.dtAdded;
            clsBlogTag.iAddedBy = tblBlogTag.iAddedBy;
            clsBlogTag.dtEdited = tblBlogTag.dtEdited;
            clsBlogTag.iEditedBy = tblBlogTag.iEditedBy;

            clsBlogTag.strTitle = tblBlogTag.strTitle;
            clsBlogTag.strDescription = tblBlogTag.strDescription;
            clsBlogTag.bIsDeleted = tblBlogTag.bIsDeleted;

            return clsBlogTag;
        }
    }
}
