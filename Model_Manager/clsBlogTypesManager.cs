﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsBlogTypesManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsBlogTypes> getAllBlogTypesList()
        {
            List<clsBlogTypes> lstBlogTypes = new List<clsBlogTypes>();
            var lstGetBlogTypesList = db.tblBlogTypes.Where(BlogType => BlogType.bIsDeleted == false).ToList();

            if (lstGetBlogTypesList.Count > 0)
            {
                //Managers
                clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();

                foreach (var item in lstGetBlogTypesList)
                {
                    clsBlogTypes clsBlogType = new clsBlogTypes();

                    clsBlogType.iBlogTypeID = item.iBlogTypeID;
                    clsBlogType.dtAdded = item.dtAdded;
                    clsBlogType.iAddedBy = item.iAddedBy;
                    clsBlogType.dtEdited = item.dtEdited;
                    clsBlogType.iEditedBy = item.iEditedBy;

                    clsBlogType.strTitle = item.strTitle;
                    clsBlogType.strDescription = item.strDescription;
                    clsBlogType.bIsDeleted = item.bIsDeleted;

                    clsBlogType.lstBlogPosts = new List<clsBlogPosts>();

                    if (item.tblBlogPosts.Count > 0)
                    {
                        foreach (var BlogPost in item.tblBlogPosts)
                        {
                            clsBlogPosts clsBlogPost = clsBlogPostsManager.convertBlogPostsTableToClass(BlogPost);
                            clsBlogType.lstBlogPosts.Add(clsBlogPost);
                        }
                    }

                    lstBlogTypes.Add(clsBlogType);
                }
            }

            return lstBlogTypes;
        }

        public List<clsBlogTypes> getAllBlogTypesOnlyList()
        {
            List<clsBlogTypes> lstBlogTypes = new List<clsBlogTypes>();
            var lstGetBlogTypesList = db.tblBlogTypes.Where(BlogType => BlogType.bIsDeleted == false).ToList();

            if (lstGetBlogTypesList.Count > 0)
            {

                foreach (var item in lstGetBlogTypesList)
                {
                    clsBlogTypes clsBlogType = new clsBlogTypes();

                    clsBlogType.iBlogTypeID = item.iBlogTypeID;
                    clsBlogType.dtAdded = item.dtAdded;
                    clsBlogType.iAddedBy = item.iAddedBy;
                    clsBlogType.dtEdited = item.dtEdited;
                    clsBlogType.iEditedBy = item.iEditedBy;

                    clsBlogType.strTitle = item.strTitle;
                    clsBlogType.strDescription = item.strDescription;
                    clsBlogType.bIsDeleted = item.bIsDeleted;

                    clsBlogType.lstBlogPosts = new List<clsBlogPosts>();

                    lstBlogTypes.Add(clsBlogType);
                }
            }

            return lstBlogTypes;
        }

        //Get
        public clsBlogTypes getBlogTypeByID(int iBlogTypeID)
        {
            clsBlogTypes clsBlogType = null;
            tblBlogTypes tblBlogTypes = db.tblBlogTypes.FirstOrDefault(BlogType => BlogType.iBlogTypeID == iBlogTypeID && BlogType.bIsDeleted == false);

            if (tblBlogTypes != null)
            {
                //Managers
                clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();

                clsBlogType = new clsBlogTypes();

                clsBlogType.iBlogTypeID = tblBlogTypes.iBlogTypeID;
                clsBlogType.dtAdded = tblBlogTypes.dtAdded;
                clsBlogType.iAddedBy = tblBlogTypes.iAddedBy;
                clsBlogType.dtEdited = tblBlogTypes.dtEdited;
                clsBlogType.iEditedBy = tblBlogTypes.iEditedBy;

                clsBlogType.strTitle = tblBlogTypes.strTitle;
                clsBlogType.strDescription = tblBlogTypes.strDescription;
                clsBlogType.bIsDeleted = tblBlogTypes.bIsDeleted;

                clsBlogType.lstBlogPosts = new List<clsBlogPosts>();

                if (tblBlogTypes.tblBlogPosts.Count > 0)
                {
                    foreach (var BlogPost in tblBlogTypes.tblBlogPosts)
                    {
                        clsBlogPosts clsBlogPost = clsBlogPostsManager.convertBlogPostsTableToClass(BlogPost);
                        clsBlogType.lstBlogPosts.Add(clsBlogPost);
                    }
                }
            }

            return clsBlogType;
        }

        //Save
        public int saveBlogType(clsBlogTypes clsBlogType)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblBlogTypes tblBlogTypes = new tblBlogTypes();

            tblBlogTypes.iBlogTypeID = clsBlogType.iBlogTypeID;

            tblBlogTypes.strTitle = clsBlogType.strTitle;
            tblBlogTypes.strDescription = clsBlogType.strDescription;
            tblBlogTypes.bIsDeleted = clsBlogType.bIsDeleted;

            //Add
            if (tblBlogTypes.iBlogTypeID == 0)
            {
                tblBlogTypes.dtAdded = DateTime.Now;
                //tblBlogTypes.iAddedBy = clsCMSUser.iCMSUserID;
                tblBlogTypes.iAddedBy = 1;
                tblBlogTypes.dtEdited = DateTime.Now;
                //tblBlogTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogTypes.iEditedBy = 1;

                db.tblBlogTypes.Add(tblBlogTypes);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblBlogTypes.dtAdded = clsBlogType.dtAdded;
                tblBlogTypes.iAddedBy = clsBlogType.iAddedBy;
                tblBlogTypes.dtEdited = DateTime.Now;
                //tblBlogTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogTypes.iEditedBy = 1;

                db.Set<tblBlogTypes>().AddOrUpdate(tblBlogTypes);
                db.SaveChanges();
            }
            //}
            return tblBlogTypes.iBlogTypeID;
        }

        //Remove
        public void removeBlogTypeByID(int iBlogTypeID)
        {
            tblBlogTypes tblBlogType = db.tblBlogTypes.Find(iBlogTypeID);
            if (tblBlogType != null)
            {
                tblBlogType.bIsDeleted = true;
                db.Entry(tblBlogType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfBlogTypeExists(string strTitle)
        {
            bool bBlogTypeExists = db.tblBlogTypes.Any(BlogType => BlogType.strTitle.ToLower() == strTitle.ToLower() && BlogType.bIsDeleted == false);
            return bBlogTypeExists;
        }

        //Convert database table to class
        public clsBlogTypes convertBlogTypesTableToClass(tblBlogTypes tblBlogType)
        {
            clsBlogTypes clsBlogType = new clsBlogTypes();

            clsBlogType.iBlogTypeID = tblBlogType.iBlogTypeID;
            clsBlogType.dtAdded = tblBlogType.dtAdded;
            clsBlogType.iAddedBy = tblBlogType.iAddedBy;
            clsBlogType.dtEdited = tblBlogType.dtEdited;
            clsBlogType.iEditedBy = tblBlogType.iEditedBy;

            clsBlogType.strTitle = tblBlogType.strTitle;
            clsBlogType.strDescription = tblBlogType.strDescription;
            clsBlogType.bIsDeleted = tblBlogType.bIsDeleted;

            return clsBlogType;
        }
    }
}
