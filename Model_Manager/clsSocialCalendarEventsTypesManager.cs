﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsSocialCalendarEventsTypesManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsSocialCalendarEventsTypes> getAllSocialCalendarEventsTypesList()
        {
            List<clsSocialCalendarEventsTypes> lstSocialCalendarEventsTypes = new List<clsSocialCalendarEventsTypes>();
            var lstGetSocialCalendarEventsTypesList = db.tblSocialCalendarEventsTypes.Where(SocialCalendarEventsType => SocialCalendarEventsType.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsTypesList.Count > 0)
            {
                //Managers
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();

                foreach (var item in lstGetSocialCalendarEventsTypesList)
                {
                    clsSocialCalendarEventsTypes clsSocialCalendarEventsType = new clsSocialCalendarEventsTypes();

                    clsSocialCalendarEventsType.iSocialCalendarEventsTypeID = item.iSocialCalendarEventsTypeID;
                    clsSocialCalendarEventsType.dtAdded = item.dtAdded;
                    clsSocialCalendarEventsType.iAddedBy = item.iAddedBy;
                    clsSocialCalendarEventsType.dtEdited = item.dtEdited;
                    clsSocialCalendarEventsType.iEditedBy = item.iEditedBy;

                    clsSocialCalendarEventsType.strTitle = item.strTitle;
                    clsSocialCalendarEventsType.strDescription = item.strDescription;
                    clsSocialCalendarEventsType.strColour = item.strColour;
                    clsSocialCalendarEventsType.bIsDeleted = item.bIsDeleted;

                    clsSocialCalendarEventsType.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();

                    if (item.tblSocialCalendarEvents.Count > 0)
                    {
                        foreach (var SocialCalendarEvent in item.tblSocialCalendarEvents)
                        {
                            clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.convertSocialCalendarEventsTableToClass(SocialCalendarEvent);
                            clsSocialCalendarEventsType.lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                        }
                    }

                    lstSocialCalendarEventsTypes.Add(clsSocialCalendarEventsType);
                }
            }

            return lstSocialCalendarEventsTypes;
        }

        public List<clsSocialCalendarEventsTypes> getAllSocialCalendarEventsTypesOnlyList()
        {
            List<clsSocialCalendarEventsTypes> lstSocialCalendarEventsTypes = new List<clsSocialCalendarEventsTypes>();
            var lstGetSocialCalendarEventsTypesList = db.tblSocialCalendarEventsTypes.Where(SocialCalendarEventsType => SocialCalendarEventsType.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsTypesList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsTypesList)
                {
                    clsSocialCalendarEventsTypes clsSocialCalendarEventsType = new clsSocialCalendarEventsTypes();

                    clsSocialCalendarEventsType.iSocialCalendarEventsTypeID = item.iSocialCalendarEventsTypeID;
                    clsSocialCalendarEventsType.dtAdded = item.dtAdded;
                    clsSocialCalendarEventsType.iAddedBy = item.iAddedBy;
                    clsSocialCalendarEventsType.dtEdited = item.dtEdited;
                    clsSocialCalendarEventsType.iEditedBy = item.iEditedBy;

                    clsSocialCalendarEventsType.strTitle = item.strTitle;
                    clsSocialCalendarEventsType.strDescription = item.strDescription;
                    clsSocialCalendarEventsType.strColour = item.strColour;
                    clsSocialCalendarEventsType.bIsDeleted = item.bIsDeleted;

                    clsSocialCalendarEventsType.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();

                    lstSocialCalendarEventsTypes.Add(clsSocialCalendarEventsType);
                }
            }

            return lstSocialCalendarEventsTypes;
        }

        //Get
        public clsSocialCalendarEventsTypes getSocialCalendarEventsTypeOnlyByID(int iSocialCalendarEventsTypeID)
        {
            clsSocialCalendarEventsTypes clsSocialCalendarEventsType = null;
            tblSocialCalendarEventsTypes tblSocialCalendarEventsTypes = db.tblSocialCalendarEventsTypes.FirstOrDefault(SocialCalendarEventsType => SocialCalendarEventsType.iSocialCalendarEventsTypeID == iSocialCalendarEventsTypeID && SocialCalendarEventsType.bIsDeleted == false);

            if (tblSocialCalendarEventsTypes != null)
            {
                //Managers
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();

                clsSocialCalendarEventsType = new clsSocialCalendarEventsTypes();

                clsSocialCalendarEventsType.iSocialCalendarEventsTypeID = tblSocialCalendarEventsTypes.iSocialCalendarEventsTypeID;
                clsSocialCalendarEventsType.dtAdded = tblSocialCalendarEventsTypes.dtAdded;
                clsSocialCalendarEventsType.iAddedBy = tblSocialCalendarEventsTypes.iAddedBy;
                clsSocialCalendarEventsType.dtEdited = tblSocialCalendarEventsTypes.dtEdited;
                clsSocialCalendarEventsType.iEditedBy = tblSocialCalendarEventsTypes.iEditedBy;

                clsSocialCalendarEventsType.strTitle = tblSocialCalendarEventsTypes.strTitle;
                clsSocialCalendarEventsType.strDescription = tblSocialCalendarEventsTypes.strDescription;
                clsSocialCalendarEventsType.strColour = tblSocialCalendarEventsTypes.strColour;
                clsSocialCalendarEventsType.bIsDeleted = tblSocialCalendarEventsTypes.bIsDeleted;

                clsSocialCalendarEventsType.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();

                if (tblSocialCalendarEventsTypes.tblSocialCalendarEvents.Count > 0)
                {
                    foreach (var SocialCalendarEvent in tblSocialCalendarEventsTypes.tblSocialCalendarEvents)
                    {
                        clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.convertSocialCalendarEventsTableToClass(SocialCalendarEvent);
                        clsSocialCalendarEventsType.lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                    }
                }
            }

            return clsSocialCalendarEventsType;
        }

        //Get
        public clsSocialCalendarEventsTypes getSocialCalendarEventsTypeByID(int iSocialCalendarEventsTypeID)
        {
            clsSocialCalendarEventsTypes clsSocialCalendarEventsType = null;
            tblSocialCalendarEventsTypes tblSocialCalendarEventsTypes = db.tblSocialCalendarEventsTypes.FirstOrDefault(SocialCalendarEventsType => SocialCalendarEventsType.iSocialCalendarEventsTypeID == iSocialCalendarEventsTypeID && SocialCalendarEventsType.bIsDeleted == false);

            if (tblSocialCalendarEventsTypes != null)
            {
                //Managers
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();

                clsSocialCalendarEventsType = new clsSocialCalendarEventsTypes();

                clsSocialCalendarEventsType.iSocialCalendarEventsTypeID = tblSocialCalendarEventsTypes.iSocialCalendarEventsTypeID;
                clsSocialCalendarEventsType.dtAdded = tblSocialCalendarEventsTypes.dtAdded;
                clsSocialCalendarEventsType.iAddedBy = tblSocialCalendarEventsTypes.iAddedBy;
                clsSocialCalendarEventsType.dtEdited = tblSocialCalendarEventsTypes.dtEdited;
                clsSocialCalendarEventsType.iEditedBy = tblSocialCalendarEventsTypes.iEditedBy;

                clsSocialCalendarEventsType.strTitle = tblSocialCalendarEventsTypes.strTitle;
                clsSocialCalendarEventsType.strDescription = tblSocialCalendarEventsTypes.strDescription;
                clsSocialCalendarEventsType.strColour = tblSocialCalendarEventsTypes.strColour;
                clsSocialCalendarEventsType.bIsDeleted = tblSocialCalendarEventsTypes.bIsDeleted;

                clsSocialCalendarEventsType.lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
            }

            return clsSocialCalendarEventsType;
        }

        //Save
        public int saveSocialCalendarEventsType(clsSocialCalendarEventsTypes clsSocialCalendarEventsType)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblSocialCalendarEventsTypes tblSocialCalendarEventsTypes = new tblSocialCalendarEventsTypes();

            tblSocialCalendarEventsTypes.iSocialCalendarEventsTypeID = clsSocialCalendarEventsType.iSocialCalendarEventsTypeID;

            tblSocialCalendarEventsTypes.strTitle = clsSocialCalendarEventsType.strTitle;
            tblSocialCalendarEventsTypes.strDescription = clsSocialCalendarEventsType.strDescription;
            tblSocialCalendarEventsTypes.strColour = clsSocialCalendarEventsType.strColour;
            tblSocialCalendarEventsTypes.bIsDeleted = clsSocialCalendarEventsType.bIsDeleted;

            //Add
            if (tblSocialCalendarEventsTypes.iSocialCalendarEventsTypeID == 0)
            {
                tblSocialCalendarEventsTypes.dtAdded = DateTime.Now;
                //tblSocialCalendarEventsTypes.iAddedBy = clsCMSUser.iCMSUserID;
                tblSocialCalendarEventsTypes.iAddedBy = 1;
                tblSocialCalendarEventsTypes.dtEdited = DateTime.Now;
                //tblSocialCalendarEventsTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblSocialCalendarEventsTypes.iEditedBy = 1;

                db.tblSocialCalendarEventsTypes.Add(tblSocialCalendarEventsTypes);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblSocialCalendarEventsTypes.dtAdded = clsSocialCalendarEventsType.dtAdded;
                tblSocialCalendarEventsTypes.iAddedBy = clsSocialCalendarEventsType.iAddedBy;
                tblSocialCalendarEventsTypes.dtEdited = DateTime.Now;
                //tblSocialCalendarEventsTypes.iEditedBy = clsCMSUser.iCMSUserID;
                tblSocialCalendarEventsTypes.iEditedBy = 1;

                db.Set<tblSocialCalendarEventsTypes>().AddOrUpdate(tblSocialCalendarEventsTypes);
                db.SaveChanges();
            }
            //}
            return tblSocialCalendarEventsTypes.iSocialCalendarEventsTypeID;
        }

        //Remove
        public void removeSocialCalendarEventsTypeByID(int iSocialCalendarEventsTypeID)
        {
            tblSocialCalendarEventsTypes tblSocialCalendarEventsType = db.tblSocialCalendarEventsTypes.Find(iSocialCalendarEventsTypeID);
            if (tblSocialCalendarEventsType != null)
            {
                tblSocialCalendarEventsType.bIsDeleted = true;
                db.Entry(tblSocialCalendarEventsType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfSocialCalendarEventsTypeExists(int iSocialCalendarEventsTypeID)
        {
            bool bSocialCalendarEventsTypeExists = db.tblSocialCalendarEventsTypes.Any(SocialCalendarEventsType => SocialCalendarEventsType.iSocialCalendarEventsTypeID == iSocialCalendarEventsTypeID && SocialCalendarEventsType.bIsDeleted == false);
            return bSocialCalendarEventsTypeExists;
        }

        //Convert database table to class
        public clsSocialCalendarEventsTypes convertSocialCalendarEventsTypesTableToClass(tblSocialCalendarEventsTypes tblSocialCalendarEventsType)
        {
            clsSocialCalendarEventsTypes clsSocialCalendarEventsType = new clsSocialCalendarEventsTypes();

            clsSocialCalendarEventsType.iSocialCalendarEventsTypeID = tblSocialCalendarEventsType.iSocialCalendarEventsTypeID;
            clsSocialCalendarEventsType.dtAdded = tblSocialCalendarEventsType.dtAdded;
            clsSocialCalendarEventsType.iAddedBy = tblSocialCalendarEventsType.iAddedBy;
            clsSocialCalendarEventsType.dtEdited = tblSocialCalendarEventsType.dtEdited;
            clsSocialCalendarEventsType.iEditedBy = tblSocialCalendarEventsType.iEditedBy;

            clsSocialCalendarEventsType.strTitle = tblSocialCalendarEventsType.strTitle;
            clsSocialCalendarEventsType.strDescription = tblSocialCalendarEventsType.strDescription;
            clsSocialCalendarEventsType.strColour = tblSocialCalendarEventsType.strColour;
            clsSocialCalendarEventsType.bIsDeleted = tblSocialCalendarEventsType.bIsDeleted;

            return clsSocialCalendarEventsType;
        }
    }
}
