﻿using SoftservePortalNew.Assistant_Classes;
using SoftservePortalNew.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using Microsoft.ApplicationInsights;

namespace SoftservePortalNew.Model_Manager
{
    public class clsSocialCalendarEventsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsSocialCalendarEvents> getAllSocialCalendarEventsList()
        {
            var lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {
                //Managers
                var clsClientsManager = new clsClientsManager();
                var clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    var clsSocialCalendarEvent = new clsSocialCalendarEvents
                    {
                        iSocialCalendarEventID = item.iSocialCalendarEventID,
                        dtAdded = item.dtAdded,
                        iAddedBy = item.iAddedBy,
                        dtEdited = item.dtEdited,
                        iEditedBy = item.iEditedBy,
                        strTitle = item.strTitle,
                        strDescription = item.strDescription,
                        strKeywords = item.strKeywords,
                        strLinks = item.strLinks,
                        dtStartDate = item.dtStartDate,
                        dtEndDate = item.dtEndDate,
                        iClientID = item.iClientID,
                        iSocialCalendarEventsTypeID = item.iSocialCalendarEventsTypeID,
                        bIsRepeatEvent = item.bIsRepeatEvent,
                        strRepeatEventType = item.strRepeatEventType,
                        iRepeatEventStartDateID = item.iRepeatEventStartDateID,
                        dtRepeatEventStartDate = item.dtRepeatEventStartDate,
                        bIsDeleted = item.bIsDeleted
                    };

                    if (item.tblClients != null)
                        clsSocialCalendarEvent.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblSocialCalendarEventsTypes != null)
                        clsSocialCalendarEvent.clsSocialCalendarEventsType = clsSocialCalendarEventsTypesManager.convertSocialCalendarEventsTypesTableToClass(item.tblSocialCalendarEventsTypes);

                    lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                }
            }

            return lstSocialCalendarEvents;
        }

        public List<clsSocialCalendarEvents> getAllSocialCalendarEventsOnlyList()
        {
            var lstSocialCalendarEvents = new List<clsSocialCalendarEvents>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    var clsSocialCalendarEvent = new clsSocialCalendarEvents
                    {
                        iSocialCalendarEventID = item.iSocialCalendarEventID,
                        dtAdded = item.dtAdded,
                        iAddedBy = item.iAddedBy,
                        dtEdited = item.dtEdited,
                        iEditedBy = item.iEditedBy,
                        strTitle = item.strTitle,
                        strDescription = item.strDescription,
                        strKeywords = item.strKeywords,
                        strLinks = item.strLinks,
                        dtStartDate = item.dtStartDate,
                        dtEndDate = item.dtEndDate,
                        iClientID = item.iClientID,
                        iSocialCalendarEventsTypeID = item.iSocialCalendarEventsTypeID,
                        bIsRepeatEvent = item.bIsRepeatEvent,
                        strRepeatEventType = item.strRepeatEventType,
                        iRepeatEventStartDateID = item.iRepeatEventStartDateID,
                        dtRepeatEventStartDate = item.dtRepeatEventStartDate,
                        bIsDeleted = item.bIsDeleted
                    };

                    lstSocialCalendarEvents.Add(clsSocialCalendarEvent);
                }
            }

            return lstSocialCalendarEvents;
        }

        public List<clsSocialCalendarEventsItems> getAllSocialCalendarEventsItemsOnlyList()
        {
            var lstSocialCalendarEventsEventsItems = new List<clsSocialCalendarEventsItems>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    var clsSocialCalendarEventsItem = new clsSocialCalendarEventsItems
                    {
                        id = item.iSocialCalendarEventID.ToString(),
                        title = item.strTitle
                    };

                    if (item.dtEndDate != null)
                    {
                        var tsStartDateTimeOfDay = item.dtStartDate.TimeOfDay;
                        var tsEndDateTimeOfDay = Convert.ToDateTime(item.dtEndDate).TimeOfDay;
                        if (tsStartDateTimeOfDay.Hours == 0 && tsStartDateTimeOfDay.Minutes == 0 && tsStartDateTimeOfDay.Seconds == 0
                            && tsEndDateTimeOfDay.Hours == 0 && tsEndDateTimeOfDay.Minutes == 0 && tsEndDateTimeOfDay.Seconds == 0)
                        {
                            clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd");
                            clsSocialCalendarEventsItem.end = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd") + "T" + item.dtStartDate.ToString("HH:mm:ss");
                            clsSocialCalendarEventsItem.end = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(item.dtEndDate).ToString("HH:mm:ss");
                        }
                    }
                    else
                    {
                        clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd");
                        clsSocialCalendarEventsItem.end = null;
                    }
                    clsSocialCalendarEventsItem.color = item.tblSocialCalendarEventsTypes != null ? item.tblSocialCalendarEventsTypes.strColour : null;

                    lstSocialCalendarEventsEventsItems.Add(clsSocialCalendarEventsItem);
                }
            }

            return lstSocialCalendarEventsEventsItems;
        }

        public List<clsSocialCalendarEventsItems> getAllSocialCalendarEventsItemsByClientIDOnlyList(int iClientID)
        {
            List<clsSocialCalendarEventsItems> lstSocialCalendarEventsEventsItems = new List<clsSocialCalendarEventsItems>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.iClientID == iClientID && SocialCalendarEvent.bIsDeleted == false).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    clsSocialCalendarEventsItems clsSocialCalendarEventsItem = new clsSocialCalendarEventsItems();

                    clsSocialCalendarEventsItem.id = item.iSocialCalendarEventID.ToString();
                    clsSocialCalendarEventsItem.title = item.strTitle;
                    if (item.dtEndDate != null)
                    {
                        TimeSpan tsStartDateTimeOfDay = item.dtStartDate.TimeOfDay;
                        TimeSpan tsEndDateTimeOfDay = Convert.ToDateTime(item.dtEndDate).TimeOfDay;
                        if (tsStartDateTimeOfDay.Hours == 0 && tsStartDateTimeOfDay.Minutes == 0 && tsStartDateTimeOfDay.Seconds == 0
                            && tsEndDateTimeOfDay.Hours == 0 && tsEndDateTimeOfDay.Minutes == 0 && tsEndDateTimeOfDay.Seconds == 0)
                        {
                            clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd");
                            clsSocialCalendarEventsItem.end = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd") + "T" + item.dtStartDate.ToString("HH:mm:ss");
                            clsSocialCalendarEventsItem.end = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(item.dtEndDate).ToString("HH:mm:ss");
                        }
                    }
                    else
                    {
                        clsSocialCalendarEventsItem.start = item.dtStartDate.ToString("yyyy-MM-dd");
                        clsSocialCalendarEventsItem.end = null;
                    }
                    if (item.tblSocialCalendarEventsTypes != null)
                        clsSocialCalendarEventsItem.color = item.tblSocialCalendarEventsTypes.strColour;
                    else
                        clsSocialCalendarEventsItem.color = null;

                    lstSocialCalendarEventsEventsItems.Add(clsSocialCalendarEventsItem);
                }
            }

            return lstSocialCalendarEventsEventsItems;
        }

        public List<clsSocialCalendarEventsViewItems> getAllSocialCalendarEventsViewItemsOnlyList()
        {
            List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItems = new List<clsSocialCalendarEventsViewItems>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.bIsDeleted == false).OrderBy(SocialCalendarEvent => SocialCalendarEvent.dtStartDate).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    clsSocialCalendarEventsViewItems clsSocialCalendarEventsViewItem = new clsSocialCalendarEventsViewItems();

                    clsSocialCalendarEventsViewItem.title = item.strTitle;
                    clsSocialCalendarEventsViewItem.location = item.strDescription;
                    clsSocialCalendarEventsViewItem.eventType = (item.tblSocialCalendarEventsTypes != null) ? item.tblSocialCalendarEventsTypes.strTitle : "";

                    if (item.dtEndDate != null)
                    {
                        TimeSpan tsStartDateTimeOfDay = item.dtStartDate.TimeOfDay;
                        TimeSpan tsEndDateTimeOfDay = Convert.ToDateTime(item.dtEndDate).TimeOfDay;
                        if (tsStartDateTimeOfDay.Hours == 0 && tsStartDateTimeOfDay.Minutes == 0 && tsStartDateTimeOfDay.Seconds == 0
                            && tsEndDateTimeOfDay.Hours == 0 && tsEndDateTimeOfDay.Minutes == 0 && tsEndDateTimeOfDay.Seconds == 0)
                        {
                            clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                            //Custom Date
                            clsSocialCalendarEventsViewItem.startDate = Convert.ToDateTime(item.dtStartDate).ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                            clsSocialCalendarEventsViewItem.endDate = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                            //Custom Date
                            clsSocialCalendarEventsViewItem.startDate = Convert.ToDateTime(item.dtStartDate).ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                            //clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("yyyy-MM-dd") + "T" + item.dtStartDate.ToString("HH:mm:ss");
                            clsSocialCalendarEventsViewItem.endDate = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(item.dtEndDate).ToString("HH:mm:ss");
                        }
                    }
                    else
                    {
                        clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                        //Custom Date
                        clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                        //clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("yyyy-MM-dd");
                        clsSocialCalendarEventsViewItem.endDate = null;
                    }
                    //if (item.tblSocialCalendarEventsTypes != null)
                    //    clsSocialCalendarEventsItem.color = item.tblSocialCalendarEventsTypes.strColour;
                    //else
                    //    clsSocialCalendarEventsItem.color = null;

                    lstSocialCalendarEventsViewItems.Add(clsSocialCalendarEventsViewItem);
                }
            }

            return lstSocialCalendarEventsViewItems;
        }

        public List<clsSocialCalendarEventsViewItems> getAllSocialCalendarEventsViewItemsByClientIDOnlyList(int iClientID)
        {
            List<clsSocialCalendarEventsViewItems> lstSocialCalendarEventsViewItems = new List<clsSocialCalendarEventsViewItems>();
            var lstGetSocialCalendarEventsList = db.tblSocialCalendarEvents.Where(SocialCalendarEvent => SocialCalendarEvent.iClientID == iClientID && SocialCalendarEvent.bIsDeleted == false).OrderBy(SocialCalendarEvent => SocialCalendarEvent.dtStartDate).ToList();

            if (lstGetSocialCalendarEventsList.Count > 0)
            {

                foreach (var item in lstGetSocialCalendarEventsList)
                {
                    clsSocialCalendarEventsViewItems clsSocialCalendarEventsViewItem = new clsSocialCalendarEventsViewItems();

                    clsSocialCalendarEventsViewItem.title = item.strTitle;
                    clsSocialCalendarEventsViewItem.location = item.strDescription;
                    clsSocialCalendarEventsViewItem.eventType = (item.tblSocialCalendarEventsTypes != null) ? item.tblSocialCalendarEventsTypes.strTitle : "";

                    if (item.dtEndDate != null)
                    {
                        TimeSpan tsStartDateTimeOfDay = item.dtStartDate.TimeOfDay;
                        TimeSpan tsEndDateTimeOfDay = Convert.ToDateTime(item.dtEndDate).TimeOfDay;
                        if (tsStartDateTimeOfDay.Hours == 0 && tsStartDateTimeOfDay.Minutes == 0 && tsStartDateTimeOfDay.Seconds == 0
                            && tsEndDateTimeOfDay.Hours == 0 && tsEndDateTimeOfDay.Minutes == 0 && tsEndDateTimeOfDay.Seconds == 0)
                        {
                            clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                            //Custom Date
                            clsSocialCalendarEventsViewItem.startDate = Convert.ToDateTime(item.dtStartDate).ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                            clsSocialCalendarEventsViewItem.endDate = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                            //Custom Date
                            clsSocialCalendarEventsViewItem.startDate = Convert.ToDateTime(item.dtStartDate).ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                            //clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("yyyy-MM-dd") + "T" + item.dtStartDate.ToString("HH:mm:ss");
                            clsSocialCalendarEventsViewItem.endDate = Convert.ToDateTime(item.dtEndDate).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(item.dtEndDate).ToString("HH:mm:ss");
                        }
                    }
                    else
                    {
                        clsSocialCalendarEventsViewItem.date = item.dtStartDate.ToString("yyyy-MM-dd");
                        //Custom Date
                        clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("dd MMMM yyyy") + " (" + item.dtStartDate.DayOfWeek.ToString() + ")";
                        //clsSocialCalendarEventsViewItem.startDate = item.dtStartDate.ToString("yyyy-MM-dd");
                        clsSocialCalendarEventsViewItem.endDate = null;
                    }
                    //if (item.tblSocialCalendarEventsTypes != null)
                    //    clsSocialCalendarEventsItem.color = item.tblSocialCalendarEventsTypes.strColour;
                    //else
                    //    clsSocialCalendarEventsItem.color = null;

                    lstSocialCalendarEventsViewItems.Add(clsSocialCalendarEventsViewItem);
                }
            }

            return lstSocialCalendarEventsViewItems;
        }

        //Get
        public clsSocialCalendarEvents getSocialCalendarEventByID(int iSocialCalendarEventID)
        {
            clsSocialCalendarEvents clsSocialCalendarEvent = null;
            var tblSocialCalendarEvent = db.tblSocialCalendarEvents.FirstOrDefault(SocialCalendarEvent => SocialCalendarEvent.iSocialCalendarEventID == iSocialCalendarEventID && SocialCalendarEvent.bIsDeleted == false);

            if (tblSocialCalendarEvent != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsSocialCalendarEventsTypesManager clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();

                clsSocialCalendarEvent = new clsSocialCalendarEvents();

                clsSocialCalendarEvent.iSocialCalendarEventID = tblSocialCalendarEvent.iSocialCalendarEventID;
                clsSocialCalendarEvent.dtAdded = tblSocialCalendarEvent.dtAdded;
                clsSocialCalendarEvent.iAddedBy = tblSocialCalendarEvent.iAddedBy;
                clsSocialCalendarEvent.dtEdited = tblSocialCalendarEvent.dtEdited;
                clsSocialCalendarEvent.iEditedBy = tblSocialCalendarEvent.iEditedBy;

                clsSocialCalendarEvent.strTitle = tblSocialCalendarEvent.strTitle;
                clsSocialCalendarEvent.strDescription = tblSocialCalendarEvent.strDescription;
                clsSocialCalendarEvent.strKeywords = tblSocialCalendarEvent.strKeywords;
                clsSocialCalendarEvent.strLinks = tblSocialCalendarEvent.strLinks;
                clsSocialCalendarEvent.dtStartDate = tblSocialCalendarEvent.dtStartDate;

                clsSocialCalendarEvent.dtEndDate = tblSocialCalendarEvent.dtEndDate;
                clsSocialCalendarEvent.iClientID = tblSocialCalendarEvent.iClientID;
                clsSocialCalendarEvent.iSocialCalendarEventsTypeID = tblSocialCalendarEvent.iSocialCalendarEventsTypeID;
                clsSocialCalendarEvent.bIsRepeatEvent = tblSocialCalendarEvent.bIsRepeatEvent;
                clsSocialCalendarEvent.strRepeatEventType = tblSocialCalendarEvent.strRepeatEventType;

                clsSocialCalendarEvent.iRepeatEventStartDateID = tblSocialCalendarEvent.iRepeatEventStartDateID;
                clsSocialCalendarEvent.dtRepeatEventStartDate = tblSocialCalendarEvent.dtRepeatEventStartDate;
                clsSocialCalendarEvent.bIsDeleted = tblSocialCalendarEvent.bIsDeleted;

                if (tblSocialCalendarEvent.tblClients != null)
                    clsSocialCalendarEvent.clsClient = clsClientsManager.convertClientsTableToClass(tblSocialCalendarEvent.tblClients);
                if (tblSocialCalendarEvent.tblSocialCalendarEventsTypes != null)
                    clsSocialCalendarEvent.clsSocialCalendarEventsType = clsSocialCalendarEventsTypesManager.convertSocialCalendarEventsTypesTableToClass(tblSocialCalendarEvent.tblSocialCalendarEventsTypes);
            }

            return clsSocialCalendarEvent;
        }

        //Get
        public clsSocialCalendarEvents getSocialCalendarEvent(string strTitle, DateTime dtStartDate, DateTime dtEndDate)
        {
            clsSocialCalendarEvents clsSocialCalendarEvent = null;
            var tblSocialCalendarEvent = db.tblSocialCalendarEvents.FirstOrDefault(SocialCalendarEvent => SocialCalendarEvent.strTitle.ToLower() == strTitle.ToLower()
            && SocialCalendarEvent.dtStartDate == dtStartDate && SocialCalendarEvent.dtEndDate == dtEndDate && SocialCalendarEvent.bIsDeleted == false);

            if (tblSocialCalendarEvent != null)
            {
                //Managers
                var clsClientsManager = new clsClientsManager();
                var clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();

                clsSocialCalendarEvent = new clsSocialCalendarEvents
                {
                    iSocialCalendarEventID = tblSocialCalendarEvent.iSocialCalendarEventID,
                    dtAdded = tblSocialCalendarEvent.dtAdded,
                    iAddedBy = tblSocialCalendarEvent.iAddedBy,
                    dtEdited = tblSocialCalendarEvent.dtEdited,
                    iEditedBy = tblSocialCalendarEvent.iEditedBy,
                    strTitle = tblSocialCalendarEvent.strTitle,
                    strDescription = tblSocialCalendarEvent.strDescription,
                    strKeywords = tblSocialCalendarEvent.strKeywords,
                    strLinks = tblSocialCalendarEvent.strLinks,
                    dtStartDate = tblSocialCalendarEvent.dtStartDate,
                    dtEndDate = tblSocialCalendarEvent.dtEndDate,
                    iClientID = tblSocialCalendarEvent.iClientID,
                    iSocialCalendarEventsTypeID = tblSocialCalendarEvent.iSocialCalendarEventsTypeID,
                    bIsRepeatEvent = tblSocialCalendarEvent.bIsRepeatEvent,
                    strRepeatEventType = tblSocialCalendarEvent.strRepeatEventType,
                    iRepeatEventStartDateID = tblSocialCalendarEvent.iRepeatEventStartDateID,
                    dtRepeatEventStartDate = tblSocialCalendarEvent.dtRepeatEventStartDate,
                    bIsDeleted = tblSocialCalendarEvent.bIsDeleted
                };





                if (tblSocialCalendarEvent.tblClients != null)
                    clsSocialCalendarEvent.clsClient = clsClientsManager.convertClientsTableToClass(tblSocialCalendarEvent.tblClients);
                if (tblSocialCalendarEvent.tblSocialCalendarEventsTypes != null)
                    clsSocialCalendarEvent.clsSocialCalendarEventsType = clsSocialCalendarEventsTypesManager.convertSocialCalendarEventsTypesTableToClass(tblSocialCalendarEvent.tblSocialCalendarEventsTypes);
            }

            return clsSocialCalendarEvent;
        }

        //Save
        public int saveSocialCalendarEvent(clsSocialCalendarEvents clsSocialCalendarEvent)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            var tblSocialCalendarEvent = new tblSocialCalendarEvents
            {
                iSocialCalendarEventID = clsSocialCalendarEvent.iSocialCalendarEventID,
                strTitle = clsSocialCalendarEvent.strTitle,
                strDescription = clsSocialCalendarEvent.strDescription,
                strKeywords = clsSocialCalendarEvent.strKeywords,
                strLinks = clsSocialCalendarEvent.strLinks,
                dtStartDate = clsSocialCalendarEvent.dtStartDate,
                dtEndDate = clsSocialCalendarEvent.dtEndDate,
                iClientID = clsSocialCalendarEvent.iClientID,
                iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID,
                bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent,
                strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType,
                iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID,
                dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate,
                bIsDeleted = clsSocialCalendarEvent.bIsDeleted
            };
                var user = HttpContext.Current.Session["clsUser"] as clsUsers;
            //Add
            if (tblSocialCalendarEvent.iSocialCalendarEventID == 0)
            {
                tblSocialCalendarEvent.dtAdded = DateTime.Now;
                tblSocialCalendarEvent.iAddedBy = user.iUserID; // TODO: Check who and grab from sesh
                tblSocialCalendarEvent.dtEdited = DateTime.Now;
                tblSocialCalendarEvent.iEditedBy = user.iUserID; // TODO: Check who and grab from sesh

                CreateNotification(14, tblSocialCalendarEvent); // ID 14 for Event Added

                db.tblSocialCalendarEvents.Add(tblSocialCalendarEvent);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                tblSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                tblSocialCalendarEvent.dtEdited = DateTime.Now;
                tblSocialCalendarEvent.iEditedBy = user.iUserID; // TODO: Check who and grab from sesh

                CreateNotification(15, tblSocialCalendarEvent); // ID 15 for Event Edited

                db.Set<tblSocialCalendarEvents>().AddOrUpdate(tblSocialCalendarEvent);
                db.SaveChanges();
            }
            return tblSocialCalendarEvent.iSocialCalendarEventID;
        }

        //Remove
        public void removeSocialCalendarByID(int iSocialCalendarEventID)
        {
            tblSocialCalendarEvents tblSocialCalendarEvent = db.tblSocialCalendarEvents.Find(iSocialCalendarEventID);
            if (tblSocialCalendarEvent != null)
            {
                tblSocialCalendarEvent.bIsDeleted = true;
                db.Entry(tblSocialCalendarEvent).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfSocialCalendarEventExists(int iSocialCalendarEventID)
        {
            bool bSocialCalendarEventExists = db.tblSocialCalendarEvents.Any(SocialCalendarEvent => SocialCalendarEvent.iSocialCalendarEventID == iSocialCalendarEventID && SocialCalendarEvent.bIsDeleted == false);
            return bSocialCalendarEventExists;
        }

        //Check
        public bool checkIfSocialCalendarEventExists(string strTitle, DateTime dtStartDate, DateTime dtEndDate)
        {
            return db.tblSocialCalendarEvents.Any(SocialCalendarEvent => SocialCalendarEvent.strTitle.ToLower() == strTitle.ToLower()
            && SocialCalendarEvent.dtStartDate == dtStartDate && SocialCalendarEvent.dtEndDate == dtEndDate && SocialCalendarEvent.bIsDeleted == false);
        }

        ////Check With Event Type
        public bool checkIfSocialCalendarEventExists(string strTitle, DateTime dtStartDate, DateTime dtEndDate, int iEventType)
        {
            return db.tblSocialCalendarEvents.Any(SocialCalendarEvent => SocialCalendarEvent.strTitle.ToLower() == strTitle.ToLower()
            && SocialCalendarEvent.dtStartDate == dtStartDate && SocialCalendarEvent.dtEndDate == dtEndDate
            && SocialCalendarEvent.iSocialCalendarEventsTypeID == iEventType && SocialCalendarEvent.bIsDeleted == false);
        }

        //Convert database table to class
        public clsSocialCalendarEvents convertSocialCalendarEventsTableToClass(tblSocialCalendarEvents tblSocialCalendarEvent)
        {
            clsSocialCalendarEvents clsSocialCalendarEvent = new clsSocialCalendarEvents();

            clsSocialCalendarEvent.iSocialCalendarEventID = tblSocialCalendarEvent.iSocialCalendarEventID;
            clsSocialCalendarEvent.dtAdded = tblSocialCalendarEvent.dtAdded;
            clsSocialCalendarEvent.iAddedBy = tblSocialCalendarEvent.iAddedBy;
            clsSocialCalendarEvent.dtEdited = tblSocialCalendarEvent.dtEdited;
            clsSocialCalendarEvent.iEditedBy = tblSocialCalendarEvent.iEditedBy;

            clsSocialCalendarEvent.strTitle = tblSocialCalendarEvent.strTitle;
            clsSocialCalendarEvent.strDescription = tblSocialCalendarEvent.strDescription;
            clsSocialCalendarEvent.strKeywords = tblSocialCalendarEvent.strKeywords;
            clsSocialCalendarEvent.strLinks = tblSocialCalendarEvent.strLinks;
            clsSocialCalendarEvent.dtStartDate = tblSocialCalendarEvent.dtStartDate;

            clsSocialCalendarEvent.dtEndDate = tblSocialCalendarEvent.dtEndDate;
            clsSocialCalendarEvent.iClientID = tblSocialCalendarEvent.iClientID;
            clsSocialCalendarEvent.iSocialCalendarEventsTypeID = tblSocialCalendarEvent.iSocialCalendarEventsTypeID;
            clsSocialCalendarEvent.bIsRepeatEvent = tblSocialCalendarEvent.bIsRepeatEvent;
            clsSocialCalendarEvent.strRepeatEventType = tblSocialCalendarEvent.strRepeatEventType;

            clsSocialCalendarEvent.iRepeatEventStartDateID = tblSocialCalendarEvent.iRepeatEventStartDateID;
            clsSocialCalendarEvent.dtRepeatEventStartDate = tblSocialCalendarEvent.dtRepeatEventStartDate;
            clsSocialCalendarEvent.bIsDeleted = tblSocialCalendarEvent.bIsDeleted;

            return clsSocialCalendarEvent;
        }

        private static void CreateNotification(int Type, tblSocialCalendarEvents tblSocialCalendarEvent)
        {
            var clsNoteTypesManager = new clsNotificationTypesManager();
            var clsNotesManager = new clsNotificationsManager();
            var clsClientManager = new clsClientsManager();
            var clsUserManager = new clsUsersManager();
            var clsNoteType = clsNoteTypesManager.getNotificationTypeByID(Type);

            var clsClient = new clsClients();
            var clsUser = new clsUsers();

            var user = HttpContext.Current.Session["clsUser"] as clsUsers;

            if (tblSocialCalendarEvent.iClientID.HasValue)
            {
                clsClient = clsClientManager.getClientByID(tblSocialCalendarEvent.iClientID.Value);
                clsUser = clsUserManager.getUserByID(user.iUserID); // TODO: grab this ID from SESH
            }
            clsNoteType.strDescription = clsNoteType.strDescription.Replace(":,", $": {clsClient.strUsername},");
            clsNoteType.strDescription = clsNoteType.strDescription.Replace("by:", $"by: {clsUser.strFirstName}");


            var clsNotification = new clsNotifications()
            {
                dtAdded = tblSocialCalendarEvent.dtAdded,
                iAddedBy = clsClient.iClientID, // TODO: Add check to see whether it is a request or not
                dtEdited = tblSocialCalendarEvent.dtEdited,
                iEditedBy = clsUser.iUserID, // TODO: Add check to see whether it is a request or not

                iClientID = clsClient.iClientID,
                iUserID = clsUser.iUserID,
                iNotificationTypeID = clsNoteType.iNotificationTypeID,
                strDescription = clsNoteType.strDescription,
                strTitle = clsNoteType.strTitle,
                strComment = "Test Comment", //Todo: fix this up

                bIsDeleted = false,
                bIsRead = false
            };

            clsNotesManager.saveNotification(clsNotification);
        }
    }
}
