﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsTaskTagsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsTaskTags> getAllTaskTagsList()
        {
            List<clsTaskTags> lstTaskTags = new List<clsTaskTags>();
            var lstGetTaskTagsList = db.tblTaskTags.Where(TaskTag => TaskTag.bIsDeleted == false).ToList();

            if (lstGetTaskTagsList.Count > 0)
            {
                //Managers
                clsTasksManager clsTasksManager = new clsTasksManager();

                foreach (var item in lstGetTaskTagsList)
                {
                    clsTaskTags clsTaskTag = new clsTaskTags();

                    clsTaskTag.iTaskTagID = item.iTaskTagID;
                    clsTaskTag.dtAdded = item.dtAdded;
                    clsTaskTag.iAddedBy = item.iAddedBy;
                    clsTaskTag.dtEdited = item.dtEdited;
                    clsTaskTag.iEditedBy = item.iEditedBy;

                    clsTaskTag.strTitle = item.strTitle;
                    clsTaskTag.strDescription = item.strDescription;
                    clsTaskTag.bIsDeleted = item.bIsDeleted;

                    clsTaskTag.lstTasks = new List<clsTasks>();

                    if (item.tblTasks.Count > 0)
                    {
                        foreach (var TaskItem in item.tblTasks)
                        {
                            clsTasks clsTask = clsTasksManager.convertTasksTableToClass(TaskItem);
                            clsTaskTag.lstTasks.Add(clsTask);
                        }
                    }

                    lstTaskTags.Add(clsTaskTag);
                }
            }

            return lstTaskTags;
        }

        public List<clsTaskTags> getAllTaskTagsOnlyList()
        {
            List<clsTaskTags> lstTaskTags = new List<clsTaskTags>();
            var lstGetTaskTagsList = db.tblTaskTags.Where(TaskTag => TaskTag.bIsDeleted == false).ToList();

            if (lstGetTaskTagsList.Count > 0)
            {

                foreach (var item in lstGetTaskTagsList)
                {
                    clsTaskTags clsTaskTag = new clsTaskTags();

                    clsTaskTag.iTaskTagID = item.iTaskTagID;
                    clsTaskTag.dtAdded = item.dtAdded;
                    clsTaskTag.iAddedBy = item.iAddedBy;
                    clsTaskTag.dtEdited = item.dtEdited;
                    clsTaskTag.iEditedBy = item.iEditedBy;

                    clsTaskTag.strTitle = item.strTitle;
                    clsTaskTag.strDescription = item.strDescription;
                    clsTaskTag.bIsDeleted = item.bIsDeleted;

                    clsTaskTag.lstTasks = new List<clsTasks>();

                    lstTaskTags.Add(clsTaskTag);
                }
            }

            return lstTaskTags;
        }

        //Get
        public clsTaskTags getTaskTagByID(int iTaskTagID)
        {
            clsTaskTags clsTaskTag = null;
            tblTaskTags tblTaskTags = db.tblTaskTags.FirstOrDefault(TaskTag => TaskTag.iTaskTagID == iTaskTagID && TaskTag.bIsDeleted == false);

            if (tblTaskTags != null)
            {
                //Managers
                clsTasksManager clsTasksManager = new clsTasksManager();

                clsTaskTag = new clsTaskTags();

                clsTaskTag.iTaskTagID = tblTaskTags.iTaskTagID;
                clsTaskTag.dtAdded = tblTaskTags.dtAdded;
                clsTaskTag.iAddedBy = tblTaskTags.iAddedBy;
                clsTaskTag.dtEdited = tblTaskTags.dtEdited;
                clsTaskTag.iEditedBy = tblTaskTags.iEditedBy;

                clsTaskTag.strTitle = tblTaskTags.strTitle;
                clsTaskTag.strDescription = tblTaskTags.strDescription;
                clsTaskTag.bIsDeleted = tblTaskTags.bIsDeleted;

                clsTaskTag.lstTasks = new List<clsTasks>();

                if (tblTaskTags.tblTasks.Count > 0)
                {
                    foreach (var TaskItem in tblTaskTags.tblTasks)
                    {
                        clsTasks clsTask = clsTasksManager.convertTasksTableToClass(TaskItem);
                        clsTaskTag.lstTasks.Add(clsTask);
                    }
                }
            }

            return clsTaskTag;
        }

        //Save
        public int saveTaskTag(clsTaskTags clsTaskTag)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblTaskTags tblTaskTags = new tblTaskTags();

            tblTaskTags.iTaskTagID = clsTaskTag.iTaskTagID;

            tblTaskTags.strTitle = clsTaskTag.strTitle;
            tblTaskTags.strDescription = clsTaskTag.strDescription;
            tblTaskTags.bIsDeleted = clsTaskTag.bIsDeleted;

            //Add
            if (tblTaskTags.iTaskTagID == 0)
            {
                tblTaskTags.dtAdded = DateTime.Now;
                //tblTaskTags.iAddedBy = clsCMSUser.iCMSUserID;
                tblTaskTags.iAddedBy = 1;
                tblTaskTags.dtEdited = DateTime.Now;
                //tblTaskTags.iEditedBy = clsCMSUser.iCMSUserID;
                tblTaskTags.iEditedBy = 1;

                db.tblTaskTags.Add(tblTaskTags);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblTaskTags.dtAdded = clsTaskTag.dtAdded;
                tblTaskTags.iAddedBy = clsTaskTag.iAddedBy;
                tblTaskTags.dtEdited = DateTime.Now;
                //tblTaskTags.iEditedBy = clsCMSUser.iCMSUserID;
                tblTaskTags.iEditedBy = 1;

                db.Set<tblTaskTags>().AddOrUpdate(tblTaskTags);
                db.SaveChanges();
            }
            //}
            return tblTaskTags.iTaskTagID;
        }

        //Remove
        public void removeTaskTagByID(int iTaskTagID)
        {
            tblTaskTags tblTaskTag = db.tblTaskTags.Find(iTaskTagID);
            if (tblTaskTag != null)
            {
                tblTaskTag.bIsDeleted = true;
                db.Entry(tblTaskTag).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfTaskTagExists(int iTaskTagID)
        {
            bool bTaskTagExists = db.tblTaskTags.Any(TaskTag => TaskTag.iTaskTagID == iTaskTagID && TaskTag.bIsDeleted == false);
            return bTaskTagExists;
        }

        //Convert database table to class
        public clsTaskTags convertTaskTagsTableToClass(tblTaskTags tblTaskTag)
        {
            clsTaskTags clsTaskTag = new clsTaskTags();

            clsTaskTag.iTaskTagID = tblTaskTag.iTaskTagID;
            clsTaskTag.dtAdded = tblTaskTag.dtAdded;
            clsTaskTag.iAddedBy = tblTaskTag.iAddedBy;
            clsTaskTag.dtEdited = tblTaskTag.dtEdited;
            clsTaskTag.iEditedBy = tblTaskTag.iEditedBy;

            clsTaskTag.strTitle = tblTaskTag.strTitle;
            clsTaskTag.strDescription = tblTaskTag.strDescription;
            clsTaskTag.bIsDeleted = tblTaskTag.bIsDeleted;

            return clsTaskTag;
        }
    }
}
