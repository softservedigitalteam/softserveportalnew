﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsBlogPostsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsBlogPosts> getAllBlogPostsList()
        {
            List<clsBlogPosts> lstBlogPosts = new List<clsBlogPosts>();
            var lstGetBlogPostsList = db.tblBlogPosts.Where(BlogPost => BlogPost.bIsDeleted == false).ToList();

            if (lstGetBlogPostsList.Count > 0)
            {
                //Managers
                clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
                //clsBlogPostsBlogTagsLinkTableManager clsBlogPostsBlogTagsLinkTableManager = new clsBlogPostsBlogTagsLinkTableManager();

                foreach (var item in lstGetBlogPostsList)
                {
                    clsBlogPosts clsBlogPost = new clsBlogPosts();

                    clsBlogPost.iBlogPostID = item.iBlogPostID;
                    clsBlogPost.dtAdded = item.dtAdded;
                    clsBlogPost.iAddedBy = item.iAddedBy;
                    clsBlogPost.dtEdited = item.dtEdited;
                    clsBlogPost.iEditedBy = item.iEditedBy;

                    clsBlogPost.strTitle = item.strTitle;
                    clsBlogPost.strTagLine = item.strTagLine;
                    clsBlogPost.strDescription = item.strDescription;
                    clsBlogPost.iBlogTypeID = item.iBlogTypeID;
                    clsBlogPost.strPathToImages = item.strPathToImages;

                    clsBlogPost.strMasterImage = item.strMasterImage;
                    clsBlogPost.strPathToAudio = item.strPathToAudio;
                    clsBlogPost.strMasterAudio = item.strMasterAudio;
                    clsBlogPost.strAudioURLLink = item.strAudioURLLink;
                    clsBlogPost.strPathToVideos = item.strPathToVideos;

                    clsBlogPost.strMasterVideo = item.strMasterVideo;
                    clsBlogPost.strVideoURLLink = item.strVideoURLLink;
                    clsBlogPost.dtReleaseDate = item.dtReleaseDate;
                    clsBlogPost.strTags = item.strTags;
                    clsBlogPost.bDisplay = item.bDisplay;
                    clsBlogPost.bIsDeleted = item.bIsDeleted;

                    //clsBlogPost.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                    if (item.tblBlogTypes != null)
                        clsBlogPost.clsBlogType = clsBlogTypesManager.convertBlogTypesTableToClass(item.tblBlogTypes);
                    //if (item.tblBlogPostsBlogTagsLinkTable.Count > 0)
                    //{
                    //    foreach (var BlogPostsBlogTagsLinkTableItem in item.tblBlogPostsBlogTagsLinkTable)
                    //    {
                    //        clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = clsBlogPostsBlogTagsLinkTableManager.convertBlogPostsBlogTagsLinkTableTableToClass(BlogPostsBlogTagsLinkTableItem);
                    //        clsBlogPost.lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
                    //    }
                    //}

                    lstBlogPosts.Add(clsBlogPost);
                }
            }

            return lstBlogPosts;
        }

        public List<clsBlogPosts> getAllBlogPostsOnlyList()
        {
            List<clsBlogPosts> lstBlogPosts = new List<clsBlogPosts>();
            var lstGetBlogPostsList = db.tblBlogPosts.Where(BlogPost => BlogPost.bIsDeleted == false).ToList();

            if (lstGetBlogPostsList.Count > 0)
            {

                foreach (var item in lstGetBlogPostsList)
                {
                    clsBlogPosts clsBlogPost = new clsBlogPosts();

                    clsBlogPost.iBlogPostID = item.iBlogPostID;
                    clsBlogPost.dtAdded = item.dtAdded;
                    clsBlogPost.iAddedBy = item.iAddedBy;
                    clsBlogPost.dtEdited = item.dtEdited;
                    clsBlogPost.iEditedBy = item.iEditedBy;

                    clsBlogPost.strTitle = item.strTitle;
                    clsBlogPost.strTagLine = item.strTagLine;
                    clsBlogPost.strDescription = item.strDescription;
                    clsBlogPost.iBlogTypeID = item.iBlogTypeID;
                    clsBlogPost.strPathToImages = item.strPathToImages;

                    clsBlogPost.strMasterImage = item.strMasterImage;
                    clsBlogPost.strPathToAudio = item.strPathToAudio;
                    clsBlogPost.strMasterAudio = item.strMasterAudio;
                    clsBlogPost.strAudioURLLink = item.strAudioURLLink;
                    clsBlogPost.strPathToVideos = item.strPathToVideos;

                    clsBlogPost.strMasterVideo = item.strMasterVideo;
                    clsBlogPost.strVideoURLLink = item.strVideoURLLink;
                    clsBlogPost.dtReleaseDate = item.dtReleaseDate;
                    clsBlogPost.strTags = item.strTags;
                    clsBlogPost.bDisplay = item.bDisplay;
                    clsBlogPost.bIsDeleted = item.bIsDeleted;

                    //clsBlogPost.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                    lstBlogPosts.Add(clsBlogPost);
                }
            }

            return lstBlogPosts;
        }

        //Get
        public clsBlogPosts getBlogPostByID(int iBlogPostID)
        {
            clsBlogPosts clsBlogPost = null;
            tblBlogPosts tblBlogPosts = db.tblBlogPosts.FirstOrDefault(BlogPost => BlogPost.iBlogPostID == iBlogPostID && BlogPost.bIsDeleted == false);

            if (tblBlogPosts != null)
            {
                //Managers
                clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
                //clsBlogPostsBlogTagsLinkTableManager clsBlogPostsBlogTagsLinkTableManager = new clsBlogPostsBlogTagsLinkTableManager();

                clsBlogPost = new clsBlogPosts();

                clsBlogPost.iBlogPostID = tblBlogPosts.iBlogPostID;
                clsBlogPost.dtAdded = tblBlogPosts.dtAdded;
                clsBlogPost.iAddedBy = tblBlogPosts.iAddedBy;
                clsBlogPost.dtEdited = tblBlogPosts.dtEdited;
                clsBlogPost.iEditedBy = tblBlogPosts.iEditedBy;

                clsBlogPost.strTitle = tblBlogPosts.strTitle;
                clsBlogPost.strTagLine = tblBlogPosts.strTagLine;
                clsBlogPost.strDescription = tblBlogPosts.strDescription;
                clsBlogPost.iBlogTypeID = tblBlogPosts.iBlogTypeID;
                clsBlogPost.strPathToImages = tblBlogPosts.strPathToImages;

                clsBlogPost.strMasterImage = tblBlogPosts.strMasterImage;
                clsBlogPost.strPathToAudio = tblBlogPosts.strPathToAudio;
                clsBlogPost.strMasterAudio = tblBlogPosts.strMasterAudio;
                clsBlogPost.strAudioURLLink = tblBlogPosts.strAudioURLLink;
                clsBlogPost.strPathToVideos = tblBlogPosts.strPathToVideos;

                clsBlogPost.strMasterVideo = tblBlogPosts.strMasterVideo;
                clsBlogPost.strVideoURLLink = tblBlogPosts.strVideoURLLink;
                clsBlogPost.dtReleaseDate = tblBlogPosts.dtReleaseDate;
                clsBlogPost.strTags = tblBlogPosts.strTags;
                clsBlogPost.bDisplay = tblBlogPosts.bDisplay;
                clsBlogPost.bIsDeleted = tblBlogPosts.bIsDeleted;

                //clsBlogPost.lstBlogPostsBlogTagsLinkTable = new List<clsBlogPostsBlogTagsLinkTable>();

                if (tblBlogPosts.tblBlogTypes != null)
                    clsBlogPost.clsBlogType = clsBlogTypesManager.convertBlogTypesTableToClass(tblBlogPosts.tblBlogTypes);
                //if (tblBlogPosts.tblBlogPostsBlogTagsLinkTable.Count > 0)
                //{
                //    foreach (var BlogPostsBlogTagsLinkTableItem in tblBlogPosts.tblBlogPostsBlogTagsLinkTable)
                //    {
                //        clsBlogPostsBlogTagsLinkTable clsBlogPostsBlogTagsLinkTable = clsBlogPostsBlogTagsLinkTableManager.convertBlogPostsBlogTagsLinkTableTableToClass(BlogPostsBlogTagsLinkTableItem);
                //        clsBlogPost.lstBlogPostsBlogTagsLinkTable.Add(clsBlogPostsBlogTagsLinkTable);
                //    }
                //}
            }

            return clsBlogPost;
        }

        //Save
        public int saveBlogPost(clsBlogPosts clsBlogPost)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblBlogPosts tblBlogPosts = new tblBlogPosts();

            tblBlogPosts.iBlogPostID = clsBlogPost.iBlogPostID;

            tblBlogPosts.strTitle = clsBlogPost.strTitle;
            tblBlogPosts.strTagLine = clsBlogPost.strTagLine;
            tblBlogPosts.strDescription = clsBlogPost.strDescription;
            tblBlogPosts.iBlogTypeID = clsBlogPost.iBlogTypeID;
            tblBlogPosts.strPathToImages = clsBlogPost.strPathToImages;

            tblBlogPosts.strMasterImage = clsBlogPost.strMasterImage;
            tblBlogPosts.strPathToAudio = clsBlogPost.strPathToAudio;
            tblBlogPosts.strMasterAudio = clsBlogPost.strMasterAudio;
            tblBlogPosts.strAudioURLLink = clsBlogPost.strAudioURLLink;
            tblBlogPosts.strPathToVideos = clsBlogPost.strPathToVideos;

            tblBlogPosts.strMasterVideo = clsBlogPost.strMasterVideo;
            tblBlogPosts.strVideoURLLink = clsBlogPost.strVideoURLLink;
            tblBlogPosts.dtReleaseDate = clsBlogPost.dtReleaseDate;
            tblBlogPosts.strTags = clsBlogPost.strTags;
            tblBlogPosts.bDisplay = clsBlogPost.bDisplay;
            tblBlogPosts.bIsDeleted = clsBlogPost.bIsDeleted;

            //Add
            if (tblBlogPosts.iBlogPostID == 0)
            {
                tblBlogPosts.dtAdded = DateTime.Now;
                //tblBlogPosts.iAddedBy = clsCMSUser.iCMSUserID;
                tblBlogPosts.iAddedBy = 1;
                tblBlogPosts.dtEdited = DateTime.Now;
                //tblBlogPosts.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogPosts.iEditedBy = 1;

                db.tblBlogPosts.Add(tblBlogPosts);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblBlogPosts.dtAdded = clsBlogPost.dtAdded;
                tblBlogPosts.iAddedBy = clsBlogPost.iAddedBy;
                tblBlogPosts.dtEdited = DateTime.Now;
                //tblBlogPosts.iEditedBy = clsCMSUser.iCMSUserID;
                tblBlogPosts.iEditedBy = 1;

                db.Set<tblBlogPosts>().AddOrUpdate(tblBlogPosts);
                db.SaveChanges();
            }
            //}
            return tblBlogPosts.iBlogPostID;
        }

        //Remove
        public void removeBlogPostByID(int iBlogPostID)
        {
            tblBlogPosts tblBlogPost = db.tblBlogPosts.Find(iBlogPostID);
            if (tblBlogPost != null)
            {
                tblBlogPost.bIsDeleted = true;
                db.Entry(tblBlogPost).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfBlogPostExists(int iBlogPostID)
        {
            bool bBlogPostExists = db.tblBlogPosts.Any(BlogPost => BlogPost.iBlogPostID == iBlogPostID && BlogPost.bIsDeleted == false);
            return bBlogPostExists;
        }

        //Convert database table to class
        public clsBlogPosts convertBlogPostsTableToClass(tblBlogPosts tblBlogPost)
        {
            clsBlogPosts clsBlogPost = new clsBlogPosts();

            clsBlogPost.iBlogPostID = tblBlogPost.iBlogPostID;
            clsBlogPost.dtAdded = tblBlogPost.dtAdded;
            clsBlogPost.iAddedBy = tblBlogPost.iAddedBy;
            clsBlogPost.dtEdited = tblBlogPost.dtEdited;
            clsBlogPost.iEditedBy = tblBlogPost.iEditedBy;

            clsBlogPost.strTitle = tblBlogPost.strTitle;
            clsBlogPost.strTagLine = tblBlogPost.strTagLine;
            clsBlogPost.strDescription = tblBlogPost.strDescription;
            clsBlogPost.iBlogTypeID = tblBlogPost.iBlogTypeID;
            clsBlogPost.strPathToImages = tblBlogPost.strPathToImages;

            clsBlogPost.strMasterImage = tblBlogPost.strMasterImage;
            clsBlogPost.strPathToAudio = tblBlogPost.strPathToAudio;
            clsBlogPost.strMasterAudio = tblBlogPost.strMasterAudio;
            clsBlogPost.strAudioURLLink = tblBlogPost.strAudioURLLink;
            clsBlogPost.strPathToVideos = tblBlogPost.strPathToVideos;

            clsBlogPost.strMasterVideo = tblBlogPost.strMasterVideo;
            clsBlogPost.strVideoURLLink = tblBlogPost.strVideoURLLink;
            clsBlogPost.dtReleaseDate = tblBlogPost.dtReleaseDate;
            clsBlogPost.strTags = tblBlogPost.strTags;
            clsBlogPost.bDisplay = tblBlogPost.bDisplay;
            clsBlogPost.bIsDeleted = tblBlogPost.bIsDeleted;

            return clsBlogPost;
        }
    }
}
