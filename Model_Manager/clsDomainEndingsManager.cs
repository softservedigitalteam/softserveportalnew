﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsDomainEndingsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsDomainEndings> getAllDomainEndingsList()
        {
            List<clsDomainEndings> lstDomainEndings = new List<clsDomainEndings>();
            var lstGetDomainEndingsList = db.tblDomainEndings.Where(DomainEnding => DomainEnding.bIsDeleted == false).ToList();

            if (lstGetDomainEndingsList.Count > 0)
            {
                //Managers
                clsDomainsManager clsDomainsManager = new clsDomainsManager();

                foreach (var item in lstGetDomainEndingsList)
                {
                    clsDomainEndings clsDomainEnding = new clsDomainEndings();

                    clsDomainEnding.iDomainEndingID = item.iDomainEndingID;
                    clsDomainEnding.dtAdded = item.dtAdded;
                    clsDomainEnding.iAddedBy = item.iAddedBy;
                    clsDomainEnding.dtEdited = item.dtEdited;
                    clsDomainEnding.iEditedBy = item.iEditedBy;

                    clsDomainEnding.strTitle = item.strTitle;
                    clsDomainEnding.bIsDeleted = item.bIsDeleted;

                    clsDomainEnding.lstDomains = new List<clsDomains>();

                    if (item.tblDomains.Count > 0)
                    {
                        foreach (var Domain in item.tblDomains)
                        {
                            if (Domain.bIsDeleted != true)
                            {
                                clsDomains clsDomain = clsDomainsManager.convertDomainsTableToClass(Domain);
                                clsDomainEnding.lstDomains.Add(clsDomain);
                            }
                            
                        }
                    }

                    lstDomainEndings.Add(clsDomainEnding);
                }
            }

            return lstDomainEndings;
        }

        public List<clsDomainEndings> getAllDomainEndingsOnlyList()
        {
            List<clsDomainEndings> lstDomainEndings = new List<clsDomainEndings>();
            var lstGetDomainEndingsList = db.tblDomainEndings.Where(DomainEnding => DomainEnding.bIsDeleted == false).ToList();

            if (lstGetDomainEndingsList.Count > 0)
            {

                foreach (var item in lstGetDomainEndingsList)
                {
                    clsDomainEndings clsDomainEnding = new clsDomainEndings();

                    clsDomainEnding.iDomainEndingID = item.iDomainEndingID;
                    clsDomainEnding.dtAdded = item.dtAdded;
                    clsDomainEnding.iAddedBy = item.iAddedBy;
                    clsDomainEnding.dtEdited = item.dtEdited;
                    clsDomainEnding.iEditedBy = item.iEditedBy;

                    clsDomainEnding.strTitle = item.strTitle;
                    clsDomainEnding.bIsDeleted = item.bIsDeleted;

                    clsDomainEnding.lstDomains = new List<clsDomains>();

                    lstDomainEndings.Add(clsDomainEnding);
                }
            }

            return lstDomainEndings;
        }

        //Get
        public clsDomainEndings getDomainEndingByID(int iDomainEndingID)
        {
            clsDomainEndings clsDomainEnding = null;
            tblDomainEndings tblDomainEndings = db.tblDomainEndings.FirstOrDefault(DomainEnding => DomainEnding.iDomainEndingID == iDomainEndingID && DomainEnding.bIsDeleted == false);

            if (tblDomainEndings != null)
            {
                //Managers
                clsDomainsManager clsDomainsManager = new clsDomainsManager();

                clsDomainEnding = new clsDomainEndings();

                clsDomainEnding.iDomainEndingID = tblDomainEndings.iDomainEndingID;
                clsDomainEnding.dtAdded = tblDomainEndings.dtAdded;
                clsDomainEnding.iAddedBy = tblDomainEndings.iAddedBy;
                clsDomainEnding.dtEdited = tblDomainEndings.dtEdited;
                clsDomainEnding.iEditedBy = tblDomainEndings.iEditedBy;

                clsDomainEnding.strTitle = tblDomainEndings.strTitle;
                clsDomainEnding.bIsDeleted = tblDomainEndings.bIsDeleted;

                clsDomainEnding.lstDomains = new List<clsDomains>();

                if (tblDomainEndings.tblDomains.Count > 0)
                {
                    foreach (var Domain in tblDomainEndings.tblDomains)
                    {
                        if (Domain.bIsDeleted != true)
                        {
                            clsDomains clsDomain = clsDomainsManager.convertDomainsTableToClass(Domain);
                            clsDomainEnding.lstDomains.Add(clsDomain);
                        }

                    }
                }
            }

            return clsDomainEnding;
        }

        //Save
        public int saveDomainEnding(clsDomainEndings clsDomainEnding)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblDomainEndings tblDomainEndings = new tblDomainEndings();

            tblDomainEndings.iDomainEndingID = clsDomainEnding.iDomainEndingID;

            tblDomainEndings.strTitle = clsDomainEnding.strTitle;
            tblDomainEndings.bIsDeleted = clsDomainEnding.bIsDeleted;

            //Add
            if (tblDomainEndings.iDomainEndingID == 0)
            {
                tblDomainEndings.dtAdded = DateTime.Now;
                //tblDomainEndings.iAddedBy = clsCMSUser.iCMSUserID;
                tblDomainEndings.iAddedBy = 1;
                tblDomainEndings.dtEdited = DateTime.Now;
                //tblDomainEndings.iEditedBy = clsCMSUser.iCMSUserID;
                tblDomainEndings.iEditedBy = 1;

                db.tblDomainEndings.Add(tblDomainEndings);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblDomainEndings.dtAdded = clsDomainEnding.dtAdded;
                tblDomainEndings.iAddedBy = clsDomainEnding.iAddedBy;
                tblDomainEndings.dtEdited = DateTime.Now;
                //tblDomainEndings.iEditedBy = clsCMSUser.iCMSUserID;
                tblDomainEndings.iEditedBy = 1;

                db.Set<tblDomainEndings>().AddOrUpdate(tblDomainEndings);
                db.SaveChanges();
            }
            //}
            return tblDomainEndings.iDomainEndingID;
        }

        //Remove
        public void removeDomainEndingByID(int iDomainEndingID)
        {
            tblDomainEndings tblDomainEnding = db.tblDomainEndings.Find(iDomainEndingID);
            if (tblDomainEnding != null)
            {
                tblDomainEnding.bIsDeleted = true;
                db.Entry(tblDomainEnding).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfDomainEndingExists(string strTitle)
        {
            bool bDomainEndingExists = db.tblDomainEndings.Any(DomainEnding => DomainEnding.strTitle.ToLower() == strTitle.ToLower() && DomainEnding.bIsDeleted == false);
            return bDomainEndingExists;
        }

        //Convert database table to class
        public clsDomainEndings convertDomainEndingsTableToClass(tblDomainEndings tblDomainEnding)
        {
            clsDomainEndings clsDomainEnding = new clsDomainEndings();

            clsDomainEnding.iDomainEndingID = tblDomainEnding.iDomainEndingID;
            clsDomainEnding.dtAdded = tblDomainEnding.dtAdded;
            clsDomainEnding.iAddedBy = tblDomainEnding.iAddedBy;
            clsDomainEnding.dtEdited = tblDomainEnding.dtEdited;
            clsDomainEnding.iEditedBy = tblDomainEnding.iEditedBy;

            clsDomainEnding.strTitle = tblDomainEnding.strTitle;
            clsDomainEnding.bIsDeleted = tblDomainEnding.bIsDeleted;

            return clsDomainEnding;
        }
    }
}
