﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsTasksManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsTasks> getAllTasksList()
        {
            List<clsTasks> lstTasks = new List<clsTasks>();
            var lstGetTasksList = db.tblTasks.Where(Task => Task.bIsDeleted == false).ToList();

            if (lstGetTasksList.Count > 0)
            {
                //Managers
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                foreach (var item in lstGetTasksList)
                {
                    clsTasks clsTask = new clsTasks();

                    clsTask.iTaskID = item.iTaskID;
                    clsTask.dtAdded = item.dtAdded;
                    clsTask.iAddedBy = item.iAddedBy;
                    clsTask.dtEdited = item.dtEdited;
                    clsTask.iEditedBy = item.iEditedBy;

                    clsTask.strTaskName = item.strTaskName;
                    clsTask.strDuration = item.strDuration;
                    clsTask.dtTimeSessionElapsed = item.dtTimeSessionElapsed;
                    clsTask.bIsTaskcompleted = item.bIsTaskcompleted;
                    clsTask.iAsanaTaskID = item.iAsanaTaskID;

                    clsTask.iProjectID = item.iProjectID;
                    clsTask.iTaskTagID = item.iTaskTagID;
                    clsTask.bIsDeleted = item.bIsDeleted;

                    if (item.tblTaskTags != null)
                        clsTask.clsTaskTag = clsTaskTagsManager.convertTaskTagsTableToClass(item.tblTaskTags);
                    if (item.tblProjects != null)
                        clsTask.clsProject = clsProjectsManager.convertProjectsTableToClass(item.tblProjects);

                    lstTasks.Add(clsTask);
                }
            }

            return lstTasks;
        }

        public List<clsTasks> getAllTasksOnlyList()
        {
            List<clsTasks> lstTasks = new List<clsTasks>();
            var lstGetTasksList = db.tblTasks.Where(Task => Task.bIsDeleted == false).ToList();

            if (lstGetTasksList.Count > 0)
            {

                foreach (var item in lstGetTasksList)
                {
                    clsTasks clsTask = new clsTasks();

                    clsTask.iTaskID = item.iTaskID;
                    clsTask.dtAdded = item.dtAdded;
                    clsTask.iAddedBy = item.iAddedBy;
                    clsTask.dtEdited = item.dtEdited;
                    clsTask.iEditedBy = item.iEditedBy;

                    clsTask.strTaskName = item.strTaskName;
                    clsTask.strDuration = item.strDuration;
                    clsTask.dtTimeSessionElapsed = item.dtTimeSessionElapsed;
                    clsTask.bIsTaskcompleted = item.bIsTaskcompleted;
                    clsTask.iAsanaTaskID = item.iAsanaTaskID;

                    clsTask.iProjectID = item.iProjectID;
                    clsTask.iTaskTagID = item.iTaskTagID;
                    clsTask.bIsDeleted = item.bIsDeleted;

                    lstTasks.Add(clsTask);
                }
            }

            return lstTasks;
        }

        public List<clsTasks> getAllTasksListByProjectID(int iProjectID)
        {
            List<clsTasks> lstTasks = new List<clsTasks>();
            var lstGetTasksList = db.tblTasks.Where(Task => Task.iProjectID == iProjectID && Task.bIsDeleted == false).ToList();

            if (lstGetTasksList.Count > 0)
            {
                //Managers
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                foreach (var item in lstGetTasksList)
                {
                    clsTasks clsTask = new clsTasks();

                    clsTask.iTaskID = item.iTaskID;
                    clsTask.dtAdded = item.dtAdded;
                    clsTask.iAddedBy = item.iAddedBy;
                    clsTask.dtEdited = item.dtEdited;
                    clsTask.iEditedBy = item.iEditedBy;

                    clsTask.strTaskName = item.strTaskName;
                    clsTask.strDuration = item.strDuration;
                    clsTask.dtTimeSessionElapsed = item.dtTimeSessionElapsed;
                    clsTask.bIsTaskcompleted = item.bIsTaskcompleted;
                    clsTask.iAsanaTaskID = item.iAsanaTaskID;

                    clsTask.iProjectID = item.iProjectID;
                    clsTask.iTaskTagID = item.iTaskTagID;
                    clsTask.bIsDeleted = item.bIsDeleted;

                    if (item.tblTaskTags != null)
                        clsTask.clsTaskTag = clsTaskTagsManager.convertTaskTagsTableToClass(item.tblTaskTags);
                    if (item.tblProjects != null)
                        clsTask.clsProject = clsProjectsManager.convertProjectsTableToClass(item.tblProjects);

                    lstTasks.Add(clsTask);
                }
            }

            return lstTasks;
        }

        public List<clsTasks> getAllTasksListByTaskTagID(int iTaskTagID)
        {
            List<clsTasks> lstTasks = new List<clsTasks>();
            var lstGetTasksList = db.tblTasks.Where(Task => Task.iTaskTagID == iTaskTagID && Task.bIsDeleted == false).ToList();

            if (lstGetTasksList.Count > 0)
            {
                //Managers
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                foreach (var item in lstGetTasksList)
                {
                    clsTasks clsTask = new clsTasks();

                    clsTask.iTaskID = item.iTaskID;
                    clsTask.dtAdded = item.dtAdded;
                    clsTask.iAddedBy = item.iAddedBy;
                    clsTask.dtEdited = item.dtEdited;
                    clsTask.iEditedBy = item.iEditedBy;

                    clsTask.strTaskName = item.strTaskName;
                    clsTask.strDuration = item.strDuration;
                    clsTask.dtTimeSessionElapsed = item.dtTimeSessionElapsed;
                    clsTask.bIsTaskcompleted = item.bIsTaskcompleted;
                    clsTask.iAsanaTaskID = item.iAsanaTaskID;

                    clsTask.iProjectID = item.iProjectID;
                    clsTask.iTaskTagID = item.iTaskTagID;
                    clsTask.bIsDeleted = item.bIsDeleted;

                    if (item.tblTaskTags != null)
                        clsTask.clsTaskTag = clsTaskTagsManager.convertTaskTagsTableToClass(item.tblTaskTags);
                    if (item.tblProjects != null)
                        clsTask.clsProject = clsProjectsManager.convertProjectsTableToClass(item.tblProjects);

                    lstTasks.Add(clsTask);
                }
            }

            return lstTasks;
        }

        //Get
        public clsTasks getTaskByID(int iTaskID)
        {
            clsTasks clsTask = null;
            tblTasks tblTasks = db.tblTasks.FirstOrDefault(Task => Task.iTaskID == iTaskID && Task.bIsDeleted == false);

            if (tblTasks != null)
            {
                //Managers
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                clsTask = new clsTasks();

                clsTask.iTaskID = tblTasks.iTaskID;
                clsTask.dtAdded = tblTasks.dtAdded;
                clsTask.iAddedBy = tblTasks.iAddedBy;
                clsTask.dtEdited = tblTasks.dtEdited;
                clsTask.iEditedBy = tblTasks.iEditedBy;

                clsTask.strTaskName = tblTasks.strTaskName;
                clsTask.strDuration = tblTasks.strDuration;
                clsTask.dtTimeSessionElapsed = tblTasks.dtTimeSessionElapsed;
                clsTask.bIsTaskcompleted = tblTasks.bIsTaskcompleted;
                clsTask.iAsanaTaskID = tblTasks.iAsanaTaskID;

                clsTask.iProjectID = tblTasks.iProjectID;
                clsTask.iTaskTagID = tblTasks.iTaskTagID;
                clsTask.bIsDeleted = tblTasks.bIsDeleted;

                if (tblTasks.tblTaskTags != null)
                    clsTask.clsTaskTag = clsTaskTagsManager.convertTaskTagsTableToClass(tblTasks.tblTaskTags);
                if (tblTasks.tblProjects != null)
                    clsTask.clsProject = clsProjectsManager.convertProjectsTableToClass(tblTasks.tblProjects);
            }

            return clsTask;
        }

        //Get
        public clsTasks getTaskByIDAndTaskTagID(int iTaskID, int iTaskTagID)
        {
            clsTasks clsTask = null;
            tblTasks tblTasks = db.tblTasks.FirstOrDefault(Task => Task.iTaskID == iTaskID && Task.iTaskTagID == iTaskTagID && Task.bIsDeleted == false);

            if (tblTasks != null)
            {
                //Managers
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();

                clsTask = new clsTasks();

                clsTask.iTaskID = tblTasks.iTaskID;
                clsTask.dtAdded = tblTasks.dtAdded;
                clsTask.iAddedBy = tblTasks.iAddedBy;
                clsTask.dtEdited = tblTasks.dtEdited;
                clsTask.iEditedBy = tblTasks.iEditedBy;

                clsTask.strTaskName = tblTasks.strTaskName;
                clsTask.strDuration = tblTasks.strDuration;
                clsTask.dtTimeSessionElapsed = tblTasks.dtTimeSessionElapsed;
                clsTask.bIsTaskcompleted = tblTasks.bIsTaskcompleted;
                clsTask.iAsanaTaskID = tblTasks.iAsanaTaskID;

                clsTask.iProjectID = tblTasks.iProjectID;
                clsTask.iTaskTagID = tblTasks.iTaskTagID;
                clsTask.bIsDeleted = tblTasks.bIsDeleted;

                if (tblTasks.tblTaskTags != null)
                    clsTask.clsTaskTag = clsTaskTagsManager.convertTaskTagsTableToClass(tblTasks.tblTaskTags);
                if (tblTasks.tblProjects != null)
                    clsTask.clsProject = clsProjectsManager.convertProjectsTableToClass(tblTasks.tblProjects);
            }

            return clsTask;
        }

        //Save
        public int saveTask(clsTasks clsTask)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblTasks tblTasks = new tblTasks();

            tblTasks.iTaskID = clsTask.iTaskID;

            tblTasks.strTaskName = clsTask.strTaskName;
            tblTasks.strDuration = clsTask.strDuration;
            tblTasks.dtTimeSessionElapsed = clsTask.dtTimeSessionElapsed;
            tblTasks.bIsTaskcompleted = clsTask.bIsTaskcompleted;
            tblTasks.iAsanaTaskID = clsTask.iAsanaTaskID;

            tblTasks.iProjectID = clsTask.iProjectID;
            tblTasks.iTaskTagID = clsTask.iTaskTagID;
            tblTasks.bIsDeleted = clsTask.bIsDeleted;

            //Add
            if (tblTasks.iTaskID == 0)
            {
                tblTasks.dtAdded = DateTime.Now;
                //tblTasks.iAddedBy = clsCMSUser.iCMSUserID;
                tblTasks.iAddedBy = 1;
                tblTasks.dtEdited = DateTime.Now;
                //tblTasks.iEditedBy = clsCMSUser.iCMSUserID;
                tblTasks.iEditedBy = 1;

                db.tblTasks.Add(tblTasks);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblTasks.dtAdded = clsTask.dtAdded;
                tblTasks.iAddedBy = clsTask.iAddedBy;
                tblTasks.dtEdited = DateTime.Now;
                //tblTasks.iEditedBy = clsCMSUser.iCMSUserID;
                tblTasks.iEditedBy = 1;

                db.Set<tblTasks>().AddOrUpdate(tblTasks);
                db.SaveChanges();
            }
            //}
            return tblTasks.iTaskID;
        }

        public int saveClientChangeLogTask(clsTasks clsTask)
        {
            tblTasks tblTasks = new tblTasks();

            tblTasks.iTaskID = clsTask.iTaskID;
            tblTasks.dtAdded = clsTask.dtAdded;
            tblTasks.iAddedBy = clsTask.iAddedBy;
            tblTasks.dtEdited = clsTask.dtEdited;
            tblTasks.iEditedBy = clsTask.iEditedBy;

            tblTasks.strTaskName = clsTask.strTaskName;
            tblTasks.strDuration = clsTask.strDuration;
            tblTasks.dtTimeSessionElapsed = clsTask.dtTimeSessionElapsed;
            tblTasks.bIsTaskcompleted = clsTask.bIsTaskcompleted;
            tblTasks.iAsanaTaskID = clsTask.iAsanaTaskID;

            tblTasks.iProjectID = clsTask.iProjectID;
            tblTasks.iTaskTagID = clsTask.iTaskTagID;
            tblTasks.bIsDeleted = clsTask.bIsDeleted;

            if (tblTasks.iTaskID == 0)
            {
                db.tblTasks.Add(tblTasks);
                db.SaveChanges();
            }
            else
            {
                db.Set<tblTasks>().AddOrUpdate(tblTasks);
                db.SaveChanges();
            }

            return tblTasks.iTaskID;
        }

        //Remove
        public void removeTaskByID(int iTaskID)
        {
            tblTasks tblTask = db.tblTasks.Find(iTaskID);
            if (tblTask != null)
            {
                tblTask.bIsDeleted = true;
                db.Entry(tblTask).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfTaskExists(int iTaskID)
        {
            bool bTaskExists = db.tblTasks.Any(Task => Task.iTaskID == iTaskID && Task.bIsDeleted == false);
            return bTaskExists;
        }

        //Convert database table to class
        public clsTasks convertTasksTableToClass(tblTasks tblTask)
        {
            clsTasks clsTask = new clsTasks();

            clsTask.iTaskID = tblTask.iTaskID;
            clsTask.dtAdded = tblTask.dtAdded;
            clsTask.iAddedBy = tblTask.iAddedBy;
            clsTask.dtEdited = tblTask.dtEdited;
            clsTask.iEditedBy = tblTask.iEditedBy;

            clsTask.strTaskName = tblTask.strTaskName;
            clsTask.strDuration = tblTask.strDuration;
            clsTask.dtTimeSessionElapsed = tblTask.dtTimeSessionElapsed;
            clsTask.bIsTaskcompleted = tblTask.bIsTaskcompleted;
            clsTask.iAsanaTaskID = tblTask.iAsanaTaskID;

            clsTask.iProjectID = tblTask.iProjectID;
            clsTask.iTaskTagID = tblTask.iTaskTagID;
            clsTask.bIsDeleted = tblTask.bIsDeleted;

            return clsTask;
        }
    }
}
