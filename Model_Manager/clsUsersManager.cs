﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsUsersManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsUsers> getAllUsersList()
        {
            List<clsUsers> lstUsers = new List<clsUsers>();
            var lstGetUsersList = db.tblUsers.Where(User => User.bIsDeleted == false).ToList();

            if (lstGetUsersList.Count > 0)
            {
                //Managers

                foreach (var item in lstGetUsersList)
                {
                    clsUsers clsUser = new clsUsers();

                    clsUser.iUserID = item.iUserID;
                    clsUser.dtAdded = item.dtAdded;
                    clsUser.iAddedBy = item.iAddedBy;
                    clsUser.dtEdited = item.dtEdited;
                    clsUser.iEditedBy = item.iEditedBy;

                    clsUser.strFirstName = item.strFirstName;
                    clsUser.strSurname = item.strSurname;
                    clsUser.strEmailAddress = item.strEmailAddress;
                    clsUser.strPassword = item.strPassword;
                    clsUser.strClientID = item.strClientID;

                    clsUser.strClientSecret = item.strClientSecret;
                    clsUser.strRedirectURL = item.strRedirectURL;
                    clsUser.strRefreshToken = item.strRefreshToken;
                    clsUser.strPathToImages = item.strPathToImages;
                    clsUser.strMasterImage = item.strMasterImage;
                    clsUser.bIsDeleted = item.bIsDeleted;

                    lstUsers.Add(clsUser);
                }
            }

            return lstUsers;
        }

        public List<clsUsers> getAllUsersOnlyList()
        {
            List<clsUsers> lstUsers = new List<clsUsers>();
            var lstGetUsersList = db.tblUsers.Where(User => User.bIsDeleted == false).ToList();

            if (lstGetUsersList.Count > 0)
            {

                foreach (var item in lstGetUsersList)
                {
                    clsUsers clsUser = new clsUsers();

                    clsUser.iUserID = item.iUserID;
                    clsUser.dtAdded = item.dtAdded;
                    clsUser.iAddedBy = item.iAddedBy;
                    clsUser.dtEdited = item.dtEdited;
                    clsUser.iEditedBy = item.iEditedBy;

                    clsUser.strFirstName = item.strFirstName;
                    clsUser.strSurname = item.strSurname;
                    clsUser.strEmailAddress = item.strEmailAddress;
                    clsUser.strPassword = item.strPassword;
                    clsUser.strClientID = item.strClientID;

                    clsUser.strClientSecret = item.strClientSecret;
                    clsUser.strRedirectURL = item.strRedirectURL;
                    clsUser.strRefreshToken = item.strRefreshToken;
                    clsUser.strPathToImages = item.strPathToImages;
                    clsUser.strMasterImage = item.strMasterImage;
                    clsUser.bIsDeleted = item.bIsDeleted;

                    lstUsers.Add(clsUser);
                }
            }

            return lstUsers;
        }

        //Get
        public clsUsers getUserByID(int iUserID)
        {
            clsUsers clsUser = null;
            tblUsers tblUsers = db.tblUsers.FirstOrDefault(User => User.iUserID == iUserID && User.bIsDeleted == false);

            if (tblUsers != null)
            {
                //Managers

                clsUser = new clsUsers();

                clsUser.iUserID = tblUsers.iUserID;
                clsUser.dtAdded = tblUsers.dtAdded;
                clsUser.iAddedBy = tblUsers.iAddedBy;
                clsUser.dtEdited = tblUsers.dtEdited;
                clsUser.iEditedBy = tblUsers.iEditedBy;

                clsUser.strFirstName = tblUsers.strFirstName;
                clsUser.strSurname = tblUsers.strSurname;
                clsUser.strEmailAddress = tblUsers.strEmailAddress;
                clsUser.strPassword = tblUsers.strPassword;
                clsUser.strClientID = tblUsers.strClientID;

                clsUser.strClientSecret = tblUsers.strClientSecret;
                clsUser.strRedirectURL = tblUsers.strRedirectURL;
                clsUser.strRefreshToken = tblUsers.strRefreshToken;
                clsUser.strPathToImages = tblUsers.strPathToImages;
                clsUser.strMasterImage = tblUsers.strMasterImage;
                clsUser.bIsDeleted = tblUsers.bIsDeleted;
            }

            return clsUser;
        }

        public clsUsers getUserByEmail(string strEmailAddress)
        {
            clsUsers clsUser = null;
            tblUsers tblUsers = db.tblUsers.FirstOrDefault(User => User.strEmailAddress.ToLower() == strEmailAddress.ToLower() && User.bIsDeleted == false);

            if (tblUsers != null)
            {
                //Managers

                clsUser = new clsUsers();

                clsUser.iUserID = tblUsers.iUserID;
                clsUser.dtAdded = tblUsers.dtAdded;
                clsUser.iAddedBy = tblUsers.iAddedBy;
                clsUser.dtEdited = tblUsers.dtEdited;
                clsUser.iEditedBy = tblUsers.iEditedBy;

                clsUser.strFirstName = tblUsers.strFirstName;
                clsUser.strSurname = tblUsers.strSurname;
                clsUser.strEmailAddress = tblUsers.strEmailAddress;
                clsUser.strPassword = tblUsers.strPassword;
                clsUser.strClientID = tblUsers.strClientID;

                clsUser.strClientSecret = tblUsers.strClientSecret;
                clsUser.strRedirectURL = tblUsers.strRedirectURL;
                clsUser.strRefreshToken = tblUsers.strRefreshToken;
                clsUser.strPathToImages = tblUsers.strPathToImages;
                clsUser.strMasterImage = tblUsers.strMasterImage;
                clsUser.bIsDeleted = tblUsers.bIsDeleted;
            }

            return clsUser;
        }

        //Save
        public int saveUser(clsUsers clsUser)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblUsers tblUsers = new tblUsers();

            tblUsers.iUserID = clsUser.iUserID;

            tblUsers.strFirstName = clsUser.strFirstName;
            tblUsers.strSurname = clsUser.strSurname;
            tblUsers.strEmailAddress = clsUser.strEmailAddress.ToLower();
            tblUsers.strPassword = clsUser.strPassword;
            tblUsers.strClientID = clsUser.strClientID;

            tblUsers.strClientSecret = clsUser.strClientSecret;
            tblUsers.strRedirectURL = clsUser.strRedirectURL;
            tblUsers.strRefreshToken = clsUser.strRefreshToken;
            tblUsers.strPathToImages = clsUser.strPathToImages;
            tblUsers.strMasterImage = clsUser.strMasterImage;
            tblUsers.bIsDeleted = clsUser.bIsDeleted;

            //Add
            if (tblUsers.iUserID == 0)
            {
                tblUsers.dtAdded = DateTime.Now;
                //tblUsers.iAddedBy = clsCMSUser.iCMSUserID;
                tblUsers.iAddedBy = 1;
                tblUsers.dtEdited = DateTime.Now;
                //tblUsers.iEditedBy = clsCMSUser.iCMSUserID;
                tblUsers.iEditedBy = 1;

                db.tblUsers.Add(tblUsers);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblUsers.dtAdded = clsUser.dtAdded;
                tblUsers.iAddedBy = clsUser.iAddedBy;
                tblUsers.dtEdited = DateTime.Now;
                //tblUsers.iEditedBy = clsCMSUser.iCMSUserID;
                tblUsers.iEditedBy = 1;

                db.Set<tblUsers>().AddOrUpdate(tblUsers);
                db.SaveChanges();
            }
            //}
            return tblUsers.iUserID;
        }

        //Remove
        public void removeUserByID(int iUserID)
        {
            tblUsers tblUser = db.tblUsers.Find(iUserID);
            if (tblUser != null)
            {
                tblUser.bIsDeleted = true;
                db.Entry(tblUser).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfUserExists(int iUserID)
        {
            bool bUserExists = db.tblUsers.Any(User => User.iUserID == iUserID && User.bIsDeleted == false);
            return bUserExists;
        }

        //Convert database table to class
        public clsUsers convertUsersTableToClass(tblUsers tblUser)
        {
            clsUsers clsUser = new clsUsers();

            clsUser.iUserID = tblUser.iUserID;
            clsUser.dtAdded = tblUser.dtAdded;
            clsUser.iAddedBy = tblUser.iAddedBy;
            clsUser.dtEdited = tblUser.dtEdited;
            clsUser.iEditedBy = tblUser.iEditedBy;

            clsUser.strFirstName = tblUser.strFirstName;
            clsUser.strSurname = tblUser.strSurname;
            clsUser.strEmailAddress = tblUser.strEmailAddress;
            clsUser.strPassword = tblUser.strPassword;
            clsUser.strClientID = tblUser.strClientID;

            clsUser.strClientSecret = tblUser.strClientSecret;
            clsUser.strRedirectURL = tblUser.strRedirectURL;
            clsUser.strRefreshToken = tblUser.strRefreshToken;
            clsUser.strPathToImages = tblUser.strPathToImages;
            clsUser.strMasterImage = tblUser.strMasterImage;
            clsUser.bIsDeleted = tblUser.bIsDeleted;

            return clsUser;
        }
    }
}
