﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Model_Manager
{
    public class clsProjectsManager
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        //Get All
        public List<clsProjects> getAllProjectsList()
        {
            List<clsProjects> lstProjects = new List<clsProjects>();
            var lstGetProjectsList = db.tblProjects.Where(Project => Project.bIsDeleted == false).ToList();

            if (lstGetProjectsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                foreach (var item in lstGetProjectsList)
                {
                    clsProjects clsProject = new clsProjects();

                    clsProject.iProjectID = item.iProjectID;
                    clsProject.dtAdded = item.dtAdded;
                    clsProject.iAddedBy = item.iAddedBy;
                    clsProject.dtEdited = item.dtEdited;
                    clsProject.iEditedBy = item.iEditedBy;

                    clsProject.strAsanaProjectID = item.strAsanaProjectID;
                    clsProject.strTitle = item.strTitle;
                    clsProject.strDescription = item.strDescription;
                    clsProject.iClientID = item.iClientID;
                    clsProject.bIsDeleted = item.bIsDeleted;

                    clsProject.lstTasks = new List<clsTasks>();

                    if (item.tblClients != null)
                        clsProject.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblTasks.Count > 0)
                    {
                        foreach (tblTasks Task in item.tblTasks)
                        {
                            clsTasks clsTask = clsTasksManager.convertTasksTableToClass(Task);
                            clsProject.lstTasks.Add(clsTask);
                        }
                    }

                    lstProjects.Add(clsProject);
                }
            }

            return lstProjects;
        }

        public List<clsProjects> getAllProjectsOnlyList()
        {
            List<clsProjects> lstProjects = new List<clsProjects>();
            var lstGetProjectsList = db.tblProjects.Where(Project => Project.bIsDeleted == false).ToList();

            if (lstGetProjectsList.Count > 0)
            {

                foreach (var item in lstGetProjectsList)
                {
                    clsProjects clsProject = new clsProjects();

                    clsProject.iProjectID = item.iProjectID;
                    clsProject.dtAdded = item.dtAdded;
                    clsProject.iAddedBy = item.iAddedBy;
                    clsProject.dtEdited = item.dtEdited;
                    clsProject.iEditedBy = item.iEditedBy;

                    clsProject.strAsanaProjectID = item.strAsanaProjectID;
                    clsProject.strTitle = item.strTitle;
                    clsProject.strDescription = item.strDescription;
                    clsProject.iClientID = item.iClientID;
                    clsProject.bIsDeleted = item.bIsDeleted;

                    clsProject.lstTasks = new List<clsTasks>();

                    lstProjects.Add(clsProject);
                }
            }

            return lstProjects;
        }

        public List<clsProjects> getAllProjectsListByClientID(int iClientID)
        {
            List<clsProjects> lstProjects = new List<clsProjects>();
            var lstGetProjectsList = db.tblProjects.Where(Project => Project.iClientID == iClientID && Project.bIsDeleted == false).ToList();

            if (lstGetProjectsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                foreach (var item in lstGetProjectsList)
                {
                    clsProjects clsProject = new clsProjects();

                    clsProject.iProjectID = item.iProjectID;
                    clsProject.dtAdded = item.dtAdded;
                    clsProject.iAddedBy = item.iAddedBy;
                    clsProject.dtEdited = item.dtEdited;
                    clsProject.iEditedBy = item.iEditedBy;

                    clsProject.strAsanaProjectID = item.strAsanaProjectID;
                    clsProject.strTitle = item.strTitle;
                    clsProject.strDescription = item.strDescription;
                    clsProject.iClientID = item.iClientID;
                    clsProject.bIsDeleted = item.bIsDeleted;

                    clsProject.lstTasks = new List<clsTasks>();

                    if (item.tblClients != null)
                        clsProject.clsClient = clsClientsManager.convertClientsTableToClass(item.tblClients);
                    if (item.tblTasks.Count > 0)
                    {
                        foreach (tblTasks Task in item.tblTasks)
                        {
                            clsTasks clsTask = clsTasksManager.convertTasksTableToClass(Task);
                            clsProject.lstTasks.Add(clsTask);
                        }
                    }

                    lstProjects.Add(clsProject);
                }
            }

            return lstProjects;
        }

        public List<clsProjects> getAllProjectsListOnlyByClientID(int iClientID)
        {
            List<clsProjects> lstProjects = new List<clsProjects>();
            var lstGetProjectsList = db.tblProjects.Where(Project => Project.iClientID == iClientID && Project.bIsDeleted == false).ToList();

            if (lstGetProjectsList.Count > 0)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                foreach (var item in lstGetProjectsList)
                {
                    clsProjects clsProject = new clsProjects();

                    clsProject.iProjectID = item.iProjectID;
                    clsProject.dtAdded = item.dtAdded;
                    clsProject.iAddedBy = item.iAddedBy;
                    clsProject.dtEdited = item.dtEdited;
                    clsProject.iEditedBy = item.iEditedBy;

                    clsProject.strAsanaProjectID = item.strAsanaProjectID;
                    clsProject.strTitle = item.strTitle;
                    clsProject.strDescription = item.strDescription;
                    clsProject.iClientID = item.iClientID;
                    clsProject.bIsDeleted = item.bIsDeleted;

                    clsProject.lstTasks = new List<clsTasks>();

                    lstProjects.Add(clsProject);
                }
            }

            return lstProjects;
        }

        //Get
        public clsProjects getProjectByID(int iProjectID)
        {
            clsProjects clsProject = null;
            tblProjects tblProjects = db.tblProjects.FirstOrDefault(Project => Project.iProjectID == iProjectID && Project.bIsDeleted == false);

            if (tblProjects != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                clsProject = new clsProjects();

                clsProject.iProjectID = tblProjects.iProjectID;
                clsProject.dtAdded = tblProjects.dtAdded;
                clsProject.iAddedBy = tblProjects.iAddedBy;
                clsProject.dtEdited = tblProjects.dtEdited;
                clsProject.iEditedBy = tblProjects.iEditedBy;

                clsProject.strAsanaProjectID = tblProjects.strAsanaProjectID;
                clsProject.strTitle = tblProjects.strTitle;
                clsProject.strDescription = tblProjects.strDescription;
                clsProject.iClientID = tblProjects.iClientID;
                clsProject.bIsDeleted = tblProjects.bIsDeleted;

                clsProject.lstTasks = new List<clsTasks>();

                if (tblProjects.tblClients != null)
                    clsProject.clsClient = clsClientsManager.convertClientsTableToClass(tblProjects.tblClients);
                if (tblProjects.tblTasks.Count > 0)
                {
                    foreach (tblTasks Task in tblProjects.tblTasks)
                    {
                        clsTasks clsTask = clsTasksManager.convertTasksTableToClass(Task);
                        clsProject.lstTasks.Add(clsTask);
                    }
                }
            }

            return clsProject;
        }

        public clsProjects getProjectByIDAndClientID(int iProjectID, int iClientID)
        {
            clsProjects clsProject = null;
            tblProjects tblProjects = db.tblProjects.FirstOrDefault(Project => Project.iProjectID == iProjectID && Project.iClientID == iClientID && Project.bIsDeleted == false);

            if (tblProjects != null)
            {
                //Managers
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                clsProject = new clsProjects();

                clsProject.iProjectID = tblProjects.iProjectID;
                clsProject.dtAdded = tblProjects.dtAdded;
                clsProject.iAddedBy = tblProjects.iAddedBy;
                clsProject.dtEdited = tblProjects.dtEdited;
                clsProject.iEditedBy = tblProjects.iEditedBy;

                clsProject.strAsanaProjectID = tblProjects.strAsanaProjectID;
                clsProject.strTitle = tblProjects.strTitle;
                clsProject.strDescription = tblProjects.strDescription;
                clsProject.iClientID = tblProjects.iClientID;
                clsProject.bIsDeleted = tblProjects.bIsDeleted;

                clsProject.lstTasks = new List<clsTasks>();

                if (tblProjects.tblClients != null)
                    clsProject.clsClient = clsClientsManager.convertClientsTableToClass(tblProjects.tblClients);
                if (tblProjects.tblTasks.Count > 0)
                {
                    foreach (tblTasks Task in tblProjects.tblTasks)
                    {
                        clsTasks clsTask = clsTasksManager.convertTasksTableToClass(Task);
                        clsProject.lstTasks.Add(clsTask);
                    }
                }
            }

            return clsProject;
        }

        //Save
        public int saveProject(clsProjects clsProject)
        {
            //if (HttpContext.Current.Session["clsCMSUser"] != null)
            //{
            //clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
            tblProjects tblProjects = new tblProjects();

            tblProjects.iProjectID = clsProject.iProjectID;

            tblProjects.strAsanaProjectID = clsProject.strAsanaProjectID;
            tblProjects.strTitle = clsProject.strTitle;
            tblProjects.strDescription = clsProject.strDescription;
            tblProjects.iClientID = clsProject.iClientID;
            tblProjects.bIsDeleted = clsProject.bIsDeleted;

            //Add
            if (tblProjects.iProjectID == 0)
            {
                tblProjects.dtAdded = DateTime.Now;
                //tblProjects.iAddedBy = clsCMSUser.iCMSUserID;
                tblProjects.iAddedBy = 1;
                tblProjects.dtEdited = DateTime.Now;
                //tblProjects.iEditedBy = clsCMSUser.iCMSUserID;
                tblProjects.iEditedBy = 1;

                db.tblProjects.Add(tblProjects);
                db.SaveChanges();
            }
            //Update
            else
            {
                tblProjects.dtAdded = clsProject.dtAdded;
                tblProjects.iAddedBy = clsProject.iAddedBy;
                tblProjects.dtEdited = DateTime.Now;
                //tblProjects.iEditedBy = clsCMSUser.iCMSUserID;
                tblProjects.iEditedBy = 1;

                db.Set<tblProjects>().AddOrUpdate(tblProjects);
                db.SaveChanges();
            }
            //}
            return tblProjects.iProjectID;
        }

        //Remove
        public void removeProjectByID(int iProjectID)
        {
            tblProjects tblProject = db.tblProjects.Find(iProjectID);
            if (tblProject != null)
            {
                tblProject.bIsDeleted = true;
                db.Entry(tblProject).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfProjectExists(int iProjectID)
        {
            bool bProjectExists = db.tblProjects.Any(Project => Project.iProjectID == iProjectID && Project.bIsDeleted == false);
            return bProjectExists;
        }

        //Convert database table to class
        public clsProjects convertProjectsTableToClass(tblProjects tblProject)
        {
            clsProjects clsProject = new clsProjects();

            clsProject.iProjectID = tblProject.iProjectID;
            clsProject.dtAdded = tblProject.dtAdded;
            clsProject.iAddedBy = tblProject.iAddedBy;
            clsProject.dtEdited = tblProject.dtEdited;
            clsProject.iEditedBy = tblProject.iEditedBy;

            clsProject.strAsanaProjectID = tblProject.strAsanaProjectID;
            clsProject.strTitle = tblProject.strTitle;
            clsProject.strDescription = tblProject.strDescription;
            clsProject.iClientID = tblProject.iClientID;
            clsProject.bIsDeleted = tblProject.bIsDeleted;

            return clsProject;
        }
    }
}
