﻿using Microsoft.AspNet.SignalR;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace SoftservePortalNew.SignalR.Utilities
{
    public class NotificationUtility
    {
        //Here we will add a function for register notification (will add sql dependency)
        public void RegisterNotification(DateTime currentTime)
        {
            var conStr = ConfigurationManager.ConnectionStrings["sqlConString"].ConnectionString;
            const string sqlCommand = @"SELECT * from [dbo].[tblNotifications] where [dtEdited] > @AddedOn";


            //you can notice here I have added table name like this [dbo].[tblUsers] with [dbo], its mandatory when you use Sql Dependency
            using (var con = new SqlConnection(conStr))
            {
                var cmd = new SqlCommand(sqlCommand, con);
                cmd.Parameters.AddWithValue("@AddedOn", currentTime);
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                cmd.Notification = null;
                var sqlDep = new SqlDependency(cmd);
                sqlDep.OnChange += sqlDep_OnChange;
                //we must have to execute the command here
                using (var reader = cmd.ExecuteReader())
                {
                    // nothing need to add here now
                }
            }
        }

        void sqlDep_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                var sqlDep = sender as SqlDependency;
                sqlDep.OnChange -= sqlDep_OnChange;

                //from here we will send notification message to client
                var notificationHub = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                notificationHub.Clients.All.notify("added");

                //re-register notification
                RegisterNotification(DateTime.Now);

            }
        }

        public List<clsNotifications> GetNotifications(DateTime afterDate)
        {
            using (var dc = new SoftservePortalNewContext())
            {
                var lstNotifications = dc.tblNotifications.Where(a => a.dtEdited > afterDate && a.bIsDeleted != true && a.bIsRead != true).ToList();
                var lstClsNotifications = new List<clsNotifications>();

                var clsNotificationManager = new clsNotificationsManager();
                foreach (var notification in lstNotifications)
                {
                    var clsNotifications = clsNotificationManager.getNotificationByID(notification.iNotificationID);

                    lstClsNotifications.Add(clsNotifications);
                }

                return lstClsNotifications;
            }
        }
    }
}