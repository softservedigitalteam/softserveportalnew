﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.Calendar;
using SoftservePortalNew.Assistant_Classes;

namespace SoftservePortalNew.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar

        public ActionResult SocialCalendar()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsSocialCalendarEventsTypesManager clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();
            clsSocialCalendarView clsSocialCalendarView = new clsSocialCalendarView();

            clsSocialCalendarView.lstSocialCalendarEventsTypes = clsSocialCalendarEventsTypesManager.getAllSocialCalendarEventsTypesOnlyList();
            return View(clsSocialCalendarView);
        }
        public ActionResult SocialCalendarView()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
            List<clsSocialCalendarEventsViewItems> lstSocialCalendarEvents = clsSocialCalendarEventsManager.getAllSocialCalendarEventsViewItemsOnlyList();

            return View(lstSocialCalendarEvents);
        }

        public ActionResult GetSocialCalendarEvents()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventAdded = false;

            try
            {
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                List<clsSocialCalendarEventsItems> lstSocialCalendarEvents = clsSocialCalendarEventsManager.getAllSocialCalendarEventsItemsOnlyList();

                return Json(new { lstSocialCalendarEvents = lstSocialCalendarEvents }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { bEventAdded = bEventAdded }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSocialCalendarViewEvents()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventAdded = false;

            try
            {
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                List<clsSocialCalendarEventsViewItems> lstSocialCalendarViewEvents = clsSocialCalendarEventsManager.getAllSocialCalendarEventsViewItemsOnlyList();

                return Json(new { lstSocialCalendarViewEvents = lstSocialCalendarViewEvents }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { bEventAdded = bEventAdded }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSocialCalendarEventInfo(string strEventID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsSocialCalendarEvents clsSocialCalendarEvent = null;
            bool bEventExists = false;
            int iEventID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    bEventExists = true;
                    return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddSocialCalendarEvent(string strTitle, string strEventStart, string strEventEnd, string strEventType, string strEventDescription, string strEventKeywords, string strEventLinks, string strRepeatEventType)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventAdded = false;
            bool bEventExists = false;
            bool bIsRepeatEvent = false;
            int iEventID = 0;

            try
            {
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = new clsSocialCalendarEvents();

                DateTime dtStartDate = Convert.ToDateTime(strEventStart);
                DateTime dtEndDate = Convert.ToDateTime(strEventEnd);

                int iEventType = 0;
                if (strEventType != null && strEventType != "")
                    iEventType = Convert.ToInt32(strEventType);

                bEventExists = clsSocialCalendarEventsManager.checkIfSocialCalendarEventExists(strTitle, dtStartDate, dtEndDate);

                if (bEventExists == true)
                {
                    return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists, iEventID = iEventID }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    clsSocialCalendarEvent.strTitle = strTitle;
                    clsSocialCalendarEvent.dtStartDate = dtStartDate;
                    clsSocialCalendarEvent.dtEndDate = dtEndDate;
                    if (iEventType != 0)
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = iEventType;
                    clsSocialCalendarEvent.strDescription = strEventDescription;
                    clsSocialCalendarEvent.strKeywords = strEventKeywords;
                    clsSocialCalendarEvent.strLinks = strEventLinks;
                    if (strRepeatEventType != null && strRepeatEventType != "")
                    {
                        clsSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle + " (Repeat)";
                        iEventID = clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);

                        bIsRepeatEvent = true;

                        clsSocialCalendarEvent.iSocialCalendarEventID = iEventID;
                        clsSocialCalendarEvent.bIsRepeatEvent = true;
                        clsSocialCalendarEvent.strRepeatEventType = strRepeatEventType;
                        clsSocialCalendarEvent.iRepeatEventStartDateID = iEventID;
                        clsSocialCalendarEvent.dtRepeatEventStartDate = dtStartDate;
                    }

                    if (iEventID == 0)
                        iEventID = clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);

                    int iDifferenceInDays = (dtEndDate - dtStartDate).Days;

                    if (clsSocialCalendarEvent.bIsRepeatEvent == true)
                    {
                        if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "DAILY")
                        {
                            if (!(iDifferenceInDays > 1))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                int iStartDay = dtStartDate.Day + 1; //Begin Repeat on following day
                                int iDaysInMonth = DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month);

                                while (iStartDay <= iDaysInMonth)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(1);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(1);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartDay++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "WEEKLY")
                        {
                            if (!(iDifferenceInDays > 7))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 16 weeks
                                int iStartWeek = 1;
                                int iEndWeek = 16;

                                while (iStartWeek <= iEndWeek)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(7);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(7);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartWeek++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "BIWEEKLY")
                        {
                            if (!(iDifferenceInDays > 14))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 16 weeks
                                int iStartWeek = 1;
                                int iEndWeek = 16;

                                while (iStartWeek <= iEndWeek)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(14);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(14);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartWeek++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "MONTHLY")
                        {
                            if (!(iDifferenceInDays > 28))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 6 Months
                                int iStartMonth = 1;
                                int iEndMonth = 6;

                                while (iStartMonth <= iEndMonth)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddMonths(1);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddMonths(1);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartMonth++;
                                }
                            }
                        }
                    }

                    bEventAdded = true;
                    return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists, iEventID = iEventID, bIsRepeatEvent = bIsRepeatEvent }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateSocialCalendarEventDateTime(string strEventID, string strEventStart, string strEventEnd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventUpdated = false;
            int iEventID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    DateTime dtStartDate = Convert.ToDateTime(strEventStart);
                    DateTime dtEndDate = Convert.ToDateTime(strEventEnd);

                    clsSocialCalendarEvent.dtStartDate = dtStartDate;
                    clsSocialCalendarEvent.dtEndDate = dtEndDate;

                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);
                    bEventUpdated = true;
                }

                return Json(new { bEventUpdated = bEventUpdated}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSocialCalendarEventInfo(string strEventID, string strTitle, string strEventType, string strEventDescription, string strEventKeywords, string strEventLinks)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventUpdated = false;
            int iEventID = 0;
            int iEventTypeID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);
                if (strEventType != null && strEventType != "")
                    iEventTypeID = Convert.ToInt32(strEventType);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    clsSocialCalendarEvent.strTitle = strTitle;
                    if (iEventTypeID != 0)
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = iEventTypeID;
                    else
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = null;
                    clsSocialCalendarEvent.strDescription = strEventDescription;
                    clsSocialCalendarEvent.strKeywords = strEventKeywords;
                    clsSocialCalendarEvent.strLinks = strEventLinks;

                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);
                    bEventUpdated = true;
                }

                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}