﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Softserve_Frontend_Mar_2018.Controllers
{
    public class SolutionsController : Controller
    {
        // GET: Solutions
        public ActionResult Home()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;

            }
            else if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;
            }

            return View();
        }

        public ActionResult Showcase()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;

            }
            else if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;
            }

            return View();
        }
    }
}