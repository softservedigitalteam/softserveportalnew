﻿using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.Analytics;
using System.Net.Http;
using Google.Apis.Auth;

namespace Softserve_Frontend_Mar_2018.Controllers
{
    public class AnalyticsController : Controller
    {
        // GET: Analytics
        public ActionResult Google()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsGogleAnalytics clsGogleAnalytics = new clsGogleAnalytics();

            clsGogleAnalytics.lstDomains = clsDomainsManager.getAllDomainsWithGoogleAnalyticsList().Where(Domain => Domain.clsClient != null).ToList();

            return View(clsGogleAnalytics);
        }

        public ActionResult Facebook()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;

            }
            else if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;
            }

            analytics analytics = new analytics();
            using (StreamReader r = new StreamReader(@"C:\Users\SoftservePC1\Documents\SoftserveDigital Repositories\SoftservePortalNew\SoftservePortalNew\Content\app-assets\json\posts.json"))
            {
                string json = r.ReadToEnd();
                analytics.RootObject = JsonConvert.DeserializeObject<RootObject>(json);
            }
            using (StreamReader s = new StreamReader(@"C:\Users\SoftservePC1\Documents\SoftserveDigital Repositories\SoftservePortalNew\SoftservePortalNew\Content\app-assets\json\page-impressions.json"))
            {
                string jsons = s.ReadToEnd();
                analytics.Page_Impressions = JsonConvert.DeserializeObject<Page_Impressions>(jsons);
            }
            return View(analytics);
        }

        public ActionResult _DomainGoogleAnalyticsReportPartial()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

               return RedirectToAction("Index", "Home");
            }

            return PartialView();
        }

        private static void WriteLine(object result)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult GetDomainGoogleAnalyticsReport(int iDomainID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bHasTokenAndPropertyID = false;

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsDomains clsDomain = clsDomainsManager.getDomainByID(iDomainID);

            string strGAToken = "";
            string strGAPropertyID = "";

            //string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "\\Content\\app-assets\\json\\AneliticksApi-7acaf0fff6a7.json";
            string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "Content\\app-assets\\json\\portalanalytics-215414-7f01944d6065.json";
            
            string[] scopes = new string[] { AnalyticsReportingService.Scope.AnalyticsReadonly }; // Put your scopes here

            var stream = new FileStream(jsonPath, FileMode.Open, FileAccess.Read);

            try
            {
                Task<string> task = GoogleCredential.FromStream(stream).CreateScoped(scopes).UnderlyingCredential.GetAccessTokenForRequestAsync();
                task.Wait();
                string bearer = task.Result;

                strGAToken = bearer;
            }
            catch (AggregateException ex)
            {
                throw ex.InnerException;
            }

            if (clsDomain != null)
                strGAPropertyID = clsDomain.strGoogleAnalyticsCode;

            TempData["strGAToken"] = strGAToken;
            TempData["strGAPropertyID"] = strGAPropertyID;
            TempData["iDomainID"] = iDomainID;

            bHasTokenAndPropertyID = true;

            return Json(new { bHasTokenAndPropertyID = bHasTokenAndPropertyID }, JsonRequestBehavior.AllowGet);
        }
    }
}