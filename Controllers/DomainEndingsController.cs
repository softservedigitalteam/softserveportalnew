﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Controllers
{
    public class DomainEndingsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: DomainEndings
        public ActionResult DomainEndings()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();
            List<clsDomainEndings> lstDomainEndings = clsDomainEndingsManager.getAllDomainEndingsOnlyList().OrderBy(DomainEnding => DomainEnding.strTitle).ToList();

            return View(lstDomainEndings); 
        }

        public ActionResult DomainEndingAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainEndings clsDomainEnding = new clsDomainEndings();

            return View(clsDomainEnding);
        }

        [HttpPost]
        public ActionResult DomainEndingAdd(clsDomainEndings clsDomainEnding)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();
            clsDomainEndings clsDomainEndingNew = new clsDomainEndings();

            clsDomainEndingNew.strTitle = clsDomainEnding.strTitle;

            clsDomainEndingsManager.saveDomainEnding(clsDomainEndingNew);
            return RedirectToAction("DomainEndings", "DomainEndings");
        }

        public ActionResult DomainEndingEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("DomainEndings", "DomainEndings");
            }

            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();
            clsDomainEndings clsDomainEnding = clsDomainEndingsManager.getDomainEndingByID(id);

            return View(clsDomainEnding);
        }

        [HttpPost]
        public ActionResult DomainEndingEdit(clsDomainEndings clsDomainEnding)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();
            clsDomainEndings clsDomainEndingUpdate = clsDomainEndingsManager.getDomainEndingByID(clsDomainEnding.iDomainEndingID);

            clsDomainEndingUpdate.strTitle = clsDomainEnding.strTitle;

            clsDomainEndingsManager.saveDomainEnding(clsDomainEndingUpdate);
            return RedirectToAction("DomainEndings", "DomainEndings");
        }

        [HttpPost]
        public ActionResult DeleteDomainEnding(int iDomainEndingID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();
            clsDomainEndingsManager.removeDomainEndingByID(iDomainEndingID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfDomainEndingExists(string strTitle)
        {
            bool bCanUseDomainEnding = false;
            bool bExists = db.tblDomainEndings.Any(DomainEnding => DomainEnding.strTitle.ToLower() == strTitle.ToLower() && DomainEnding.bIsDeleted == false);

            if (bExists == false)
                bCanUseDomainEnding = true;

            return Json(bCanUseDomainEnding, JsonRequestBehavior.AllowGet);
        }

    }
}