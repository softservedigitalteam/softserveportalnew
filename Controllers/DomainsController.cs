﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using SoftservePortalNew.View_Models.Domains;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.View_Models.Clients;
using SoftservePortalNew.Assistant_Classes;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Controllers
{
    public class DomainsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: Domains
        public ActionResult DomainManagement()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult Domains()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            List<clsDomains> lstDomains = clsDomainsManager.getAllDomainsList().OrderBy(Domain => Domain.clsClient.strCompanyName).ToList();

            return View(lstDomains);
        }

        public ActionResult DomainAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();

            clsDomainAdd clsDomainAdd = new clsDomainAdd();
            clsDomainAdd.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
            clsDomainAdd.lstDomainEndings = clsDomainEndingsManager.getAllDomainEndingsOnlyList().OrderBy(DomainEnding => DomainEnding.strTitle).ToList();

            return View(clsDomainAdd);
        }

        [HttpPost]
        public ActionResult DomainAdd(clsDomainAdd clsDomainAdd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsDomains clsDomainNew = new clsDomains();

            clsDomainNew.iClientID = clsDomainAdd.clsDomain.iClientID;
            clsDomainNew.strTitle = clsDomainAdd.clsDomain.strTitle;
            clsDomainNew.iDomainEndingID = clsDomainAdd.clsDomain.iDomainEndingID;
            //clsDomainNew.dtRegistrationDate = clsDomainAdd.clsDomain.dtRegistrationDate;
            //clsDomainNew.dtNextDueDate = clsDomainAdd.clsDomain.dtNextDueDate;

            //clsDomainNew.strDomainStatus = clsDomainAdd.clsDomain.strDomainStatus;
            clsDomainNew.strGoogleAnalyticsCode = clsDomainAdd.clsDomain.strGoogleAnalyticsCode;
            clsDomainNew.bAutoRenew = clsDomainAdd.clsDomain.bAutoRenew;

            clsDomainsManager.saveDomain(clsDomainNew);

            return RedirectToAction("Domains", "Domains");
        }

        public ActionResult DomainEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("Domains", "Domains");
            }

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsDomainEndingsManager clsDomainEndingsManager = new clsDomainEndingsManager();

            clsDomainEdit clsDomainEdit = new clsDomainEdit();
            clsDomainEdit.clsDomain = clsDomainsManager.getDomainByID(id);

            clsDomainEdit.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
            clsDomainEdit.lstDomainEndings = clsDomainEndingsManager.getAllDomainEndingsOnlyList().OrderBy(DomainEnding => DomainEnding.strTitle).ToList();

            return View(clsDomainEdit);
        }

        [HttpPost]
        public ActionResult DomainEdit(clsDomainEdit clsDomainEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsDomains clsDomainUpdate = clsDomainsManager.getDomainByID(clsDomainEdit.clsDomain.iDomainID);

            clsDomainUpdate.iClientID = clsDomainEdit.clsDomain.iClientID;
            clsDomainUpdate.strTitle = clsDomainEdit.clsDomain.strTitle;
            clsDomainUpdate.iDomainEndingID = clsDomainEdit.clsDomain.iDomainEndingID;
            //clsDomainUpdate.dtRegistrationDate = clsDomainEdit.clsDomain.dtRegistrationDate;
            //clsDomainUpdate.dtNextDueDate = clsDomainEdit.clsDomain.dtNextDueDate;

            //clsDomainUpdate.strDomainStatus = clsDomainEdit.clsDomain.strDomainStatus;
            clsDomainUpdate.strGoogleAnalyticsCode = clsDomainEdit.clsDomain.strGoogleAnalyticsCode;
            clsDomainUpdate.bAutoRenew = clsDomainEdit.clsDomain.bAutoRenew;

            clsDomainsManager.saveDomain(clsDomainUpdate);

            return RedirectToAction("Domains", "Domains");
        }

        [HttpPost]
        public ActionResult DeleteDomain(int iDomainID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsDomainsManager clsDomainsManager = new clsDomainsManager();
            clsDomainsManager.removeDomainByID(iDomainID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfDomainExists(string strTitle)
        {
            bool bCanUseDomain = false;
            bool bExists = db.tblDomains.Any(Domain => Domain.strTitle.ToLower() == strTitle.ToLower() && Domain.bIsDeleted == false);

            if (bExists == false)
                bCanUseDomain = true;

            return Json(bCanUseDomain, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DomainPurchase()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> DomainAvailability(string strDomainName)
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDomainAvailable = false;
            bool isValidDomain = true;

            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                string strEncryptedKey = Encrypt("CheckDomainAvailability");
                string url = "http://localhost:12283/api/Domains/GetDomainAvailability?strDomainName=" + strDomainName + "&strKey=" + strEncryptedKey;
                var responseString = await client.GetStringAsync(url);
                if (responseString.ToLower() == "true")
                    bIsDomainAvailable = true;
            }

            return Json(new { bIsDomainAvailable = bIsDomainAvailable, isValidDomain = isValidDomain, strDomainName = strDomainName }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> _DomainRecommendations(string strDomainName)
        {
            string strDomains = "";
            clsDomainRecommendationsList clsDomainRecommendationsList = new clsDomainRecommendationsList();

            if (strDomainName.Contains(".co.za"))
            {
                strDomainName = strDomainName.Replace(".co.za", "");

                //.co.za domains
                //strDomains += strDomainName + "-enterprise.co.za,";
                //strDomains += strDomainName + "-home.co.za,";
                //strDomains += strDomainName + "-care.co.za,";
                //strDomains += strDomainName + "-business.co.za,";
                //strDomains += strDomainName + "-products.co.za";

                //.web.za
                strDomains += strDomainName + ".web.za";
                ////.net.za
                //strDomains += strDomainName + ".net.za,";
                ////.org.za
                //strDomains += strDomainName + ".org.za";
                ////.joburg
                //strDomains += strDomainName + ".joburg,";
                ////.durban
                //strDomains += strDomainName + ".durban,";
                ////.capetown
                //strDomains += strDomainName + ".capetown,";
                ////.africa
                //strDomains += strDomainName + ".africa";
            }

            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                string strEncryptedKey = Encrypt("CheckDomainAvailability");
                string url = "http://localhost:12283/api/Domains/GetMultipleDomainsAvailability?strDomainNames=" + strDomains + "&strKey=" + strEncryptedKey;
                string responseString = await client.GetStringAsync(url);

                XmlDocument responseDoc = new XmlDocument();
                responseDoc.LoadXml(responseString);

                XmlNodeList responseList = responseDoc.GetElementsByTagName("domain:name");
                if (responseList.Count > 0)
                {
                    foreach (XmlNode xmlnDomain in responseList)
                    {
                        if (xmlnDomain != null)
                        {
                            if (xmlnDomain.Attributes != null)
                            {
                                if (xmlnDomain.Attributes["avail"] != null)
                                {
                                    if (xmlnDomain.Attributes["avail"].Value.ToString() == "1")
                                    {
                                        if (xmlnDomain.InnerText.Contains(".co.za"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsCOZA.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 150, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".web.za"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsWEBZA.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 250, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".net.za"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsNETZA.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 250, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".org.za"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsORGZA.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 250, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".joburg"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsJOBURG.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 500, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".durban"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsDURBAN.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 500, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".capetown"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsCAPETOWN.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 500, bIsDomainAvailable = true });
                                        else if (xmlnDomain.InnerText.Contains(".africa"))
                                            clsDomainRecommendationsList.lstclsDomainRecommendationsAFRICA.Add(new clsDomainRecommendations { strDomainName = xmlnDomain.InnerText, dblPrice = 500, bIsDomainAvailable = true });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return PartialView(clsDomainRecommendationsList);
        }

        private string Encrypt(string encryptString)
        {
            string strEncryptionKey = "***0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()***";
            byte[] bClearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(strEncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bClearBytes, 0, bClearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }

        private string Decrypt(string cipherText)
        {
            string strEncryptionKey = "***0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()***";
            cipherText = cipherText.Replace(" ", "+");
            byte[] bClearBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(strEncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bClearBytes, 0, bClearBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}