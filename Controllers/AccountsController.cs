﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.Accounts;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Assistant_Classes;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace Softserve_Frontend_Mar_2018.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        [AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.bIsUserLoggedIn = null;
            ViewBag.bIsClientLoggedIn = null;

            clsLogin clsLogin = new clsLogin();

            HttpCookie hcClientEmailCookie = Request.Cookies["strLoginEmail"];
            HttpCookie hcClientPasswordCookie = Request.Cookies["strLoginPassword"];
            if (hcClientEmailCookie != null)
            {
                clsLogin.strEmail = hcClientEmailCookie.Value; //Get the email
                clsLogin.bRememberMe = true;
            }
            if (hcClientPasswordCookie != null)
            {
                clsLogin.strPassword = hcClientPasswordCookie.Value; //Get the password
            }

            return View(clsLogin);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(clsLogin clsLogin)
        {
            //Managers
            clsUsersManager clsUsersManager = new clsUsersManager();
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsPortalLoginsManager clsPortalLoginsManager = new clsPortalLoginsManager();
            clsPortalLogins clsPortalLogin = new clsPortalLogins();

            //User
            clsUsers clsUser = clsUsersManager.getUserByEmail(clsLogin.strEmail.ToLower());
            //Client
            clsClients clsClient = clsClientsManager.getClientByEmail(clsLogin.strEmail.ToLower());

            if (clsUser != null)
            {
                return doUserLogin(clsLogin, clsUser, clsPortalLoginsManager, clsPortalLogin);
            }
            else if (clsClient != null)
            {
                return doClientLogin(clsLogin, clsClient, clsPortalLoginsManager, clsPortalLogin);
            }
            else
            {
                //Save login attempt
                clsPortalLogin.strUsername = clsLogin.strEmail.ToLower();
                clsPortalLogin.strPassword = clsLogin.strPassword;
                clsPortalLogin.strIPAddress = Request.UserHostAddress;
                clsPortalLogin.bIsSuccessfulLoginAttempt = false;
                clsPortalLoginsManager.savePortalLogin(clsPortalLogin);

                ModelState.AddModelError("loginError", "Incorrect Login Credentials Entered");
                clsLogin.strPassword = "";
                return View(clsLogin);
            }
        }


        private ActionResult doUserLogin(clsLogin clsLogin, clsUsers clsUser, clsPortalLoginsManager clsPortalLoginsManager, clsPortalLogins clsPortalLogin)
        {
            string strPasswordHash = "";
            if (clsLogin.strPassword != null && clsLogin.strPassword != "")
                strPasswordHash = clsCommonFunctions.GetMd5Sum(clsLogin.strPassword);

            //If password is incorrect
            if (clsUser.strPassword != strPasswordHash)
            {
                //Save login attempt
                clsPortalLogin.strUsername = clsLogin.strEmail.ToLower();
                clsPortalLogin.strPassword = clsLogin.strPassword;
                clsPortalLogin.strIPAddress = Request.UserHostAddress;
                clsPortalLogin.bIsSuccessfulLoginAttempt = false;
                clsPortalLoginsManager.savePortalLogin(clsPortalLogin);

                ModelState.AddModelError("loginError", "Incorrect Login Credentials Entered");
                clsLogin.strPassword = "";
                return View(clsLogin);
            }
            else
            {
                //Set cookie
                if (clsLogin.bRememberMe == true)
                {
                    //Save email
                    HttpCookie hcCMSUserEmailCookie = new HttpCookie("strLoginEmail");
                    hcCMSUserEmailCookie.Value = clsLogin.strEmail;
                    hcCMSUserEmailCookie.Expires = DateTime.Now.AddDays(30); // expires after 30 days
                    HttpContext.Response.Cookies.Add(hcCMSUserEmailCookie);

                    //Save password
                    HttpCookie hcCMSUserPasswordCookie = new HttpCookie("strLoginPassword");
                    hcCMSUserPasswordCookie.Value = clsLogin.strPassword;
                    hcCMSUserPasswordCookie.Expires = DateTime.Now.AddDays(30); // expires after 30 days
                    HttpContext.Response.Cookies.Add(hcCMSUserPasswordCookie);
                }
                else
                {
                    if (Request.Cookies["strLoginEmail"] != null)
                    {
                        Response.Cookies["strLoginEmail"].Expires = DateTime.Now.AddDays(-1);
                    }
                    if (Request.Cookies["strLoginPassword"] != null)
                    {
                        Response.Cookies["strLoginPassword"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                //Save login attempt
                clsPortalLogin.strUsername = clsLogin.strEmail.ToLower();
                clsPortalLogin.strPassword = "";
                clsPortalLogin.strIPAddress = Request.UserHostAddress;
                clsPortalLogin.bIsSuccessfulLoginAttempt = true;
                clsPortalLoginsManager.savePortalLogin(clsPortalLogin);

                //Reset error message
                ModelState.AddModelError("loginError", "");

                Session["clsUser"] = clsUser;
                Session["clsClient"] = null;

                return RedirectToAction("Index", "Home"); // login succeed 
            }
        }

        private ActionResult doClientLogin(clsLogin clsLogin, clsClients clsClient, clsPortalLoginsManager clsPortalLoginsManager, clsPortalLogins clsPortalLogin)
        {
            string strPasswordHash = "";
            if (clsLogin.strPassword != null && clsLogin.strPassword != "")
                strPasswordHash = clsCommonFunctions.GetMd5Sum(clsLogin.strPassword);

            //If password is incorrect
            if (clsClient.strPassword != strPasswordHash)
            {
                //Save login attempt
                clsPortalLogin.strUsername = clsLogin.strEmail.ToLower();
                clsPortalLogin.strPassword = clsLogin.strPassword;
                clsPortalLogin.strIPAddress = Request.UserHostAddress;
                clsPortalLogin.bIsSuccessfulLoginAttempt = false;
                clsPortalLoginsManager.savePortalLogin(clsPortalLogin);

                ModelState.AddModelError("loginError", "Incorrect Login Credentials Entered");
                clsLogin.strPassword = "";
                return View(clsLogin);
            }
            else
            {
                //Set cookie
                if (clsLogin.bRememberMe == true)
                {
                    //Save email
                    HttpCookie hcCMSUserEmailCookie = new HttpCookie("strLoginEmail");
                    hcCMSUserEmailCookie.Value = clsLogin.strEmail;
                    hcCMSUserEmailCookie.Expires = DateTime.Now.AddDays(30); // expires after 30 days
                    HttpContext.Response.Cookies.Add(hcCMSUserEmailCookie);

                    //Save password
                    HttpCookie hcCMSUserPasswordCookie = new HttpCookie("strLoginPassword");
                    hcCMSUserPasswordCookie.Value = clsLogin.strPassword;
                    hcCMSUserPasswordCookie.Expires = DateTime.Now.AddDays(30); // expires after 30 days
                    HttpContext.Response.Cookies.Add(hcCMSUserPasswordCookie);
                }
                else
                {
                    if (Request.Cookies["strLoginEmail"] != null)
                    {
                        Response.Cookies["strLoginEmail"].Expires = DateTime.Now.AddDays(-1);
                    }
                    if (Request.Cookies["strLoginPassword"] != null)
                    {
                        Response.Cookies["strLoginPassword"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                //Save login attempt
                clsPortalLogin.strUsername = clsLogin.strEmail.ToLower();
                clsPortalLogin.strPassword = "";
                clsPortalLogin.strIPAddress = Request.UserHostAddress;
                clsPortalLogin.bIsSuccessfulLoginAttempt = true;
                clsPortalLoginsManager.savePortalLogin(clsPortalLogin);

                //Reset error message
                ModelState.AddModelError("loginError", "");

                Session["clsUser"] = null;
                Session["clsClient"] = clsClient;

                if (clsClient.bIsVerified != true)
                    return RedirectToAction("Verification", "Accounts"); // Verify

                if (clsClient.strIsApproved == "Approved")
                {
                    if (clsClient.bIsProfileCompleted != true)
                    {
                        return RedirectToAction("CompleteProfile", "Accounts"); // Complete Profile
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home"); // login succeed 
                    }
                }
                else if (clsClient.strIsApproved == "Rejected")
                {
                    ModelState.AddModelError("loginError", "Your account has not been approved to enter this site.");
                    clsLogin.strPassword = "";
                    return View(clsLogin);
                }
                else if (clsClient.strIsApproved == "Pending")
                {
                    ModelState.AddModelError("loginError", "Your account is in the process of being approved. You will recieve an email once your account has been approved.");
                    clsLogin.strPassword = "";
                    return View(clsLogin);
                }
                else
                {
                    ModelState.AddModelError("loginError", "Your account is in the process of being approved.");
                    clsLogin.strPassword = "";
                    return View(clsLogin);
                }
            }
        }
        public ActionResult Logout()
        {
            Session["clsUser"] = null;
            Session["clsClient"] = null;

            ViewBag.bIsUserLoggedIn = null;
            ViewBag.bIsClientLoggedIn = null;

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            clsRegister clsRegister = new clsRegister();
            return View(clsRegister);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(clsRegister clsRegister)
        {
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClients clsClient = new clsClients();

            clsClient.strCompanyName = "";
            clsClient.strUsername = clsRegister.strUsername.ToLower();
            clsClient.strEmail = clsRegister.strEmail.ToLower();
            clsClient.strPassword = clsCommonFunctions.GetMd5Sum(clsRegister.strNewPassword);
            clsClient.strVerificationKey = clsCommonFunctions.strCreateRandomString(6);
            clsClient.strIsApproved = "Pending";
            clsClientsManager.saveClient(clsClient);

            sendVerificationEmail(clsClient.strEmail, clsClient.strVerificationKey);
            return RedirectToAction("Login", "Accounts"); // register succeed 
        }

        public ActionResult Verification()
        {
            //Redirect to login if null session exists
            if (Session["clsClient"] == null)
                return RedirectToAction("Login", "Accounts");

            clsClients clsClient = (clsClients)Session["clsClient"];
            if (clsClient.bIsVerified == true)
            {
                if (clsClient.strIsApproved == "Approved")
                {
                    if (clsClient.bIsProfileCompleted == true)
                    {
                        return RedirectToAction("Index", "Home"); //Logged in
                    }
                    else
                    {
                        return RedirectToAction("CompleteProfile", "Accounts"); //Complete Profile
                    }
                        
                }
                else
                {
                    return RedirectToAction("Login", "Accounts");
                }
            }

            clsVerification clsVerification = new clsVerification();
            return View(clsVerification);
        }

        [HttpPost]
        public ActionResult Verification(clsVerification clsVerification)
        {
            //Redirect to login if null session exists
            if (Session["clsClient"] == null)
                return RedirectToAction("Login", "Accounts");

            clsClients clsClient = (clsClients)Session["clsClient"];
            if (clsClient.strVerificationKey != clsVerification.strVerificationKey)
            {
                ModelState.AddModelError("verificationError", "Incorrect Verification Key. Please ensure the correct key is entered. (Case sensitive)");
                clsVerification.strVerificationKey = "";
                return View(clsVerification);
            }

            string strEmailApprovalString = "";
            strEmailApprovalString = clsClient.strUsername + "-" + clsClient.strEmail + "-" + clsClient.strPassword;
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClient.bIsVerified = true;

            clsClientsManager.saveClient(clsClient);
            sendApprovalEmail(clsClient.strEmail, strEmailApprovalString);

            ModelState.AddModelError("verificationError", " "); // Clear error

            clsVerification.strVerificationKey = "";
            ModelState.AddModelError("verificationSuccess", "Your account has been verified. An email has been sent to Softserve to approve your account. You will recieve an email once you have successfully been approved.");

            TempData["bIsVerified"] = true;
            Session["clsClient"] = null;
            return View(clsVerification);
        }

        private void sendVerificationEmail(string strEmail, string strVerificationKey)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Your verification key for your Softserve Portal registration is: ");
            sb.AppendLine("<b>" + strVerificationKey + "</b><br/><br/>");
            sb.AppendLine("Regards <br/>");
            sb.AppendLine("The Softserve Team <br/>");

            Attachment[] emptyAttach = new Attachment[] { };
            //clsEmailComponent.SendMail("noreply@softservedigital.co.za", strEmail, "", "support@softservedigital.co.za, admin@softservedigital.co.za", "Verification Key", sb.ToString(), emptyAttach, true);
            //TEST (remove)
            clsEmailComponent.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Verification Key", sb.ToString(), emptyAttach, true);
        }

        private void sendApprovalEmail(string strEmail, string strEmailApprovalString)
        {
            string strWebRootURL = ConfigurationManager.AppSettings["WebRootURL"].ToString();
            string strApprove = strWebRootURL + "/Accounts/ApproveUser?user=" + strEmailApprovalString;
            string strReject = strWebRootURL + "/Accounts/RejectUser?user=" + strEmailApprovalString;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Please click one of the links below to approve or reject new Portal User: ");
            sb.AppendLine("<b>" + strEmail + "</b><br/><br/>");
            sb.AppendLine("<a href=" + strApprove + " target='_blank'>Approve</a><br/><br/>");
            sb.AppendLine("<a href=" + strReject + " target='_blank'>Reject</a><br/><br/>");
            sb.AppendLine("Regards <br/>");
            sb.AppendLine("The Softserve Team <br/>");

            Attachment[] emptyAttach = new Attachment[] { };
            //clsEmailComponent.SendMail("noreply@softservedigital.co.za", "admin@softservedigital.co.za", "", "support@softservedigital.co.za", "Approve User", sb.ToString(), emptyAttach, true);
            //TEST (remove)
            clsEmailComponent.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Approve User", sb.ToString(), emptyAttach, true);
        }

        public ActionResult ApproveUser(string user)
        {
            //Managers
            clsClientsManager clsClientsManager = new clsClientsManager();
            string[] strUserInfo = user.Split('-');
            string strUsername = strUserInfo[0];
            string strEmail = strUserInfo[1];
            string strPassword = strUserInfo[2];

            //User
            clsClients clsClient = clsClientsManager.getClientByEmail(strEmail.ToLower());

            if (clsClient != null)
            {
                if (clsClient.strPassword == strPassword && clsClient.strUsername == strUsername)
                {
                    clsClient.strIsApproved = "Approved";
                    clsClientsManager.saveClient(clsClient);
                    sendApprovedEmail(strEmail);
                }
            }

            return RedirectToAction("UserApproved", "Accounts");
        }

        public ActionResult RejectUser(string user)
        {
            //Managers
            clsClientsManager clsClientsManager = new clsClientsManager();
            string[] strUserInfo = user.Split('-');
            string strUsername = strUserInfo[0];
            string strEmail = strUserInfo[1];
            string strPassword = strUserInfo[2];

            //User
            clsClients clsClient = clsClientsManager.getClientByEmail(strEmail.ToLower());

            if (clsClient != null)
            {
                if (clsClient.strPassword == strPassword && clsClient.strUsername == strUsername)
                {
                    clsClient.strIsApproved = "Rejected";
                    clsClientsManager.saveClient(clsClient);
                }
            }

            return RedirectToAction("UserRejected", "Accounts");
        }

        public ActionResult UserApproved()
        {
            return View();
        }
        public ActionResult UserRejected()
        {
            return View();
        }

        public ActionResult ForgotPassword()
        {
            clsForgotPassword clsForgotPassword = new clsForgotPassword();
            return View(clsForgotPassword);
        }

        [HttpPost]
        public ActionResult ForgotPassword(clsForgotPassword clsForgotPassword)
        {
            //Redirect to login if email sent
            if (Session["bForgotPasswordEmailSent"] != null)
            {
                if (Convert.ToBoolean(Session["bForgotPasswordEmailSent"]) == true)
                {
                    ModelState.AddModelError("forgotPasswordSuccess", "");
                    Session["bForgotPasswordEmailSent"] = null;
                    return RedirectToAction("Login", "Accounts");
                }
            }

            clsClientsManager clsClientsManager = new clsClientsManager();

            //User
            clsClients clsClient = clsClientsManager.getClientByEmail(clsForgotPassword.strEmail.ToLower());
            if (clsClient != null)
            {
                string strNewPassword = clsCommonFunctions.strCreateRandomString(6);

                clsClient.strPassword = clsCommonFunctions.GetMd5Sum(strNewPassword);
                //clsClientsManager.saveClient(clsClient);

                clsForgotPassword.strEmail = "";
                sendForgotPasswordEmail(clsClient.strEmail, strNewPassword);
            }

            ModelState.AddModelError("forgotPasswordSuccess", "Your password has been reset. A new password has been sent to your email account to log in.");
            Session["bForgotPasswordEmailSent"] = true;
            TempData["bForgotPasswordEmailSent"] = true;

            return View(clsForgotPassword);
        }

        private void sendForgotPasswordEmail(string strEmail, string strNewPassword)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Your password has been reset. Your new password is: ");
            sb.AppendLine("<b>" + strNewPassword + "</b><br/><br/>");
            sb.AppendLine("Regards <br/>");
            sb.AppendLine("The Softserve Team <br/>");

            Attachment[] emptyAttach = new Attachment[] { };
            //clsEmailComponent.SendMail("noreply@softservedigital.co.za", strEmail, "", "support@softservedigital.co.za", "Password Reset", sb.ToString(), emptyAttach, true);
            //TEST (remove)
            clsEmailComponent.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Password Reset", sb.ToString(), emptyAttach, true);
        }

        private void sendApprovedEmail(string strEmail)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Your account has been approved. You may now log in with the below email: <br/>");
            sb.AppendLine("<b>" + strEmail + "</b><br/><br/>");
            sb.AppendLine("Regards <br/>");
            sb.AppendLine("The Softserve Team <br/>");

            Attachment[] emptyAttach = new Attachment[] { };
            //clsEmailComponent.SendMail("noreply@softservedigital.co.za", "strEmail", "", "admin@softservedigital.co.za,support@softservedigital.co.za", "Account Approved", sb.ToString(), emptyAttach, true);
            //TEST (remove)
            clsEmailComponent.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Account Approved", sb.ToString(), emptyAttach, true);
        }

        public ActionResult CompleteProfile()
        {
            //Redirect to login if null session exists
            if (Session["clsClient"] == null)
                return RedirectToAction("Login", "Accounts");

            clsClients clsClient = (clsClients)Session["clsClient"];
            if (clsClient.bIsVerified == true)
            {
                if (clsClient.strIsApproved == "Approved")
                {
                    if (clsClient.bIsProfileCompleted == true)
                    {
                        return RedirectToAction("Index", "Home");//Logged in
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Accounts");
                }
            }
            else
            {
                return RedirectToAction("Verification", "Accounts");
            }

            clsCompleteProfile clsCompleteProfile = new clsCompleteProfile();

            return View(clsCompleteProfile);
        }

        [HttpPost]
        public ActionResult CompleteProfile(clsCompleteProfile clsCompleteProfile)
        {
            //Redirect to login if null session exists
            if (Session["clsClient"] == null)
                return RedirectToAction("Login", "Accounts");

            clsClients clsClient = (clsClients)Session["clsClient"];
            clsClientContactDetails clsClientContactDetails = new clsClientContactDetails();
            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();

            //Managers
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
            clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

            clsClient.strCompanyName = clsCompleteProfile.strCompanyName;
            clsClient.bIsProfileCompleted = true;
            clsClientsManager.saveClient(clsClient);

            clsClientContactDetails.strContactName = clsCompleteProfile.strContactName;
            clsClientContactDetails.strContactEmail = clsCompleteProfile.strEmail;
            clsClientContactDetails.strContactNumber = clsCompleteProfile.strContactNumber;
            clsClientContactDetails.bIsPrimaryContact = true;
            int iClientContactDetailID = clsClientContactDetailsManager.saveClientContactDetail(clsClientContactDetails);

            clsClientClientContactDetailsLinkTable.iClientID = clsClient.iClientID;
            clsClientClientContactDetailsLinkTable.iClientContactDetailID = iClientContactDetailID;
            clsClientClientContactDetailsLinkTableManager.saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable);

            return RedirectToAction("Index", "Home"); //Logged in
        }

    }
}