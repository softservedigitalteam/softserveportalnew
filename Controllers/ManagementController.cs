﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.View_Models.Management;
using SoftservePortalNew.Model_Manager;
using System.Globalization;
using SoftservePortalNew.Models;
using System.Text;
using System.Net.Mail;
using SoftservePortalNew.Assistant_Classes;

namespace SoftservePortalNew.Controllers
{
    public class ManagementController : Controller
    {
        // GET: Management

        public ActionResult ClientChangeLogs()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();

            clsClientChangeLogs clsClientChangeLogs = new clsClientChangeLogs();
            clsClientChangeLogs.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();

            return View(clsClientChangeLogs);
        }

        [HttpPost]
        public ActionResult ClientChangeLogs(clsClientChangeLogs clsClientChangeLogs)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            TempData["ChangeLogClientID"] = clsClientChangeLogs.iClientID;
            TempData["ChangeLogMonth"] = DateTime.Today.Month;
            TempData["ChangeLogYear"] = DateTime.Today.Year;

            return RedirectToAction("ClientChangeLog", "Management");
        }

        public ActionResult ClientChangeLog()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }


            int iClientID = 0;
            if (TempData["ChangeLogClientID"] != null)
                iClientID = Convert.ToInt32(TempData["ChangeLogClientID"]);

            if (iClientID != 0)
            {
                clsClientChangeLog clsClientChangeLog = new clsClientChangeLog();
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                clsTimeTrackUsersManager clsTimeTrackUsersManager = new clsTimeTrackUsersManager();

                clsClientChangeLog.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
                clsClientChangeLog.lstTaskTags = clsTaskTagsManager.getAllTaskTagsOnlyList();
                clsClientChangeLog.lstTimeTrackUsers = clsTimeTrackUsersManager.getAllTimeTrackUsersOnlyList();

                clsClientChangeLog.iClientID = iClientID;

                if (TempData["ChangeLogMonth"] != null && TempData["ChangeLogYear"] != null)
                {
                    int iMonth = Convert.ToInt32(TempData["ChangeLogMonth"]);
                    int iYear = Convert.ToInt32(TempData["ChangeLogYear"]);

                    DateTime dtNewDate = new DateTime(iYear, iMonth, 1);
                    clsClientChangeLog.strSelectedDate = dtNewDate.ToString("MMMMM yyyy");
                }
                else
                {
                    clsClientChangeLog.strSelectedDate = DateTime.Today.ToString("MMMMM yyyy");
                }

                return View(clsClientChangeLog);
            }

            return RedirectToAction("ClientChangeLogs", "Management");
        }

        public ActionResult _ClientChangeLogPartialView()
        {
            int iClientID = 0;
            if (TempData["ChangeLogClientID"] != null)
                iClientID = Convert.ToInt32(TempData["ChangeLogClientID"]);

            if (iClientID != 0)
            {
                clsClientChangeLog clsClientChangeLog = new clsClientChangeLog();
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();
                clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();

                clsClientChangeLog.strTasksMonthTitle = DateTime.Today.ToString("MMMMM");

                clsClientChangeLog.clsClient = clsClientsManager.getClientByID(iClientID);
                clsClientChangeLog.lstProjects = clsProjectsManager.getAllProjectsListByClientID(iClientID);
                clsClientChangeLog.lstTaskTags = clsTaskTagsManager.getAllTaskTagsOnlyList();

                if (TempData["ChangeLogMonth"] != null && TempData["ChangeLogYear"] != null)
                {
                    int iMonth = Convert.ToInt32(TempData["ChangeLogMonth"]);
                    int iYear = Convert.ToInt32(TempData["ChangeLogYear"]);
                    DateTime dtStartDate = new DateTime(iYear, iMonth, 1);
                    DateTime dtEndDate = dtStartDate.AddMonths(1);

                    //Filter Tasks List By Month
                    foreach (clsProjects clsProjects in clsClientChangeLog.lstProjects)
                        clsProjects.lstTasks = clsProjects.lstTasks.Where(Task => Task.dtAdded >= dtStartDate && Task.dtAdded < dtEndDate).ToList();

                    clsClientChangeLog.strTasksMonthTitle = dtStartDate.ToString("MMMMM");
                }
                else
                {
                    clsClientChangeLog.strTasksMonthTitle = DateTime.Today.ToString("MMMMM");
                }

                return PartialView(clsClientChangeLog);
            }

            return RedirectToAction("ClientChangeLogs", "Management");
        }
        
        [HttpPost]
        public ActionResult AddNewTask(int iProjectID, string strTaskTitle, string strTaskStartDate, string strTaskStartTime, string strTaskEndDate, string strTaskEndTime, int iTaskTypeID, int iTaskAssigneeID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bNewTaskAdded = false;

            if (iProjectID != 0 && iTaskAssigneeID != 0 && iTaskTypeID != 0
                && strTaskTitle != null && strTaskTitle != "" 
                && strTaskStartDate != null && strTaskStartDate != ""
                && strTaskStartTime != null && strTaskStartTime != ""
                && strTaskEndDate != null && strTaskEndDate != ""
                && strTaskEndTime != null && strTaskEndTime != "")
            {
                clsTasksManager clsTasksManager = new clsTasksManager();

                DateTime dtStartDate = Convert.ToDateTime(strTaskStartDate);
                DateTime dtStartTime = DateTime.Parse(strTaskStartTime, CultureInfo.CurrentCulture);
                DateTime dtTaskStartDate = new DateTime(dtStartDate.Year, dtStartDate.Month, dtStartDate.Day, dtStartTime.Hour, dtStartTime.Minute, dtStartTime.Second);

                DateTime dtEndDate = Convert.ToDateTime(strTaskEndDate);
                DateTime dtEndTime = DateTime.Parse(strTaskEndTime, CultureInfo.CurrentCulture);
                DateTime dtTaskEndDate = new DateTime(dtEndDate.Year, dtEndDate.Month, dtEndDate.Day, dtEndTime.Hour, dtEndTime.Minute, dtEndTime.Second);

                if (dtTaskEndDate > dtTaskStartDate)
                {
                    clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();
                    List<clsTaskTags> lstTaskTags = clsTaskTagsManager.getAllTaskTagsOnlyList();

                    TimeSpan tsDuration = dtTaskEndDate - dtTaskStartDate;

                    clsTasks clsNewTask = new clsTasks();
                    clsNewTask.dtAdded = dtTaskStartDate;
                    clsNewTask.iAddedBy = iTaskAssigneeID;
                    clsNewTask.dtEdited = dtTaskEndDate;
                    clsNewTask.iEditedBy = iTaskAssigneeID;

                    clsNewTask.strTaskName = strTaskTitle;
                    clsNewTask.strDuration = tsDuration.TotalHours + "h " + tsDuration.Minutes + "m " + tsDuration.Seconds + "s";
                    clsNewTask.dtTimeSessionElapsed = dtTaskEndDate;
                    clsNewTask.bIsTaskcompleted = true;
                    clsNewTask.iAsanaTaskID = 0;

                    clsNewTask.iProjectID = iProjectID;
                    clsNewTask.iTaskTagID = iTaskTypeID;

                    int iNewTaskID = clsTasksManager.saveClientChangeLogTask(clsNewTask);

                    bNewTaskAdded = true;
                    return Json(new { bNewTaskAdded = bNewTaskAdded, iNewTaskID = iNewTaskID, lstTaskTags = lstTaskTags }, JsonRequestBehavior.AllowGet);
                }
                
            }

            return Json(new { bNewTaskAdded = bNewTaskAdded }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateClientChangeLog(int iClientID, string strSelectedDate)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bClientChangeLogUpdated = false;

            if (iClientID != 0 && strSelectedDate != null && strSelectedDate != "")
            {
                string[] dtArray = strSelectedDate.Split(' ');

                int iMonth = DateTime.ParseExact(dtArray[0], "MMMM", CultureInfo.CurrentCulture).Month;
                int iYear = Convert.ToInt32(dtArray[1]);

                TempData["ChangeLogClientID"] = iClientID;
                TempData["ChangeLogMonth"] = iMonth;
                TempData["ChangeLogYear"] = iYear;

                bClientChangeLogUpdated = true;
                return Json(new { bClientChangeLogUpdated = bClientChangeLogUpdated }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { bClientChangeLogUpdated = bClientChangeLogUpdated }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSelectedTaskTagID(int iSelectedTaskID, int iNewSelectedTaskTagID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bTaskUpdated = false;

            if (iSelectedTaskID != 0 && iSelectedTaskID != 0)
            {
                clsTasksManager clsTasksManager = new clsTasksManager();
                clsTasks clsTask = clsTasksManager.getTaskByID(iSelectedTaskID);
                clsTask.iTaskTagID = iNewSelectedTaskTagID;
                clsTasksManager.saveTask(clsTask);

                bTaskUpdated = true;

                return Json(new { bTaskUpdated = bTaskUpdated }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { bTaskUpdated = bTaskUpdated }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSelectedTaskName(string strTaskID, string strNewTaskName)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bTaskUpdated = false;

            if (strTaskID != null && strTaskID != "" && strNewTaskName != null && strNewTaskName != "")
            {
                strTaskID = strTaskID.Replace("cTaskTitleCell", "");
                int iTaskID = Convert.ToInt32(strTaskID);

                clsTasksManager clsTasksManager = new clsTasksManager();
                clsTasks clsTask = clsTasksManager.getTaskByID(iTaskID);

                clsTask.strTaskName = strNewTaskName;
                clsTasksManager.saveTask(clsTask);

                bTaskUpdated = true;

                return Json(new { bTaskUpdated = bTaskUpdated }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { bTaskUpdated = bTaskUpdated }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTasksTagsBulk(string strSelectedTaskTag, string strSelectedTasks)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bTasksUpdated = false;

            if (strSelectedTaskTag != null && strSelectedTaskTag != "" && strSelectedTasks != null && strSelectedTasks != "")
            {
                string[] aSelectedTasks = strSelectedTasks.Split(',');
                if (aSelectedTasks.Count() > 0)
                {
                    clsTasksManager clsTasksManager = new clsTasksManager();
                    clsTaskTagsManager clsTaskTagsManager = new clsTaskTagsManager();

                    List<clsTaskTags> lstTaskTags = clsTaskTagsManager.getAllTaskTagsOnlyList();
                    int iTaskTagID = lstTaskTags.FirstOrDefault(TaskTag => TaskTag.strTitle.ToLower() == strSelectedTaskTag.ToLower()).iTaskTagID;


                    foreach (string strSelectedTask in aSelectedTasks)
                    {
                        clsTasks clsTask = clsTasksManager.getTaskByID(Convert.ToInt32(strSelectedTask));
                        clsTask.iTaskTagID = iTaskTagID;
                        clsTasksManager.saveTask(clsTask);
                    }

                    bTasksUpdated = true;
                    return Json(new { bTasksUpdated = bTasksUpdated }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { bTasksUpdated = bTasksUpdated }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EmailClientChangeLogInfo(int iClientID, int iProjectID, string strSelectedTasks)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEmailClientChangeLogUpdated = false;

            if (iClientID != 0 && iProjectID != 0 && strSelectedTasks != null && strSelectedTasks != "")
            {
                TempData["EmailChangeLogClientID"] = iClientID;
                TempData["EmailChangeLogProjectID"] = iProjectID;
                TempData["strSelectedTasks"] = strSelectedTasks;
                
                bEmailClientChangeLogUpdated = true;

                return Json(new { bEmailClientChangeLogUpdated = bEmailClientChangeLogUpdated }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { bEmailClientChangeLogUpdated = bEmailClientChangeLogUpdated }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailClientChangeLog()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            int iClientID = 0;
            int iProjectID = 0;
            string strSelectedTasks = "";

            if (TempData["EmailChangeLogClientID"] != null && TempData["EmailChangeLogProjectID"] != null && TempData["strSelectedTasks"] != null)
            {
                iClientID = Convert.ToInt32(TempData["EmailChangeLogClientID"]);
                iProjectID = Convert.ToInt32(TempData["EmailChangeLogProjectID"]);
                strSelectedTasks = TempData["strSelectedTasks"].ToString();

                string[] aSelectedTasks = strSelectedTasks.Split(',');

                clsClientsManager clsClientsManager = new clsClientsManager();
                clsProjectsManager clsProjectsManager = new clsProjectsManager();
                clsTasksManager clsTasksManager = new clsTasksManager();

                clsEmailClientChangeLog clsEmailClientChangeLog = new clsEmailClientChangeLog();
                clsClients clsClient = clsClientsManager.getClientByID(iClientID);

                clsEmailClientChangeLog.ClientProject = clsProjectsManager.getProjectByIDAndClientID(iProjectID, iClientID);
                clsEmailClientChangeLog.strClientEmail = clsClient.strEmail;
                clsEmailClientChangeLog.strEmailSubject = clsEmailClientChangeLog.ClientProject.strTitle + " Tasks Report";

                List<clsTasks> lstNewTasks = new List<clsTasks>();
                List<clsTasks> lstEnhancedTasks = new List<clsTasks>();
                List<clsTasks> lstFixedTasks = new List<clsTasks>();
                List<clsTasks> lstRemovedTasks = new List<clsTasks>();

                clsEmailClientChangeLog.strNewTasks = "";
                clsEmailClientChangeLog.strEnhancedTasks = "";
                clsEmailClientChangeLog.strFixedTasks = "";
                clsEmailClientChangeLog.strRemovedTasks = "";

                foreach (string strTaskID in aSelectedTasks)
                {
                    clsTasks clsTasks = clsTasksManager.getTaskByID(Convert.ToInt32(strTaskID));
                    if (clsTasks != null)
                    {
                        if (clsTasks.clsTaskTag.strTitle == "New")
                            lstNewTasks.Add(clsTasks);
                        else if (clsTasks.clsTaskTag.strTitle == "Enhancement")
                            lstEnhancedTasks.Add(clsTasks);
                        else if (clsTasks.clsTaskTag.strTitle == "Fixed")
                            lstFixedTasks.Add(clsTasks);
                        else if (clsTasks.clsTaskTag.strTitle == "Removed")
                            lstRemovedTasks.Add(clsTasks);
                        else
                            lstNewTasks.Add(clsTasks);
                    }
                }

                if (lstNewTasks.Count > 0)
                    foreach (clsTasks clsTask in lstNewTasks)
                        clsEmailClientChangeLog.strNewTasks += clsTask.strTaskName + "\r\n";
                if (lstEnhancedTasks.Count > 0)
                    foreach (clsTasks clsTask in lstEnhancedTasks)
                        clsEmailClientChangeLog.strEnhancedTasks += clsTask.strTaskName + "\r\n";
                if (lstFixedTasks.Count > 0)
                    foreach (clsTasks clsTask in lstFixedTasks)
                        clsEmailClientChangeLog.strFixedTasks += clsTask.strTaskName + "\r\n";
                if (lstRemovedTasks.Count > 0)
                    foreach (clsTasks clsTask in lstRemovedTasks)
                        clsEmailClientChangeLog.strRemovedTasks += clsTask.strTaskName + "\r\n";

                return View(clsEmailClientChangeLog);
            }

            return RedirectToAction("ClientChangeLogs", "Management");
        }

        [HttpPost]
        public ActionResult SendEmailClientChangeLog(clsEmailClientChangeLog clsEmailClientChangeLog)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEmailSent = false;
            string strEmail = strEmailContents(clsEmailClientChangeLog);

            try
            {
                Attachment[] emptyAttach = new Attachment[] { };
                //clsEmailComponent.SendMail("noreply@softservedigital.co.za", clsEmailClientChangeLog.strClientEmail, "admin@softservedigital.co.za, support@softservedigital.co.za", "", clsEmailClientChangeLog.strEmailSubject, strEmail, emptyAttach, true);
                //TEST (remove)
                clsEmailComponent.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", clsEmailClientChangeLog.strEmailSubject, strEmail, emptyAttach, true);

                bEmailSent = true;
                return Json(new { bEmailSent = bEmailSent }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { bEmailSent = bEmailSent }, JsonRequestBehavior.AllowGet);
            }
            
        }

        private string strEmailContents(clsEmailClientChangeLog clsEmailClientChangeLog)
        {
            StringBuilder strbMailBuilder = new StringBuilder();

            strbMailBuilder.AppendLine("<!-- Content -->");
            //strbMailBuilder.AppendLine("<tr>");
            //strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
            strbMailBuilder.AppendLine("<tbody>");

            strbMailBuilder.AppendLine("<!-- Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End Spacing -->");

            strbMailBuilder.AppendLine("<!-- Text Header -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table style='width:100%; background-color:#c72026;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td valign='middle' align='center' style='line-height:1;'>");
            strbMailBuilder.AppendLine("<p style='font-family:Folks-Bold; font-size:26px; color:white;'>Project Tasks</p>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End Text Header -->");

            //strbMailBuilder.AppendLine("<!-- Spacing -->");
            //strbMailBuilder.AppendLine("<tr>");
            //strbMailBuilder.AppendLine("<td>");
            //strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
            //strbMailBuilder.AppendLine("<tbody>");
            //strbMailBuilder.AppendLine("<tr>");
            //strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

            //strbMailBuilder.AppendLine("</td>");
            //strbMailBuilder.AppendLine("</tr>");
            //strbMailBuilder.AppendLine("</tbody>");
            //strbMailBuilder.AppendLine("</table>");
            //strbMailBuilder.AppendLine("</td>");
            //strbMailBuilder.AppendLine("</tr>");
            //strbMailBuilder.AppendLine("<!-- End Spacing -->");

            //strbMailBuilder.AppendLine("<!-- Inner Content 1 -->");
            //strbMailBuilder.AppendLine("<tr>");
            //strbMailBuilder.AppendLine("<td>");
            //strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
            //strbMailBuilder.AppendLine("<tbody>");
            //strbMailBuilder.AppendLine("<tr>");

            ////strbMailBuilder.AppendLine("<td valign='middle' align='center'>");
            //strbMailBuilder.AppendLine("<td>");
            //strbMailBuilder.AppendLine("<!-- Batch Info -->");

            ////INFO
            //strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
            //strbMailBuilder.AppendLine("<b>Date: </b>" + dtDateTime);
            //strbMailBuilder.AppendLine("</p>");

            //strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
            //strbMailBuilder.AppendLine("<b>Batch Code: </b>" + strResponseBatchCode);
            //strbMailBuilder.AppendLine("</p>");

            //strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
            //strbMailBuilder.AppendLine("<b>Amount Paid Out: </b>R" + strResponseAmountPaidOut);
            //strbMailBuilder.AppendLine("</p>");
            ////END INFO

            //strbMailBuilder.AppendLine("<!-- End Batch Info -->");
            //strbMailBuilder.AppendLine("</td>");

            //strbMailBuilder.AppendLine("</tr>");
            //strbMailBuilder.AppendLine("</tbody>");
            //strbMailBuilder.AppendLine("</table>");
            //strbMailBuilder.AppendLine("</td>");
            //strbMailBuilder.AppendLine("</tr>");
            //strbMailBuilder.AppendLine("<!-- End Inner Content 1 -->");

            #region NEW TASKS
            //***************************** -- NEW TASKS -- *****************************//

            if (clsEmailClientChangeLog.strNewTasks != null && clsEmailClientChangeLog.strNewTasks != "")
            {
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Text Header -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#c72026;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td valign='middle' align='center' style='line-height:1;'>");
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Bold; font-size:26px; color:white;'>NEW</p>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Text Header -->");

                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Inner Content 2 -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");

                //strbMailBuilder.AppendLine("<td valign='middle' align='center'>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<!-- New Tasks Info -->");

                //NEW TASKS INFO
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
                strbMailBuilder.AppendLine(clsEmailClientChangeLog.strNewTasks.Replace("\r\n", "<br />"));
                strbMailBuilder.AppendLine("</p>");
                //END NEW TASKS INFO

                strbMailBuilder.AppendLine("<!-- End New Tasks Info -->");
                strbMailBuilder.AppendLine("</td>");

                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Inner Content 2 -->");
            }


            //***************************** -- NEW TASKS -- *****************************//
            #endregion

            #region ENHANCED TASKS
            //***************************** -- ENHANCED TASKS -- *****************************//

            if (clsEmailClientChangeLog.strEnhancedTasks != null && clsEmailClientChangeLog.strEnhancedTasks != "")
            {
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Text Header -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#c72026;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td valign='middle' align='center' style='line-height:1;'>");
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Bold; font-size:26px; color:white;'>ENHANCED</p>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Text Header -->");

                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Inner Content 3 -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");

                //strbMailBuilder.AppendLine("<td valign='middle' align='center'>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<!-- Enhanced Tasks Info -->");

                //NEW TASKS INFO
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
                strbMailBuilder.AppendLine(clsEmailClientChangeLog.strEnhancedTasks.Replace("\r\n", "<br />"));
                strbMailBuilder.AppendLine("</p>");
                //END NEW TASKS INFO

                strbMailBuilder.AppendLine("<!-- End Enhanced Tasks Info -->");
                strbMailBuilder.AppendLine("</td>");

                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Inner Content 3 -->");
            }


            //***************************** -- ENHANCED TASKS -- *****************************//
            #endregion

            #region FIXED TASKS
            //***************************** -- FIXED TASKS -- *****************************//

            if (clsEmailClientChangeLog.strFixedTasks != null && clsEmailClientChangeLog.strFixedTasks != "")
            {
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Text Header -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#c72026;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td valign='middle' align='center' style='line-height:1;'>");
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Bold; font-size:26px; color:white;'>FIXED</p>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Text Header -->");

                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Inner Content 4 -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");

                //strbMailBuilder.AppendLine("<td valign='middle' align='center'>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<!-- Fixed Tasks Info -->");

                //NEW TASKS INFO
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
                strbMailBuilder.AppendLine(clsEmailClientChangeLog.strFixedTasks.Replace("\r\n", "<br />"));
                strbMailBuilder.AppendLine("</p>");
                //END NEW TASKS INFO

                strbMailBuilder.AppendLine("<!-- End Fixed Tasks Info -->");
                strbMailBuilder.AppendLine("</td>");

                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Inner Content 4 -->");
            }


            //***************************** -- ENHANCED TASKS -- *****************************//
            #endregion

            #region REMOVED TASKS
            //***************************** -- REMOVED TASKS -- *****************************//

            if (clsEmailClientChangeLog.strRemovedTasks != null && clsEmailClientChangeLog.strRemovedTasks != "")
            {
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Text Header -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#c72026;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td valign='middle' align='center' style='line-height:1;'>");
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Bold; font-size:26px; color:white;'>REMOVED</p>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Text Header -->");

                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:100%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td style='height:20px;' height='20' valign='middle' align='center'>");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Spacing -->");

                strbMailBuilder.AppendLine("<!-- Inner Content 4 -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table style='width:80%; background-color:#ffffff;' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' nobg=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");

                //strbMailBuilder.AppendLine("<td valign='middle' align='center'>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<!-- Removed Tasks Info -->");

                //NEW TASKS INFO
                strbMailBuilder.AppendLine("<p style='font-family:Folks-Normal; font-size:14px; color:black;'>");
                strbMailBuilder.AppendLine(clsEmailClientChangeLog.strRemovedTasks.Replace("\r\n", "<br />"));
                strbMailBuilder.AppendLine("</p>");
                //END NEW TASKS INFO

                strbMailBuilder.AppendLine("<!-- End Removed Tasks Info -->");
                strbMailBuilder.AppendLine("</td>");

                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End Inner Content 4 -->");
            }


            //***************************** -- REMOVED TASKS -- *****************************//
            #endregion

            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            //strbMailBuilder.AppendLine("</td>");
            //strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End Content -->");

            return strbMailBuilder.ToString();
        }
    }
}