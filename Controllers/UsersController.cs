﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.Models;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Assistant_Classes;
using SoftservePortalNew.View_Models.Users;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SoftservePortalNew.Classes;

namespace SoftservePortalNew.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Users()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUsersManager clsUsersManager = new clsUsersManager();
            List<clsUsers> lstUsers = clsUsersManager.getAllUsersOnlyList();

            return View(lstUsers);
        }

        public ActionResult UserAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUserAdd clsUserAdd = new clsUserAdd();
            return View(clsUserAdd);
        }

        [HttpPost]
        public ActionResult UserAdd(clsUserAdd clsUserAdd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsers clsNewUser = new clsUsers();

            clsNewUser.strFirstName = clsUserAdd.clsUser.strFirstName;
            clsNewUser.strSurname = clsUserAdd.clsUser.strSurname;
            clsNewUser.strEmailAddress = clsUserAdd.clsUser.strEmailAddress.ToLower();
            if (clsUserAdd.clsUser.strPassword != null && clsUserAdd.clsUser.strPassword != "")
                clsNewUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserAdd.clsUser.strPassword);
            else
                clsNewUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserAdd.clsUser.strEmailAddress.ToLower());

            //Save image
            if (clsUserAdd.bIsProfileImageSelected == true)
            {
                if (clsUserAdd.strCropImageData != null && clsUserAdd.strCropImageData != "")
                {
                    string data = clsUserAdd.strCropImageData;
                    data = data.Replace("data:image/png;base64,", "");
                    string strImageName = "img_" + clsUserAdd.clsUser.strFirstName + "_" + clsUserAdd.clsUser.strSurname + ".png";
                    string strSmallImageName = "sml_img_" + clsUserAdd.clsUser.strFirstName + "_" + clsUserAdd.clsUser.strSurname + ".png";
                    string newPath = GetUniqueProfileImagePath();

                    string strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Users\\ProfileImages\\" + newPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    clsNewUser.strPathToImages = strDestPath.Replace("\\", "/");
                    clsNewUser.strMasterImage = strImageName;

                    string strImagePath = strFullDestPath + "\\" + strImageName;
                    string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                    byte[] bytes = Convert.FromBase64String(data);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    //Smaller Image
                    Image smallImage = (Image)ResizeImage(image, 35, 35);
                    //Save images
                    image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                }
                else //save default images
                {
                    string newPath = GetUniqueProfileImagePath();
                    string strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Users\\ProfileImages\\" + newPath;
                    string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    clsNewUser.strPathToImages = strDestPath.Replace("\\", "/");
                    clsNewUser.strMasterImage = "default_image.png";

                    string strImagePath = strFullDestPath + "\\" + "default_image.png";
                    string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                    //Copy small and default sizes
                    System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                    System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
                }
            }
            else if (clsUserAdd.bIsAvatarImageSelected == true && clsUserAdd.strAvatarImageName != null && clsUserAdd.strAvatarImageName != "")
            {
                string strAvatarImageName = clsUserAdd.strAvatarImageName + ".png";
                string strSmallAvatarImageName = "sml_" + clsUserAdd.strAvatarImageName + ".png";

                string newPath = GetUniqueProfileImagePath();
                string strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Users\\ProfileImages\\" + newPath;
                string strAvatarImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Avatars";
                string strFullAvatarImagespath = strAvatarImagespath + "\\" + strAvatarImageName;

                if (!System.IO.Directory.Exists(strFullDestPath))
                    System.IO.Directory.CreateDirectory(strFullDestPath);

                clsNewUser.strPathToImages = strDestPath.Replace("\\", "/");
                clsNewUser.strMasterImage = strAvatarImageName;

                string strImagePath = strFullDestPath + "\\" + strAvatarImageName;
                string strSmallImagePath = strFullDestPath + "\\" + strSmallAvatarImageName;

                byte[] bytes = System.IO.File.ReadAllBytes(strFullAvatarImagespath);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Smaller Image
                Image smallImage = (Image)ResizeImage(image, 35, 35);
                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }

            clsNewUser.strClientID = "";
            clsNewUser.strClientSecret = "";
            clsNewUser.strRedirectURL = "";
            clsNewUser.strRefreshToken = "";

            clsUsersManager.saveUser(clsNewUser);

            return RedirectToAction("Users", "Users");
        }

        public ActionResult UserEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("Users", "Users");
            }

            clsUserEdit clsUserEdit = new clsUserEdit();

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUserEdit.clsUser = clsUsersManager.getUserByID(id);

            if (clsUserEdit.clsUser.strPathToImages != null && clsUserEdit.clsUser.strPathToImages != "" && clsUserEdit.clsUser.strMasterImage != null && clsUserEdit.clsUser.strMasterImage != "")
                clsUserEdit.strFullImagePath = clsUserEdit.clsUser.strPathToImages + "/" + clsUserEdit.clsUser.strMasterImage;

            return View(clsUserEdit);
        }

        [HttpPost]
        public ActionResult UserEdit(clsUserEdit clsUserEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsers clsExistingUser = clsUsersManager.getUserByID(clsUserEdit.clsUser.iUserID);

            if (clsExistingUser != null)
            {
                clsExistingUser.strFirstName = clsUserEdit.clsUser.strFirstName;
                clsExistingUser.strSurname = clsUserEdit.clsUser.strSurname;
                clsExistingUser.strEmailAddress = clsUserEdit.clsUser.strEmailAddress.ToLower();

                if (clsUserEdit.strOldPassword != null && clsUserEdit.strOldPassword != "")
                {
                    string strOldPasswordHash = clsCommonFunctions.GetMd5Sum(clsUserEdit.strOldPassword);
                    if (strOldPasswordHash == clsExistingUser.strPassword)
                    {
                        if (clsUserEdit.strNewPassword != null && clsUserEdit.strNewPassword != "")
                        {
                            if (clsUserEdit.strNewPassword == clsUserEdit.strConfirmPassword)
                            {
                                clsExistingUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserEdit.strNewPassword);
                            }
                            else
                            {
                                ModelState.AddModelError("userPasswordError", "Incorrect Password Entered");
                                return View(clsUserEdit);
                            }
                        }
                        else
                        {
                            clsExistingUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserEdit.clsUser.strEmailAddress.ToLower());
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("userPasswordError", "Incorrect Password Entered");
                        return View(clsUserEdit);
                    }
                }

                //Save image
                if (clsUserEdit.bIsProfileImageSelected == true)
                {
                    if (clsUserEdit.strCropImageData != null && clsUserEdit.strCropImageData != "")
                    {
                        string data = clsUserEdit.strCropImageData;
                        data = data.Replace("data:image/png;base64,", "");
                        string strImageName = "img_" + clsUserEdit.clsUser.strFirstName + "_" + clsUserEdit.clsUser.strSurname + ".png";
                        string strSmallImageName = "sml_img_" + clsUserEdit.clsUser.strFirstName + "_" + clsUserEdit.clsUser.strSurname + ".png";

                        string strDestPath = "";
                        if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                        {
                            strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                        }
                        else
                        {
                            string newPath = GetUniqueProfileImagePath();
                            strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                            clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                        }

                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                        if (!System.IO.Directory.Exists(strFullDestPath))
                        {
                            System.IO.Directory.CreateDirectory(strFullDestPath);
                        }
                        else
                        {
                            //Clear files
                            string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                                System.IO.File.Delete(strFilePath);
                        }

                        clsExistingUser.strMasterImage = strImageName;

                        string strImagePath = strFullDestPath + "\\" + strImageName;
                        string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                        byte[] bytes = Convert.FromBase64String(data);

                        Image image;
                        using (MemoryStream ms = new MemoryStream(bytes))
                        {
                            image = Image.FromStream(ms);
                        }

                        //Smaller Image
                        Image smallImage = (Image)ResizeImage(image, 35, 35);
                        //Save images
                        image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                        smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else //save default images
                    {
                        string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";

                        string strDestPath = "";
                        if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                        {
                            strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                        }
                        else
                        {
                            string newPath = GetUniqueProfileImagePath();
                            strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                            clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                        }

                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                        if (!System.IO.Directory.Exists(strFullDestPath))
                        {
                            System.IO.Directory.CreateDirectory(strFullDestPath);
                        }
                        else
                        {
                            //Clear files
                            string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                                System.IO.File.Delete(strFilePath);
                        }

                        clsExistingUser.strMasterImage = "default_image.png";

                        string strImagePath = strFullDestPath + "\\" + "default_image.png";
                        string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                        //Copy small and default sizes
                        System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                        System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
                    }
                }
                else if (clsUserEdit.bIsAvatarImageSelected == true && clsUserEdit.strAvatarImageName != null && clsUserEdit.strAvatarImageName != "")
                {
                    string strAvatarImageName = clsUserEdit.strAvatarImageName + ".png";
                    string strSmallAvatarImageName = "sml_" + clsUserEdit.strAvatarImageName + ".png";
                    string strAvatarImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Avatars";
                    string strFullAvatarImagespath = strAvatarImagespath + "\\" + strAvatarImageName;

                    string strDestPath = "";
                    if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                    {
                        strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                    }
                    else
                    {
                        string newPath = GetUniqueProfileImagePath();
                        strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                        clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                    }

                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                    {
                        System.IO.Directory.CreateDirectory(strFullDestPath);
                    }
                    else
                    {
                        //Clear files
                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                        foreach (string strFilePath in strAllFilePaths)
                            System.IO.File.Delete(strFilePath);
                    }

                    clsExistingUser.strMasterImage = strAvatarImageName;

                    string strImagePath = strFullDestPath + "\\" + strAvatarImageName;
                    string strSmallImagePath = strFullDestPath + "\\" + strSmallAvatarImageName;

                    byte[] bytes = System.IO.File.ReadAllBytes(strFullAvatarImagespath);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    //Smaller Image
                    Image smallImage = (Image)ResizeImage(image, 35, 35);
                    //Save images
                    image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                }

                ModelState.AddModelError("userPasswordError", "");
                clsUsersManager.saveUser(clsExistingUser);

                if (Session["clsUser"] != null)
                {
                    clsUsers clsUser = (clsUsers)Session["clsUser"];
                    if (clsExistingUser.iUserID == clsUser.iUserID)
                    {
                        Session["clsUser"] = null;
                        Session["clsUser"] = clsExistingUser;
                    }
                }
            }

            return RedirectToAction("Users", "Users");
        }

        public ActionResult UserProfile()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUserEdit clsUserEdit = new clsUserEdit();

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUserEdit.clsUser = (clsUsers)Session["clsUser"];

            if (clsUserEdit.clsUser.strPathToImages != null && clsUserEdit.clsUser.strPathToImages != "" && clsUserEdit.clsUser.strMasterImage != null && clsUserEdit.clsUser.strMasterImage != "")
                clsUserEdit.strFullImagePath = clsUserEdit.clsUser.strPathToImages + "/" + clsUserEdit.clsUser.strMasterImage;

            return View(clsUserEdit);
        }

        public ActionResult UserProfileEdit()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsUserEdit clsUserEdit = new clsUserEdit();

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUserEdit.clsUser = (clsUsers)Session["clsUser"];

            if (clsUserEdit.clsUser.strPathToImages != null && clsUserEdit.clsUser.strPathToImages != "" && clsUserEdit.clsUser.strMasterImage != null && clsUserEdit.clsUser.strMasterImage != "")
                clsUserEdit.strFullImagePath = clsUserEdit.clsUser.strPathToImages + "/" + clsUserEdit.clsUser.strMasterImage;

            return View(clsUserEdit);
        }

        [HttpPost]
        public ActionResult UserProfileEdit(clsUserEdit clsUserEdit)
        {
            if (Session["clsUser"] == null)
                return RedirectToAction("Index", "Home");

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsers clsExistingUser = clsUsersManager.getUserByID(clsUserEdit.clsUser.iUserID);

            if (clsExistingUser != null)
            {
                clsExistingUser.strFirstName = clsUserEdit.clsUser.strFirstName;
                clsExistingUser.strSurname = clsUserEdit.clsUser.strSurname;
                clsExistingUser.strEmailAddress = clsUserEdit.clsUser.strEmailAddress.ToLower();

                if (clsUserEdit.strOldPassword != null && clsUserEdit.strOldPassword != "")
                {
                    string strOldPasswordHash = clsCommonFunctions.GetMd5Sum(clsUserEdit.strOldPassword);
                    if (strOldPasswordHash == clsExistingUser.strPassword)
                    {
                        if (clsUserEdit.strNewPassword != null && clsUserEdit.strNewPassword != "")
                        {
                            if (clsUserEdit.strNewPassword == clsUserEdit.strConfirmPassword)
                            {
                                clsExistingUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserEdit.strNewPassword);
                            }
                            else
                            {
                                ModelState.AddModelError("userPasswordError", "Incorrect Password Entered");
                                return View(clsUserEdit);
                            }
                        }
                        else
                        {
                            clsExistingUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserEdit.clsUser.strEmailAddress.ToLower());
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("userPasswordError", "Incorrect Password Entered");
                        return View(clsUserEdit);
                    }
                }

                //Save image
                if (clsUserEdit.bIsProfileImageSelected == true)
                {
                    if (clsUserEdit.strCropImageData != null && clsUserEdit.strCropImageData != "")
                    {
                        string data = clsUserEdit.strCropImageData;
                        data = data.Replace("data:image/png;base64,", "");
                        string strImageName = "img_" + clsUserEdit.clsUser.strFirstName + "_" + clsUserEdit.clsUser.strSurname + ".png";
                        string strSmallImageName = "sml_img_" + clsUserEdit.clsUser.strFirstName + "_" + clsUserEdit.clsUser.strSurname + ".png";

                        string strDestPath = "";
                        if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                        {
                            strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                        }
                        else
                        {
                            string newPath = GetUniqueProfileImagePath();
                            strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                            clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                        }

                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                        if (!System.IO.Directory.Exists(strFullDestPath))
                        {
                            System.IO.Directory.CreateDirectory(strFullDestPath);
                        }
                        else
                        {
                            //Clear files
                            string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                                System.IO.File.Delete(strFilePath);
                        }

                        clsExistingUser.strMasterImage = strImageName;

                        string strImagePath = strFullDestPath + "\\" + strImageName;
                        string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                        byte[] bytes = Convert.FromBase64String(data);

                        Image image;
                        using (MemoryStream ms = new MemoryStream(bytes))
                        {
                            image = Image.FromStream(ms);
                        }

                        //Smaller Image
                        Image smallImage = (Image)ResizeImage(image, 35, 35);
                        //Save images
                        image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                        smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else //save default images
                    {
                        string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";

                        string strDestPath = "";
                        if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                        {
                            strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                        }
                        else
                        {
                            string newPath = GetUniqueProfileImagePath();
                            strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                            clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                        }

                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                        if (!System.IO.Directory.Exists(strFullDestPath))
                        {
                            System.IO.Directory.CreateDirectory(strFullDestPath);
                        }
                        else
                        {
                            //Clear files
                            string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                                System.IO.File.Delete(strFilePath);
                        }

                        clsExistingUser.strMasterImage = "default_image.png";

                        string strImagePath = strFullDestPath + "\\" + "default_image.png";
                        string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                        //Copy small and default sizes
                        System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                        System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
                    }
                }
                else if (clsUserEdit.bIsAvatarImageSelected == true && clsUserEdit.strAvatarImageName != null && clsUserEdit.strAvatarImageName != "")
                {
                    string strAvatarImageName = clsUserEdit.strAvatarImageName + ".png";
                    string strSmallAvatarImageName = "sml_" + clsUserEdit.strAvatarImageName + ".png";
                    string strAvatarImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Avatars";
                    string strFullAvatarImagespath = strAvatarImagespath + "\\" + strAvatarImageName;

                    string strDestPath = "";
                    if (clsExistingUser.strPathToImages != null && clsExistingUser.strPathToImages != "")
                    {
                        strDestPath = clsExistingUser.strPathToImages.Replace("/", "\\");
                    }
                    else
                    {
                        string newPath = GetUniqueProfileImagePath();
                        strDestPath = "Documents\\Users\\ProfileImages\\" + newPath;
                        clsExistingUser.strPathToImages = strDestPath.Replace("\\", "/");
                    }

                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                    {
                        System.IO.Directory.CreateDirectory(strFullDestPath);
                    }
                    else
                    {
                        //Clear files
                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                        foreach (string strFilePath in strAllFilePaths)
                            System.IO.File.Delete(strFilePath);
                    }

                    clsExistingUser.strMasterImage = strAvatarImageName;

                    string strImagePath = strFullDestPath + "\\" + strAvatarImageName;
                    string strSmallImagePath = strFullDestPath + "\\" + strSmallAvatarImageName;

                    byte[] bytes = System.IO.File.ReadAllBytes(strFullAvatarImagespath);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    //Smaller Image
                    Image smallImage = (Image)ResizeImage(image, 35, 35);
                    //Save images
                    image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                }

                ModelState.AddModelError("userPasswordError", "");
                clsUsersManager.saveUser(clsExistingUser);

                Session["clsUser"] = null;
                Session["clsUser"] = clsExistingUser;
            }

            return RedirectToAction("UserProfile", "Users");
        }

        //Delete
        [HttpPost]
        public ActionResult UserDelete(int iUserID)
        {
            //Redirect to login if null session exists
            if (Session["clsUser"] == null)
                return RedirectToAction("Index", "Home");

            bool bIsSuccess = false;

            if (iUserID == 0)
            {
                return RedirectToAction("Users", "Users");
            }

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsersManager.removeUserByID(iUserID);

            bIsSuccess = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Get unique path
        private string GetUniqueProfileImagePath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Users\\ProfileImages";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\ProfileImage" + iCount) == true)
            {
                iCount++;
            }
            return "ProfileImage" + iCount;
        }

        //Resize image
        private Bitmap ResizeImage(Image image, int width, int height)
        {
            Rectangle recRectangle = new Rectangle(0, 0, width, height);
            Bitmap bitImage = new Bitmap(width, height);

            bitImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(bitImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapImageMode = new ImageAttributes())
                {
                    wrapImageMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, recRectangle, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapImageMode);
                }
            }

            return bitImage;
        }
    }
}