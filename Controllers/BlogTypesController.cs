﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.BlogTypes;

namespace SoftservePortalNew.Controllers
{
    public class BlogTypesController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: BlogTypes
        public ActionResult BlogTypes()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            List<clsBlogTypes> lstBlogTypes = clsBlogTypesManager.getAllBlogTypesOnlyList();

            return View(lstBlogTypes); 
        }

        public ActionResult BlogTypeAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTypes clsBlogType = new clsBlogTypes();

            return View(clsBlogType);
        }

        [HttpPost]
        public ActionResult BlogTypeAdd(clsBlogTypes clsBlogType)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTypes clsBlogTypeNew = new clsBlogTypes();

            clsBlogTypeNew.strTitle = clsBlogType.strTitle;
            clsBlogTypeNew.strDescription = clsBlogType.strDescription;

            clsBlogTypesManager.saveBlogType(clsBlogTypeNew);
            return RedirectToAction("BlogTypes", "BlogTypes");
        }

        public ActionResult BlogTypeEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("BlogTypes", "BlogTypes");
            }

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTypes clsBlogType = clsBlogTypesManager.getBlogTypeByID(id);

            clsBlogTypeEdit clsBlogTypeEdit = new clsBlogTypeEdit();
            clsBlogTypeEdit.iBlogTypeID = clsBlogType.iBlogTypeID;
            clsBlogTypeEdit.strTitle = clsBlogType.strTitle;
            clsBlogTypeEdit.strDescription = clsBlogType.strDescription;

            return View(clsBlogTypeEdit);
        }

        [HttpPost]
        public ActionResult BlogTypeEdit(clsBlogTypeEdit clsBlogTypeEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTypes clsBlogTypeUpdate = clsBlogTypesManager.getBlogTypeByID(clsBlogTypeEdit.iBlogTypeID);

            clsBlogTypeUpdate.strDescription = clsBlogTypeEdit.strDescription;

            clsBlogTypesManager.saveBlogType(clsBlogTypeUpdate);
            return RedirectToAction("BlogTypes", "BlogTypes");
        }

        [HttpPost]
        public ActionResult DeleteBlogType(int iBlogTypeID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTypesManager.removeBlogTypeByID(iBlogTypeID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfBlogTypeExists(string strTitle)
        {
            bool bCanUseBlogType = false;
            bool bExists = db.tblBlogTypes.Any(BlogType => BlogType.strTitle.ToLower() == strTitle.ToLower() && BlogType.bIsDeleted == false);

            if (bExists == false)
                bCanUseBlogType = true;

            return Json(bCanUseBlogType, JsonRequestBehavior.AllowGet);
        }

    }
}