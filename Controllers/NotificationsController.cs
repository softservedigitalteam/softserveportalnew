﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.Notifications;

namespace SoftservePortalNew.Controllers
{
    public class NotificationsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: Notifications
        public ActionResult Notifications()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
            List<clsNotifications> lstNotifications = clsNotificationsManager.getAllNotificationsList().OrderBy(Notification => Notification.clsClient.strCompanyName).ToList();

            return View(lstNotifications); 
        }

        public ActionResult NotificationAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();

            clsNotificationAdd clsNotificationAdd = new clsNotificationAdd();
            clsNotificationAdd.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
            clsNotificationAdd.lstNotificationTypes = clsNotificationTypesManager.getAllNotificationTypesOnlyList().OrderBy(NotificationType => NotificationType.strTitle).ToList();

            return View(clsNotificationAdd);
        }

        [HttpPost]
        public ActionResult NotificationAdd(clsNotificationAdd clsNotificationAdd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
            clsNotifications clsNotificationNew = new clsNotifications();

            clsNotificationNew.strTitle = clsNotificationAdd.clsNotification.strTitle;
            clsNotificationNew.strDescription = clsNotificationAdd.clsNotification.strDescription;
            clsNotificationNew.iClientID = clsNotificationAdd.clsNotification.iClientID;
            clsNotificationNew.iNotificationTypeID = clsNotificationAdd.clsNotification.iNotificationTypeID;
            clsNotificationNew.bIsRead = clsNotificationAdd.clsNotification.bIsRead;

            clsNotificationsManager.saveNotification(clsNotificationNew);

            return RedirectToAction("Notifications", "Notifications");
        }

        public ActionResult NotificationEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("Notifications", "Notifications");
            }

            clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();

            clsNotificationEdit clsNotificationEdit = new clsNotificationEdit();
            clsNotificationEdit.clsNotification = clsNotificationsManager.getNotificationByID(id);

            clsNotificationEdit.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
            clsNotificationEdit.lstNotificationTypes = clsNotificationTypesManager.getAllNotificationTypesOnlyList().OrderBy(NotificationType => NotificationType.strTitle).ToList();

            return View(clsNotificationEdit);
        }

        [HttpPost]
        public ActionResult NotificationEdit(clsNotificationEdit clsNotificationEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
            clsNotifications clsNotificationUpdate = clsNotificationsManager.getNotificationByID(clsNotificationEdit.clsNotification.iNotificationID);

            clsNotificationUpdate.strTitle = clsNotificationEdit.clsNotification.strTitle;
            clsNotificationUpdate.strDescription = clsNotificationEdit.clsNotification.strDescription;
            clsNotificationUpdate.iClientID = clsNotificationEdit.clsNotification.iClientID;
            clsNotificationUpdate.iNotificationTypeID = clsNotificationEdit.clsNotification.iNotificationTypeID;
            clsNotificationUpdate.bIsRead = clsNotificationEdit.clsNotification.bIsRead;

            clsNotificationsManager.saveNotification(clsNotificationUpdate);

            return RedirectToAction("Notifications", "Notifications");
        }

        [HttpPost]
        public ActionResult DeleteNotification(int iNotificationID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsNotificationsManager clsNotificationsManager = new clsNotificationsManager();
            clsNotificationsManager.removeNotificationByID(iNotificationID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfNotificationExists(string strTitle)
        {
            bool bCanUseNotification = false;
            bool bExists = db.tblNotifications.Any(Notification => Notification.strTitle.ToLower() == strTitle.ToLower() && Notification.bIsDeleted == false);

            if (bExists == false)
                bCanUseNotification = true;

            return Json(bCanUseNotification, JsonRequestBehavior.AllowGet);
        }

    }
}