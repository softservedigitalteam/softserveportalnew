﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.ClientLinks;


namespace SoftservePortalNew.Controllers
{
    public class ClientLinksController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: ClientLinks
        public ActionResult ClientLinks()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
            List<clsClientLinks> lstClientLinks = clsClientLinksManager.getAllClientLinksList();

            return View(lstClientLinks);
        }

        public ActionResult ClientLinkAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientLinkAdd clsClientLinkAdd = new clsClientLinkAdd();

            clsClientLinkAdd.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Clients => Clients.strCompanyName).ToList();

            return View(clsClientLinkAdd);
        }

        [HttpPost]
        public ActionResult ClientLinkAdd(clsClientLinkAdd clsClientLinkAdd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
            clsClientLinks clsClientLinkNew = new clsClientLinks();

            clsClientLinkNew.strTitle = clsClientLinkAdd.clsClientLink.strTitle;
            clsClientLinkNew.strDescription = clsClientLinkAdd.clsClientLink.strDescription;
            clsClientLinkNew.strLink = clsClientLinkAdd.clsClientLink.strLink;
            clsClientLinkNew.iClientID = clsClientLinkAdd.clsClientLink.iClientID;

            clsClientLinksManager.saveClientLink(clsClientLinkNew);

            return RedirectToAction("ClientLinks", "ClientLinks");
        }

        public ActionResult ClientLinkEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("ClientLinks", "ClientLinks");
            }
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
            clsClientLinkEdit clsClientLinkEdit = new clsClientLinkEdit();

            clsClientLinkEdit.clsClientLink = clsClientLinksManager.getClientLinkByID(id);
            clsClientLinkEdit.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Clients => Clients.strCompanyName).ToList();

            return View(clsClientLinkEdit);
        }

        [HttpPost]
        public ActionResult ClientLinkEdit(clsClientLinkEdit clsClientLinkEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
            clsClientLinks clsClientLinkUpdate = clsClientLinksManager.getClientLinkByID(clsClientLinkEdit.clsClientLink.iClientLinkID);

            clsClientLinkUpdate.strTitle = clsClientLinkEdit.clsClientLink.strTitle;
            clsClientLinkUpdate.strDescription = clsClientLinkEdit.clsClientLink.strDescription;
            clsClientLinkUpdate.strLink = clsClientLinkEdit.clsClientLink.strLink;
            clsClientLinkUpdate.iClientID = clsClientLinkEdit.clsClientLink.iClientID;

            clsClientLinksManager.saveClientLink(clsClientLinkUpdate);
            return RedirectToAction("ClientLinks", "ClientLinks");
        }

        [HttpPost]
        public ActionResult DeleteClientLink(int iClientLinkID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsClientLinksManager clsClientLinksManager = new clsClientLinksManager();
            clsClientLinksManager.removeClientLinkByID(iClientLinkID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfClientLinkExists(string strTitle)
        {
            bool bCanUseClientLink = false;
            bool bExists = db.tblClientLinks.Any(ClientLink => ClientLink.strTitle.ToLower() == strTitle.ToLower() && ClientLink.bIsDeleted == false);

            if (bExists == false)
                bCanUseClientLink = true;

            return Json(bCanUseClientLink, JsonRequestBehavior.AllowGet);
        }
    }
}