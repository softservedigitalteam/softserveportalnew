﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.BlogTags;

namespace SoftservePortalNew.Controllers
{
    public class BlogTagsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: BlogTags
        public ActionResult BlogTags()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();
            List<clsBlogTags> lstBlogTags = clsBlogTagsManager.getAllBlogTagsOnlyList();

            return View(lstBlogTags);
        }

        public ActionResult BlogTagAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTags clsBlogTag = new clsBlogTags();

            return View(clsBlogTag);
        }

        [HttpPost]
        public ActionResult BlogTagAdd(clsBlogTags clsBlogTag)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();
            clsBlogTags clsBlogTagNew = new clsBlogTags();

            clsBlogTagNew.strTitle = clsBlogTag.strTitle;
            clsBlogTagNew.strDescription = clsBlogTag.strDescription;

            clsBlogTagsManager.saveBlogTag(clsBlogTagNew);
            return RedirectToAction("BlogTags", "BlogTags");
        }

        public ActionResult BlogTagEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("BlogTags", "BlogTags");
            }

            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();
            clsBlogTags clsBlogTag = clsBlogTagsManager.getBlogTagByID(id);

            clsBlogTagEdit clsBlogTagEdit = new clsBlogTagEdit();
            clsBlogTagEdit.iBlogTagID = clsBlogTag.iBlogTagID;
            clsBlogTagEdit.strTitle = clsBlogTag.strTitle;
            clsBlogTagEdit.strDescription = clsBlogTag.strDescription;

            return View(clsBlogTagEdit);
        }

        [HttpPost]
        public ActionResult BlogTagEdit(clsBlogTagEdit clsBlogTagEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }
            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();
            clsBlogTags clsBlogTagUpdate = clsBlogTagsManager.getBlogTagByID(clsBlogTagEdit.iBlogTagID);

            clsBlogTagUpdate.strDescription = clsBlogTagEdit.strDescription;

            clsBlogTagsManager.saveBlogTag(clsBlogTagUpdate);
            return RedirectToAction("BlogTags", "BlogTags");
        }

        [HttpPost]
        public ActionResult DeleteBlogTag(int iBlogTagID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();
            clsBlogTagsManager.removeBlogTagByID(iBlogTagID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfBlogTagExists(string strTitle)
        {
            bool bCanUseBlogTag = false;
            bool bExists = db.tblBlogTags.Any(BlogTag => BlogTag.strTitle.ToLower() == strTitle.ToLower() && BlogTag.bIsDeleted == false);

            if (bExists == false)
                bCanUseBlogTag = true;

            return Json(bCanUseBlogTag, JsonRequestBehavior.AllowGet);
        }
    }
}