﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.LayoutPartialViews;

namespace SoftservePortalNew.Controllers
{
    public class LayoutPartialViewsController : Controller
    {
        // GET: LayoutPartialViews
        public ActionResult _LoginPartial()
        {
            return PartialView();
        }

        public ActionResult _LogoutPartial()
        {
            clsLogoutPartialView clsLogoutPartialView = new clsLogoutPartialView();

            if (Session["clsUser"] != null)
            {
                clsUsers clsUser = (clsUsers)Session["clsUser"];
                clsLogoutPartialView.bIsUser = true;
                clsLogoutPartialView.strName = clsUser.strFirstName + " " + clsUser.strSurname;
                if (clsUser.strPathToImages != null && clsUser.strPathToImages != "" && clsUser.strMasterImage != null && clsUser.strMasterImage != "")
                    clsLogoutPartialView.strImagePath = clsUser.strPathToImages + "/" + clsUser.strMasterImage;
                else
                    clsLogoutPartialView.strImagePath = "";
            }
            else if (Session["clsClient"] != null)
            {
                clsClients clsClient = (clsClients)Session["clsClient"];
                clsLogoutPartialView.bIsClient = true;
                clsLogoutPartialView.strName = clsClient.strCompanyName;
                if (clsClient.strPathToImages != null && clsClient.strPathToImages != "" && clsClient.strMasterImage != null && clsClient.strMasterImage != "")
                    clsLogoutPartialView.strImagePath = clsClient.strPathToImages + "/" + clsClient.strMasterImage;
                else
                    clsLogoutPartialView.strImagePath = "";
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            return PartialView(clsLogoutPartialView);
        }

        public ActionResult _LoginInfoPartial()
        {
            return PartialView();
        }

        public ActionResult _MainMenuUserPartial()
        {
            return PartialView();
        }

        public ActionResult _MainMenuClientPartial()
        {
            return PartialView();
        }

        public ActionResult _MainMenuTogglePartial()
        {
            return PartialView();
        }

    }
}