﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.CIDocuments;
using SoftservePortalNew.Assistant_Classes;
using System.IO.Compression;
using System.IO;
using System.Data.Entity.Migrations;

namespace SoftservePortalNew.Controllers
{
    public class CIDocumentsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: CIDocuments
        public ActionResult CIDocuments()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
            List<clsCIDocuments> lstCIDocuments = clsCIDocumentsManager.getAllCIDocumentsList().OrderBy(CIDocuments => CIDocuments.strTitle).ToList();

            return View(lstCIDocuments);
        }

        public ActionResult CIDocumentAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsCIDocumentAdd clsCIDocumentAdd = new clsCIDocumentAdd();

            clsCIDocumentAdd.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Clients => Clients.strCompanyName).ToList();

            return View(clsCIDocumentAdd);
        }

        [HttpPost]
        public ActionResult CIDocumentAdd(clsCIDocumentAdd clsCIDocumentAdd, HttpPostedFileBase inCIDocumentsImage, HttpPostedFileBase[] inCIDocuments)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();

            clsCIDocuments clsCIDocumentNew = new clsCIDocuments();

            clsCIDocumentNew.strTitle = clsCIDocumentAdd.clsCIDocument.strTitle;
            clsCIDocumentNew.strDescription = clsCIDocumentAdd.clsCIDocument.strDescription;
            clsCIDocumentNew.iClientID = clsCIDocumentAdd.clsCIDocument.iClientID;
            clsCIDocumentNew.bIsCIGuide = clsCIDocumentAdd.clsCIDocument.bIsCIGuide;

            //CI Document Images
            // Verify file is selected
            if (inCIDocumentsImage != null)
            {
                if (inCIDocumentsImage.ContentLength > 0)
                {
                    // File Name
                    string strFileName = inCIDocumentsImage.FileName;

                    //get Unique Path
                    string strNewPath = GetUniqueCIDocumentsImagesPath();
                    string strDestPath = "Documents\\CIDocumentsImages\\" + strNewPath;

                    //Full path
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocumentsImages\\" + strNewPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    string strFullCIDocumentsImagepath = strFullDestPath + "\\" + strFileName;

                    clsCIDocumentNew.strPathToImages = strDestPath.Replace("\\", "/");
                    clsCIDocumentNew.strMasterImage = strFileName;

                    inCIDocumentsImage.SaveAs(strFullCIDocumentsImagepath);
                }
            }

            //CI Documents
            // Verify file is selected
            if (inCIDocuments != null)
            {
                if (inCIDocuments.Count() > 0)
                {
                    //get Unique Path
                    string strNewPath = GetUniqueCIDocumentsPath();
                    string strDestPath = "Documents\\CIDocuments\\" + strNewPath;
                    string strFullPackageName = clsCIDocumentAdd.clsCIDocument.strTitle + "_Package.zip";

                    //Full path
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\" + strNewPath;
                    string strPackageFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\" + strNewPath + "\\" + strFullPackageName;

                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    clsCIDocumentNew.strPathToDocuments = strDestPath.Replace("\\", "/");
                    clsCIDocumentNew.strMasterDocument = strFullPackageName;

                    foreach (HttpPostedFileBase inCIDocument in inCIDocuments)
                    {
                        if (inCIDocument != null)
                        {
                            if (inCIDocument.ContentLength > 0)
                            {
                                // File Name
                                string strFileName = inCIDocument.FileName;
                                string strFullCIDocumentspath = strFullDestPath + "\\" + strFileName;

                                inCIDocument.SaveAs(strFullCIDocumentspath);
                            }
                        }
                    }

                    string[] strGetAllFilePaths = System.IO.Directory.GetFiles(strFullDestPath);

                    if (strGetAllFilePaths.Count() > 0)
                    {
                        using (ZipArchive zaPackagedZipFile = ZipFile.Open(strPackageFullDestPath, ZipArchiveMode.Create))
                        {
                            foreach (string strFilePath in strGetAllFilePaths)
                                zaPackagedZipFile.CreateEntryFromFile(strFilePath, Path.GetFileName(strFilePath));
                        }
                    }
                }
            }

            clsCIDocumentNew.bIsMultiple = (inCIDocuments.Count() > 1) ? true : false;

            clsCIDocumentsManager.saveCIDocument(clsCIDocumentNew);
            return RedirectToAction("CIDocuments", "CIDocuments");
        }

        public ActionResult CIDocumentEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("CIDocuments", "CIDocuments");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
            clsCIDocumentEdit clsCIDocumentEdit = new clsCIDocumentEdit();

            clsCIDocumentEdit.clsCIDocument = clsCIDocumentsManager.getCIDocumentByID(id);
            clsCIDocumentEdit.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Clients => Clients.strCompanyName).ToList(); ;

            if (clsCIDocumentEdit.clsCIDocument.strPathToImages != null && clsCIDocumentEdit.clsCIDocument.strPathToImages != ""
                && clsCIDocumentEdit.clsCIDocument.strMasterImage != null && clsCIDocumentEdit.clsCIDocument.strMasterImage != "")
                clsCIDocumentEdit.strFullImagePath = clsCIDocumentEdit.clsCIDocument.strPathToImages + "/" + clsCIDocumentEdit.clsCIDocument.strMasterImage;

            string strDestPath = clsCIDocumentEdit.clsCIDocument.strPathToDocuments.Replace("/", "\\");
            string strFullDocumentsDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

            if (System.IO.Directory.Exists(strFullDocumentsDestPath))
            {
                string[] strAllFilePaths = Directory.GetFiles(strFullDocumentsDestPath);
                foreach (string strFilePath in strAllFilePaths)
                {
                    string strFileName = System.IO.Path.GetFileName(strFilePath);
                    if (!strFileName.Contains(".zip"))
                        clsCIDocumentEdit.lstDocumentsList.Add(strFileName);
                }
            }

            return View(clsCIDocumentEdit);
        }

        [HttpPost]
        public ActionResult CIDocumentEdit(clsCIDocumentEdit clsCIDocumentEdit, HttpPostedFileBase inCIDocumentsImage, HttpPostedFileBase[] inCIDocuments)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();

            clsCIDocuments clsCIDocumentUpdate = clsCIDocumentsManager.getCIDocumentByID(clsCIDocumentEdit.clsCIDocument.iCIDocumentID);
            string[] strGetAllFilePaths = null;
            string strOldTitle = clsCIDocumentUpdate.strTitle;

            clsCIDocumentUpdate.strTitle = clsCIDocumentEdit.clsCIDocument.strTitle;
            clsCIDocumentUpdate.strDescription = clsCIDocumentEdit.clsCIDocument.strDescription;
            clsCIDocumentUpdate.iClientID = clsCIDocumentEdit.clsCIDocument.iClientID;
            clsCIDocumentUpdate.bIsCIGuide = clsCIDocumentEdit.clsCIDocument.bIsCIGuide;

            //CI Document Images
            // Verify file is selected
            if (clsCIDocumentEdit.strFullImagePath == null || clsCIDocumentEdit.strFullImagePath == "")
            {
                if (inCIDocumentsImage != null)
                {
                    if (inCIDocumentsImage.ContentLength > 0)
                    {
                        if (clsCIDocumentUpdate.strPathToImages != null && clsCIDocumentUpdate.strPathToImages != "")
                        {
                            // File Name
                            string strFileName = inCIDocumentsImage.FileName;

                            string strDestPath = clsCIDocumentUpdate.strPathToImages.Replace("/", "\\");
                            string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                            if (!System.IO.Directory.Exists(strFullDestPath))
                            {
                                System.IO.Directory.CreateDirectory(strFullDestPath);
                            }
                            else
                            {
                                //Clear files
                                string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                foreach (string strFilePath in strAllFilePaths)
                                    System.IO.File.Delete(strFilePath);
                            }

                            string strFullCIDocumentsImagepath = strFullDestPath + "\\" + strFileName;
                            clsCIDocumentUpdate.strMasterImage = strFileName;

                            inCIDocumentsImage.SaveAs(strFullCIDocumentsImagepath);
                        }
                        else
                        {
                            // File Name
                            string strFileName = inCIDocumentsImage.FileName;

                            //get Unique Path
                            string strNewPath = GetUniqueCIDocumentsImagesPath();
                            string strDestPath = "Documents\\CIDocumentsImages\\" + strNewPath;

                            //Full path
                            string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocumentsImages\\" + strNewPath;
                            if (!System.IO.Directory.Exists(strFullDestPath))
                                System.IO.Directory.CreateDirectory(strFullDestPath);

                            string strFullCIDocumentsImagepath = strFullDestPath + "\\" + strFileName;

                            clsCIDocumentUpdate.strPathToImages = strDestPath.Replace("\\", "/");
                            clsCIDocumentUpdate.strMasterImage = strFileName;

                            inCIDocumentsImage.SaveAs(strFullCIDocumentsImagepath);
                        }
                    }
                }
                else
                {
                    clsCIDocumentUpdate.strMasterImage = null;
                }
            }
            else if (inCIDocumentsImage != null)
            {
                if (inCIDocumentsImage.ContentLength > 0)
                {
                    if (clsCIDocumentUpdate.strPathToImages != null && clsCIDocumentUpdate.strPathToImages != "")
                    {
                        // File Name
                        string strFileName = inCIDocumentsImage.FileName;

                        string strDestPath = clsCIDocumentUpdate.strPathToImages.Replace("/", "\\");
                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                        if (!System.IO.Directory.Exists(strFullDestPath))
                        {
                            System.IO.Directory.CreateDirectory(strFullDestPath);
                        }
                        else
                        {
                            //Clear files
                            string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                                System.IO.File.Delete(strFilePath);
                        }

                        string strFullCIDocumentsImagepath = strFullDestPath + "\\" + strFileName;
                        clsCIDocumentUpdate.strMasterImage = strFileName;

                        inCIDocumentsImage.SaveAs(strFullCIDocumentsImagepath);
                    }
                    else
                    {
                        // File Name
                        string strFileName = inCIDocumentsImage.FileName;

                        //get Unique Path
                        string strNewPath = GetUniqueCIDocumentsImagesPath();
                        string strDestPath = "Documents\\CIDocumentsImages\\" + strNewPath;

                        //Full path
                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocumentsImages\\" + strNewPath;
                        if (!System.IO.Directory.Exists(strFullDestPath))
                            System.IO.Directory.CreateDirectory(strFullDestPath);

                        string strFullCIDocumentsImagepath = strFullDestPath + "\\" + strFileName;

                        clsCIDocumentUpdate.strPathToImages = strDestPath.Replace("\\", "/");
                        clsCIDocumentUpdate.strMasterImage = strFileName;

                        inCIDocumentsImage.SaveAs(strFullCIDocumentsImagepath);
                    }
                }
            }

            //CI Documents
            // Verify file is selected
            if (inCIDocuments != null)
            {
                if (inCIDocuments.Count() > 0)
                {
                    if (clsCIDocumentUpdate.strPathToDocuments != null && clsCIDocumentUpdate.strPathToDocuments != "")
                    {
                        string strDestPath = clsCIDocumentUpdate.strPathToDocuments.Replace("/", "\\");
                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                        string strNewFullPackageName = clsCIDocumentUpdate.strTitle + "_Package.zip";

                        if (!System.IO.Directory.Exists(strFullDestPath))
                            System.IO.Directory.CreateDirectory(strFullDestPath);

                        //Full path
                        string strOldFullPackageName = strOldTitle + "_Package.zip";
                        string strOldPackageFullDestPath = strFullDestPath + "\\" + strOldFullPackageName;
                        string strNewPackageFullDestPath = strFullDestPath + "\\" + strNewFullPackageName;

                        if (System.IO.File.Exists(strOldPackageFullDestPath))
                            System.IO.File.Delete(strOldPackageFullDestPath);

                        clsCIDocumentUpdate.strMasterDocument = strNewFullPackageName;

                        foreach (HttpPostedFileBase inCIDocument in inCIDocuments)
                        {
                            if (inCIDocument != null)
                            {
                                if (inCIDocument.ContentLength > 0)
                                {
                                    // File Name
                                    string strFileName = inCIDocument.FileName;
                                    string strFullCIDocumentspath = strFullDestPath + "\\" + strFileName;

                                    inCIDocument.SaveAs(strFullCIDocumentspath);
                                }
                            }
                        }

                        strGetAllFilePaths = System.IO.Directory.GetFiles(strFullDestPath);

                        if (strGetAllFilePaths.Count() > 0)
                        {
                            using (ZipArchive zaPackagedZipFile = ZipFile.Open(strNewPackageFullDestPath, ZipArchiveMode.Create))
                            {
                                foreach (string strFilePath in strGetAllFilePaths)
                                    zaPackagedZipFile.CreateEntryFromFile(strFilePath, Path.GetFileName(strFilePath));
                            }
                        }
                    }
                    else
                    {
                        //get Unique Path
                        string strNewPath = GetUniqueCIDocumentsPath();
                        string strDestPath = "Documents\\CIDocuments\\" + strNewPath;
                        string strFullPackageName = clsCIDocumentEdit.clsCIDocument.strTitle + "_Package.zip";

                        //Full path
                        string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\" + strNewPath;
                        string strPackageFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\" + strNewPath + "\\" + strFullPackageName;

                        if (!System.IO.Directory.Exists(strFullDestPath))
                            System.IO.Directory.CreateDirectory(strFullDestPath);

                        clsCIDocumentUpdate.strPathToDocuments = strDestPath.Replace("\\", "/");
                        clsCIDocumentUpdate.strMasterDocument = strFullPackageName;

                        foreach (HttpPostedFileBase inCIDocument in inCIDocuments)
                        {
                            if (inCIDocument != null)
                            {
                                if (inCIDocument.ContentLength > 0)
                                {
                                    // File Name
                                    string strFileName = inCIDocument.FileName;
                                    string strFullCIDocumentspath = strFullDestPath + "\\" + strFileName;

                                    inCIDocument.SaveAs(strFullCIDocumentspath);
                                }
                            }

                        }

                        strGetAllFilePaths = System.IO.Directory.GetFiles(strFullDestPath);

                        if (strGetAllFilePaths.Count() > 0)
                        {
                            using (ZipArchive zaPackagedZipFile = ZipFile.Open(strFullPackageName, ZipArchiveMode.Create))
                            {
                                foreach (string strFilePath in strGetAllFilePaths)
                                    zaPackagedZipFile.CreateEntryFromFile(strFilePath, Path.GetFileName(strFilePath));
                            }
                        }
                    }
                }
            }
            else
            {
                if (clsCIDocumentUpdate.strPathToDocuments != null && clsCIDocumentUpdate.strPathToDocuments != "")
                {
                    string strDestPath = clsCIDocumentUpdate.strPathToDocuments.Replace("/", "\\");
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                    string strNewFullPackageName = clsCIDocumentUpdate.strTitle + "_Package.zip";

                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    //Full path
                    string strOldFullPackageName = strOldTitle + "_Package.zip";
                    string strOldPackageFullDestPath = strFullDestPath + "\\" + strOldFullPackageName;
                    string strNewPackageFullDestPath = strFullDestPath + "\\" + strNewFullPackageName;

                    if (System.IO.File.Exists(strOldPackageFullDestPath))
                        System.IO.File.Delete(strOldPackageFullDestPath);

                    clsCIDocumentUpdate.strMasterDocument = strNewFullPackageName;

                    strGetAllFilePaths = System.IO.Directory.GetFiles(strFullDestPath);

                    if (strGetAllFilePaths.Count() > 0)
                    {
                        using (ZipArchive zaPackagedZipFile = ZipFile.Open(strNewPackageFullDestPath, ZipArchiveMode.Create))
                        {
                            foreach (string strFilePath in strGetAllFilePaths)
                                zaPackagedZipFile.CreateEntryFromFile(strFilePath, Path.GetFileName(strFilePath));
                        }
                    }
                }
            }

            if (strGetAllFilePaths != null)
                clsCIDocumentUpdate.bIsMultiple = (strGetAllFilePaths.Count() > 1) ? true : false;
            else
                clsCIDocumentUpdate.bIsMultiple = false;


            clsCIDocumentsManager.saveCIDocument(clsCIDocumentUpdate);
            return RedirectToAction("CIDocuments", "CIDocuments");
        }

        public ActionResult ClientCIDocuments()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClients clsClient = (clsClients)Session["clsClient"];

            clsClientCIDocuments clsClientCIDocuments = new clsClientCIDocuments();
            clsClientsManager clsClientsManager = new clsClientsManager();
            bool bHasCIPresentation = false;

            //TEST
            //clsClientCIDocuments.clsClient = clsClientsManager.getClientByID(57);
            clsClientCIDocuments.clsClient = clsClient;

            if (clsClientCIDocuments.clsClient.lstCIDocuments.Count > 0)
            {
                foreach (clsCIDocuments clsCIDocument in clsClientCIDocuments.clsClient.lstCIDocuments)
                {
                    if (clsCIDocument.bIsCIGuide == true)
                    {
                        string strDestPath = clsCIDocument.strPathToDocuments.Replace("/", "\\");
                        string strFullDocumentsDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                        if (System.IO.Directory.Exists(strFullDocumentsDestPath))
                        {
                            string[] strAllFilePaths = Directory.GetFiles(strFullDocumentsDestPath);
                            foreach (string strFilePath in strAllFilePaths)
                            {
                                string strFileName = System.IO.Path.GetFileName(strFilePath);
                                if (strFileName.Contains(".pdf"))
                                {
                                    bHasCIPresentation = true;
                                    clsClientCIDocuments.strCIGuideDocumentPath = clsCIDocument.strPathToDocuments;
                                    clsClientCIDocuments.strCIGuideDocumentName = strFileName;
                                    break;
                                }
                            }
                        }
                    }
                    if (bHasCIPresentation == true)
                        break;
                }
            }
            return View(clsClientCIDocuments);
        }

        [HttpPost]
        public ActionResult GetClientCIDocuments(List<clsCIDocuments> lstClientCIDocuments)
        {
            List<string> lstClientCIDocsPartialViews = new List<string>();
            if (lstClientCIDocuments.Count > 0)
            {
                string strPartialView = "_ClientCIDocumentsPartialView";

                foreach (clsCIDocuments clsCIDocument in lstClientCIDocuments)
                {
                    ViewDataDictionary vddPartialViewData = new ViewDataDictionary(clsCIDocument);

                    using (var swClientCIDocumentsPartialView = new StringWriter())
                    {
                        ViewEngineResult verClientCIDocumentsPartialView = ViewEngines.Engines.FindPartialView(this.ControllerContext, strPartialView);
                        var viewContext = new ViewContext(this.ControllerContext, verClientCIDocumentsPartialView.View, vddPartialViewData, new TempDataDictionary(), swClientCIDocumentsPartialView);
                        verClientCIDocumentsPartialView.View.Render(viewContext, swClientCIDocumentsPartialView);

                        lstClientCIDocsPartialViews.Add(swClientCIDocumentsPartialView.GetStringBuilder().ToString());
                    }
                }
            }

            return Json(new { lstClientCIDocsPartialViews = lstClientCIDocsPartialViews }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _ClientCIDocumentsPartialView(clsCIDocuments clsCIDocument)
        {
            return PartialView();
        }

        public ActionResult ClientCIDocumentsView(int id)
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientCIDocumentsView clsClientCIDocumentsView = new clsClientCIDocumentsView();
            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
            clsClientCIDocumentsView.clsCIDocument = clsCIDocumentsManager.getCIDocumentByID(id);

            string strDestPath = clsClientCIDocumentsView.clsCIDocument.strPathToDocuments.Replace("/", "\\");
            string strFullDocumentsDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

            if (System.IO.Directory.Exists(strFullDocumentsDestPath))
            {
                string[] strAllFilePaths = Directory.GetFiles(strFullDocumentsDestPath);
                foreach (string strFilePath in strAllFilePaths)
                {
                    string strFileName = System.IO.Path.GetFileName(strFilePath);
                    if (!strFileName.Contains(".zip"))
                        clsClientCIDocumentsView.lstDocumentsList.Add(strFileName);
                }
            }

            return View(clsClientCIDocumentsView);
        }

        public ActionResult ClientAllCIDocumentsView(int id)
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientAllCIDocumentsView clsClientAllCIDocumentsView = new clsClientAllCIDocumentsView();
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientAllCIDocumentsView.clsClient = clsClientsManager.getClientByID(id);

            if (clsClientAllCIDocumentsView.clsClient.lstCIDocuments.Count > 0)
            {
                foreach (clsCIDocuments clsCIDocument in clsClientAllCIDocumentsView.clsClient.lstCIDocuments)
                {
                    string strDestPath = clsCIDocument.strPathToDocuments.Replace("/", "\\");
                    string strFullDocumentsDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                    if (System.IO.Directory.Exists(strFullDocumentsDestPath))
                    {
                        string[] strAllFilePaths = Directory.GetFiles(strFullDocumentsDestPath);
                        foreach (string strFilePath in strAllFilePaths)
                        {
                            string strFileName = System.IO.Path.GetFileName(strFilePath);
                            if (strFileName.Contains(".zip"))
                                clsClientAllCIDocumentsView.lstDocumentsList.Add(clsCIDocument.strPathToDocuments + "/" + clsCIDocument.strMasterDocument);

                        }
                    }
                }

                if (clsClientAllCIDocumentsView.lstDocumentsList.Count > 0)
                {
                    string strFullCIPackPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\FullCIPack";

                    string[] strAllFilePaths = Directory.GetFiles(strFullCIPackPath);
                    //Clear Files
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);

                    clsClientAllCIDocumentsView.strFullCoperateIdentityPackName = clsClientAllCIDocumentsView.clsClient.strCompanyName + "_Full_Coperate_Identity_Pack.zip";
                    clsClientAllCIDocumentsView.strFullCoperateIdentityPackPath = "Documents/CIDocuments/FullCIPack/" + clsClientAllCIDocumentsView.clsClient.strCompanyName + "_Full_Coperate_Identity_Pack.zip";

                    string strPathToFullCIPackFile = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments\\FullCIPack\\" + clsClientAllCIDocumentsView.clsClient.strCompanyName + "_Full_Coperate_Identity_Pack.zip";

                    using (ZipArchive zaPackagedZipFile = ZipFile.Open(strPathToFullCIPackFile, ZipArchiveMode.Create))
                    {
                        foreach (string strZIPDocument in clsClientAllCIDocumentsView.lstDocumentsList)
                        {
                            string strPathToZipDocument = AppDomain.CurrentDomain.BaseDirectory + "\\" + strZIPDocument.Replace("/", "\\");
                            zaPackagedZipFile.CreateEntryFromFile(strPathToZipDocument, Path.GetFileName(strPathToZipDocument));
                        }
                    }
                }
            }

            return View(clsClientAllCIDocumentsView);
        }

        [HttpPost]
        public ActionResult DeleteCIDocument(int iCIDocumentID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsCIDocumentsManager clsCIDocumentsManager = new clsCIDocumentsManager();
            clsCIDocumentsManager.removeCIDocumentByID(iCIDocumentID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfCIDocumentExists(string strTitle)
        {
            bool bCanUseCIDocument = false;
            bool bExists = db.tblCIDocuments.Any(CIDocument => CIDocument.strTitle.ToLower() == strTitle.ToLower() && CIDocument.bIsDeleted == false);

            if (bExists == false)
                bCanUseCIDocument = true;

            return Json(bCanUseCIDocument, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSelectedCIDocument(int iCIDocumentID, string strItemTitle)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                RedirectToAction("Index", "Home");
            }

            bool bDocumentDeleted = false;
            var tblCIDocument = db.tblCIDocuments.FirstOrDefault(CIDocument => CIDocument.iCIDocumentID == iCIDocumentID && CIDocument.bIsDeleted == false);

            if (tblCIDocument != null)
            {
                string strDestPath = tblCIDocument.strPathToDocuments.Replace("/", "\\");
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                string strNewFullPackageName = tblCIDocument.strTitle + "_Package.zip";

                string strExistingFileFullDestPath = strFullDestPath + "\\" + strItemTitle;
                string strNewPackageFullDestPath = strFullDestPath + "\\" + strNewFullPackageName;

                if (System.IO.File.Exists(strExistingFileFullDestPath))
                    System.IO.File.Delete(strExistingFileFullDestPath);
                if (System.IO.File.Exists(strNewPackageFullDestPath))
                    System.IO.File.Delete(strNewPackageFullDestPath);

                string[] strGetAllFilePaths = System.IO.Directory.GetFiles(strFullDestPath);

                if (strGetAllFilePaths.Count() > 0)
                {
                    using (ZipArchive zaPackagedZipFile = ZipFile.Open(strNewPackageFullDestPath, ZipArchiveMode.Create))
                    {
                        foreach (string strFilePath in strGetAllFilePaths)
                            zaPackagedZipFile.CreateEntryFromFile(strFilePath, Path.GetFileName(strFilePath));
                    }
                }

                if (strGetAllFilePaths != null)
                    tblCIDocument.bIsMultiple = (strGetAllFilePaths.Count() > 1) ? true : false;
                else
                    tblCIDocument.bIsMultiple = false;

                db.Set<tblCIDocuments>().AddOrUpdate(tblCIDocument);
                db.SaveChanges();
            }

            bDocumentDeleted = true;

            return Json(new { bDocumentDeleted = bDocumentDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Get unique path
        private string GetUniqueCIDocumentsImagesPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocumentsImages";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\CIDocumentsImage" + iCount) == true)
            {
                iCount++;
            }
            return "CIDocumentsImage" + iCount;
        }

        //Get unique path
        private string GetUniqueCIDocumentsPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\CIDocuments";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\CIDocuments" + iCount) == true)
            {
                iCount++;
            }
            return "CIDocuments" + iCount;
        }

    }
}