﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.View_Models.SocialCalendar;
using SoftservePortalNew.Model_Manager;
using System.Globalization;
using SoftservePortalNew.Models;
using System.Text;
using System.Net.Mail;
using SoftservePortalNew.Assistant_Classes;

namespace SoftservePortalNew.Controllers
{
    public class SocialCalendarController : Controller
    {
        // GET: SocialCalendar
        public ActionResult SocialCalendarClients()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();

            clsSocialCalendarClients clsSocialCalendarClients = new clsSocialCalendarClients();
            clsSocialCalendarClients.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();

            return View(clsSocialCalendarClients);
        }

        [HttpPost]
        public ActionResult SocialCalendarClients(clsSocialCalendarClients clsSocialCalendarClients)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            TempData["iClientSocialCalendarID"] = clsSocialCalendarClients.iClientID;

            return RedirectToAction("ClientSocialCalendar", "SocialCalendar");
        }

        public ActionResult SocialCalendarView()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult GetSocialCalendarViewEventItems()
        {
            try
            {
                if (Session["clsClient"] != null)
                {
                    clsClients clsClient = (clsClients)Session["clsClient"];

                    clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                    clsSocialCalendarEventsTypesManager clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();

                    List<clsSocialCalendarEventsViewItems> lstSocialCalendarEvents = clsSocialCalendarEventsManager.getAllSocialCalendarEventsViewItemsByClientIDOnlyList(clsClient.iClientID);

                    clsSocialCalendarView clsSocialCalendarView = new clsSocialCalendarView();

                    clsSocialCalendarView.lstSocialCalendarEventsViewItemsFacebook = lstSocialCalendarEvents.Where(SocialCalendarEventsViewItems => SocialCalendarEventsViewItems.eventType.ToLower() == "facebook").ToList();
                    clsSocialCalendarView.lstSocialCalendarEventsViewItemsInstagram = lstSocialCalendarEvents.Where(SocialCalendarEventsViewItems => SocialCalendarEventsViewItems.eventType.ToLower() == "instagram").ToList();
                    clsSocialCalendarView.lstSocialCalendarEventsViewItemsTwitter = lstSocialCalendarEvents.Where(SocialCalendarEventsViewItems => SocialCalendarEventsViewItems.eventType.ToLower() == "twitter").ToList();
                    clsSocialCalendarView.lstSocialCalendarEventsViewItemsGooglePus = lstSocialCalendarEvents.Where(SocialCalendarEventsViewItems => SocialCalendarEventsViewItems.eventType.ToLower() == "google plus").ToList();

                    return Json(new { clsSocialCalendarView = clsSocialCalendarView }, JsonRequestBehavior.AllowGet);
                }

                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ClientSocialCalendar()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (TempData["iClientSocialCalendarID"] != null)
            {
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsSocialCalendarEventsTypesManager clsSocialCalendarEventsTypesManager = new clsSocialCalendarEventsTypesManager();

                clsClientSocialCalendar clsClientSocialCalendar = new clsClientSocialCalendar();

                clsClientSocialCalendar.iClientID = Convert.ToInt32(TempData["iClientSocialCalendarID"]);
                TempData["iClientSocialCalendarID"] = clsClientSocialCalendar.iClientID;

                clsClientSocialCalendar.strClientCompanyName = clsClientsManager.getClientByIDOnly(clsClientSocialCalendar.iClientID).strCompanyName;
                clsClientSocialCalendar.lstClients = clsClientsManager.getAllClientsOnlyList().OrderBy(Client => Client.strCompanyName).ToList();
                clsClientSocialCalendar.lstSocialCalendarEventsTypes = clsSocialCalendarEventsTypesManager.getAllSocialCalendarEventsTypesOnlyList();

                return View(clsClientSocialCalendar);
            }
            else
            {
                return RedirectToAction("SocialCalendarClients", "SocialCalendar");
            }            
        }

        //Get Client Social Calendar
        [HttpPost]
        public ActionResult GetClientSocialCalendar(int iSelectedClientID)
        {
            bool bClientExists = false;

            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (iSelectedClientID != 0)
            {
                TempData["iClientSocialCalendarID"] = null; //Clear Temp Data
                TempData["iClientSocialCalendarID"] = iSelectedClientID;
                bClientExists = true;
            }
            

            return Json(new { bClientExists = bClientExists }, JsonRequestBehavior.AllowGet);
        }

        //Get Client Social Calendar Events
        public ActionResult GetClientSocialCalendarEvents()
        {
            bool bEventAdded = false;

            try
            {
                int iClientID = Convert.ToInt32(TempData.Peek("iClientSocialCalendarID"));
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                List<clsSocialCalendarEventsItems> lstSocialCalendarEvents = clsSocialCalendarEventsManager.getAllSocialCalendarEventsItemsByClientIDOnlyList(iClientID);

                return Json(new { lstSocialCalendarEvents = lstSocialCalendarEvents }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { bEventAdded = bEventAdded }, JsonRequestBehavior.AllowGet);
            }
        }

        //Add New Event
        [HttpPost]
        public ActionResult AddSocialCalendarEvent(string strTitle, string strEventStart, string strEventEnd, string strEventType, string strEventDescription, string strEventKeywords, string strEventLinks, string strRepeatEventType)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventAdded = false;
            bool bEventExists = false;
            bool bIsRepeatEvent = false;
            int iEventID = 0;

            int iClientID = Convert.ToInt32(TempData.Peek("iClientSocialCalendarID"));

            try
            {
                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = new clsSocialCalendarEvents();

                DateTime dtStartDate = Convert.ToDateTime(strEventStart);
                DateTime dtEndDate = Convert.ToDateTime(strEventEnd);

                int iEventType = 0;
                if (strEventType != null && strEventType != "")
                    iEventType = Convert.ToInt32(strEventType);

                bEventExists = clsSocialCalendarEventsManager.checkIfSocialCalendarEventExists(strTitle, dtStartDate, dtEndDate);

                if (bEventExists == true)
                {
                    return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists, iEventID = iEventID }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    clsSocialCalendarEvent.strTitle = strTitle;
                    clsSocialCalendarEvent.dtStartDate = dtStartDate;
                    clsSocialCalendarEvent.dtEndDate = dtEndDate;
                    if (iEventType != 0)
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = iEventType;
                    clsSocialCalendarEvent.strDescription = strEventDescription;
                    clsSocialCalendarEvent.strKeywords = strEventKeywords;
                    clsSocialCalendarEvent.strLinks = strEventLinks;
                    clsSocialCalendarEvent.iClientID = iClientID;

                    if (strRepeatEventType != null && strRepeatEventType != "")
                    {
                        clsSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle + " (Repeat)";
                        iEventID = clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);

                        bIsRepeatEvent = true;

                        clsSocialCalendarEvent.iSocialCalendarEventID = iEventID;
                        clsSocialCalendarEvent.bIsRepeatEvent = true;
                        clsSocialCalendarEvent.strRepeatEventType = strRepeatEventType;
                        clsSocialCalendarEvent.iRepeatEventStartDateID = iEventID;
                        clsSocialCalendarEvent.dtRepeatEventStartDate = dtStartDate;
                    }

                    if (iEventID == 0)
                        iEventID = clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);

                    int iDifferenceInDays = (dtEndDate - dtStartDate).Days;

                    if (clsSocialCalendarEvent.bIsRepeatEvent == true)
                    {
                        if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "DAILY")
                        {
                            if (!(iDifferenceInDays > 1))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                int iStartDay = dtStartDate.Day + 1; //Begin Repeat on following day
                                int iDaysInMonth = DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month);

                                while (iStartDay <= iDaysInMonth)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(1);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(1);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartDay++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "WEEKLY")
                        {
                            if (!(iDifferenceInDays > 7))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 16 weeks
                                int iStartWeek = 1;
                                int iEndWeek = 16;

                                while (iStartWeek <= iEndWeek)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(7);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(7);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartWeek++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "BIWEEKLY")
                        {
                            if (!(iDifferenceInDays > 14))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 16 weeks
                                int iStartWeek = 1;
                                int iEndWeek = 16;

                                while (iStartWeek <= iEndWeek)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddDays(14);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddDays(14);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartWeek++;
                                }
                            }
                        }
                        else if (clsSocialCalendarEvent.strRepeatEventType.ToUpper() == "MONTHLY")
                        {
                            if (!(iDifferenceInDays > 28))
                            {
                                DateTime dtReapeatDateTimeDailyStart = dtStartDate;
                                DateTime dtReapeatDateTimeDailyEnd = dtEndDate;

                                //Do for following 6 Months
                                int iStartMonth = 1;
                                int iEndMonth = 6;

                                while (iStartMonth <= iEndMonth)
                                {
                                    clsSocialCalendarEvents clsNewSocialCalendarEvent = new clsSocialCalendarEvents();

                                    dtReapeatDateTimeDailyStart = dtReapeatDateTimeDailyStart.AddMonths(1);
                                    dtReapeatDateTimeDailyEnd = dtReapeatDateTimeDailyEnd.AddMonths(1);

                                    clsNewSocialCalendarEvent.dtAdded = clsSocialCalendarEvent.dtAdded;
                                    clsNewSocialCalendarEvent.iAddedBy = clsSocialCalendarEvent.iAddedBy;
                                    clsNewSocialCalendarEvent.dtEdited = clsSocialCalendarEvent.dtEdited;
                                    clsNewSocialCalendarEvent.iEditedBy = clsSocialCalendarEvent.iEditedBy;

                                    clsNewSocialCalendarEvent.strTitle = clsSocialCalendarEvent.strTitle;
                                    clsNewSocialCalendarEvent.strDescription = clsSocialCalendarEvent.strDescription;
                                    clsNewSocialCalendarEvent.strKeywords = clsSocialCalendarEvent.strKeywords;
                                    clsNewSocialCalendarEvent.strLinks = clsSocialCalendarEvent.strLinks;
                                    clsNewSocialCalendarEvent.dtStartDate = dtReapeatDateTimeDailyStart;

                                    clsNewSocialCalendarEvent.dtEndDate = dtReapeatDateTimeDailyEnd;
                                    clsNewSocialCalendarEvent.iClientID = clsSocialCalendarEvent.iClientID;
                                    clsNewSocialCalendarEvent.iSocialCalendarEventsTypeID = clsSocialCalendarEvent.iSocialCalendarEventsTypeID;
                                    clsNewSocialCalendarEvent.bIsRepeatEvent = clsSocialCalendarEvent.bIsRepeatEvent;
                                    clsNewSocialCalendarEvent.strRepeatEventType = clsSocialCalendarEvent.strRepeatEventType;

                                    clsNewSocialCalendarEvent.iRepeatEventStartDateID = clsSocialCalendarEvent.iRepeatEventStartDateID;
                                    clsNewSocialCalendarEvent.dtRepeatEventStartDate = clsSocialCalendarEvent.dtRepeatEventStartDate;
                                    clsNewSocialCalendarEvent.bIsDeleted = clsSocialCalendarEvent.bIsDeleted;

                                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsNewSocialCalendarEvent);

                                    iStartMonth++;
                                }
                            }
                        }
                    }

                    bEventAdded = true;
                    return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists, iEventID = iEventID, bIsRepeatEvent = bIsRepeatEvent }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { bEventAdded = bEventAdded, bEventExists = bEventExists }, JsonRequestBehavior.AllowGet);
            }
        }

        //Get Event Info
        public ActionResult GetSocialCalendarEventInfo(string strEventID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsSocialCalendarEvents clsSocialCalendarEvent = null;
            bool bEventExists = false;
            int iEventID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    bEventExists = true;
                    return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { bEventExists = bEventExists, clsSocialCalendarEvent = clsSocialCalendarEvent }, JsonRequestBehavior.AllowGet);
            }
        }

        //Save Event Info
        [HttpPost]
        public ActionResult SaveSocialCalendarEventInfo(string strEventID, string strTitle, string strEventType, string strEventDescription, string strEventKeywords, string strEventLinks)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventUpdated = false;
            int iEventID = 0;
            int iEventTypeID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);
                if (strEventType != null && strEventType != "")
                    iEventTypeID = Convert.ToInt32(strEventType);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    clsSocialCalendarEvent.strTitle = strTitle;
                    if (iEventTypeID != 0)
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = iEventTypeID;
                    else
                        clsSocialCalendarEvent.iSocialCalendarEventsTypeID = null;
                    clsSocialCalendarEvent.strDescription = strEventDescription;
                    clsSocialCalendarEvent.strKeywords = strEventKeywords;
                    clsSocialCalendarEvent.strLinks = strEventLinks;

                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);
                    bEventUpdated = true;
                }

                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);
            }
        }

        //EventDrop - EventResize: Update Event Date Time
        [HttpPost]
        public ActionResult UpdateSocialCalendarEventDateTime(string strEventID, string strEventStart, string strEventEnd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bEventUpdated = false;
            int iEventID = 0;

            try
            {
                if (strEventID != null && strEventID != "")
                    iEventID = Convert.ToInt32(strEventID);

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();
                clsSocialCalendarEvents clsSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(iEventID);

                if (clsSocialCalendarEvent != null)
                {
                    DateTime dtStartDate = Convert.ToDateTime(strEventStart);
                    DateTime dtEndDate = Convert.ToDateTime(strEventEnd);

                    clsSocialCalendarEvent.dtStartDate = dtStartDate;
                    clsSocialCalendarEvent.dtEndDate = dtEndDate;

                    clsSocialCalendarEventsManager.saveSocialCalendarEvent(clsSocialCalendarEvent);
                    bEventUpdated = true;
                }

                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { bEventUpdated = bEventUpdated }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}