﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;

namespace SoftservePortalNew.Controllers
{
    public class NotificationTypesController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: NotificationTypes
        public ActionResult NotificationTypes()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();
            List<clsNotificationTypes> lstNotificationTypes = clsNotificationTypesManager.getAllNotificationTypesOnlyList();

            return View(lstNotificationTypes); 
        }

        public ActionResult NotificationTypeAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationTypes clsNotificationType = new clsNotificationTypes();

            return View(clsNotificationType);
        }

        [HttpPost]
        public ActionResult NotificationTypeAdd(clsNotificationTypes clsNotificationType)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();
            clsNotificationTypes clsNotificationTypeNew = new clsNotificationTypes();

            clsNotificationTypeNew.strTitle = clsNotificationType.strTitle;
            clsNotificationTypeNew.strDescription = clsNotificationType.strDescription;

            clsNotificationTypesManager.saveNotificationType(clsNotificationTypeNew);

            return RedirectToAction("NotificationTypes", "NotificationTypes");
        }

        public ActionResult NotificationTypeEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("NotificationTypes", "NotificationTypes");
            }

            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();
            clsNotificationTypes clsNotificationType = clsNotificationTypesManager.getNotificationTypeByID(id);

            return View(clsNotificationType);
        }

        [HttpPost]
        public ActionResult NotificationTypeEdit(clsNotificationTypes clsNotificationType)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();
            clsNotificationTypes clsNotificationTypeUpdate = clsNotificationTypesManager.getNotificationTypeByID(clsNotificationType.iNotificationTypeID);

            clsNotificationTypeUpdate.strTitle = clsNotificationType.strTitle;
            clsNotificationTypeUpdate.strDescription = clsNotificationType.strDescription;

            clsNotificationTypesManager.saveNotificationType(clsNotificationTypeUpdate);

            return RedirectToAction("NotificationTypes", "NotificationTypes");
        }

        [HttpPost]
        public ActionResult DeleteNotificationType(int iNotificationTypeID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsNotificationTypesManager clsNotificationTypesManager = new clsNotificationTypesManager();
            clsNotificationTypesManager.removeNotificationTypeByID(iNotificationTypeID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfNotificationTypeExists(string strTitle)
        {
            bool bCanUseNotificationType = false;
            bool bExists = db.tblNotificationTypes.Any(NotificationType => NotificationType.strTitle.ToLower() == strTitle.ToLower() && NotificationType.bIsDeleted == false);

            if (bExists == false)
                bCanUseNotificationType = true;

            return Json(bCanUseNotificationType, JsonRequestBehavior.AllowGet);
        }

    }
}