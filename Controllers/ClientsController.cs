﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SoftservePortalNew.Models;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.View_Models.Clients;
using SoftservePortalNew.Assistant_Classes;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SoftservePortalNew.Classes;
using System.Text;
using NReco.PdfGenerator;

namespace SoftservePortalNew.Controllers
{
    public class ClientsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: Clients

        public ActionResult Clients()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            List<clsClients> lstClients = clsClientsManager.getAllClientsOnlyList();

            return View(lstClients);
        }

        public ActionResult ClientAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientAdd clsClientAdd = new clsClientAdd();

            return View(clsClientAdd);
        }

        //Add Client
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientAdd(clsClientAdd clsClientAdd)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();

            clsClients clsNewClients = new clsClients();

            clsNewClients.strCompanyName = clsClientAdd.clsClient.strCompanyName;
            clsNewClients.strCompanyLandline = clsClientAdd.clsClient.strCompanyLandline;
            clsNewClients.strRegisteredName = clsClientAdd.clsClient.strRegisteredName;
            clsNewClients.strRegistrationNumber = clsClientAdd.clsClient.strRegistrationNumber;
            if (clsClientAdd.clsClient.strUsername != null && clsClientAdd.clsClient.strUsername != "")
                clsNewClients.strUsername = clsClientAdd.clsClient.strUsername;
            else
                clsNewClients.strUsername = clsClientAdd.clsClient.strEmail.ToLower();

            clsNewClients.strEmail = clsClientAdd.clsClient.strEmail.ToLower();

            clsNewClients.strVATNumber = clsClientAdd.clsClient.strVATNumber;
            clsNewClients.strPhysicalAddress = clsClientAdd.clsClient.strPhysicalAddress;
            clsNewClients.strPostalAddress = clsClientAdd.clsClient.strPostalAddress;
            clsNewClients.dblBillingRate = clsClientAdd.clsClient.dblBillingRate;
            clsNewClients.strURL = clsClientAdd.clsClient.strURL;

            if (clsClientAdd.clsClient.strPassword != null && clsClientAdd.clsClient.strPassword != "")
                clsNewClients.strPassword = clsCommonFunctions.GetMd5Sum(clsClientAdd.clsClient.strPassword);
            else
                clsNewClients.strPassword = clsCommonFunctions.GetMd5Sum(clsClientAdd.clsClient.strEmail.ToLower());

            clsNewClients.strVerificationKey = "";
            clsNewClients.bIsVerified = true;
            clsNewClients.strIsApproved = "Approved";
            clsNewClients.bIsProfileCompleted = true;

            //Save image
            if (clsClientAdd.strCropImageData != null && clsClientAdd.strCropImageData != "")
            {
                string data = clsClientAdd.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgLogo_" + clsClientAdd.clsClient.strCompanyName + ".png";
                string strSmallImageName = "sml_imgLogo_" + clsClientAdd.clsClient.strCompanyName + ".png";
                string newPath = GetUniqueLogoPath();

                string strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Clients\\Logos\\" + newPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                    System.IO.Directory.CreateDirectory(strFullDestPath);

                clsNewClients.strPathToImages = strDestPath.Replace("\\", "/");
                clsNewClients.strMasterImage = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;
                string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Smaller Image
                Image smallImage = (Image)ResizeImage(image, 35, 35);
                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string newPath = GetUniqueLogoPath();
                string strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Clients\\Logos\\" + newPath;
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";
                if (!System.IO.Directory.Exists(strFullDestPath))
                    System.IO.Directory.CreateDirectory(strFullDestPath);

                clsNewClients.strPathToImages = strDestPath.Replace("\\", "/");
                clsNewClients.strMasterImage = "default_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_image.png";
                string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                //Copy small and default sizes
                System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
            }

            int iClientID = clsClientsManager.saveClient(clsNewClients);

            //Add Client Contact Details
            clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
            clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

            clsClientContactDetails clsNewClientContactDetails = new clsClientContactDetails();
            clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable = new clsClientClientContactDetailsLinkTable();
            
            clsNewClientContactDetails.strContactName = clsClientAdd.clsClientContactDetails.strContactName;
            clsNewClientContactDetails.strContactEmail = clsClientAdd.clsClientContactDetails.strContactEmail;
            clsNewClientContactDetails.strContactNumber = clsClientAdd.clsClientContactDetails.strContactNumber;
            clsNewClientContactDetails.bIsPrimaryContact = true;

            int iClientContactDetailsID1 = clsClientContactDetailsManager.saveClientContactDetail(clsNewClientContactDetails);

            clsClientClientContactDetailsLinkTable.iClientID = iClientID;
            clsClientClientContactDetailsLinkTable.iClientContactDetailID = iClientContactDetailsID1;
            clsClientClientContactDetailsLinkTableManager.saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable);

            if ((clsClientAdd.clsClientSecondaryContactDetails.strContactName != null && clsClientAdd.clsClientSecondaryContactDetails.strContactName != "")
                || (clsClientAdd.clsClientSecondaryContactDetails.strContactEmail != null && clsClientAdd.clsClientSecondaryContactDetails.strContactEmail != "")
                || (clsClientAdd.clsClientSecondaryContactDetails.strContactNumber != null && clsClientAdd.clsClientSecondaryContactDetails.strContactNumber != ""))
            {
                clsClientContactDetails clsNewClientContactDetails2 = new clsClientContactDetails();
                clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable2 = new clsClientClientContactDetailsLinkTable();

                if (clsClientAdd.clsClientSecondaryContactDetails.strContactName != null && clsClientAdd.clsClientSecondaryContactDetails.strContactName != "")
                    clsNewClientContactDetails2.strContactName = clsClientAdd.clsClientSecondaryContactDetails.strContactName;
                else
                    clsNewClientContactDetails2.strContactName = "";

                if (clsClientAdd.clsClientSecondaryContactDetails.strContactEmail != null && clsClientAdd.clsClientSecondaryContactDetails.strContactEmail != "")
                    clsNewClientContactDetails2.strContactEmail = clsClientAdd.clsClientSecondaryContactDetails.strContactEmail;
                else
                    clsNewClientContactDetails2.strContactEmail = "";

                if (clsClientAdd.clsClientSecondaryContactDetails.strContactNumber != null && clsClientAdd.clsClientSecondaryContactDetails.strContactNumber != "")
                    clsNewClientContactDetails2.strContactNumber = clsClientAdd.clsClientSecondaryContactDetails.strContactNumber;
                else
                    clsNewClientContactDetails2.strContactNumber = "";

                clsNewClientContactDetails2.bIsPrimaryContact = false;
                int iClientContactDetailsID2 = clsClientContactDetailsManager.saveClientContactDetail(clsNewClientContactDetails2);

                clsClientClientContactDetailsLinkTable2.iClientID = iClientID;
                clsClientClientContactDetailsLinkTable2.iClientContactDetailID = iClientContactDetailsID2;
                clsClientClientContactDetailsLinkTableManager.saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable2);
            }

            return RedirectToAction("Clients", "Clients");
        }

        //Edit Client
        public ActionResult ClientEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("Clients", "Clients");
            }

            clsClientEdit clsClientEdit = new clsClientEdit();
            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();

            clsClientEdit.clsClient = clsClientsManager.getClientByID(id);

            clsClientEdit.strUsername = clsClientEdit.clsClient.strUsername;
            clsClientEdit.strEmail = clsClientEdit.clsClient.strEmail;

            if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 0)
            {
                clsClientContactDetails clsClientContactDetails1 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[0].iClientContactDetailID);
                if (clsClientContactDetails1 != null)
                    clsClientEdit.clsClientContactDetails = clsClientContactDetails1;

                if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 1)
                {
                    clsClientContactDetails clsClientContactDetails2 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[1].iClientContactDetailID);

                    if (clsClientContactDetails2 != null)
                    {
                        clsClientSecondaryContactDetails clsClientSecondaryContactDetails = new clsClientSecondaryContactDetails();

                        clsClientEdit.clsClientSecondaryContactDetails.iClientContactDetailID = clsClientContactDetails2.iClientContactDetailID;
                        clsClientEdit.clsClientSecondaryContactDetails.strContactName = clsClientContactDetails2.strContactName;
                        clsClientEdit.clsClientSecondaryContactDetails.strContactEmail = clsClientContactDetails2.strContactEmail;
                        clsClientEdit.clsClientSecondaryContactDetails.strContactNumber = clsClientContactDetails2.strContactNumber;
                        clsClientEdit.clsClientSecondaryContactDetails.bIsPrimaryContact = clsClientContactDetails2.bIsPrimaryContact;
                    }
                }
            }

            if (clsClientEdit.clsClient.strPathToImages != null && clsClientEdit.clsClient.strPathToImages != "" && clsClientEdit.clsClient.strMasterImage != null && clsClientEdit.clsClient.strMasterImage != "")
                clsClientEdit.strFullImagePath = clsClientEdit.clsClient.strPathToImages + "/" + clsClientEdit.clsClient.strMasterImage;

            return View(clsClientEdit);
        }

        //Edit Client
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientEdit(clsClientEdit clsClientEdit)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClients clsExistingClient = clsClientsManager.getClientByID(clsClientEdit.clsClient.iClientID);

            clsExistingClient.strCompanyName = clsClientEdit.clsClient.strCompanyName;
            clsExistingClient.strCompanyLandline = clsClientEdit.clsClient.strCompanyLandline;
            clsExistingClient.strRegisteredName = clsClientEdit.clsClient.strRegisteredName;
            clsExistingClient.strRegistrationNumber = clsClientEdit.clsClient.strRegistrationNumber;
            if (clsClientEdit.clsClient.strUsername != null && clsClientEdit.clsClient.strUsername != "")
                clsExistingClient.strUsername = clsClientEdit.clsClient.strUsername;
            else
                clsExistingClient.strUsername = clsClientEdit.strEmail.ToLower();

            clsExistingClient.strEmail = clsClientEdit.strEmail.ToLower();

            clsExistingClient.strVATNumber = clsClientEdit.clsClient.strVATNumber;
            clsExistingClient.strPhysicalAddress = clsClientEdit.clsClient.strPhysicalAddress;
            clsExistingClient.strPostalAddress = clsClientEdit.clsClient.strPostalAddress;
            clsExistingClient.dblBillingRate = clsClientEdit.clsClient.dblBillingRate;
            clsExistingClient.strURL = clsClientEdit.clsClient.strURL;

            if (clsClientEdit.bResetPassword == true)
                clsExistingClient.strPassword = clsCommonFunctions.GetMd5Sum(clsClientEdit.strEmail.ToLower());
            else if (clsClientEdit.strNewPassword != null && clsClientEdit.strNewPassword != "")
                clsExistingClient.strPassword = clsCommonFunctions.GetMd5Sum(clsClientEdit.strNewPassword);

            //clsExistingClient.strVerificationKey = clsClientEdit.clsClient.strVerificationKey;
            //clsExistingClient.bIsVerified = clsClientEdit.clsClient.bIsVerified;
            //clsExistingClient.strIsApproved = clsClientEdit.clsClient.strIsApproved;
            //clsExistingClient.bIsProfileCompleted = clsClientEdit.clsClient.bIsProfileCompleted;

            //Save image
            if (clsClientEdit.strCropImageData != null && clsClientEdit.strCropImageData != "")
            {
                string data = clsClientEdit.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgLogo_" + clsClientEdit.clsClient.strCompanyName + ".png";
                string strSmallImageName = "sml_imgLogo_" + clsClientEdit.clsClient.strCompanyName + ".png";

                string strDestPath = "";
                if (clsExistingClient.strPathToImages != null && clsExistingClient.strPathToImages != "")
                {
                    strDestPath = clsExistingClient.strPathToImages.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniqueLogoPath();
                    strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                    clsExistingClient.strPathToImages = strDestPath.Replace("\\", "/");
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsExistingClient.strMasterImage = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;
                string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Smaller Image
                Image smallImage = (Image)ResizeImage(image, 35, 35);
                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";

                string strDestPath = "";
                if (clsExistingClient.strPathToImages != null && clsExistingClient.strPathToImages != "")
                {
                    strDestPath = clsExistingClient.strPathToImages.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniqueLogoPath();
                    strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                    clsExistingClient.strPathToImages = strDestPath.Replace("\\", "/");
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsExistingClient.strMasterImage = "default_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_image.png";
                string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                //Copy small and default sizes
                System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
            }

            int iClientID = clsClientsManager.saveClient(clsExistingClient);

            //Add Client Contact Details
            clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
            clsClientContactDetails clsExistingClientContactDetails = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClientContactDetails.iClientContactDetailID);

            if (clsExistingClientContactDetails != null)
            {
                clsExistingClientContactDetails.strContactName = clsClientEdit.clsClientContactDetails.strContactName;
                clsExistingClientContactDetails.strContactEmail = clsClientEdit.clsClientContactDetails.strContactEmail;
                clsExistingClientContactDetails.strContactNumber = clsClientEdit.clsClientContactDetails.strContactNumber;

                int iClientContactDetailsID1 = clsClientContactDetailsManager.saveClientContactDetail(clsExistingClientContactDetails);
            }

            clsClientContactDetails clsExistingClientContactDetails2 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClientSecondaryContactDetails.iClientContactDetailID);

            if (clsExistingClientContactDetails2 != null)
            {
                if (clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                    clsExistingClientContactDetails2.strContactName = clsClientEdit.clsClientSecondaryContactDetails.strContactName;
                else
                    clsExistingClientContactDetails2.strContactName = "";

                if (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                    clsExistingClientContactDetails2.strContactEmail = clsClientEdit.clsClientSecondaryContactDetails.strContactEmail;
                else
                    clsExistingClientContactDetails2.strContactEmail = "";

                if (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != "")
                    clsExistingClientContactDetails2.strContactNumber = clsClientEdit.clsClientSecondaryContactDetails.strContactNumber;
                else
                    clsExistingClientContactDetails2.strContactNumber = "";

                int iClientContactDetailsID2 = clsClientContactDetailsManager.saveClientContactDetail(clsExistingClientContactDetails2);
            }
            else if ((clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                || (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                || (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != ""))
            {
                clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

                clsClientContactDetails clsNewClientContactDetails2 = new clsClientContactDetails();
                clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable2 = new clsClientClientContactDetailsLinkTable();

                if (clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                    clsNewClientContactDetails2.strContactName = clsClientEdit.clsClientSecondaryContactDetails.strContactName;
                else
                    clsNewClientContactDetails2.strContactName = "";

                if (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                    clsNewClientContactDetails2.strContactEmail = clsClientEdit.clsClientSecondaryContactDetails.strContactEmail;
                else
                    clsNewClientContactDetails2.strContactEmail = "";

                if (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != "")
                    clsNewClientContactDetails2.strContactNumber = clsClientEdit.clsClientSecondaryContactDetails.strContactNumber;
                else
                    clsNewClientContactDetails2.strContactNumber = "";

                clsNewClientContactDetails2.bIsPrimaryContact = false;
                int iClientContactDetailsID2 = clsClientContactDetailsManager.saveClientContactDetail(clsNewClientContactDetails2);

                clsClientClientContactDetailsLinkTable2.iClientID = iClientID;
                clsClientClientContactDetailsLinkTable2.iClientContactDetailID = iClientContactDetailsID2;
                clsClientClientContactDetailsLinkTableManager.saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable2);
            }

            return RedirectToAction("Clients", "Clients");
        }

        //Delete Page
        [HttpPost]
        public ActionResult ClientDelete(int iClientID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsSuccess = false;

            if (iClientID == 0)
            {
                return RedirectToAction("Clients", "Clients");
            }

            clsClientsManager clsClientsManager = new clsClientsManager();
            clsClientsManager.removeClientByID(iClientID);

            bIsSuccess = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfClientExists([Bind(Prefix = "clsClient.strEmail")] string strEmail)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblClients.Any(Client => Client.strEmail.ToLower() == strEmail.ToLower() && Client.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
        //Check if exists
        [HttpPost]
        public JsonResult checkIfClientUsernameExists([Bind(Prefix = "clsClient.strUsername")] string strUsername)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblClients.Any(Client => Client.strUsername.ToLower() == strUsername.ToLower() && Client.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
        //Check if exists
        [HttpPost]
        public JsonResult checkIfClientContactEmailExists([Bind(Prefix = "clsClientContactDetails.strContactEmail")] string strContactEmail)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblClientContactDetails.Any(ClientContactDetail => ClientContactDetail.strContactEmail.ToLower() == strContactEmail.ToLower() && ClientContactDetail.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult checkIfSecondaryClientContactEmailExists([Bind(Prefix = "clsClientSecondaryContactDetails.strContactEmail")] string strContactEmail)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblClientContactDetails.Any(ClientContactDetail => ClientContactDetail.strContactEmail.ToLower() == strContactEmail.ToLower() && ClientContactDetail.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClientProfile()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;

                clsClientEdit clsClientEdit = new clsClientEdit();
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();

                clsClientEdit.clsClient = (clsClients)Session["clsClient"];

                clsClientEdit.strUsername = clsClientEdit.clsClient.strUsername;
                clsClientEdit.strEmail = clsClientEdit.clsClient.strEmail;

                if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 0)
                {
                    clsClientContactDetails clsClientContactDetails1 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[0].iClientContactDetailID);
                    if (clsClientContactDetails1 != null)
                        clsClientEdit.clsClientContactDetails = clsClientContactDetails1;

                    if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 1)
                    {
                        clsClientContactDetails clsClientContactDetails2 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[1].iClientContactDetailID);

                        if (clsClientContactDetails2 != null)
                        {
                            clsClientSecondaryContactDetails clsClientSecondaryContactDetails = new clsClientSecondaryContactDetails();

                            clsClientEdit.clsClientSecondaryContactDetails.iClientContactDetailID = clsClientContactDetails2.iClientContactDetailID;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactName = clsClientContactDetails2.strContactName;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactEmail = clsClientContactDetails2.strContactEmail;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactNumber = clsClientContactDetails2.strContactNumber;
                            clsClientEdit.clsClientSecondaryContactDetails.bIsPrimaryContact = clsClientContactDetails2.bIsPrimaryContact;
                        }
                    }
                }

                if (clsClientEdit.clsClient.strPathToImages != null && clsClientEdit.clsClient.strPathToImages != "" && clsClientEdit.clsClient.strMasterImage != null && clsClientEdit.clsClient.strMasterImage != "")
                    clsClientEdit.strFullImagePath = clsClientEdit.clsClient.strPathToImages + "/" + clsClientEdit.clsClient.strMasterImage;

                return View(clsClientEdit);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ClientProfileEdit()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;

                clsClientEdit clsClientEdit = new clsClientEdit();
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();

                clsClientEdit.clsClient = (clsClients)Session["clsClient"];

                clsClientEdit.strUsername = clsClientEdit.clsClient.strUsername;
                clsClientEdit.strEmail = clsClientEdit.clsClient.strEmail;

                if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 0)
                {
                    clsClientContactDetails clsClientContactDetails1 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[0].iClientContactDetailID);
                    if (clsClientContactDetails1 != null)
                        clsClientEdit.clsClientContactDetails = clsClientContactDetails1;

                    if (clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable.Count > 1)
                    {
                        clsClientContactDetails clsClientContactDetails2 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClient.lstClientClientContactDetailsLinkTable[1].iClientContactDetailID);

                        if (clsClientContactDetails2 != null)
                        {
                            clsClientSecondaryContactDetails clsClientSecondaryContactDetails = new clsClientSecondaryContactDetails();

                            clsClientEdit.clsClientSecondaryContactDetails.iClientContactDetailID = clsClientContactDetails2.iClientContactDetailID;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactName = clsClientContactDetails2.strContactName;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactEmail = clsClientContactDetails2.strContactEmail;
                            clsClientEdit.clsClientSecondaryContactDetails.strContactNumber = clsClientContactDetails2.strContactNumber;
                            clsClientEdit.clsClientSecondaryContactDetails.bIsPrimaryContact = clsClientContactDetails2.bIsPrimaryContact;
                        }
                    }
                }

                if (clsClientEdit.clsClient.strPathToImages != null && clsClientEdit.clsClient.strPathToImages != "" && clsClientEdit.clsClient.strMasterImage != null && clsClientEdit.clsClient.strMasterImage != "")
                    clsClientEdit.strFullImagePath = clsClientEdit.clsClient.strPathToImages + "/" + clsClientEdit.clsClient.strMasterImage;

                return View(clsClientEdit);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult ClientProfileEdit(clsClientEdit clsClientEdit)
        {
            if (Session["clsClient"] != null)
            {
                clsClientsManager clsClientsManager = new clsClientsManager();
                clsClients clsExistingClient = clsClientsManager.getClientByID(clsClientEdit.clsClient.iClientID);

                clsExistingClient.strCompanyName = clsClientEdit.clsClient.strCompanyName;
                clsExistingClient.strCompanyLandline = clsClientEdit.clsClient.strCompanyLandline;
                clsExistingClient.strRegisteredName = clsClientEdit.clsClient.strRegisteredName;
                clsExistingClient.strRegistrationNumber = clsClientEdit.clsClient.strRegistrationNumber;
                clsExistingClient.strUsername = clsClientEdit.strUsername;
                clsExistingClient.strEmail = clsClientEdit.strEmail.ToLower();

                clsExistingClient.strVATNumber = clsClientEdit.clsClient.strVATNumber;
                clsExistingClient.strPhysicalAddress = clsClientEdit.clsClient.strPhysicalAddress;
                clsExistingClient.strPostalAddress = clsClientEdit.clsClient.strPostalAddress;
                clsExistingClient.dblBillingRate = clsClientEdit.clsClient.dblBillingRate;
                clsExistingClient.strURL = clsClientEdit.clsClient.strURL;

                if (clsClientEdit.bResetPassword == true)
                    clsExistingClient.strPassword = clsCommonFunctions.GetMd5Sum(clsClientEdit.strEmail.ToLower());
                else if (clsClientEdit.strNewPassword != null && clsClientEdit.strNewPassword != "")
                    clsExistingClient.strPassword = clsCommonFunctions.GetMd5Sum(clsClientEdit.strNewPassword);

                //clsExistingClient.strVerificationKey = clsClientEdit.clsClient.strVerificationKey;
                //clsExistingClient.bIsVerified = clsClientEdit.clsClient.bIsVerified;
                //clsExistingClient.strIsApproved = clsClientEdit.clsClient.strIsApproved;
                //clsExistingClient.bIsProfileCompleted = clsClientEdit.clsClient.bIsProfileCompleted;

                //Save image
                if (clsClientEdit.strCropImageData != null && clsClientEdit.strCropImageData != "")
                {
                    string data = clsClientEdit.strCropImageData;
                    data = data.Replace("data:image/png;base64,", "");
                    string strImageName = "imgLogo_" + clsClientEdit.clsClient.strCompanyName + ".png";
                    string strSmallImageName = "sml_imgLogo_" + clsClientEdit.clsClient.strCompanyName + ".png";

                    string strDestPath = "";
                    if (clsExistingClient.strPathToImages != null && clsExistingClient.strPathToImages != "")
                    {
                        strDestPath = clsExistingClient.strPathToImages.Replace("/", "\\");
                    }
                    else
                    {
                        string newPath = GetUniqueLogoPath();
                        strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                        clsExistingClient.strPathToImages = strDestPath.Replace("\\", "/");
                    }

                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                    {
                        System.IO.Directory.CreateDirectory(strFullDestPath);
                    }
                    else
                    {
                        //Clear files
                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                        foreach (string strFilePath in strAllFilePaths)
                            System.IO.File.Delete(strFilePath);
                    }

                    clsExistingClient.strMasterImage = strImageName;

                    string strImagePath = strFullDestPath + "\\" + strImageName;
                    string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                    byte[] bytes = Convert.FromBase64String(data);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    //Smaller Image
                    Image smallImage = (Image)ResizeImage(image, 35, 35);
                    //Save images
                    image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
                }
                else //save default images
                {
                    string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Default_Images";

                    string strDestPath = "";
                    if (clsExistingClient.strPathToImages != null && clsExistingClient.strPathToImages != "")
                    {
                        strDestPath = clsExistingClient.strPathToImages.Replace("/", "\\");
                    }
                    else
                    {
                        string newPath = GetUniqueLogoPath();
                        strDestPath = "Documents\\Clients\\Logos\\" + newPath;
                        clsExistingClient.strPathToImages = strDestPath.Replace("\\", "/");
                    }

                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                    {
                        System.IO.Directory.CreateDirectory(strFullDestPath);
                    }
                    else
                    {
                        //Clear files
                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                        foreach (string strFilePath in strAllFilePaths)
                            System.IO.File.Delete(strFilePath);
                    }

                    clsExistingClient.strMasterImage = "default_image.png";

                    string strImagePath = strFullDestPath + "\\" + "default_image.png";
                    string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                    //Copy small and default sizes
                    System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                    System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
                }

                int iClientID = clsClientsManager.saveClient(clsExistingClient);

                //Add Client Contact Details
                clsClientContactDetailsManager clsClientContactDetailsManager = new clsClientContactDetailsManager();
                clsClientContactDetails clsExistingClientContactDetails = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClientContactDetails.iClientContactDetailID);

                if (clsExistingClientContactDetails != null)
                {
                    clsExistingClientContactDetails.strContactName = clsClientEdit.clsClientContactDetails.strContactName;
                    clsExistingClientContactDetails.strContactEmail = clsClientEdit.clsClientContactDetails.strContactEmail;
                    clsExistingClientContactDetails.strContactNumber = clsClientEdit.clsClientContactDetails.strContactNumber;

                    int iClientContactDetailsID1 = clsClientContactDetailsManager.saveClientContactDetail(clsExistingClientContactDetails);
                }

                clsClientContactDetails clsExistingClientContactDetails2 = clsClientContactDetailsManager.getClientContactDetailByID(clsClientEdit.clsClientSecondaryContactDetails.iClientContactDetailID);

                if (clsExistingClientContactDetails2 != null)
                {
                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                        clsExistingClientContactDetails2.strContactName = clsClientEdit.clsClientSecondaryContactDetails.strContactName;
                    else
                        clsExistingClientContactDetails2.strContactName = "";

                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                        clsExistingClientContactDetails2.strContactEmail = clsClientEdit.clsClientSecondaryContactDetails.strContactEmail;
                    else
                        clsExistingClientContactDetails2.strContactEmail = "";

                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != "")
                        clsExistingClientContactDetails2.strContactNumber = clsClientEdit.clsClientSecondaryContactDetails.strContactNumber;
                    else
                        clsExistingClientContactDetails2.strContactNumber = "";

                    int iClientContactDetailsID2 = clsClientContactDetailsManager.saveClientContactDetail(clsExistingClientContactDetails2);
                }
                else if ((clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                    || (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                    || (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != ""))
                {
                    clsClientClientContactDetailsLinkTableManager clsClientClientContactDetailsLinkTableManager = new clsClientClientContactDetailsLinkTableManager();

                    clsClientContactDetails clsNewClientContactDetails2 = new clsClientContactDetails();
                    clsClientClientContactDetailsLinkTable clsClientClientContactDetailsLinkTable2 = new clsClientClientContactDetailsLinkTable();

                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactName != null && clsClientEdit.clsClientSecondaryContactDetails.strContactName != "")
                        clsNewClientContactDetails2.strContactName = clsClientEdit.clsClientSecondaryContactDetails.strContactName;
                    else
                        clsNewClientContactDetails2.strContactName = "";

                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != null && clsClientEdit.clsClientSecondaryContactDetails.strContactEmail != "")
                        clsNewClientContactDetails2.strContactEmail = clsClientEdit.clsClientSecondaryContactDetails.strContactEmail;
                    else
                        clsNewClientContactDetails2.strContactEmail = "";

                    if (clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != null && clsClientEdit.clsClientSecondaryContactDetails.strContactNumber != "")
                        clsNewClientContactDetails2.strContactNumber = clsClientEdit.clsClientSecondaryContactDetails.strContactNumber;
                    else
                        clsNewClientContactDetails2.strContactNumber = "";

                    clsNewClientContactDetails2.bIsPrimaryContact = false;
                    int iClientContactDetailsID2 = clsClientContactDetailsManager.saveClientContactDetail(clsNewClientContactDetails2);

                    clsClientClientContactDetailsLinkTable2.iClientID = iClientID;
                    clsClientClientContactDetailsLinkTable2.iClientContactDetailID = iClientContactDetailsID2;
                    clsClientClientContactDetailsLinkTableManager.saveClientClientContactDetailsLinkTable(clsClientClientContactDetailsLinkTable2);
                }

                Session["clsClient"] = null;
                Session["clsClient"] = clsExistingClient;

                return RedirectToAction("ClientProfile", "Clients");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //Get unique path
        private string GetUniqueLogoPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\Clients\\Logos";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\Logo" + iCount) == true)
            {
                iCount++;
            }
            return "Logo" + iCount;
        }

        //Resize image
        private Bitmap ResizeImage(Image image, int width, int height)
        {
            Rectangle recRectangle = new Rectangle(0, 0, width, height);
            Bitmap bitImage = new Bitmap(width, height);

            bitImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(bitImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapImageMode = new ImageAttributes())
                {
                    wrapImageMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, recRectangle, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapImageMode);
                }
            }

            return bitImage;
        }

        public ActionResult ClientSocialMediaReport()
        {
            if (Session["clsClient"] != null)
            {
                ViewBag.bIsUserLoggedIn = false;
                ViewBag.bIsClientLoggedIn = true;

                clsSocialCalendarEventsManager clsSocialCalendarEventsManager = new clsSocialCalendarEventsManager();

                clsClientSocialMediaReport clsClientSocialMediaReport = new clsClientSocialMediaReport();

                clsClients clsClient = (clsClients)Session["clsClient"];

                if (clsClient.lstSocialCalendarEvents.Count > 0)
                {
                    foreach (clsSocialCalendarEvents clsSocialCalendarEvent in clsClient.lstSocialCalendarEvents)
                    {
                        clsSocialCalendarEvents clsNewSocialCalendarEvent = clsSocialCalendarEventsManager.getSocialCalendarEventByID(Convert.ToInt32(clsSocialCalendarEvent.iSocialCalendarEventID));
                        if (clsNewSocialCalendarEvent != null)
                            clsClientSocialMediaReport.lstSocialCalendarEvents.Add(clsNewSocialCalendarEvent);
                    }

                    clsClientSocialMediaReport.lstSocialCalendarEvents.OrderBy(SocialMediaItem => SocialMediaItem.dtStartDate);
                }

                GenerateReportPDF(clsClientSocialMediaReport);

                return View(clsClientSocialMediaReport);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private void GenerateReportPDF(clsClientSocialMediaReport clsClientSocialMediaReport)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder cssSB = new StringBuilder();

            //StyleSheets
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/vendors.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/calendars/fullcalendar.min.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/forms/selects/select2.min.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/forms/selects/selectize.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/forms/selects/selectize.default.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/pickers/daterange/daterangepicker.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/pickers/pickadate/pickadate.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/forms/icheck/icheck.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/forms/icheck/custom.css") + "'>");

            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/app.css") + "'>");

            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/core/menu/menu-types/vertical-compact-menu.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/core/colors/palette-gradient.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/core/colors/palette-climacon.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/pages/users.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/pages/timeline.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/calendars/fullcalendar.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/forms/selectize/selectize.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/calendars/clndr.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/forms/checkboxes-radios.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/pickers/daterange/daterange.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/css/plugins/animate/animate.css") + "'>");

            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/tables/datatable/datatables.min.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css") + "'>");

            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/fonts/icomoon.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/fonts/flag-icon-css/css/flag-icon.min.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/sliders/slick/slick.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/extensions/pace.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/charts/morris.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/extensions/unslider.css") + "'>");
            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/app-assets/vendors/css/weather-icons/climacons.min.css") + "'>");

            cssSB.Append("<link rel='stylesheet' type='text/css' href='" + Server.MapPath("~/Content/assets/css/style.css") + "'>");

            if (clsClientSocialMediaReport.lstSocialCalendarEvents.Count > 0)
            {
                DateTime dtCurrentTime = new DateTime();
                int count = 0;
                int rowItemCount = 0;
                int tableRowItemCount = 0;

                foreach (clsSocialCalendarEvents SocialCalendarEvent in clsClientSocialMediaReport.lstSocialCalendarEvents)
                {
                    if (SocialCalendarEvent.dtStartDate != dtCurrentTime)
                    {
                        if (tableRowItemCount % 6 == 0)
                        {
                            sb.Append("<table class='table-setting-size'>");
                        }
                        if (rowItemCount % 2 == 0)
                        {
                            sb.Append("<tr class='text-center'>");
                            sb.Append("<td class='table-column-spacing'>");
                        }
                        else
                        {
                            sb.Append("<td class='table-column-spacing'>");
                        }

                        sb.AppendLine("<h4>" + SocialCalendarEvent.dtStartDate.DayOfWeek + " (" + SocialCalendarEvent.dtStartDate.ToString("dd MMM yyy") + ")</h4>");
                        dtCurrentTime = SocialCalendarEvent.dtStartDate;
                        //rowItemCount = 0;

                        if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                        {
                            if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Facebook")
                            {
                                #region FACEBOOK
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-facebook mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-facebook card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-facebook'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-facebook'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Instagram")
                            {
                                #region INSTAGRAM
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-instagram mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-instagram card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-instagram'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-instagram'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Twitter")
                            {
                                #region TWITTER
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-twitter mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-twitter card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-twitter'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-twitter'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Google Plus")
                            {
                                #region GOOGLE PLUS
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-googleplus mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-googleplus card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-googleplus'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-googleplus'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else
                            {
                                #region OTHER
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header border-info mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='info card-title lead'>" + SocialCalendarEvent.strTitle + "</p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content border-info'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='info'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                        }
                        else
                        {
                            #region OTHER
                            // -- ** Start Card Div ** -- //
                            sb.Append("<div class='card'>");


                            // -- ** Start Card Header Div ** -- //
                            sb.Append("<div class='card-header border-info mt-1'>");
                            // -- ** Start p End p ** -- //
                            sb.Append("<p class='info card-title lead'>" + SocialCalendarEvent.strTitle + "</p>");
                            // -- ** Start p End p ** -- //
                            sb.Append("</div>");
                            // -- ** End Card Header Div ** -- //

                            // -- ** Start Inner Card Content Div ** -- //
                            sb.Append("<div class='card-content border-info'>");

                            // -- ** Start Inner Card Content Body Div ** -- //
                            sb.Append("<div class='card-body'>");
                            sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                            if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                            else
                                sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                            else
                                sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                            else
                                sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                            else
                                sb.Append("<p><b> ALL DAY EVENT </b></p>");

                            if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                sb.Append("<p><b>Event Type: </b><span class='info'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                            else
                                sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                            sb.Append("</div>");
                            // -- ** End Inner Card Content Body Div ** -- //

                            sb.Append("</div>");
                            // -- ** End Inner Card Content Div ** -- //

                            sb.Append("</div>");
                            // -- ** End Card Div ** -- //
                            #endregion
                        }

                        if (rowItemCount % 2 == 0)
                        {
                            sb.Append("</td>");
                            rowItemCount++;
                        }
                        else
                        {
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            rowItemCount++;
                        }
                        if (tableRowItemCount % 6 == 5)
                        {
                            sb.Append("</table>");
                        }
                        else
                        {
                            tableRowItemCount++;
                        }

                    }
                    else
                    {
                        if (tableRowItemCount % 6 == 0)
                        {
                            sb.Append("<table class='table-setting-size'>");
                        }
                        if (rowItemCount % 2 == 0)
                        {
                            sb.Append("<tr class='text-center'>");
                            sb.Append("<td class='table-column-spacing'>");
                        }
                        else
                        {
                            sb.Append("<td class='table-column-spacing'>");
                        }

                        if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                        {
                            if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Facebook")
                            {
                                #region FACEBOOK
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-facebook mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-facebook card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-facebook'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-facebook'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Instagram")
                            {
                                #region INSTAGRAM
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-instagram mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-instagram card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-instagram'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-instagram'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Twitter")
                            {
                                #region TWITTER
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-twitter mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-twitter card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-twitter'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-twitter'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else if (SocialCalendarEvent.clsSocialCalendarEventsType.strTitle == "Google Plus")
                            {
                                #region GOOGLE PLUS
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header sce-border-googleplus mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='sce-googleplus card-title lead'>" + SocialCalendarEvent.strTitle + " <b>(" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + ")</b></p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content sce-border-googleplus'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='sce-googleplus'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                            else
                            {
                                #region OTHER
                                // -- ** Start Card Div ** -- //
                                sb.Append("<div class='card'>");


                                // -- ** Start Card Header Div ** -- //
                                sb.Append("<div class='card-header border-info mt-1'>");
                                // -- ** Start p End p ** -- //
                                sb.Append("<p class='info card-title lead'>" + SocialCalendarEvent.strTitle + "</p>");
                                // -- ** Start p End p ** -- //
                                sb.Append("</div>");
                                // -- ** End Card Header Div ** -- //

                                // -- ** Start Inner Card Content Div ** -- //
                                sb.Append("<div class='card-content border-info'>");

                                // -- ** Start Inner Card Content Body Div ** -- //
                                sb.Append("<div class='card-body'>");
                                sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                                if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                    sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                                else
                                    sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                    sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                                else
                                    sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                    sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                                else
                                    sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                                if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                    sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                                else
                                    sb.Append("<p><b> ALL DAY EVENT </b></p>");

                                if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                    sb.Append("<p><b>Event Type: </b><span class='info'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                                else
                                    sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Body Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Inner Card Content Div ** -- //

                                sb.Append("</div>");
                                // -- ** End Card Div ** -- //
                                #endregion
                            }
                        }
                        else
                        {
                            #region OTHER
                            // -- ** Start Card Div ** -- //
                            sb.Append("<div class='card'>");


                            // -- ** Start Card Header Div ** -- //
                            sb.Append("<div class='card-header border-info mt-1'>");
                            // -- ** Start p End p ** -- //
                            sb.Append("<p class='info card-title lead'>" + SocialCalendarEvent.strTitle + "</p>");
                            // -- ** Start p End p ** -- //
                            sb.Append("</div>");
                            // -- ** End Card Header Div ** -- //

                            // -- ** Start Inner Card Content Div ** -- //
                            sb.Append("<div class='card-content border-info'>");

                            // -- ** Start Inner Card Content Body Div ** -- //
                            sb.Append("<div class='card-body'>");
                            sb.Append("<p><b>Title: </b>" + SocialCalendarEvent.strTitle + "</p>");

                            if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
                                sb.Append("<p><b>Description: </b>" + SocialCalendarEvent.strDescription + "</p>");
                            else
                                sb.Append("<p><b>Description: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
                                sb.Append("<p><b>Keywords: </b>" + SocialCalendarEvent.strKeywords + "</p>");
                            else
                                sb.Append("<p><b>Keywords: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
                                sb.Append("<p><b>Link: </b>" + SocialCalendarEvent.strLinks + "</p>");
                            else
                                sb.Append("<p><b>Link: </b> ** NONE ** </p>");

                            if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
                                sb.Append("<p><b>Time: </b>" + SocialCalendarEvent.dtStartDate.ToString("HH:mm") + "</p>");
                            else
                                sb.Append("<p><b> ALL DAY EVENT </b></p>");

                            if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
                                sb.Append("<p><b>Event Type: </b><span class='info'><b>" + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle + "</b><span></p>");
                            else
                                sb.Append("<p><b>Event Type: </b> Normal Event </p>");

                            sb.Append("</div>");
                            // -- ** End Inner Card Content Body Div ** -- //

                            sb.Append("</div>");
                            // -- ** End Inner Card Content Div ** -- //

                            sb.Append("</div>");
                            // -- ** End Card Div ** -- //
                            #endregion
                        }

                        if (rowItemCount % 2 == 0)
                        {
                            sb.Append("</td>");
                            rowItemCount++;
                        }
                        else
                        {
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            rowItemCount++;
                        }

                        if (tableRowItemCount % 6 == 5)
                        {
                            sb.Append("</table>");
                        }
                        else
                        {
                            tableRowItemCount++;
                        }

                    }
                }
            }
            else
            {
                sb.AppendLine("<h3>No Events To Report</h3>");
            }

            string strSocialMediaReportPDFPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\SocialMediaReport\\SocialMediaReport.pdf";
            string strSocialMediaReportPDFTemplatePath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\SocialMediaReport\\SocialMediaReport_Template.html";

            if (System.IO.File.Exists(strSocialMediaReportPDFPath))
                System.IO.File.Delete(strSocialMediaReportPDFPath);

            string strSocialMediaReportPDFTemplate = System.IO.File.ReadAllText(strSocialMediaReportPDFTemplatePath)
                .Replace("{SocialCalendarEventItems}", sb.ToString())
                .Replace("{SocialCalendarEventStyleSheets}", cssSB.ToString());

            HtmlToPdfConverter htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            byte[] pdfBytes = htmlToPdf.GeneratePdf(strSocialMediaReportPDFTemplate);

            using (FileStream fstream = new FileStream(strSocialMediaReportPDFPath, FileMode.OpenOrCreate))
            {
                fstream.Write(pdfBytes, 0, pdfBytes.Length);
            }
        }

        //private void GenerateReportPDF(clsClientSocialMediaReport clsClientSocialMediaReport)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    if (clsClientSocialMediaReport.lstSocialCalendarEvents.Count > 0)
        //    {
        //        DateTime dtCurrentTime = new DateTime();
        //        int count = 0;
        //        foreach (clsSocialCalendarEvents SocialCalendarEvent in clsClientSocialMediaReport.lstSocialCalendarEvents)
        //        {
        //            if (SocialCalendarEvent.dtStartDate != dtCurrentTime)
        //            {
        //                if (count == 0)
        //                {
        //                    sb.AppendLine("<div>"); //Opening Div
        //                    sb.AppendLine("<h3 class='font-weight-400'>" + SocialCalendarEvent.dtStartDate.DayOfWeek + " (" + SocialCalendarEvent.dtStartDate.ToString("dd MMM yyy") + ")</h3>");
        //                }
        //                else
        //                {
        //                    sb.AppendLine("</div>"); //Opening Div
        //                    sb.AppendLine("<div>"); //Opening Div
        //                    sb.AppendLine("<h3 class='font-weight-400'>" + SocialCalendarEvent.dtStartDate.DayOfWeek + " (" + SocialCalendarEvent.dtStartDate.ToString("dd MMM yyy") + ")</h3>");
        //                }

        //                dtCurrentTime = SocialCalendarEvent.dtStartDate;
        //                count++;
        //            }

        //            sb.AppendLine("<div>");
        //            sb.AppendLine("<ul class='list-styling'>");

        //            //Event
        //            sb.AppendLine("<li>");
        //            sb.AppendLine("<b>Event:</b> " + SocialCalendarEvent.strTitle);
        //            sb.AppendLine("</li>");

        //            //Date Time
        //            sb.AppendLine("<li>");
        //            if (SocialCalendarEvent.dtStartDate.ToString("HH:mm") != "00:00")
        //                sb.AppendLine("<b>Time:</b> " + SocialCalendarEvent.dtStartDate.ToString("HH:mm"));
        //            else
        //                sb.AppendLine("<b>Time:</b> ** ALL DAY EVENT **");
        //            sb.AppendLine("</li>");

        //            //Description
        //            sb.AppendLine("<li>");
        //            if (SocialCalendarEvent.strDescription != null && SocialCalendarEvent.strDescription != "")
        //                sb.AppendLine("<b>Description:</b> " + SocialCalendarEvent.strDescription);
        //            else
        //                sb.AppendLine("<b>Description:</b> ** NONE **");
        //            sb.AppendLine("</li>");

        //            //Keywords
        //            sb.AppendLine("<li>");
        //            if (SocialCalendarEvent.strKeywords != null && SocialCalendarEvent.strKeywords != "")
        //                sb.AppendLine("<b>Keywords:</b> " + SocialCalendarEvent.strKeywords);
        //            else
        //                sb.AppendLine("<b>Keywords:</b> ** NONE **");
        //            sb.AppendLine("</li>");

        //            //Link
        //            sb.AppendLine("<li>");
        //            if (SocialCalendarEvent.strLinks != null && SocialCalendarEvent.strLinks != "")
        //                sb.AppendLine("<b>Link:</b> " + SocialCalendarEvent.strLinks);
        //            else
        //                sb.AppendLine("<b>Link:</b> ** NONE **");
        //            sb.AppendLine("</li>");

        //            //Event Type
        //            sb.AppendLine("<li>");
        //            if (SocialCalendarEvent.clsSocialCalendarEventsType != null)
        //                sb.AppendLine("<b>Event Type:</b> " + SocialCalendarEvent.clsSocialCalendarEventsType.strTitle);
        //            else
        //                sb.AppendLine("<b>Event Type:</b> Normal Event");
        //            sb.AppendLine("</li>");


        //            sb.AppendLine("</ul>");
        //            sb.AppendLine("</div>");

        //            sb.AppendLine("<br />");
        //        }
        //    }
        //    else
        //    {
        //        sb.AppendLine("<h3>No Events To Report</h3>");
        //    }

        //    string strSocialMediaReportPDFPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\SocialMediaReport\\SocialMediaReport.pdf";
        //    string strSocialMediaReportPDFTemplatePath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\SocialMediaReport\\SocialMediaReport_Template.html";

        //    if (System.IO.File.Exists(strSocialMediaReportPDFPath))
        //        System.IO.File.Delete(strSocialMediaReportPDFPath);

        //    string strSocialMediaReportPDFTemplate = System.IO.File.ReadAllText(strSocialMediaReportPDFTemplatePath).Replace("{SocialCalendarEventItems}", sb.ToString());

        //    HtmlToPdfConverter htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        //    byte[] pdfBytes = htmlToPdf.GeneratePdf(strSocialMediaReportPDFTemplate);

        //    using (FileStream fstream = new FileStream(strSocialMediaReportPDFPath, FileMode.OpenOrCreate))
        //    {
        //        fstream.Write(pdfBytes, 0, pdfBytes.Length);
        //    }
        //}

    }
}