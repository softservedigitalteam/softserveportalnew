﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SoftservePortalNew.Model_Manager;
using SoftservePortalNew.Models;
using SoftservePortalNew.View_Models.BlogPosts;

namespace SoftservePortalNew.Controllers
{
    public class BlogPostsController : Controller
    {
        SoftservePortalNewContext db = new SoftservePortalNewContext();

        // GET: BlogPosts
        public ActionResult BlogPosts()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
            List<clsBlogPosts> lstBlogPosts = clsBlogPostsManager.getAllBlogPostsList();

            return View(lstBlogPosts);
        }

        public ActionResult BlogPostAdd()
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();

            clsBlogPostAdd clsBlogPostAdd = new clsBlogPostAdd();
            clsBlogPostAdd.lstBlogTypes = clsBlogTypesManager.getAllBlogTypesOnlyList();
            clsBlogPostAdd.lstBlogTags = clsBlogTagsManager.getAllBlogTagsOnlyList();

            return View(clsBlogPostAdd);
        }

        [HttpPost]
        public ActionResult BlogPostAdd(clsBlogPostAdd clsBlogPostAdd, HttpPostedFileBase inBlogImage, HttpPostedFileBase inBlogAudio, HttpPostedFileBase inBlogVideo)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;

            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
            clsBlogPosts clsBlogPostNew = new clsBlogPosts();

            clsBlogPostNew.strTitle = clsBlogPostAdd.clsBlogPost.strTitle;
            clsBlogPostNew.strTagLine = clsBlogPostAdd.clsBlogPost.strTagLine;
            clsBlogPostNew.strDescription = clsBlogPostAdd.clsBlogPost.strDescription;
            clsBlogPostNew.iBlogTypeID = clsBlogPostAdd.clsBlogPost.iBlogTypeID;
            clsBlogPostNew.dtReleaseDate = Convert.ToDateTime(clsBlogPostAdd.strReleaseDate);
            clsBlogPostNew.strTags = clsBlogPostAdd.clsBlogPost.strTags;
            clsBlogPostNew.bDisplay = clsBlogPostAdd.clsBlogPost.bDisplay;

            //Blog Image
            // Verify file is selected
            if (inBlogImage != null)
            {
                if (inBlogImage.ContentLength > 0)
                {
                    // File Name
                    string strFileName = inBlogImage.FileName;

                    //get Unique Image Path
                    string strNewPath = GetUniqueImagePath();
                    string strDestPath = "Documents\\BlogPosts\\BlogImages\\" + strNewPath;

                    //Full path
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogImages\\" + strNewPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    string strFullImagepath = strFullDestPath + "\\" + strFileName;

                    clsBlogPostNew.strPathToImages = strDestPath.Replace("\\", "/");
                    clsBlogPostNew.strMasterImage = strFileName;

                    inBlogImage.SaveAs(strFullImagepath);
                }
            }

            //Blog Audio
            if (clsBlogPostAdd.clsBlogPost.strAudioURLLink != null && clsBlogPostAdd.clsBlogPost.strAudioURLLink != "")
            {
                clsBlogPostNew.strAudioURLLink = clsBlogPostAdd.clsBlogPost.strAudioURLLink;
            }
            // Verify file is selected
            else if (inBlogAudio != null)
            {
                if (inBlogAudio.ContentLength > 0)
                {
                    // File Name
                    string strFileName = inBlogAudio.FileName;

                    //get Unique Audio Path
                    string strNewPath = GetUniqueAudioPath();
                    string strDestPath = "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;

                    //Full path
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                    clsBlogPostNew.strPathToAudio = strDestPath.Replace("\\", "/");
                    clsBlogPostNew.strMasterAudio = strFileName;

                    inBlogAudio.SaveAs(strFullAudiopath);
                }
            }

            //Blog Video
            if (clsBlogPostAdd.clsBlogPost.strVideoURLLink != null && clsBlogPostAdd.clsBlogPost.strVideoURLLink != "")
            {
                clsBlogPostNew.strVideoURLLink = clsBlogPostAdd.clsBlogPost.strVideoURLLink;
            }
            // Verify file is selected
            else if (inBlogVideo != null)
            {
                if (inBlogVideo.ContentLength > 0)
                {
                    // File Name
                    string strFileName = inBlogVideo.FileName;

                    //get Unique Video Path
                    string strNewPath = GetUniqueVideoPath();
                    string strDestPath = "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;

                    //Full path
                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;
                    if (!System.IO.Directory.Exists(strFullDestPath))
                        System.IO.Directory.CreateDirectory(strFullDestPath);

                    string strFullVideopath = strFullDestPath + "\\" + strFileName;

                    clsBlogPostNew.strPathToVideos = strDestPath.Replace("\\", "/");
                    clsBlogPostNew.strMasterVideo = strFileName;

                    inBlogVideo.SaveAs(strFullVideopath);
                }
            }

            clsBlogPostsManager.saveBlogPost(clsBlogPostNew);
            return RedirectToAction("BlogPosts", "BlogPosts");
        }

        public ActionResult BlogPostEdit(int id)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            if (id == 0)
            {
                return RedirectToAction("BlogPosts", "BlogPosts");
            }

            clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            clsBlogTagsManager clsBlogTagsManager = new clsBlogTagsManager();

            clsBlogPostEdit clsBlogPostEdit = new clsBlogPostEdit();
            clsBlogPostEdit.clsBlogPost = clsBlogPostsManager.getBlogPostByID(id);

            clsBlogPostEdit.lstBlogTypes = clsBlogTypesManager.getAllBlogTypesOnlyList();
            clsBlogPostEdit.lstBlogTags = clsBlogTagsManager.getAllBlogTagsOnlyList();

            clsBlogPostEdit.strSelectedBlogType = clsBlogPostEdit.lstBlogTypes.FirstOrDefault(BlogType => BlogType.iBlogTypeID == clsBlogPostEdit.clsBlogPost.iBlogTypeID).strTitle;

            //Set Release Date
            if (clsBlogPostEdit.clsBlogPost.dtReleaseDate != null)
            {
                DateTime dtReleaseDate = Convert.ToDateTime(clsBlogPostEdit.clsBlogPost.dtReleaseDate);
                clsBlogPostEdit.strReleaseDate = dtReleaseDate.ToString("dd MMMM yyyy");
            }

            if (clsBlogPostEdit.clsBlogPost.strPathToImages != null && clsBlogPostEdit.clsBlogPost.strPathToImages != ""
                && clsBlogPostEdit.clsBlogPost.strMasterImage != null && clsBlogPostEdit.clsBlogPost.strMasterImage != "")
            {
                clsBlogPostEdit.strImageName = clsBlogPostEdit.clsBlogPost.strMasterImage;
                if (clsBlogPostEdit.strImageName != null && clsBlogPostEdit.strImageName != "")
                    clsBlogPostEdit.strFullImagePath = clsBlogPostEdit.clsBlogPost.strPathToImages + "/" + clsBlogPostEdit.clsBlogPost.strMasterImage;
            }
            if (clsBlogPostEdit.clsBlogPost.strPathToAudio != null && clsBlogPostEdit.clsBlogPost.strPathToAudio != ""
                && clsBlogPostEdit.clsBlogPost.strMasterAudio != null && clsBlogPostEdit.clsBlogPost.strMasterAudio != "")
            {
                clsBlogPostEdit.strAudioName = clsBlogPostEdit.clsBlogPost.strMasterAudio;
                if (clsBlogPostEdit.strAudioName != null && clsBlogPostEdit.strAudioName != "")
                    clsBlogPostEdit.strFullAudioPath = clsBlogPostEdit.clsBlogPost.strPathToAudio + "/" + clsBlogPostEdit.clsBlogPost.strMasterAudio;
            }
            if (clsBlogPostEdit.clsBlogPost.strPathToVideos != null && clsBlogPostEdit.clsBlogPost.strPathToVideos != ""
                && clsBlogPostEdit.clsBlogPost.strMasterVideo != null && clsBlogPostEdit.clsBlogPost.strMasterVideo != "")
            {
                clsBlogPostEdit.strVideoName = clsBlogPostEdit.clsBlogPost.strMasterVideo;
                if (clsBlogPostEdit.strVideoName != null && clsBlogPostEdit.strVideoName != "")
                    clsBlogPostEdit.strFullVideoPath = clsBlogPostEdit.clsBlogPost.strPathToVideos + "/" + clsBlogPostEdit.clsBlogPost.strMasterVideo;
            }

            return View(clsBlogPostEdit);
        }

        [HttpPost]
        public ActionResult BlogPostEdit(clsBlogPostEdit clsBlogPostEdit, HttpPostedFileBase inBlogImage, HttpPostedFileBase inBlogAudio, HttpPostedFileBase inBlogVideo)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
            clsBlogTypesManager clsBlogTypesManager = new clsBlogTypesManager();
            string strSelectedBlogType = "";

            clsBlogPosts clsUpdateBlogPost = clsBlogPostsManager.getBlogPostByID(clsBlogPostEdit.clsBlogPost.iBlogPostID);
            if (clsBlogPostEdit.clsBlogPost.iBlogTypeID != null && clsBlogPostEdit.clsBlogPost.iBlogTypeID != 0)
                strSelectedBlogType = clsBlogTypesManager.getAllBlogTypesOnlyList().FirstOrDefault(BlogType => BlogType.iBlogTypeID == clsBlogPostEdit.clsBlogPost.iBlogTypeID).strTitle;

            clsUpdateBlogPost.strTitle = clsBlogPostEdit.clsBlogPost.strTitle;
            clsUpdateBlogPost.strTagLine = clsBlogPostEdit.clsBlogPost.strTagLine;
            clsUpdateBlogPost.strDescription = clsBlogPostEdit.clsBlogPost.strDescription;
            clsUpdateBlogPost.iBlogTypeID = clsBlogPostEdit.clsBlogPost.iBlogTypeID;
            clsUpdateBlogPost.dtReleaseDate = Convert.ToDateTime(clsBlogPostEdit.strReleaseDate);
            clsUpdateBlogPost.strTags = clsBlogPostEdit.clsBlogPost.strTags;
            clsUpdateBlogPost.bDisplay = clsBlogPostEdit.clsBlogPost.bDisplay;

            if (strSelectedBlogType != "")
            {
                if (strSelectedBlogType.ToLower() == "text")
                {
                    //Blog Image
                    #region Blog Image
                    if (clsBlogPostEdit.strFullImagePath == null || clsBlogPostEdit.strFullImagePath == "")
                    {
                        // Verify file is selected
                        if (inBlogImage != null)
                        {
                            if (inBlogImage.ContentLength > 0)
                            {
                                // File Name
                                string strFileName = inBlogImage.FileName;

                                if (clsBlogPostEdit.clsBlogPost.strPathToImages == null || clsBlogPostEdit.clsBlogPost.strPathToImages == "")
                                {
                                    //get Unique Image Path
                                    string strNewPath = GetUniqueImagePath();
                                    string strDestPath = "Documents\\BlogPosts\\BlogImages\\" + strNewPath;

                                    //Full path
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogImages\\" + strNewPath;
                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                        System.IO.Directory.CreateDirectory(strFullDestPath);

                                    string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToImages = strDestPath.Replace("\\", "/");
                                    clsUpdateBlogPost.strMasterImage = strFileName;

                                    inBlogImage.SaveAs(strFullImagepath);
                                }
                                else if (clsBlogPostEdit.clsBlogPost.strMasterImage == null || clsBlogPostEdit.clsBlogPost.strMasterImage == "")
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToImages.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                                    clsUpdateBlogPost.strMasterImage = strFileName;

                                    inBlogImage.SaveAs(strFullImagepath);
                                }
                                else
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToImages.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                                    clsUpdateBlogPost.strMasterImage = strFileName;

                                    inBlogImage.SaveAs(strFullImagepath);
                                }
                            }
                        }
                        else
                        {
                            clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                            clsUpdateBlogPost.strMasterImage = "";
                        }
                    }
                    else if (inBlogImage != null)
                    {
                        if (inBlogImage.ContentLength > 0)
                        {
                            // File Name
                            string strFileName = inBlogImage.FileName;

                            if (clsBlogPostEdit.clsBlogPost.strPathToImages == null || clsBlogPostEdit.clsBlogPost.strPathToImages == "")
                            {
                                //get Unique Image Path
                                string strNewPath = GetUniqueImagePath();
                                string strDestPath = "Documents\\BlogPosts\\BlogImages\\" + strNewPath;

                                //Full path
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogImages\\" + strNewPath;
                                if (!System.IO.Directory.Exists(strFullDestPath))
                                    System.IO.Directory.CreateDirectory(strFullDestPath);

                                string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToImages = strDestPath.Replace("\\", "/");
                                clsUpdateBlogPost.strMasterImage = strFileName;

                                inBlogImage.SaveAs(strFullImagepath);
                            }
                            else if (clsBlogPostEdit.clsBlogPost.strMasterImage == null || clsBlogPostEdit.clsBlogPost.strMasterImage == "")
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToImages.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                                clsUpdateBlogPost.strMasterImage = strFileName;

                                inBlogImage.SaveAs(strFullImagepath);
                            }
                            else
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToImages.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullImagepath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                                clsUpdateBlogPost.strMasterImage = strFileName;

                                inBlogImage.SaveAs(strFullImagepath);
                            }
                        }
                    }
                    else
                    {
                        clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                        clsUpdateBlogPost.strMasterImage = clsBlogPostEdit.clsBlogPost.strMasterImage;
                    }

                    clsUpdateBlogPost.strAudioURLLink = null;
                    clsUpdateBlogPost.strVideoURLLink = null;
                    clsUpdateBlogPost.strMasterAudio = null;
                    clsUpdateBlogPost.strMasterVideo = null;

                    #endregion
                }
                else if (strSelectedBlogType.ToLower() == "audio")
                {
                    //Blog Audio
                    #region Blog Audio
                    if (clsBlogPostEdit.clsBlogPost.strAudioURLLink != null && clsBlogPostEdit.clsBlogPost.strAudioURLLink != "")
                    {
                        clsUpdateBlogPost.strAudioURLLink = clsBlogPostEdit.clsBlogPost.strAudioURLLink;
                        clsUpdateBlogPost.strMasterAudio = null;
                    }
                    else if (clsBlogPostEdit.strFullAudioPath == null || clsBlogPostEdit.strFullAudioPath == "")
                    {
                        // Verify file is selected
                        if (inBlogAudio != null)
                        {
                            if (inBlogAudio.ContentLength > 0)
                            {
                                // File Name
                                string strFileName = inBlogAudio.FileName;

                                if (clsBlogPostEdit.clsBlogPost.strPathToAudio == null || clsBlogPostEdit.clsBlogPost.strPathToAudio == "")
                                {
                                    //get Unique Audio Path
                                    string strNewPath = GetUniqueAudioPath();
                                    string strDestPath = "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;

                                    //Full path
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;
                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                        System.IO.Directory.CreateDirectory(strFullDestPath);

                                    string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToAudio = strDestPath.Replace("\\", "/");
                                    clsUpdateBlogPost.strMasterAudio = strFileName;

                                    inBlogAudio.SaveAs(strFullAudiopath);
                                }
                                else if (clsBlogPostEdit.clsBlogPost.strMasterAudio == null || clsBlogPostEdit.clsBlogPost.strMasterAudio == "")
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToAudio.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                                    clsUpdateBlogPost.strMasterAudio = strFileName;

                                    inBlogAudio.SaveAs(strFullAudiopath);
                                }
                                else
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToAudio.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                                    clsUpdateBlogPost.strMasterAudio = strFileName;

                                    inBlogAudio.SaveAs(strFullAudiopath);
                                }
                            }
                        }
                        else
                        {
                            clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                            clsUpdateBlogPost.strMasterAudio = "";
                        }
                    }
                    else if (inBlogAudio != null)
                    {
                        if (inBlogAudio.ContentLength > 0)
                        {
                            // File Name
                            string strFileName = inBlogAudio.FileName;

                            if (clsBlogPostEdit.clsBlogPost.strPathToAudio == null || clsBlogPostEdit.clsBlogPost.strPathToAudio == "")
                            {
                                //get Unique Audio Path
                                string strNewPath = GetUniqueAudioPath();
                                string strDestPath = "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;

                                //Full path
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogAudio\\" + strNewPath;
                                if (!System.IO.Directory.Exists(strFullDestPath))
                                    System.IO.Directory.CreateDirectory(strFullDestPath);

                                string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToAudio = strDestPath.Replace("\\", "/");
                                clsUpdateBlogPost.strMasterAudio = strFileName;

                                inBlogAudio.SaveAs(strFullAudiopath);
                            }
                            else if (clsBlogPostEdit.clsBlogPost.strMasterAudio == null || clsBlogPostEdit.clsBlogPost.strMasterAudio == "")
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToAudio.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                                clsUpdateBlogPost.strMasterAudio = strFileName;

                                inBlogAudio.SaveAs(strFullAudiopath);
                            }
                            else
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToAudio.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullAudiopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                                clsUpdateBlogPost.strMasterAudio = strFileName;

                                inBlogAudio.SaveAs(strFullAudiopath);
                            }
                        }
                    }
                    else
                    {
                        clsUpdateBlogPost.strAudioURLLink = null;
                        clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                        clsUpdateBlogPost.strMasterAudio = clsBlogPostEdit.clsBlogPost.strMasterAudio;
                    }

                    clsUpdateBlogPost.strMasterImage = null;
                    clsUpdateBlogPost.strVideoURLLink = null;
                    clsUpdateBlogPost.strMasterVideo = null;

                    #endregion

                }
                else if (strSelectedBlogType.ToLower() == "video")
                {
                    //Blog Video
                    #region Blog Video
                    if (clsBlogPostEdit.clsBlogPost.strVideoURLLink != null && clsBlogPostEdit.clsBlogPost.strVideoURLLink != "")
                    {
                        clsUpdateBlogPost.strVideoURLLink = clsBlogPostEdit.clsBlogPost.strVideoURLLink;
                        clsUpdateBlogPost.strMasterVideo = null;
                    }
                    else if (clsBlogPostEdit.strFullVideoPath == null || clsBlogPostEdit.strFullVideoPath == "")
                    {
                        // Verify file is selected
                        if (inBlogVideo != null)
                        {
                            if (inBlogVideo.ContentLength > 0)
                            {
                                // File Name
                                string strFileName = inBlogVideo.FileName;

                                if (clsBlogPostEdit.clsBlogPost.strPathToVideos == null || clsBlogPostEdit.clsBlogPost.strPathToVideos == "")
                                {
                                    //get Unique Video Path
                                    string strNewPath = GetUniqueVideoPath();
                                    string strDestPath = "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;

                                    //Full path
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;
                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                        System.IO.Directory.CreateDirectory(strFullDestPath);

                                    string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToVideos = strDestPath.Replace("\\", "/");
                                    clsUpdateBlogPost.strMasterVideo = strFileName;

                                    inBlogVideo.SaveAs(strFullVideopath);
                                }
                                else if (clsBlogPostEdit.clsBlogPost.strMasterVideo == null || clsBlogPostEdit.clsBlogPost.strMasterVideo == "")
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToVideos.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                                    clsUpdateBlogPost.strMasterVideo = strFileName;

                                    inBlogVideo.SaveAs(strFullVideopath);
                                }
                                else
                                {
                                    string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToVideos.Replace("/", "\\"); ;
                                    string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                    if (!System.IO.Directory.Exists(strFullDestPath))
                                    {
                                        System.IO.Directory.CreateDirectory(strFullDestPath);
                                    }
                                    else
                                    {
                                        //Clear files
                                        string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                        foreach (string strFilePath in strAllFilePaths)
                                            System.IO.File.Delete(strFilePath);
                                    }

                                    string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                    clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                                    clsUpdateBlogPost.strMasterVideo = strFileName;

                                    inBlogVideo.SaveAs(strFullVideopath);
                                }
                            }
                        }
                        else
                        {
                            clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                            clsUpdateBlogPost.strMasterVideo = "";
                        }
                    }
                    else if (inBlogVideo != null)
                    {
                        if (inBlogVideo.ContentLength > 0)
                        {
                            // File Name
                            string strFileName = inBlogVideo.FileName;

                            if (clsBlogPostEdit.clsBlogPost.strPathToVideos == null || clsBlogPostEdit.clsBlogPost.strPathToVideos == "")
                            {
                                //get Unique Video Path
                                string strNewPath = GetUniqueVideoPath();
                                string strDestPath = "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;

                                //Full path
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogVideos\\" + strNewPath;
                                if (!System.IO.Directory.Exists(strFullDestPath))
                                    System.IO.Directory.CreateDirectory(strFullDestPath);

                                string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToVideos = strDestPath.Replace("\\", "/");
                                clsUpdateBlogPost.strMasterVideo = strFileName;

                                inBlogVideo.SaveAs(strFullVideopath);
                            }
                            else if (clsBlogPostEdit.clsBlogPost.strMasterVideo == null || clsBlogPostEdit.clsBlogPost.strMasterVideo == "")
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToVideos.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                                clsUpdateBlogPost.strMasterVideo = strFileName;

                                inBlogVideo.SaveAs(strFullVideopath);
                            }
                            else
                            {
                                string strDestPath = clsBlogPostEdit.clsBlogPost.strPathToVideos.Replace("/", "\\"); ;
                                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;

                                if (!System.IO.Directory.Exists(strFullDestPath))
                                {
                                    System.IO.Directory.CreateDirectory(strFullDestPath);
                                }
                                else
                                {
                                    //Clear files
                                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                                    foreach (string strFilePath in strAllFilePaths)
                                        System.IO.File.Delete(strFilePath);
                                }

                                string strFullVideopath = strFullDestPath + "\\" + strFileName;

                                clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                                clsUpdateBlogPost.strMasterVideo = strFileName;

                                inBlogVideo.SaveAs(strFullVideopath);
                            }
                        }
                    }
                    else
                    {
                        clsUpdateBlogPost.strVideoURLLink = null;
                        clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                        clsUpdateBlogPost.strMasterVideo = clsBlogPostEdit.clsBlogPost.strMasterVideo;
                    }

                    clsUpdateBlogPost.strMasterImage = null;
                    clsUpdateBlogPost.strAudioURLLink = null;
                    clsUpdateBlogPost.strMasterAudio = null;
                    #endregion
                }
                else
                {
                    clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                    clsUpdateBlogPost.strMasterImage = clsBlogPostEdit.clsBlogPost.strMasterImage;

                    clsUpdateBlogPost.strAudioURLLink = clsBlogPostEdit.clsBlogPost.strAudioURLLink;
                    clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                    clsUpdateBlogPost.strMasterAudio = clsBlogPostEdit.clsBlogPost.strMasterAudio;

                    clsUpdateBlogPost.strVideoURLLink = clsBlogPostEdit.clsBlogPost.strVideoURLLink;
                    clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                    clsUpdateBlogPost.strMasterVideo = clsBlogPostEdit.clsBlogPost.strMasterVideo;
                }
            }
            else
            {
                clsUpdateBlogPost.strPathToImages = clsBlogPostEdit.clsBlogPost.strPathToImages;
                clsUpdateBlogPost.strMasterImage = null;

                clsUpdateBlogPost.strAudioURLLink = null;
                clsUpdateBlogPost.strPathToAudio = clsBlogPostEdit.clsBlogPost.strPathToAudio;
                clsUpdateBlogPost.strMasterAudio = null;

                clsUpdateBlogPost.strVideoURLLink = null;
                clsUpdateBlogPost.strPathToVideos = clsBlogPostEdit.clsBlogPost.strPathToVideos;
                clsUpdateBlogPost.strMasterVideo = null;
            }

            clsBlogPostsManager.saveBlogPost(clsUpdateBlogPost);

            return RedirectToAction("BlogPosts", "BlogPosts");
        }

        [HttpPost]
        public ActionResult DeleteBlogPost(int iBlogPostID)
        {
            if (Session["clsUser"] != null)
            {
                ViewBag.bIsUserLoggedIn = true;
                ViewBag.bIsClientLoggedIn = false;
            }
            else
            {
                ViewBag.bIsUserLoggedIn = null;
                ViewBag.bIsClientLoggedIn = null;

                return RedirectToAction("Index", "Home");
            }

            bool bIsDeleted = false;

            clsBlogPostsManager clsBlogPostsManager = new clsBlogPostsManager();
            clsBlogPostsManager.removeBlogPostByID(iBlogPostID);

            bIsDeleted = true;
            return Json(new { bIsDeleted = bIsDeleted }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult CheckIfBlogPostExists(string strTitle)
        {
            bool bCanUseBlogPost = false;
            bool bExists = db.tblBlogPosts.Any(BlogPost => BlogPost.strTitle.ToLower() == strTitle.ToLower() && BlogPost.bIsDeleted == false);

            if (bExists == false)
                bCanUseBlogPost = true;

            return Json(bCanUseBlogPost, JsonRequestBehavior.AllowGet);
        }

        //Get unique path
        private string GetUniqueImagePath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogImages";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\BlogPostImage" + iCount) == true)
            {
                iCount++;
            }
            return "BlogPostImage" + iCount;
        }
        //Get unique path
        private string GetUniqueAudioPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogAudio";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\BlogPostAudio" + iCount) == true)
            {
                iCount++;
            }
            return "BlogPostAudio" + iCount;
        }
        //Get unique path
        private string GetUniqueVideoPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\BlogPosts\\BlogVideos";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\BlogPostVideo" + iCount) == true)
            {
                iCount++;
            }
            return "BlogPostVideo" + iCount;
        }

    }
}