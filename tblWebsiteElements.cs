//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftservePortalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblWebsiteElements
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblWebsiteElements()
        {
            this.tblProjectDesignLinkToWebsiteElements = new HashSet<tblProjectDesignLinkToWebsiteElements>();
            this.tblProjectGeneralLinkToWebsiteElements = new HashSet<tblProjectGeneralLinkToWebsiteElements>();
        }
    
        public int iWebsiteElementID { get; set; }
        public Nullable<System.DateTime> dtAdded { get; set; }
        public Nullable<int> iAddedBy { get; set; }
        public Nullable<System.DateTime> dtEdited { get; set; }
        public Nullable<int> iEditedBy { get; set; }
        public string strTitle { get; set; }
        public Nullable<bool> bIsDeleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblProjectDesignLinkToWebsiteElements> tblProjectDesignLinkToWebsiteElements { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblProjectGeneralLinkToWebsiteElements> tblProjectGeneralLinkToWebsiteElements { get; set; }
    }
}
