﻿/*=========================================================================================
    File Name: analytics.js
    Description: Takes the access token and display the website analytics for the user currently logged in, should they have one.
    ----------------------------------------------------------------------------------------
    Item Name: Softserve - Google Analytics Dashboard
    Version: 1
    Author: Softserve Digital Development
==========================================================================================*/

// Doughnut chart
// ------------------------------
$(window).on("load", function () {
    (function (w, d, s, g, js, fjs) {
        g = w.gapi || (w.gapi = {}); g.analytics = { q: [], ready: function (cb) { this.q.push(cb) } };
        js = d.createElement(s); fjs = d.getElementsByTagName(s)[0];
        js.src = 'https://apis.google.com/js/platform.js';
        fjs.parentNode.insertBefore(js, fjs); js.onload = function () { g.load('analytics') };
    }(window, document, 'script'));

    gapi.analytics.ready(function () {
        /**
         * Authorize the user with an access token obtained server side.
         */
        gapi.analytics.auth.authorize({
            'serverAuth': {
                'access_token': access_token
            }
        });
        var scriptMoment = "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js";
        renderTopCountriesChart();
        renderUsersWeeklyComparison();
        renderDevicesChart();
        /**
         * Creates a new DataChart instance showing sessions over the past 30 days.
         * It will be rendered inside an element with the id "chart-1-container".
         */
        var mainChart = new gapi.analytics.googleCharts.DataChart({
            query: {
                'ids': 'ga:149098467',
                'dimensions': 'ga:pagePathLevel1',
                'metrics': 'ga:pageviews',
                'start-date': '30daysAgo',
                'end-date': 'yesterday',
                'sort': '-ga:pageviews',
                'max-results': '6'
            },
            chart: {
                type: 'TABLE',
                container: 'main-chart-container',
                options: {
                    width: '100%'
                }
            }
        });
        mainChart.execute();

        function renderDevicesChart() {
            query({
                'ids': 'ga:149098467',
                'dimensions': 'ga:deviceCategory',
                'metrics': 'ga:sessions',
                'start-date': '30daysAgo',
                'end-date': 'yesterday',
                'sort': '-ga:sessions',
                'max-results': 5
            })
            .then(function (response) {
                var Data = [];
                var Labels = [];
                var Colours = [];
                var colors = ['#4D5360', '#949FB1', '#D4CCC5', '#E2EAE9', '#F7464A'];

                response.rows.forEach(function (row, i) {
                    Data.push({
                        value: +row[1]
                    });
                    Labels.push({
                        label: row[0]
                    });
                    Colours.push({
                        color: colors[i]
                    });

                });
                console.log(Data);
                console.log(Labels);
                console.log(Colours);

                //Get the context of the Chart canvas element we want to select
                var ctx = $("#simple-pie-chart");

                // Chart Options
                var chartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                };

                // Chart Data
                var chartData = {
                    labels: Labels.map(a => a.label),
                    datasets: [{
                        label: "Device Categories",
                        data: Data.map(a => a.value),
                        backgroundColor: Colours.map(a => a.color)
                    }]
                };

                var config = {
                    type: 'pie',

                    // Chart Options
                    options: chartOptions,

                    data: chartData
                };

                // Create the chart
                var pieSimpleChart = new Chart(ctx, config);
                document.getElementById('desktop').innerHTML = Data[0].value;
                document.getElementById('mobile').innerHTML = Data[1].value;
                document.getElementById('tablet').innerHTML = Data[2].value;

            });
        }

        function renderTopCountriesChart() {
            query({
                'ids': 'ga:149098467',
                'dimensions': 'ga:country',
                'metrics': 'ga:sessions',
                'sort': '-ga:sessions',
                'max-results': 5
            })
            .then(function (response) {
                var Data = [];
                var Labels = [];
                var Colours = [];
                var colors = ['#4D5360', '#949FB1', '#D4CCC5', '#E2EAE9', '#F7464A'];

                response.rows.forEach(function (row, i) {
                    //                            Data.push({
                    //    label: row[0],
                    //    value: +row[1],
                    //    color: colors[i]
                    //});
                    Data.push({
                        value: +row[1]
                    });
                    Labels.push({
                        label: row[0]
                    });
                    Colours.push({
                        color: colors[i]
                    });

                });
                console.log(Data);
                console.log(Labels);
                console.log(Colours);

                var ctx = $("#analytics-doughnut-chart");

                // Chart Options
                var chartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                };

                // Chart Data
                var chartData = {
                    labels: Labels.map(a => a.label),
                    datasets: [{
                        label: "Sessions by Country",
                        data: Data.map(a => a.value),
                        backgroundColor: Colours.map(a => a.color)
                    }]
                };
                console.log(chartData);
                var config = {
                    type: 'doughnut',

                    // Chart Options
                    options: chartOptions,

                    data: chartData
                };

                // Create the chart
                var doughnutSimpleChart = new Chart(ctx, config);

            });
        }

        function renderUsersWeeklyComparison() {

            // Adjust `now` to experiment with different days, for testing only...
            var now = moment(); // .subtract(3, 'day');

            var thisWeek = query({
                'ids': 'ga:149098467',
                'dimensions': 'ga:date,ga:nthDay',
                'metrics': 'ga:sessions',
                'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD'),
                'end-date': moment(now).format('YYYY-MM-DD')
            });

            var lastWeek = query({
                'ids': 'ga:149098467',
                'dimensions': 'ga:date,ga:nthDay',
                'metrics': 'ga:sessions',
                'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week')
                    .format('YYYY-MM-DD'),
                'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week')
                    .format('YYYY-MM-DD')
            });

            Promise.all([thisWeek, lastWeek]).then(function (results) {

                var data1 = results[0].rows.map(function (row) { return +row[2]; });
                var data2 = results[1].rows.map(function (row) { return +row[2]; });
                var labels = results[1].rows.map(function (row) { return +row[0]; });

                labels = labels.map(function (label) {
                    return moment(label, 'YYYYMMDD').format('ddd');
                });


                //Get the context of the Chart canvas element we want to select
                var ctx = $("#line-logarithmic");

                var chartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f3f3f3",
                                drawTicks: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'day'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f3f3f3",
                                drawTicks: false,
                            },
                            type: 'linear',
                            scaleLabel: {
                                display: true,
                                labelString: 'sessions'
                            }
                        }]
                    }
                };

                var chartData = {
                    labels: labels,
                    datasets: [
                      {
                          label: 'Last Week',
                          fillColor: 'rgba(220,220,220,0.5)',
                          strokeColor: 'rgba(220,220,220,1)',
                          pointColor: 'rgba(220,220,220,1)',
                          pointStrokeColor: '#fff',
                          data: data2
                      },
                      {
                          label: 'This Week',
                          fillColor: 'rgba(151,187,205,0.5)',
                          strokeColor: 'rgba(151,187,205,1)',
                          pointColor: 'rgba(151,187,205,1)',
                          pointStrokeColor: '#fff',
                          data: data1
                      }
                    ]
                };

                var config = {
                    type: 'line',

                    // Chart Options
                    options: chartOptions,

                    // Chart Data
                    data: chartData
                };

                // Create the chart
                var logarithmicChart = new Chart(ctx, config);

            });
        }

        function query(params) {
            return new Promise(function (resolve, reject) {
                var data = new gapi.analytics.report.Data({ query: params });
                data.once('success', function (response) { resolve(response); })
                    .once('error', function (response) { reject(response); })
                    .execute();
            });
        }

    });
});

